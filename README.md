# How to start a Sylius project:

#### 1. Download project repository into your project directory (example: Home/Projects/<your_project>). It should look like this:

![](https://i.imgur.com/O3JQTTY.png)

#### 2. From your root docker directory (example: Home/Projects/<your_project>), you need to open your CLI terminal and type in ```make up``` . After the build is done docker containers should be started. You can check if they are running by opening new terminal tab and typing ```docker ps```. There should be four containers:

![](https://i.imgur.com/PXOvawE.png)

If all of them are there, you can now type in ```make php``` and your container will open

#### 3. After everything is done, head to your browser and type in [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

It should look like this:
![](https://i.imgur.com/7JNojOB.png)