-- MariaDB dump 10.19  Distrib 10.6.5-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: chic
-- ------------------------------------------------------
-- Server version	10.6.5-MariaDB-1:10.6.5+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitbag_shipping_export`
--

DROP TABLE IF EXISTS `bitbag_shipping_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitbag_shipping_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) DEFAULT NULL,
  `shipping_gateway_id` int(11) DEFAULT NULL,
  `exported_at` datetime DEFAULT NULL,
  `label_path` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `external_id` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_20E62D9F7BE036FC` (`shipment_id`),
  KEY `IDX_20E62D9FEF84DE5E` (`shipping_gateway_id`),
  CONSTRAINT `FK_20E62D9F7BE036FC` FOREIGN KEY (`shipment_id`) REFERENCES `sylius_shipment` (`id`),
  CONSTRAINT `FK_20E62D9FEF84DE5E` FOREIGN KEY (`shipping_gateway_id`) REFERENCES `bitbag_shipping_gateway` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitbag_shipping_export`
--

LOCK TABLES `bitbag_shipping_export` WRITE;
/*!40000 ALTER TABLE `bitbag_shipping_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitbag_shipping_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitbag_shipping_gateway`
--

DROP TABLE IF EXISTS `bitbag_shipping_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitbag_shipping_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '(DC2Type:json_array)' CHECK (json_valid(`config`)),
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitbag_shipping_gateway`
--

LOCK TABLES `bitbag_shipping_gateway` WRITE;
/*!40000 ALTER TABLE `bitbag_shipping_gateway` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitbag_shipping_gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitbag_shipping_gateway_method`
--

DROP TABLE IF EXISTS `bitbag_shipping_gateway_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitbag_shipping_gateway_method` (
  `shipping_gateway_id` int(11) NOT NULL,
  `shipping_method_id` int(11) NOT NULL,
  PRIMARY KEY (`shipping_gateway_id`,`shipping_method_id`),
  KEY `IDX_8606B9CBEF84DE5E` (`shipping_gateway_id`),
  KEY `IDX_8606B9CB5F7D6850` (`shipping_method_id`),
  CONSTRAINT `FK_8606B9CB5F7D6850` FOREIGN KEY (`shipping_method_id`) REFERENCES `sylius_shipping_method` (`id`),
  CONSTRAINT `FK_8606B9CBEF84DE5E` FOREIGN KEY (`shipping_gateway_id`) REFERENCES `bitbag_shipping_gateway` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitbag_shipping_gateway_method`
--

LOCK TABLES `bitbag_shipping_gateway_method` WRITE;
/*!40000 ALTER TABLE `bitbag_shipping_gateway_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitbag_shipping_gateway_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ch_cookieconsent_log`
--

DROP TABLE IF EXISTS `ch_cookieconsent_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ch_cookieconsent_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `cookie_consent_key` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `cookie_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `cookie_value` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ch_cookieconsent_log`
--

LOCK TABLES `ch_cookieconsent_log` WRITE;
/*!40000 ALTER TABLE `ch_cookieconsent_log` DISABLE KEYS */;
INSERT INTO `ch_cookieconsent_log` VALUES (1,'172.29.0.x','622734bab3f9c','analytics','true','2022-03-08 11:49:30'),(2,'172.29.0.x','622734bab3f9c','analytics','true','2022-03-08 11:49:33');
/*!40000 ALTER TABLE `ch_cookieconsent_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consent`
--

DROP TABLE IF EXISTS `consent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consent`
--

LOCK TABLES `consent` WRITE;
/*!40000 ALTER TABLE `consent` DISABLE KEYS */;
/*!40000 ALTER TABLE `consent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consent_agreement`
--

DROP TABLE IF EXISTS `consent_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consent_agreement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consent_id` int(11) DEFAULT NULL,
  `agreed` tinyint(1) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7B4FB8E741079D63` (`consent_id`),
  CONSTRAINT `FK_7B4FB8E741079D63` FOREIGN KEY (`consent_id`) REFERENCES `consent` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consent_agreement`
--

LOCK TABLES `consent_agreement` WRITE;
/*!40000 ALTER TABLE `consent_agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `consent_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_surcharge_price`
--

DROP TABLE IF EXISTS `customer_surcharge_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_surcharge_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `channel_pricing_id` int(11) NOT NULL,
  `amount` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_surcharge_customer_channel_pricing` (`customer_id`,`channel_pricing_id`),
  KEY `IDX_A88709D19395C3F3` (`customer_id`),
  KEY `IDX_A88709D13EADFFE5` (`channel_pricing_id`),
  CONSTRAINT `FK_A88709D13EADFFE5` FOREIGN KEY (`channel_pricing_id`) REFERENCES `sylius_channel_pricing` (`id`),
  CONSTRAINT `FK_A88709D19395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_surcharge_price`
--

LOCK TABLES `customer_surcharge_price` WRITE;
/*!40000 ALTER TABLE `customer_surcharge_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_surcharge_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lexik_trans_unit`
--

DROP TABLE IF EXISTS `lexik_trans_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lexik_trans_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_domain_idx` (`key_name`,`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lexik_trans_unit`
--

LOCK TABLES `lexik_trans_unit` WRITE;
/*!40000 ALTER TABLE `lexik_trans_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `lexik_trans_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lexik_trans_unit_translations`
--

DROP TABLE IF EXISTS `lexik_trans_unit_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lexik_trans_unit_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) DEFAULT NULL,
  `trans_unit_id` int(11) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb3_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modified_manually` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trans_unit_locale_idx` (`trans_unit_id`,`locale`),
  KEY `IDX_B0AA394493CB796C` (`file_id`),
  KEY `IDX_B0AA3944C3C583C9` (`trans_unit_id`),
  CONSTRAINT `FK_B0AA394493CB796C` FOREIGN KEY (`file_id`) REFERENCES `lexik_translation_file` (`id`),
  CONSTRAINT `FK_B0AA3944C3C583C9` FOREIGN KEY (`trans_unit_id`) REFERENCES `lexik_trans_unit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lexik_trans_unit_translations`
--

LOCK TABLES `lexik_trans_unit_translations` WRITE;
/*!40000 ALTER TABLE `lexik_trans_unit_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `lexik_trans_unit_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lexik_translation_file`
--

DROP TABLE IF EXISTS `lexik_translation_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lexik_translation_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8mb3_unicode_ci NOT NULL,
  `extention` varchar(10) COLLATE utf8mb3_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_idx` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lexik_translation_file`
--

LOCK TABLES `lexik_translation_file` WRITE;
/*!40000 ALTER TABLE `lexik_translation_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `lexik_translation_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loevgaard_brand`
--

DROP TABLE IF EXISTS `loevgaard_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loevgaard_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `tec_doc_brand_id` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_680CAA0877153098` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loevgaard_brand`
--

LOCK TABLES `loevgaard_brand` WRITE;
/*!40000 ALTER TABLE `loevgaard_brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `loevgaard_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loevgaard_brand_image`
--

DROP TABLE IF EXISTS `loevgaard_brand_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loevgaard_brand_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_95D3C8B97E3C61F9` (`owner_id`),
  CONSTRAINT `FK_95D3C8B97E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `loevgaard_brand` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loevgaard_brand_image`
--

LOCK TABLES `loevgaard_brand_image` WRITE;
/*!40000 ALTER TABLE `loevgaard_brand_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `loevgaard_brand_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20170912085504','2022-03-08 10:15:40'),('20170913125128','2022-03-08 10:15:40'),('20180102140039','2022-03-08 10:15:40'),('20190109095211','2022-03-08 10:15:40'),('20190109160409','2022-03-08 10:15:40'),('20190508083953','2022-03-08 10:15:40'),('20190621035710','2022-03-08 10:15:40'),('20200319064754','2022-03-08 10:15:40'),('20200319155155','2022-03-08 10:15:40'),('20200320070914','2022-03-08 10:15:41'),('20200320070915','2022-03-08 10:15:41'),('20200320161736','2022-03-08 10:15:41'),('20200322162332','2022-03-08 10:15:41'),('20200324071637','2022-03-08 10:15:41'),('20200414101152','2022-03-08 10:15:41'),('20200417113200','2022-03-08 10:15:41'),('20200421104410','2022-03-08 10:15:41'),('20200421104424','2022-03-08 10:15:41'),('20200422082659','2022-03-08 10:15:41'),('20200422124355','2022-03-08 10:15:41'),('20200423063937','2022-03-08 10:15:41'),('20200424123539','2022-03-08 10:15:41'),('20200429083745','2022-03-08 10:15:41'),('20200429090035','2022-03-08 10:15:41'),('20200430095638','2022-03-08 10:15:41'),('20200504071050','2022-03-08 10:15:42'),('20200511140741','2022-03-08 10:15:42'),('20200512060544','2022-03-08 10:15:42'),('20200513043034','2022-03-08 10:15:42'),('20200514104337','2022-03-08 10:15:42'),('20200520094319','2022-03-08 10:15:42'),('20200521130652','2022-03-08 10:15:42'),('20200525071727','2022-03-08 10:15:42'),('20200526105143','2022-03-08 10:15:42'),('20200527125148','2022-03-08 10:15:42'),('20200617140202','2022-03-08 10:15:42'),('20200622113949','2022-03-08 10:15:42'),('20200627160234','2022-03-08 10:15:42'),('20200701072549','2022-03-08 10:15:42'),('20200701111549','2022-03-08 10:15:42'),('20200717063608','2022-03-08 10:15:42'),('20200727070531','2022-03-08 10:15:42'),('20200807102211','2022-03-08 10:15:42'),('20200921091434','2022-03-08 10:15:42');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner`
--

DROP TABLE IF EXISTS `omni_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publish_from` datetime NOT NULL,
  `publish_to` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_DC753D6E77153098` (`code`),
  KEY `IDX_DC753D6EDD842E46` (`position_id`),
  CONSTRAINT `FK_DC753D6EDD842E46` FOREIGN KEY (`position_id`) REFERENCES `omni_banner_position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner`
--

LOCK TABLES `omni_banner` WRITE;
/*!40000 ALTER TABLE `omni_banner` DISABLE KEYS */;
INSERT INTO `omni_banner` VALUES (20,18,'slider-homepage',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-12-01 00:00:00'),(21,19,'promotions-1',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-12-01 00:00:00'),(22,19,'promotions-2',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-09-01 00:00:00'),(23,19,'promotions-3',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(24,20,'brands',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(25,21,'best-offer-1',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(26,21,'best-offer-2',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(27,22,'best-offer-3',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(28,23,'best-offer-4',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(29,24,'best-offer-5',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(30,24,'best-offer-6',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(31,24,'best-offer-7',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(32,24,'best-offer-8',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-06-01 00:00:00'),(33,26,'sale-anouncement',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(34,27,'loyalty-card',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(35,15,'banner',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(36,15,'bottom-banner',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(37,15,'bottom-left-banner',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00'),(38,15,'bottom-right-banner',1,'2022-03-08 11:16:43','2022-03-08 11:16:43','2018-12-01 00:00:00','2019-10-01 00:00:00');
/*!40000 ALTER TABLE `omni_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_channels`
--

DROP TABLE IF EXISTS `omni_banner_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_channels` (
  `banner_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`banner_id`,`channel_id`),
  KEY `IDX_8C119894684EC833` (`banner_id`),
  KEY `IDX_8C11989472F5A1AA` (`channel_id`),
  CONSTRAINT `FK_8C119894684EC833` FOREIGN KEY (`banner_id`) REFERENCES `omni_banner` (`id`),
  CONSTRAINT `FK_8C11989472F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_channels`
--

LOCK TABLES `omni_banner_channels` WRITE;
/*!40000 ALTER TABLE `omni_banner_channels` DISABLE KEYS */;
INSERT INTO `omni_banner_channels` VALUES (20,2),(21,2),(22,2),(23,2),(24,2),(25,2),(26,2),(27,2),(28,2),(29,2),(30,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,2),(37,2),(38,2);
/*!40000 ALTER TABLE `omni_banner_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_image`
--

DROP TABLE IF EXISTS `omni_banner_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `content_position` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `content_space` int(11) DEFAULT NULL,
  `content_background` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_FC3866247E3C61F9` (`owner_id`),
  CONSTRAINT `FK_FC3866247E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `omni_banner` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_image`
--

LOCK TABLES `omni_banner_image` WRITE;
/*!40000 ALTER TABLE `omni_banner_image` DISABLE KEYS */;
INSERT INTO `omni_banner_image` VALUES (30,20,NULL,'2c/88/1eb764b2cc2b88203f78da6bcdc3.png','bottom',NULL,'#ffffff',0),(31,20,NULL,'5f/a3/44ecd016046070f9e7229cb3de4a.png','bottom',NULL,'#ffffff',0),(32,21,NULL,'f4/7f/c7caa1fb1c487fe679846ceeae9f.png','bottom',20,'#ffffff',0),(33,21,NULL,'0c/aa/1d4875f396bc1d9b66bbffbe1a41.png','bottom',20,'#ffffff',0),(34,22,NULL,'8d/06/254f7649afdef4d1a8157307e004.png','bottom',20,'#ffffff',0),(35,22,NULL,'9e/63/f484ab72a2712e983dd37a58eeeb.png','bottom',20,'#ffffff',0),(36,22,NULL,'4e/a1/b2aa09b09ffd7263a85ae7b5a28c.png','bottom',20,'#ffffff',0),(37,23,NULL,'db/04/d4fa1f79e4b31be73df13fa56648.png','bottom',20,'#ffffff',0),(38,24,NULL,'c0/f9/05960abb10429279d2daca674be3.png',NULL,NULL,NULL,0),(39,24,NULL,'1d/a8/fd848a368b133443c41a271789d5.png',NULL,NULL,NULL,0),(40,24,NULL,'56/a4/ef0487e3fb61ea62e75114b6707e.png',NULL,NULL,NULL,0),(41,24,NULL,'06/c2/a52b4dce26d731d85caac36fa171.png',NULL,NULL,NULL,0),(42,24,NULL,'fb/a1/41a8e404095b5f7b2a59ff71995b.png',NULL,NULL,NULL,0),(43,24,NULL,'fa/8b/a3aaa731c0822b7711bc13c708c2.png',NULL,NULL,NULL,0),(44,24,NULL,'b9/e4/d41ffcfbb883c4f7864da4c87399.png',NULL,NULL,NULL,0),(45,25,NULL,'35/43/7a41517bd5a840100c828b1961de.png','bottom',20,'#2D66B1',0),(46,26,NULL,'36/f3/30fb9e92d443a97c1c21a1c9a66b.png','bottom',20,'#EE2E4F',0),(47,27,NULL,'12/58/b312717f3e42159e69a96408f590.png','left',30,'#1DA1F2',0),(48,28,NULL,'61/53/fcde7e71710e445e861a4e06f4fc.png','right',30,'#2D66B1',0),(49,29,NULL,'c4/f0/24a885a642a1c515cc5da9ebb023.png','right',100,'#2D66B1',0),(50,30,NULL,'27/29/e8957f9b52a23c0afc3dc95950a4.png','bottom',20,'#1DA1F2',0),(51,31,NULL,'02/72/202572f7bddbb386d0f3056c73bf.png','bottom',20,'#1DA1F2',0),(52,32,NULL,'fe/5f/63d97b91c486a899040a5603793d.png','bottom',20,'#2D66B1',0),(53,33,NULL,'c1/06/197a277b6a1b90a5c34814c16269.png',NULL,NULL,NULL,0),(54,34,NULL,'b2/51/8ae3af40de6ef717b064c3cddc35.png',NULL,NULL,NULL,0),(55,35,NULL,'d3/a2/b6ebf7f6e64ccb394a293975c380.jpg',NULL,NULL,NULL,0),(56,36,NULL,'19/a1/e7a16308a83265a52e05466f16f8.png',NULL,NULL,NULL,0),(57,37,NULL,'02/1d/e792454da86314fc6061844566b1.jpg',NULL,NULL,NULL,0),(58,38,NULL,'83/54/1745256dea637fb58ca7573d6bf8.jpg',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `omni_banner_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_image_translation`
--

DROP TABLE IF EXISTS `omni_banner_image_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_image_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `omni_banner_image_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_4264B92F2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_4264B92F2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `omni_banner_image` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_image_translation`
--

LOCK TABLES `omni_banner_image_translation` WRITE;
/*!40000 ALTER TABLE `omni_banner_image_translation` DISABLE KEYS */;
INSERT INTO `omni_banner_image_translation` VALUES (88,30,NULL,NULL,'lt_LT'),(89,30,NULL,NULL,'en_US'),(90,30,NULL,NULL,'ru_RU'),(91,31,NULL,NULL,'lt_LT'),(92,31,NULL,NULL,'en_US'),(93,31,NULL,NULL,'ru_RU'),(94,32,NULL,'All clothes -20% off','lt_LT'),(95,32,NULL,'All clothes -20% off','en_US'),(96,32,NULL,'All clothes -20% off','ru_RU'),(97,33,NULL,'All printers -10% off','lt_LT'),(98,33,NULL,'All printers -10% off','en_US'),(99,33,NULL,'All printers -10% off','ru_RU'),(100,34,NULL,'New 2019 summer collection','lt_LT'),(101,34,NULL,'New 2019 summer collection','en_US'),(102,34,NULL,'New 2019 summer collection','ru_RU'),(103,35,NULL,'Headphone sale!','lt_LT'),(104,35,NULL,'Headphone sale!','en_US'),(105,35,NULL,'Headphone sale!','ru_RU'),(106,36,NULL,'Best price on multitools','lt_LT'),(107,36,NULL,'Best price on multitools','en_US'),(108,36,NULL,'Best price on multitools','ru_RU'),(109,37,NULL,'Check out our top brands!','lt_LT'),(110,37,NULL,'Check out our top brands!','en_US'),(111,37,NULL,'Check out our top brands!','ru_RU'),(112,38,NULL,NULL,'lt_LT'),(113,38,NULL,NULL,'en_US'),(114,38,NULL,NULL,'ru_RU'),(115,39,NULL,NULL,'lt_LT'),(116,39,NULL,NULL,'en_US'),(117,39,NULL,NULL,'ru_RU'),(118,40,NULL,NULL,'lt_LT'),(119,40,NULL,NULL,'en_US'),(120,40,NULL,NULL,'ru_RU'),(121,41,NULL,NULL,'lt_LT'),(122,41,NULL,NULL,'en_US'),(123,41,NULL,NULL,'ru_RU'),(124,42,NULL,NULL,'lt_LT'),(125,42,NULL,NULL,'en_US'),(126,42,NULL,NULL,'ru_RU'),(127,43,NULL,NULL,'lt_LT'),(128,43,NULL,NULL,'en_US'),(129,43,NULL,NULL,'ru_RU'),(130,44,NULL,NULL,'lt_LT'),(131,44,NULL,NULL,'en_US'),(132,44,NULL,NULL,'ru_RU'),(133,45,NULL,'<span style=\"color: #ffffff\">Massive sale of clothes</span>','lt_LT'),(134,45,NULL,'<span style=\"color: #ffffff\">Massive sale of clothes</span>','en_US'),(135,45,NULL,'<span style=\"color: #ffffff\">Massive sale of clothes</span>','ru_RU'),(136,46,NULL,'<span style=\"color: #ffffff\">Try out our city bikes</span>','lt_LT'),(137,46,NULL,'<span style=\"color: #ffffff\">Try out our city bikes</span>','en_US'),(138,46,NULL,'<span style=\"color: #ffffff\">Try out our city bikes</span>','ru_RU'),(139,47,NULL,'<span style=\"color: #ffffff\">Sustainable toys</span>','lt_LT'),(140,47,NULL,'<span style=\"color: #ffffff\">Sustainable toys</span>','en_US'),(141,47,NULL,'<span style=\"color: #ffffff\">Sustainable toys</span>','ru_RU'),(142,48,NULL,'<span style=\"color: #ffffff\">Decore finishes 15% off</span>','lt_LT'),(143,48,NULL,'<span style=\"color: #ffffff\">Decore finishes 15% off</span>','en_US'),(144,48,NULL,'<span style=\"color: #ffffff\">Decore finishes 15% off</span>','ru_RU'),(145,49,NULL,'<span style=\"color: #ffffff; font-size:35px\">Special offers for electronics</span>','lt_LT'),(146,49,NULL,'<span style=\"color: #ffffff; font-size:35px\">Special offers for electronics</span>','en_US'),(147,49,NULL,'<span style=\"color: #ffffff; font-size:35px\">Special offers for electronics</span>','ru_RU'),(148,50,NULL,'<span style=\"color: #ffffff\">15% off on thursdays!</span>','lt_LT'),(149,50,NULL,'<span style=\"color: #ffffff\">15% off on thursdays!</span>','en_US'),(150,50,NULL,'<span style=\"color: #ffffff\">15% off on thursdays!</span>','ru_RU'),(151,51,NULL,'<span style=\"color: #ffffff\">Special price for sets</span>','lt_LT'),(152,51,NULL,'<span style=\"color: #ffffff\">Special price for sets</span>','en_US'),(153,51,NULL,'<span style=\"color: #ffffff\">Special price for sets</span>','ru_RU'),(154,52,NULL,'<span style=\"color: #ffffff\">Bring your old ones here</span>','lt_LT'),(155,52,NULL,'<span style=\"color: #ffffff\">Bring your old ones here</span>','en_US'),(156,52,NULL,'<span style=\"color: #ffffff\">Bring your old ones here</span>','ru_RU'),(157,53,NULL,NULL,'lt_LT'),(158,53,NULL,NULL,'en_US'),(159,53,NULL,NULL,'ru_RU'),(160,54,NULL,NULL,'lt_LT'),(161,54,NULL,NULL,'en_US'),(162,54,NULL,NULL,'ru_RU'),(163,55,NULL,NULL,'lt_LT'),(164,55,NULL,NULL,'en_US'),(165,55,NULL,NULL,'ru_RU'),(166,56,NULL,NULL,'lt_LT'),(167,56,NULL,NULL,'en_US'),(168,56,NULL,NULL,'ru_RU'),(169,57,NULL,NULL,'lt_LT'),(170,57,NULL,NULL,'en_US'),(171,57,NULL,NULL,'ru_RU'),(172,58,NULL,NULL,'lt_LT'),(173,58,NULL,NULL,'en_US'),(174,58,NULL,NULL,'ru_RU');
/*!40000 ALTER TABLE `omni_banner_image_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_position`
--

DROP TABLE IF EXISTS `omni_banner_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_39299ED777153098` (`code`),
  KEY `IDX_39299ED79F2C3FAB` (`zone_id`),
  CONSTRAINT `FK_39299ED79F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `omni_banner_zone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_position`
--

LOCK TABLES `omni_banner_position` WRITE;
/*!40000 ALTER TABLE `omni_banner_position` DISABLE KEYS */;
INSERT INTO `omni_banner_position` VALUES (15,15,'omni-theme',0,'unified_descriptive_grid'),(16,17,'top-notifications',0,'full_width_content_line'),(17,18,'header-notification',0,'content_line'),(18,18,'main-slider',0,'big_wide_slider'),(19,19,'promotion-grid',0,'unified_descriptive_grid'),(20,20,'brands',0,'multislider'),(21,21,'best-offers-1',0,'half_screen_adoptive_grid'),(22,21,'best-offers-2',0,'half_screen_adoptive_grid'),(23,21,'best-offers-3',0,'half_screen_adoptive_grid'),(24,21,'best-offers-4',0,'half_screen_adoptive_grid'),(25,22,'flat-banner',0,'content_line'),(26,22,'wide-banner',0,'wide_content'),(27,22,'full-screen-wide-banner',0,'full_screen_wide_content'),(28,23,'static-image',0,'static_image');
/*!40000 ALTER TABLE `omni_banner_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_position_translation`
--

DROP TABLE IF EXISTS `omni_banner_position_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_position_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `omni_banner_position_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_784BAF332C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_784BAF332C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `omni_banner_position` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_position_translation`
--

LOCK TABLES `omni_banner_position_translation` WRITE;
/*!40000 ALTER TABLE `omni_banner_position_translation` DISABLE KEYS */;
INSERT INTO `omni_banner_position_translation` VALUES (43,15,'Omni','','','lt_LT'),(44,15,'Omni','','','en_US'),(45,15,'Omni','','','ru_RU'),(46,16,'Homepage top notifications','','','lt_LT'),(47,16,'Homepage top notifications','','','en_US'),(48,16,'Homepage top notifications','','','ru_RU'),(49,17,'Header notification','','','lt_LT'),(50,17,'Header notification','','','en_US'),(51,17,'Header notification','','','ru_RU'),(52,18,'Homepage header bottom','','','lt_LT'),(53,18,'Homepage header bottom','','','en_US'),(54,18,'Homepage header bottom','','','ru_RU'),(55,19,'Homepage promotion grid','','','lt_LT'),(56,19,'Homepage promotion grid','','','en_US'),(57,19,'Homepage promotion grid','','','ru_RU'),(58,20,'Our brands','','You will find more than 600 best and most popular brands in our store','lt_LT'),(59,20,'Our brands','','You will find more than 600 best and most popular brands in our store','en_US'),(60,20,'Our brands','','You will find more than 600 best and most popular brands in our store','ru_RU'),(61,21,'Homepage best offers 1','','','lt_LT'),(62,21,'Homepage best offers 1','','','en_US'),(63,21,'Homepage best offers 1','','','ru_RU'),(64,22,'Homepage best offers 2','','','lt_LT'),(65,22,'Homepage best offers 2','','','en_US'),(66,22,'Homepage best offers 2','','','ru_RU'),(67,23,'Homepage best offers 3','','','lt_LT'),(68,23,'Homepage best offers 3','','','en_US'),(69,23,'Homepage best offers 3','','','ru_RU'),(70,24,'Homepage best offers 4','','','lt_LT'),(71,24,'Homepage best offers 4','','','en_US'),(72,24,'Homepage best offers 4','','','ru_RU'),(73,25,'Flat banner','','','lt_LT'),(74,25,'Flat banner','','','en_US'),(75,25,'Flat banner','','','ru_RU'),(76,26,'Wide banner','','','lt_LT'),(77,26,'Wide banner','','','en_US'),(78,26,'Wide banner','','','ru_RU'),(79,27,'Full screen wide banner','','','lt_LT'),(80,27,'Full screen wide banner','','','en_US'),(81,27,'Full screen wide banner','','','ru_RU'),(82,28,'Product page static image','','','lt_LT'),(83,28,'Product page static image','','','en_US'),(84,28,'Product page static image','','','ru_RU');
/*!40000 ALTER TABLE `omni_banner_position_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_zone`
--

DROP TABLE IF EXISTS `omni_banner_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A63A79E477153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_zone`
--

LOCK TABLES `omni_banner_zone` WRITE;
/*!40000 ALTER TABLE `omni_banner_zone` DISABLE KEYS */;
INSERT INTO `omni_banner_zone` VALUES (26,'before-footer-zone'),(25,'before-header-zone'),(16,'body-top-zone'),(22,'homepage-bottom-bottom'),(21,'homepage-bottom-top'),(18,'homepage-header-bottom'),(17,'homepage-header-top'),(24,'homepage-hero'),(20,'homepage-middle-bottom'),(19,'homepage-middle-top'),(15,'omni-theme'),(27,'product-after-description-zone'),(23,'product-page-middle'),(28,'sidebar-bottom-zone');
/*!40000 ALTER TABLE `omni_banner_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_banner_zone_translation`
--

DROP TABLE IF EXISTS `omni_banner_zone_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_banner_zone_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `omni_banner_zone_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_CC474AE72C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_CC474AE72C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `omni_banner_zone` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_banner_zone_translation`
--

LOCK TABLES `omni_banner_zone_translation` WRITE;
/*!40000 ALTER TABLE `omni_banner_zone_translation` DISABLE KEYS */;
INSERT INTO `omni_banner_zone_translation` VALUES (43,15,'Omni theme zone','','','lt_LT'),(44,15,'Omni theme zone','','','en_US'),(45,15,'Omni theme zone','','','ru_RU'),(46,16,'Top body','','','lt_LT'),(47,16,'Top body','','','en_US'),(48,16,'Top body','','','ru_RU'),(49,17,'Homepage header top','','','lt_LT'),(50,17,'Homepage header top','','','en_US'),(51,17,'Homepage header top','','','ru_RU'),(52,18,'Homepage header bottom','','','lt_LT'),(53,18,'Homepage header bottom','','','en_US'),(54,18,'Homepage header bottom','','','ru_RU'),(55,19,'Homepage middle top','','','lt_LT'),(56,19,'Homepage middle top','','','en_US'),(57,19,'Homepage middle top','','','ru_RU'),(58,20,'Homepage middle bottom','','','lt_LT'),(59,20,'Homepage middle bottom','','','en_US'),(60,20,'Homepage middle bottom','','','ru_RU'),(61,21,'Best offers','','','lt_LT'),(62,21,'Best offers','','','en_US'),(63,21,'Best offers','','','ru_RU'),(64,22,'Homepage bottom bottom','','','lt_LT'),(65,22,'Homepage bottom bottom','','','en_US'),(66,22,'Homepage bottom bottom','','','ru_RU'),(67,23,'Product page middle','','','lt_LT'),(68,23,'Product page middle','','','en_US'),(69,23,'Product page middle','','','ru_RU'),(70,24,'Homepage hero','','','lt_LT'),(71,24,'Homepage hero','','','en_US'),(72,24,'Homepage hero','','','ru_RU'),(73,25,'Before header','','','lt_LT'),(74,25,'Before header','','','en_US'),(75,25,'Before header','','','ru_RU'),(76,26,'Before footer','','','lt_LT'),(77,26,'Before footer','','','en_US'),(78,26,'Before footer','','','ru_RU'),(79,27,'Product after description','','','lt_LT'),(80,27,'Product after description','','','en_US'),(81,27,'Product after description','','','ru_RU'),(82,28,'Sidebar bottom','','','lt_LT'),(83,28,'Sidebar bottom','','','en_US'),(84,28,'Sidebar bottom','','','ru_RU');
/*!40000 ALTER TABLE `omni_banner_zone_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_channel_country`
--

DROP TABLE IF EXISTS `omni_channel_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_channel_country` (
  `channel_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`country_id`),
  KEY `IDX_990C426672F5A1AA` (`channel_id`),
  KEY `IDX_990C4266F92F3E70` (`country_id`),
  CONSTRAINT `FK_990C426672F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`),
  CONSTRAINT `FK_990C4266F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `sylius_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_channel_country`
--

LOCK TABLES `omni_channel_country` WRITE;
/*!40000 ALTER TABLE `omni_channel_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_channel_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_node`
--

DROP TABLE IF EXISTS `omni_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `relation_type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position` int(11) DEFAULT NULL,
  `slug_from_relation` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C437E9D877153098` (`code`),
  KEY `IDX_C437E9D879066886` (`root_id`),
  KEY `IDX_C437E9D8727ACA70` (`parent_id`),
  CONSTRAINT `FK_C437E9D8727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `omni_node` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C437E9D879066886` FOREIGN KEY (`root_id`) REFERENCES `omni_node` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_node`
--

LOCK TABLES `omni_node` WRITE;
/*!40000 ALTER TABLE `omni_node` DISABLE KEYS */;
INSERT INTO `omni_node` VALUES (20,37,37,1,'t_shirts','sylius.taxon',13,'taxon',2,3,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',0,0),(21,37,37,1,'mens_t_shirts','sylius.taxon',14,'taxon',4,5,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',1,0),(22,37,37,1,'womens_t_shirts','sylius.taxon',15,'taxon',6,7,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',2,0),(23,37,37,1,'caps','sylius.taxon',16,'taxon',8,9,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',3,0),(24,38,28,1,'about',NULL,NULL,'content',3,4,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',4,0),(25,38,28,1,'terms_and_conditions',NULL,NULL,'content',5,6,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',5,0),(26,38,28,1,'privacy_policy',NULL,NULL,'content',7,8,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',6,0),(27,38,28,1,'contact',NULL,NULL,'content',9,10,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',7,0),(28,38,38,1,'your_store',NULL,NULL,'placeholder',2,11,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',8,0),(29,38,32,1,'faq\'s',NULL,NULL,'content',13,14,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',9,0),(30,38,32,1,'delivery_and_shipping',NULL,NULL,'content',15,16,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',10,0),(31,38,32,1,'Returns_policy',NULL,NULL,'content',17,18,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',11,0),(32,38,38,1,'customer_care',NULL,NULL,'placeholder',12,19,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',12,0),(33,38,36,1,'suppliers',NULL,NULL,'content',21,22,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',13,0),(34,38,36,1,'history',NULL,NULL,'content',23,24,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',14,0),(35,38,36,1,'More',NULL,NULL,'content',25,26,2,'2022-03-08 11:16:43','2022-03-08 11:16:43',15,0),(36,38,38,1,'information',NULL,NULL,'placeholder',20,27,1,'2022-03-08 11:16:43','2022-03-08 11:16:43',16,0),(37,37,NULL,1,'main_menu',NULL,NULL,'main_menu',1,10,0,'2022-03-08 11:16:43','2022-03-08 11:16:43',17,0),(38,38,NULL,1,'footer_menu',NULL,NULL,'footer_menu',1,28,0,'2022-03-08 11:16:43','2022-03-08 11:16:43',18,0);
/*!40000 ALTER TABLE `omni_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_node_channels`
--

DROP TABLE IF EXISTS `omni_node_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_node_channels` (
  `node_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`node_id`,`channel_id`),
  KEY `IDX_929BDC3460D9FD7` (`node_id`),
  KEY `IDX_929BDC372F5A1AA` (`channel_id`),
  CONSTRAINT `FK_929BDC3460D9FD7` FOREIGN KEY (`node_id`) REFERENCES `omni_node` (`id`),
  CONSTRAINT `FK_929BDC372F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_node_channels`
--

LOCK TABLES `omni_node_channels` WRITE;
/*!40000 ALTER TABLE `omni_node_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_node_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_node_image`
--

DROP TABLE IF EXISTS `omni_node_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_node_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `relation_type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6FF62DAB7E3C61F9` (`owner_id`),
  CONSTRAINT `FK_6FF62DAB7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `omni_node` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_node_image`
--

LOCK TABLES `omni_node_image` WRITE;
/*!40000 ALTER TABLE `omni_node_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_node_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_node_image_translation`
--

DROP TABLE IF EXISTS `omni_node_image_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_node_image_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `omni_node_image_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_303097FC2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_303097FC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `omni_node_image` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_node_image_translation`
--

LOCK TABLES `omni_node_image_translation` WRITE;
/*!40000 ALTER TABLE `omni_node_image_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_node_image_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_node_translation`
--

DROP TABLE IF EXISTS `omni_node_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_node_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `seo_metadata_id` int(11) DEFAULT NULL,
  `link` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `link_target` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `omni_node_translation_uniq_trans` (`translatable_id`,`locale`),
  UNIQUE KEY `path_idx` (`locale`,`slug`),
  UNIQUE KEY `UNIQ_8318E2A418F9C0D5` (`seo_metadata_id`),
  KEY `IDX_8318E2A42C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_8318E2A418F9C0D5` FOREIGN KEY (`seo_metadata_id`) REFERENCES `omni_seo_metadata` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8318E2A42C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `omni_node` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_node_translation`
--

LOCK TABLES `omni_node_translation` WRITE;
/*!40000 ALTER TABLE `omni_node_translation` DISABLE KEYS */;
INSERT INTO `omni_node_translation` VALUES (20,20,NULL,'T-shirts',NULL,'lt_LT',NULL,NULL,NULL),(21,21,NULL,'distinctio nemo in',NULL,'lt_LT',NULL,NULL,NULL),(22,22,NULL,'nihil delectus temporibus',NULL,'lt_LT',NULL,NULL,NULL),(23,23,NULL,'Caps',NULL,'lt_LT',NULL,NULL,NULL),(24,24,'about','About','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(25,25,'terms_and_conditions','Terms and conditions','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(26,26,'privacy_policy','Privacy policy','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(27,27,'contact','Contact','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(28,28,NULL,'Your store',NULL,'lt_LT',NULL,NULL,NULL),(29,29,'faq\'s','Faq\'s','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(30,30,'delivery_and_shipping','Delivery and shipping','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(31,31,'Returns_policy','Returns policy','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(32,32,NULL,'Customer care',NULL,'lt_LT',NULL,NULL,NULL),(33,33,'suppliers','Suppliers','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(34,34,'history','History','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(35,35,'More','More','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','lt_LT',NULL,NULL,NULL),(36,36,NULL,'Information',NULL,'lt_LT',NULL,NULL,NULL),(37,37,NULL,'Main menu',NULL,'lt_LT',NULL,NULL,NULL),(38,38,NULL,'Footer menu',NULL,'lt_LT',NULL,NULL,NULL);
/*!40000 ALTER TABLE `omni_node_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_parcel_machine`
--

DROP TABLE IF EXISTS `omni_parcel_machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_parcel_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_provider_idx` (`code`,`provider`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_parcel_machine`
--

LOCK TABLES `omni_parcel_machine` WRITE;
/*!40000 ALTER TABLE `omni_parcel_machine` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_parcel_machine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_search_index`
--

DROP TABLE IF EXISTS `omni_search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_search_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_id` int(11) NOT NULL,
  `resource_class` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `index` longtext COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id_idx` (`resource_id`),
  KEY `item_id_entity_idx` (`resource_id`,`resource_class`),
  FULLTEXT KEY `fulltext_search_idx` (`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_search_index`
--

LOCK TABLES `omni_search_index` WRITE;
/*!40000 ALTER TABLE `omni_search_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_seo_metadata`
--

DROP TABLE IF EXISTS `omni_seo_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_seo_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `meta_robots` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `original_url` varchar(511) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `extra_http` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  `extra_names` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  `extra_properties` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_seo_metadata`
--

LOCK TABLES `omni_seo_metadata` WRITE;
/*!40000 ALTER TABLE `omni_seo_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_seo_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `omni_shipper_config`
--

DROP TABLE IF EXISTS `omni_shipper_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omni_shipper_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `gateway` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '(DC2Type:json_array)' CHECK (json_valid(`config`)),
  PRIMARY KEY (`id`),
  KEY `IDX_DDAB9290F624B39D` (`sender_id`),
  CONSTRAINT `FK_DDAB9290F624B39D` FOREIGN KEY (`sender_id`) REFERENCES `sylius_channel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `omni_shipper_config`
--

LOCK TABLES `omni_shipper_config` WRITE;
/*!40000 ALTER TABLE `omni_shipper_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `omni_shipper_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `popular_manufacturer`
--

DROP TABLE IF EXISTS `popular_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `popular_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B7F40FABA23B42D` (`manufacturer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `popular_manufacturer`
--

LOCK TABLES `popular_manufacturer` WRITE;
/*!40000 ALTER TABLE `popular_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `popular_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_surcharge_price`
--

DROP TABLE IF EXISTS `supplier_surcharge_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_surcharge_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `amount` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BAAC9F3F9395C3F3` (`customer_id`),
  KEY `IDX_BAAC9F3F2ADD6D8C` (`supplier_id`),
  CONSTRAINT `FK_BAAC9F3F2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  CONSTRAINT `FK_BAAC9F3F9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_surcharge_price`
--

LOCK TABLES `supplier_surcharge_price` WRITE;
/*!40000 ALTER TABLE `supplier_surcharge_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_surcharge_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_address`
--

DROP TABLE IF EXISTS `sylius_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `province_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `province_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `house_number` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `apartment_number` varchar(36) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B97FF0589395C3F3` (`customer_id`),
  CONSTRAINT `FK_B97FF0589395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_address`
--

LOCK TABLES `sylius_address` WRITE;
/*!40000 ALTER TABLE `sylius_address` DISABLE KEYS */;
INSERT INTO `sylius_address` VALUES (71,37,'Amely','Walsh',NULL,'3206 Helen Flats',NULL,'Jaylenbury','56484-7072','2022-03-08 11:16:39','2022-03-08 11:16:39','FR',NULL,NULL,NULL,NULL),(72,NULL,'Amely','Walsh',NULL,'3206 Helen Flats',NULL,'Jaylenbury','56484-7072','2022-03-08 11:16:39','2022-03-08 11:16:39','FR',NULL,NULL,NULL,NULL),(73,NULL,'Amely','Walsh',NULL,'3206 Helen Flats',NULL,'Jaylenbury','56484-7072','2022-03-08 11:16:39','2022-03-08 11:16:39','FR',NULL,NULL,NULL,NULL),(74,32,'Jeffry','Pagac',NULL,'4617 Volkman Pass',NULL,'South Monserrat','08932','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(75,NULL,'Jeffry','Pagac',NULL,'4617 Volkman Pass',NULL,'South Monserrat','08932','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(76,NULL,'Jeffry','Pagac',NULL,'4617 Volkman Pass',NULL,'South Monserrat','08932','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(77,25,'Lacy','Dickinson',NULL,'9030 Gisselle Street',NULL,'Handhaven','49278','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(78,NULL,'Lacy','Dickinson',NULL,'9030 Gisselle Street',NULL,'Handhaven','49278','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(79,NULL,'Lacy','Dickinson',NULL,'9030 Gisselle Street',NULL,'Handhaven','49278','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(80,30,'Lorenzo','Mraz',NULL,'514 Zboncak Expressway Suite 314',NULL,'Dietrichton','77862-7243','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(81,NULL,'Lorenzo','Mraz',NULL,'514 Zboncak Expressway Suite 314',NULL,'Dietrichton','77862-7243','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(82,NULL,'Lorenzo','Mraz',NULL,'514 Zboncak Expressway Suite 314',NULL,'Dietrichton','77862-7243','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(83,43,'Sienna','Welch',NULL,'858 Dare Via Suite 814',NULL,'Langoshview','64875','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(84,NULL,'Sienna','Welch',NULL,'858 Dare Via Suite 814',NULL,'Langoshview','64875','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(85,NULL,'Sienna','Welch',NULL,'858 Dare Via Suite 814',NULL,'Langoshview','64875','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(86,27,'Giovanni','Cummerata',NULL,'8520 Runte Via',NULL,'Lake Myrticebury','93360-8872','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(87,NULL,'Giovanni','Cummerata',NULL,'8520 Runte Via',NULL,'Lake Myrticebury','93360-8872','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(88,NULL,'Giovanni','Cummerata',NULL,'8520 Runte Via',NULL,'Lake Myrticebury','93360-8872','2022-03-08 11:16:39','2022-03-08 11:16:39','US',NULL,NULL,NULL,NULL),(89,41,'Hailie','DuBuque',NULL,'2461 Mueller Turnpike',NULL,'Port Telly','93963','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(90,NULL,'Hailie','DuBuque',NULL,'2461 Mueller Turnpike',NULL,'Port Telly','93963','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(91,NULL,'Hailie','DuBuque',NULL,'2461 Mueller Turnpike',NULL,'Port Telly','93963','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(92,34,'Romaine','Zieme',NULL,'89505 Moore Motorway',NULL,'North Delphine','80573-9966','2022-03-08 11:16:39','2022-03-08 11:16:39','LT',NULL,NULL,NULL,NULL),(93,NULL,'Romaine','Zieme',NULL,'89505 Moore Motorway',NULL,'North Delphine','80573-9966','2022-03-08 11:16:39','2022-03-08 11:16:39','LT',NULL,NULL,NULL,NULL),(94,NULL,'Romaine','Zieme',NULL,'89505 Moore Motorway',NULL,'North Delphine','80573-9966','2022-03-08 11:16:39','2022-03-08 11:16:39','LT',NULL,NULL,NULL,NULL),(95,26,'Nora','Hartmann',NULL,'881 Eleazar Hills Suite 501',NULL,'Mrazchester','74876-3574','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(96,NULL,'Nora','Hartmann',NULL,'881 Eleazar Hills Suite 501',NULL,'Mrazchester','74876-3574','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(97,NULL,'Nora','Hartmann',NULL,'881 Eleazar Hills Suite 501',NULL,'Mrazchester','74876-3574','2022-03-08 11:16:39','2022-03-08 11:16:39','RU',NULL,NULL,NULL,NULL),(98,34,'Aliza','McKenzie',NULL,'5386 Daugherty Harbor',NULL,'Dessieton','70264','2022-03-08 11:16:39','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(99,NULL,'Aliza','McKenzie',NULL,'5386 Daugherty Harbor',NULL,'Dessieton','70264','2022-03-08 11:16:39','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(100,NULL,'Aliza','McKenzie',NULL,'5386 Daugherty Harbor',NULL,'Dessieton','70264','2022-03-08 11:16:39','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(101,24,'Willis','Altenwerth',NULL,'370 Alex Harbors',NULL,'West Erika','73787-0814','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(102,NULL,'Willis','Altenwerth',NULL,'370 Alex Harbors',NULL,'West Erika','73787-0814','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(103,NULL,'Willis','Altenwerth',NULL,'370 Alex Harbors',NULL,'West Erika','73787-0814','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(104,35,'Jaylin','Kulas',NULL,'9222 Leffler Tunnel',NULL,'Rogahnburgh','13821-3907','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(105,NULL,'Jaylin','Kulas',NULL,'9222 Leffler Tunnel',NULL,'Rogahnburgh','13821-3907','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(106,NULL,'Jaylin','Kulas',NULL,'9222 Leffler Tunnel',NULL,'Rogahnburgh','13821-3907','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(107,35,'Sylvan','Cassin',NULL,'380 Prosacco Court',NULL,'Fidelmouth','97984','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(108,NULL,'Sylvan','Cassin',NULL,'380 Prosacco Court',NULL,'Fidelmouth','97984','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(109,NULL,'Sylvan','Cassin',NULL,'380 Prosacco Court',NULL,'Fidelmouth','97984','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(110,44,'William','Lemke',NULL,'41364 Haag Stravenue',NULL,'Mervinmouth','71439-5378','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(111,NULL,'William','Lemke',NULL,'41364 Haag Stravenue',NULL,'Mervinmouth','71439-5378','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(112,NULL,'William','Lemke',NULL,'41364 Haag Stravenue',NULL,'Mervinmouth','71439-5378','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(113,44,'Boris','Hamill',NULL,'3927 Waylon Parkway',NULL,'Jordanefurt','80390','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(114,NULL,'Boris','Hamill',NULL,'3927 Waylon Parkway',NULL,'Jordanefurt','80390','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(115,NULL,'Boris','Hamill',NULL,'3927 Waylon Parkway',NULL,'Jordanefurt','80390','2022-03-08 11:16:40','2022-03-08 11:16:40','RU',NULL,NULL,NULL,NULL),(116,41,'Cielo','Hand',NULL,'7380 Kelli Extensions Suite 910',NULL,'Wolfburgh','93568','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(117,NULL,'Cielo','Hand',NULL,'7380 Kelli Extensions Suite 910',NULL,'Wolfburgh','93568','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(118,NULL,'Cielo','Hand',NULL,'7380 Kelli Extensions Suite 910',NULL,'Wolfburgh','93568','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(119,34,'Brycen','Crona',NULL,'6056 Wuckert Brook',NULL,'West Aracelishire','41781-7438','2022-03-08 11:16:40','2022-03-08 11:16:40','FR',NULL,NULL,NULL,NULL),(120,NULL,'Brycen','Crona',NULL,'6056 Wuckert Brook',NULL,'West Aracelishire','41781-7438','2022-03-08 11:16:40','2022-03-08 11:16:40','FR',NULL,NULL,NULL,NULL),(121,NULL,'Brycen','Crona',NULL,'6056 Wuckert Brook',NULL,'West Aracelishire','41781-7438','2022-03-08 11:16:40','2022-03-08 11:16:40','FR',NULL,NULL,NULL,NULL),(122,38,'Samir','Sawayn',NULL,'20115 Legros Brooks Apt. 242',NULL,'Lake Brown','31046-5288','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(123,NULL,'Samir','Sawayn',NULL,'20115 Legros Brooks Apt. 242',NULL,'Lake Brown','31046-5288','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(124,NULL,'Samir','Sawayn',NULL,'20115 Legros Brooks Apt. 242',NULL,'Lake Brown','31046-5288','2022-03-08 11:16:40','2022-03-08 11:16:40','US',NULL,NULL,NULL,NULL),(125,40,'Jovany','Krajcik',NULL,'814 Bauch Shoals',NULL,'Herzogborough','65541','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(126,NULL,'Jovany','Krajcik',NULL,'814 Bauch Shoals',NULL,'Herzogborough','65541','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(127,NULL,'Jovany','Krajcik',NULL,'814 Bauch Shoals',NULL,'Herzogborough','65541','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(128,41,'Adolphus','Rau',NULL,'530 Erick Keys Suite 034',NULL,'Rextown','62388-1078','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(129,NULL,'Adolphus','Rau',NULL,'530 Erick Keys Suite 034',NULL,'Rextown','62388-1078','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(130,NULL,'Adolphus','Rau',NULL,'530 Erick Keys Suite 034',NULL,'Rextown','62388-1078','2022-03-08 11:16:40','2022-03-08 11:16:40','LT',NULL,NULL,NULL,NULL),(131,29,'Rahul','Pouros','+1 (260) 434-3953','858 Thalia Garden Apt. 864','O\'Keefe and Sons','Homenickfurt','46696-6650','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(132,37,'Enos','Hudson','1-375-778-7447 x8836','60056 Yost Stravenue',NULL,'O\'Haraburgh','14031','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(133,37,'Jordan','Kuhn','801.283.8341 x5514','16969 Rowe Key',NULL,'Kimtown','50646-5217','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(134,43,'Arne','Watsica','809-699-9605','5951 Rogers Crest Suite 481','Tillman Group','Hegmannhaven','98770-3365','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(135,27,'Gillian','Davis','393-298-4695','326 McDermott Mountains',NULL,'Christopherport','04942-7359','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(136,40,'Gerry','Wiegand',NULL,'4722 Huels Lights Apt. 939',NULL,'Petemouth','18666-1141','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(137,44,'Raquel','Glover','(626) 380-5947','510 Dillon Hill','Renner-Cummerata','West Jessicaton','23018','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(138,26,'Emmalee','Prosacco','(937) 382-2889 x710','9080 Herta Ranch Suite 634','Breitenberg and Sons','West Aldaburgh','45995','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(139,31,'Rosemarie','Collier',NULL,'838 Theresia Spurs Suite 740',NULL,'New Alize','25733-3877','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL),(140,33,'Jessika','Jast',NULL,'2836 Bonita Ridge','Ritchie-Bayer','South Vivashire','43547-7205','2022-03-08 11:16:43','2022-03-08 11:16:43','US',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sylius_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_address_log_entries`
--

DROP TABLE IF EXISTS `sylius_address_log_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_address_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_address_log_entries`
--

LOCK TABLES `sylius_address_log_entries` WRITE;
/*!40000 ALTER TABLE `sylius_address_log_entries` DISABLE KEYS */;
INSERT INTO `sylius_address_log_entries` VALUES (71,'create','2022-03-08 11:16:39','71','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Amely\";s:8:\"lastName\";s:5:\"Walsh\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"3206 Helen Flats\";s:7:\"company\";N;s:4:\"city\";s:10:\"Jaylenbury\";s:8:\"postcode\";s:10:\"56484-7072\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(72,'create','2022-03-08 11:16:39','72','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Amely\";s:8:\"lastName\";s:5:\"Walsh\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"3206 Helen Flats\";s:7:\"company\";N;s:4:\"city\";s:10:\"Jaylenbury\";s:8:\"postcode\";s:10:\"56484-7072\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(73,'create','2022-03-08 11:16:39','73','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Amely\";s:8:\"lastName\";s:5:\"Walsh\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"3206 Helen Flats\";s:7:\"company\";N;s:4:\"city\";s:10:\"Jaylenbury\";s:8:\"postcode\";s:10:\"56484-7072\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(74,'create','2022-03-08 11:16:40','74','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jeffry\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"4617 Volkman Pass\";s:7:\"company\";N;s:4:\"city\";s:15:\"South Monserrat\";s:8:\"postcode\";s:5:\"08932\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(75,'create','2022-03-08 11:16:40','75','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jeffry\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"4617 Volkman Pass\";s:7:\"company\";N;s:4:\"city\";s:15:\"South Monserrat\";s:8:\"postcode\";s:5:\"08932\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(76,'create','2022-03-08 11:16:40','76','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jeffry\";s:8:\"lastName\";s:5:\"Pagac\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"4617 Volkman Pass\";s:7:\"company\";N;s:4:\"city\";s:15:\"South Monserrat\";s:8:\"postcode\";s:5:\"08932\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(77,'create','2022-03-08 11:16:40','77','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Lacy\";s:8:\"lastName\";s:9:\"Dickinson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"9030 Gisselle Street\";s:7:\"company\";N;s:4:\"city\";s:9:\"Handhaven\";s:8:\"postcode\";s:5:\"49278\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(78,'create','2022-03-08 11:16:40','78','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Lacy\";s:8:\"lastName\";s:9:\"Dickinson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"9030 Gisselle Street\";s:7:\"company\";N;s:4:\"city\";s:9:\"Handhaven\";s:8:\"postcode\";s:5:\"49278\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(79,'create','2022-03-08 11:16:40','79','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Lacy\";s:8:\"lastName\";s:9:\"Dickinson\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"9030 Gisselle Street\";s:7:\"company\";N;s:4:\"city\";s:9:\"Handhaven\";s:8:\"postcode\";s:5:\"49278\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(80,'create','2022-03-08 11:16:40','80','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Lorenzo\";s:8:\"lastName\";s:4:\"Mraz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:32:\"514 Zboncak Expressway Suite 314\";s:7:\"company\";N;s:4:\"city\";s:11:\"Dietrichton\";s:8:\"postcode\";s:10:\"77862-7243\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(81,'create','2022-03-08 11:16:40','81','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Lorenzo\";s:8:\"lastName\";s:4:\"Mraz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:32:\"514 Zboncak Expressway Suite 314\";s:7:\"company\";N;s:4:\"city\";s:11:\"Dietrichton\";s:8:\"postcode\";s:10:\"77862-7243\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(82,'create','2022-03-08 11:16:40','82','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Lorenzo\";s:8:\"lastName\";s:4:\"Mraz\";s:11:\"phoneNumber\";N;s:6:\"street\";s:32:\"514 Zboncak Expressway Suite 314\";s:7:\"company\";N;s:4:\"city\";s:11:\"Dietrichton\";s:8:\"postcode\";s:10:\"77862-7243\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(83,'create','2022-03-08 11:16:40','83','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sienna\";s:8:\"lastName\";s:5:\"Welch\";s:11:\"phoneNumber\";N;s:6:\"street\";s:22:\"858 Dare Via Suite 814\";s:7:\"company\";N;s:4:\"city\";s:11:\"Langoshview\";s:8:\"postcode\";s:5:\"64875\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(84,'create','2022-03-08 11:16:40','84','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sienna\";s:8:\"lastName\";s:5:\"Welch\";s:11:\"phoneNumber\";N;s:6:\"street\";s:22:\"858 Dare Via Suite 814\";s:7:\"company\";N;s:4:\"city\";s:11:\"Langoshview\";s:8:\"postcode\";s:5:\"64875\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(85,'create','2022-03-08 11:16:40','85','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sienna\";s:8:\"lastName\";s:5:\"Welch\";s:11:\"phoneNumber\";N;s:6:\"street\";s:22:\"858 Dare Via Suite 814\";s:7:\"company\";N;s:4:\"city\";s:11:\"Langoshview\";s:8:\"postcode\";s:5:\"64875\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(86,'create','2022-03-08 11:16:40','86','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Giovanni\";s:8:\"lastName\";s:9:\"Cummerata\";s:11:\"phoneNumber\";N;s:6:\"street\";s:14:\"8520 Runte Via\";s:7:\"company\";N;s:4:\"city\";s:16:\"Lake Myrticebury\";s:8:\"postcode\";s:10:\"93360-8872\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(87,'create','2022-03-08 11:16:40','87','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Giovanni\";s:8:\"lastName\";s:9:\"Cummerata\";s:11:\"phoneNumber\";N;s:6:\"street\";s:14:\"8520 Runte Via\";s:7:\"company\";N;s:4:\"city\";s:16:\"Lake Myrticebury\";s:8:\"postcode\";s:10:\"93360-8872\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(88,'create','2022-03-08 11:16:40','88','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Giovanni\";s:8:\"lastName\";s:9:\"Cummerata\";s:11:\"phoneNumber\";N;s:6:\"street\";s:14:\"8520 Runte Via\";s:7:\"company\";N;s:4:\"city\";s:16:\"Lake Myrticebury\";s:8:\"postcode\";s:10:\"93360-8872\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(89,'create','2022-03-08 11:16:40','89','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Hailie\";s:8:\"lastName\";s:7:\"DuBuque\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"2461 Mueller Turnpike\";s:7:\"company\";N;s:4:\"city\";s:10:\"Port Telly\";s:8:\"postcode\";s:5:\"93963\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(90,'create','2022-03-08 11:16:40','90','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Hailie\";s:8:\"lastName\";s:7:\"DuBuque\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"2461 Mueller Turnpike\";s:7:\"company\";N;s:4:\"city\";s:10:\"Port Telly\";s:8:\"postcode\";s:5:\"93963\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(91,'create','2022-03-08 11:16:40','91','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Hailie\";s:8:\"lastName\";s:7:\"DuBuque\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"2461 Mueller Turnpike\";s:7:\"company\";N;s:4:\"city\";s:10:\"Port Telly\";s:8:\"postcode\";s:5:\"93963\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(92,'create','2022-03-08 11:16:40','92','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Romaine\";s:8:\"lastName\";s:5:\"Zieme\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"89505 Moore Motorway\";s:7:\"company\";N;s:4:\"city\";s:14:\"North Delphine\";s:8:\"postcode\";s:10:\"80573-9966\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(93,'create','2022-03-08 11:16:40','93','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Romaine\";s:8:\"lastName\";s:5:\"Zieme\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"89505 Moore Motorway\";s:7:\"company\";N;s:4:\"city\";s:14:\"North Delphine\";s:8:\"postcode\";s:10:\"80573-9966\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(94,'create','2022-03-08 11:16:40','94','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Romaine\";s:8:\"lastName\";s:5:\"Zieme\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"89505 Moore Motorway\";s:7:\"company\";N;s:4:\"city\";s:14:\"North Delphine\";s:8:\"postcode\";s:10:\"80573-9966\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(95,'create','2022-03-08 11:16:40','95','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Nora\";s:8:\"lastName\";s:8:\"Hartmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"881 Eleazar Hills Suite 501\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mrazchester\";s:8:\"postcode\";s:10:\"74876-3574\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(96,'create','2022-03-08 11:16:40','96','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Nora\";s:8:\"lastName\";s:8:\"Hartmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"881 Eleazar Hills Suite 501\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mrazchester\";s:8:\"postcode\";s:10:\"74876-3574\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(97,'create','2022-03-08 11:16:40','97','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Nora\";s:8:\"lastName\";s:8:\"Hartmann\";s:11:\"phoneNumber\";N;s:6:\"street\";s:27:\"881 Eleazar Hills Suite 501\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mrazchester\";s:8:\"postcode\";s:10:\"74876-3574\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(98,'create','2022-03-08 11:16:40','98','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Aliza\";s:8:\"lastName\";s:8:\"McKenzie\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"5386 Daugherty Harbor\";s:7:\"company\";N;s:4:\"city\";s:9:\"Dessieton\";s:8:\"postcode\";s:5:\"70264\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(99,'create','2022-03-08 11:16:40','99','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Aliza\";s:8:\"lastName\";s:8:\"McKenzie\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"5386 Daugherty Harbor\";s:7:\"company\";N;s:4:\"city\";s:9:\"Dessieton\";s:8:\"postcode\";s:5:\"70264\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(100,'create','2022-03-08 11:16:40','100','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Aliza\";s:8:\"lastName\";s:8:\"McKenzie\";s:11:\"phoneNumber\";N;s:6:\"street\";s:21:\"5386 Daugherty Harbor\";s:7:\"company\";N;s:4:\"city\";s:9:\"Dessieton\";s:8:\"postcode\";s:5:\"70264\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(101,'create','2022-03-08 11:16:40','101','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Willis\";s:8:\"lastName\";s:10:\"Altenwerth\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"370 Alex Harbors\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Erika\";s:8:\"postcode\";s:10:\"73787-0814\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(102,'create','2022-03-08 11:16:40','102','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Willis\";s:8:\"lastName\";s:10:\"Altenwerth\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"370 Alex Harbors\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Erika\";s:8:\"postcode\";s:10:\"73787-0814\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(103,'create','2022-03-08 11:16:40','103','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Willis\";s:8:\"lastName\";s:10:\"Altenwerth\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"370 Alex Harbors\";s:7:\"company\";N;s:4:\"city\";s:10:\"West Erika\";s:8:\"postcode\";s:10:\"73787-0814\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(104,'create','2022-03-08 11:16:40','104','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jaylin\";s:8:\"lastName\";s:5:\"Kulas\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"9222 Leffler Tunnel\";s:7:\"company\";N;s:4:\"city\";s:11:\"Rogahnburgh\";s:8:\"postcode\";s:10:\"13821-3907\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(105,'create','2022-03-08 11:16:40','105','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jaylin\";s:8:\"lastName\";s:5:\"Kulas\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"9222 Leffler Tunnel\";s:7:\"company\";N;s:4:\"city\";s:11:\"Rogahnburgh\";s:8:\"postcode\";s:10:\"13821-3907\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(106,'create','2022-03-08 11:16:40','106','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jaylin\";s:8:\"lastName\";s:5:\"Kulas\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"9222 Leffler Tunnel\";s:7:\"company\";N;s:4:\"city\";s:11:\"Rogahnburgh\";s:8:\"postcode\";s:10:\"13821-3907\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(107,'create','2022-03-08 11:16:40','107','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sylvan\";s:8:\"lastName\";s:6:\"Cassin\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"380 Prosacco Court\";s:7:\"company\";N;s:4:\"city\";s:10:\"Fidelmouth\";s:8:\"postcode\";s:5:\"97984\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(108,'create','2022-03-08 11:16:40','108','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sylvan\";s:8:\"lastName\";s:6:\"Cassin\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"380 Prosacco Court\";s:7:\"company\";N;s:4:\"city\";s:10:\"Fidelmouth\";s:8:\"postcode\";s:5:\"97984\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(109,'create','2022-03-08 11:16:40','109','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Sylvan\";s:8:\"lastName\";s:6:\"Cassin\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"380 Prosacco Court\";s:7:\"company\";N;s:4:\"city\";s:10:\"Fidelmouth\";s:8:\"postcode\";s:5:\"97984\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(110,'create','2022-03-08 11:16:40','110','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"William\";s:8:\"lastName\";s:5:\"Lemke\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"41364 Haag Stravenue\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mervinmouth\";s:8:\"postcode\";s:10:\"71439-5378\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(111,'create','2022-03-08 11:16:40','111','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"William\";s:8:\"lastName\";s:5:\"Lemke\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"41364 Haag Stravenue\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mervinmouth\";s:8:\"postcode\";s:10:\"71439-5378\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(112,'create','2022-03-08 11:16:40','112','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"William\";s:8:\"lastName\";s:5:\"Lemke\";s:11:\"phoneNumber\";N;s:6:\"street\";s:20:\"41364 Haag Stravenue\";s:7:\"company\";N;s:4:\"city\";s:11:\"Mervinmouth\";s:8:\"postcode\";s:10:\"71439-5378\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(113,'create','2022-03-08 11:16:40','113','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Boris\";s:8:\"lastName\";s:6:\"Hamill\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"3927 Waylon Parkway\";s:7:\"company\";N;s:4:\"city\";s:11:\"Jordanefurt\";s:8:\"postcode\";s:5:\"80390\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(114,'create','2022-03-08 11:16:40','114','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Boris\";s:8:\"lastName\";s:6:\"Hamill\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"3927 Waylon Parkway\";s:7:\"company\";N;s:4:\"city\";s:11:\"Jordanefurt\";s:8:\"postcode\";s:5:\"80390\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(115,'create','2022-03-08 11:16:40','115','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Boris\";s:8:\"lastName\";s:6:\"Hamill\";s:11:\"phoneNumber\";N;s:6:\"street\";s:19:\"3927 Waylon Parkway\";s:7:\"company\";N;s:4:\"city\";s:11:\"Jordanefurt\";s:8:\"postcode\";s:5:\"80390\";s:11:\"countryCode\";s:2:\"RU\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(116,'create','2022-03-08 11:16:40','116','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Cielo\";s:8:\"lastName\";s:4:\"Hand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:31:\"7380 Kelli Extensions Suite 910\";s:7:\"company\";N;s:4:\"city\";s:9:\"Wolfburgh\";s:8:\"postcode\";s:5:\"93568\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(117,'create','2022-03-08 11:16:40','117','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Cielo\";s:8:\"lastName\";s:4:\"Hand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:31:\"7380 Kelli Extensions Suite 910\";s:7:\"company\";N;s:4:\"city\";s:9:\"Wolfburgh\";s:8:\"postcode\";s:5:\"93568\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(118,'create','2022-03-08 11:16:40','118','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Cielo\";s:8:\"lastName\";s:4:\"Hand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:31:\"7380 Kelli Extensions Suite 910\";s:7:\"company\";N;s:4:\"city\";s:9:\"Wolfburgh\";s:8:\"postcode\";s:5:\"93568\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(119,'create','2022-03-08 11:16:40','119','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Brycen\";s:8:\"lastName\";s:5:\"Crona\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"6056 Wuckert Brook\";s:7:\"company\";N;s:4:\"city\";s:17:\"West Aracelishire\";s:8:\"postcode\";s:10:\"41781-7438\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(120,'create','2022-03-08 11:16:40','120','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Brycen\";s:8:\"lastName\";s:5:\"Crona\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"6056 Wuckert Brook\";s:7:\"company\";N;s:4:\"city\";s:17:\"West Aracelishire\";s:8:\"postcode\";s:10:\"41781-7438\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(121,'create','2022-03-08 11:16:40','121','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Brycen\";s:8:\"lastName\";s:5:\"Crona\";s:11:\"phoneNumber\";N;s:6:\"street\";s:18:\"6056 Wuckert Brook\";s:7:\"company\";N;s:4:\"city\";s:17:\"West Aracelishire\";s:8:\"postcode\";s:10:\"41781-7438\";s:11:\"countryCode\";s:2:\"FR\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(122,'create','2022-03-08 11:16:40','122','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Samir\";s:8:\"lastName\";s:6:\"Sawayn\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"20115 Legros Brooks Apt. 242\";s:7:\"company\";N;s:4:\"city\";s:10:\"Lake Brown\";s:8:\"postcode\";s:10:\"31046-5288\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(123,'create','2022-03-08 11:16:40','123','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Samir\";s:8:\"lastName\";s:6:\"Sawayn\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"20115 Legros Brooks Apt. 242\";s:7:\"company\";N;s:4:\"city\";s:10:\"Lake Brown\";s:8:\"postcode\";s:10:\"31046-5288\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(124,'create','2022-03-08 11:16:40','124','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Samir\";s:8:\"lastName\";s:6:\"Sawayn\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"20115 Legros Brooks Apt. 242\";s:7:\"company\";N;s:4:\"city\";s:10:\"Lake Brown\";s:8:\"postcode\";s:10:\"31046-5288\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(125,'create','2022-03-08 11:16:40','125','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jovany\";s:8:\"lastName\";s:7:\"Krajcik\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"814 Bauch Shoals\";s:7:\"company\";N;s:4:\"city\";s:13:\"Herzogborough\";s:8:\"postcode\";s:5:\"65541\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(126,'create','2022-03-08 11:16:40','126','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jovany\";s:8:\"lastName\";s:7:\"Krajcik\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"814 Bauch Shoals\";s:7:\"company\";N;s:4:\"city\";s:13:\"Herzogborough\";s:8:\"postcode\";s:5:\"65541\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(127,'create','2022-03-08 11:16:40','127','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jovany\";s:8:\"lastName\";s:7:\"Krajcik\";s:11:\"phoneNumber\";N;s:6:\"street\";s:16:\"814 Bauch Shoals\";s:7:\"company\";N;s:4:\"city\";s:13:\"Herzogborough\";s:8:\"postcode\";s:5:\"65541\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(128,'create','2022-03-08 11:16:40','128','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Adolphus\";s:8:\"lastName\";s:3:\"Rau\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"530 Erick Keys Suite 034\";s:7:\"company\";N;s:4:\"city\";s:7:\"Rextown\";s:8:\"postcode\";s:10:\"62388-1078\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(129,'create','2022-03-08 11:16:40','129','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Adolphus\";s:8:\"lastName\";s:3:\"Rau\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"530 Erick Keys Suite 034\";s:7:\"company\";N;s:4:\"city\";s:7:\"Rextown\";s:8:\"postcode\";s:10:\"62388-1078\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(130,'create','2022-03-08 11:16:40','130','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:8:\"Adolphus\";s:8:\"lastName\";s:3:\"Rau\";s:11:\"phoneNumber\";N;s:6:\"street\";s:24:\"530 Erick Keys Suite 034\";s:7:\"company\";N;s:4:\"city\";s:7:\"Rextown\";s:8:\"postcode\";s:10:\"62388-1078\";s:11:\"countryCode\";s:2:\"LT\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(131,'create','2022-03-08 11:16:43','131','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Rahul\";s:8:\"lastName\";s:6:\"Pouros\";s:11:\"phoneNumber\";s:17:\"+1 (260) 434-3953\";s:6:\"street\";s:26:\"858 Thalia Garden Apt. 864\";s:7:\"company\";s:16:\"O\'Keefe and Sons\";s:4:\"city\";s:12:\"Homenickfurt\";s:8:\"postcode\";s:10:\"46696-6650\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(132,'create','2022-03-08 11:16:43','132','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Enos\";s:8:\"lastName\";s:6:\"Hudson\";s:11:\"phoneNumber\";s:20:\"1-375-778-7447 x8836\";s:6:\"street\";s:20:\"60056 Yost Stravenue\";s:7:\"company\";N;s:4:\"city\";s:11:\"O\'Haraburgh\";s:8:\"postcode\";s:5:\"14031\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(133,'create','2022-03-08 11:16:43','133','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Jordan\";s:8:\"lastName\";s:4:\"Kuhn\";s:11:\"phoneNumber\";s:18:\"801.283.8341 x5514\";s:6:\"street\";s:14:\"16969 Rowe Key\";s:7:\"company\";N;s:4:\"city\";s:7:\"Kimtown\";s:8:\"postcode\";s:10:\"50646-5217\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(134,'create','2022-03-08 11:16:43','134','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:4:\"Arne\";s:8:\"lastName\";s:7:\"Watsica\";s:11:\"phoneNumber\";s:12:\"809-699-9605\";s:6:\"street\";s:27:\"5951 Rogers Crest Suite 481\";s:7:\"company\";s:13:\"Tillman Group\";s:4:\"city\";s:12:\"Hegmannhaven\";s:8:\"postcode\";s:10:\"98770-3365\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(135,'create','2022-03-08 11:16:43','135','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Gillian\";s:8:\"lastName\";s:5:\"Davis\";s:11:\"phoneNumber\";s:12:\"393-298-4695\";s:6:\"street\";s:23:\"326 McDermott Mountains\";s:7:\"company\";N;s:4:\"city\";s:15:\"Christopherport\";s:8:\"postcode\";s:10:\"04942-7359\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(136,'create','2022-03-08 11:16:43','136','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:5:\"Gerry\";s:8:\"lastName\";s:7:\"Wiegand\";s:11:\"phoneNumber\";N;s:6:\"street\";s:26:\"4722 Huels Lights Apt. 939\";s:7:\"company\";N;s:4:\"city\";s:9:\"Petemouth\";s:8:\"postcode\";s:10:\"18666-1141\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(137,'create','2022-03-08 11:16:43','137','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:6:\"Raquel\";s:8:\"lastName\";s:6:\"Glover\";s:11:\"phoneNumber\";s:14:\"(626) 380-5947\";s:6:\"street\";s:15:\"510 Dillon Hill\";s:7:\"company\";s:16:\"Renner-Cummerata\";s:4:\"city\";s:15:\"West Jessicaton\";s:8:\"postcode\";s:5:\"23018\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(138,'create','2022-03-08 11:16:43','138','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Emmalee\";s:8:\"lastName\";s:8:\"Prosacco\";s:11:\"phoneNumber\";s:19:\"(937) 382-2889 x710\";s:6:\"street\";s:26:\"9080 Herta Ranch Suite 634\";s:7:\"company\";s:20:\"Breitenberg and Sons\";s:4:\"city\";s:14:\"West Aldaburgh\";s:8:\"postcode\";s:5:\"45995\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(139,'create','2022-03-08 11:16:43','139','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:9:\"Rosemarie\";s:8:\"lastName\";s:7:\"Collier\";s:11:\"phoneNumber\";N;s:6:\"street\";s:28:\"838 Theresia Spurs Suite 740\";s:7:\"company\";N;s:4:\"city\";s:9:\"New Alize\";s:8:\"postcode\";s:10:\"25733-3877\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL),(140,'create','2022-03-08 11:16:43','140','App\\Entity\\Addressing\\Address',1,'a:10:{s:9:\"firstName\";s:7:\"Jessika\";s:8:\"lastName\";s:4:\"Jast\";s:11:\"phoneNumber\";N;s:6:\"street\";s:17:\"2836 Bonita Ridge\";s:7:\"company\";s:13:\"Ritchie-Bayer\";s:4:\"city\";s:15:\"South Vivashire\";s:8:\"postcode\";s:10:\"43547-7205\";s:11:\"countryCode\";s:2:\"US\";s:12:\"provinceCode\";N;s:12:\"provinceName\";N;}',NULL);
/*!40000 ALTER TABLE `sylius_address_log_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_adjustment`
--

DROP TABLE IF EXISTS `sylius_adjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_adjustment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_item_id` int(11) DEFAULT NULL,
  `order_item_unit_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `is_neutral` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `origin_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ACA6E0F28D9F6D38` (`order_id`),
  KEY `IDX_ACA6E0F2E415FB15` (`order_item_id`),
  KEY `IDX_ACA6E0F2F720C233` (`order_item_unit_id`),
  CONSTRAINT `FK_ACA6E0F28D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ACA6E0F2E415FB15` FOREIGN KEY (`order_item_id`) REFERENCES `sylius_order_item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ACA6E0F2F720C233` FOREIGN KEY (`order_item_unit_id`) REFERENCES `sylius_order_item_unit` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=559 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_adjustment`
--

LOCK TABLES `sylius_adjustment` WRITE;
/*!40000 ALTER TABLE `sylius_adjustment` DISABLE KEYS */;
INSERT INTO `sylius_adjustment` VALUES (252,NULL,NULL,161,'order_promotion','New Year',-58,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(253,NULL,NULL,161,'order_promotion','Christmas',-327,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(254,NULL,NULL,162,'order_promotion','New Year',-58,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(255,NULL,NULL,162,'order_promotion','Christmas',-327,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(256,NULL,NULL,163,'order_promotion','New Year',-58,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(257,NULL,NULL,163,'order_promotion','Christmas',-327,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(258,NULL,NULL,164,'order_promotion','New Year',-143,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(259,NULL,NULL,164,'order_promotion','Christmas',-805,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(260,NULL,NULL,165,'order_promotion','New Year',-203,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(261,NULL,NULL,165,'order_promotion','Christmas',-1142,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(262,NULL,NULL,166,'order_promotion','New Year',-203,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(263,NULL,NULL,166,'order_promotion','Christmas',-1142,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(264,NULL,NULL,167,'order_promotion','New Year',-202,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(265,NULL,NULL,167,'order_promotion','Christmas',-1142,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(266,NULL,NULL,168,'order_promotion','New Year',-10,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(267,NULL,NULL,168,'order_promotion','Christmas',-54,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(268,NULL,NULL,169,'order_promotion','New Year',-9,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(269,NULL,NULL,169,'order_promotion','Christmas',-53,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(270,NULL,NULL,170,'order_promotion','New Year',-19,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(271,NULL,NULL,170,'order_promotion','Christmas',-105,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(272,NULL,NULL,171,'order_promotion','New Year',-19,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(273,NULL,NULL,171,'order_promotion','Christmas',-105,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(274,NULL,NULL,172,'order_promotion','New Year',-18,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(275,NULL,NULL,172,'order_promotion','Christmas',-105,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(276,21,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(277,NULL,NULL,173,'order_promotion','New Year',-68,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(278,NULL,NULL,173,'order_promotion','Christmas',-209,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(279,NULL,NULL,174,'order_promotion','New Year',-67,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(280,NULL,NULL,174,'order_promotion','Christmas',-209,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(281,NULL,NULL,175,'order_promotion','New Year',-67,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(282,NULL,NULL,175,'order_promotion','Christmas',-209,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(283,NULL,NULL,176,'order_promotion','New Year',-67,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(284,NULL,NULL,176,'order_promotion','Christmas',-209,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(285,NULL,NULL,177,'order_promotion','New Year',-104,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(286,NULL,NULL,177,'order_promotion','Christmas',-322,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(287,NULL,NULL,178,'order_promotion','New Year',-104,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(288,NULL,NULL,178,'order_promotion','Christmas',-322,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(289,NULL,NULL,179,'order_promotion','New Year',-104,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(290,NULL,NULL,179,'order_promotion','Christmas',-322,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(291,NULL,NULL,180,'order_promotion','New Year',-103,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(292,NULL,NULL,180,'order_promotion','Christmas',-322,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(293,NULL,NULL,181,'order_promotion','New Year',-103,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(294,NULL,NULL,181,'order_promotion','Christmas',-321,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(295,NULL,NULL,182,'order_promotion','New Year',-71,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(296,NULL,NULL,182,'order_promotion','Christmas',-221,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(297,NULL,NULL,183,'order_promotion','New Year',-71,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(298,NULL,NULL,183,'order_promotion','Christmas',-220,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(299,NULL,NULL,184,'order_promotion','New Year',-71,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(300,NULL,NULL,184,'order_promotion','Christmas',-220,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(301,22,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(302,NULL,NULL,185,'order_promotion','New Year',-176,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(303,NULL,NULL,186,'order_promotion','New Year',-171,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(304,NULL,NULL,187,'order_promotion','New Year',-327,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(305,NULL,NULL,188,'order_promotion','New Year',-326,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(306,23,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(307,NULL,NULL,189,'order_promotion','New Year',-42,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(308,NULL,NULL,189,'order_promotion','Christmas',-148,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(309,NULL,NULL,190,'order_promotion','New Year',-42,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(310,NULL,NULL,190,'order_promotion','Christmas',-147,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(311,NULL,NULL,191,'order_promotion','New Year',-54,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(312,NULL,NULL,191,'order_promotion','Christmas',-189,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(313,NULL,NULL,192,'order_promotion','New Year',-54,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(314,NULL,NULL,192,'order_promotion','Christmas',-189,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(315,NULL,NULL,193,'order_promotion','New Year',-54,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(316,NULL,NULL,193,'order_promotion','Christmas',-189,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(317,NULL,NULL,194,'order_promotion','New Year',-151,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(318,NULL,NULL,194,'order_promotion','Christmas',-529,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(319,NULL,NULL,195,'order_promotion','New Year',-151,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(320,NULL,NULL,195,'order_promotion','Christmas',-529,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(321,NULL,NULL,196,'order_promotion','New Year',-151,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(322,NULL,NULL,196,'order_promotion','Christmas',-529,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(323,NULL,NULL,197,'order_promotion','New Year',-151,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(324,NULL,NULL,197,'order_promotion','Christmas',-529,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(325,NULL,NULL,198,'order_promotion','New Year',-150,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(326,NULL,NULL,198,'order_promotion','Christmas',-529,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(327,24,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(328,NULL,NULL,199,'order_promotion','New Year',-170,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(329,NULL,NULL,199,'order_promotion','Christmas',-1146,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(330,NULL,NULL,200,'order_promotion','New Year',-170,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(331,NULL,NULL,200,'order_promotion','Christmas',-1146,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(332,NULL,NULL,201,'order_promotion','New Year',-170,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(333,NULL,NULL,201,'order_promotion','Christmas',-1145,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(334,NULL,NULL,202,'order_promotion','New Year',-98,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(335,NULL,NULL,202,'order_promotion','Christmas',-661,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(336,NULL,NULL,203,'order_promotion','New Year',-98,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(337,NULL,NULL,203,'order_promotion','Christmas',-661,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(338,NULL,NULL,204,'order_promotion','New Year',-98,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(339,NULL,NULL,204,'order_promotion','Christmas',-660,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(340,NULL,NULL,205,'order_promotion','New Year',-98,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(341,NULL,NULL,205,'order_promotion','Christmas',-660,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(342,NULL,NULL,206,'order_promotion','New Year',-98,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(343,NULL,NULL,206,'order_promotion','Christmas',-660,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(344,25,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(345,NULL,NULL,207,'order_promotion','New Year',-127,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(346,NULL,NULL,207,'order_promotion','Christmas',-966,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(347,NULL,NULL,208,'order_promotion','New Year',-14,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(348,NULL,NULL,208,'order_promotion','Christmas',-106,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(349,NULL,NULL,209,'order_promotion','New Year',-14,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(350,NULL,NULL,209,'order_promotion','Christmas',-105,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(351,NULL,NULL,210,'order_promotion','New Year',-13,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(352,NULL,NULL,210,'order_promotion','Christmas',-105,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(353,NULL,NULL,211,'order_promotion','New Year',-121,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(354,NULL,NULL,211,'order_promotion','Christmas',-922,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(355,NULL,NULL,212,'order_promotion','New Year',-121,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(356,NULL,NULL,212,'order_promotion','Christmas',-922,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(357,NULL,NULL,213,'order_promotion','New Year',-121,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(358,NULL,NULL,213,'order_promotion','Christmas',-921,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(359,NULL,NULL,214,'order_promotion','New Year',-90,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(360,NULL,NULL,214,'order_promotion','Christmas',-682,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(361,NULL,NULL,215,'order_promotion','New Year',-90,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(362,NULL,NULL,215,'order_promotion','Christmas',-682,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(363,NULL,NULL,216,'order_promotion','New Year',-89,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(364,NULL,NULL,216,'order_promotion','Christmas',-681,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(365,NULL,NULL,217,'order_promotion','New Year',-89,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(366,NULL,NULL,217,'order_promotion','Christmas',-681,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(367,NULL,NULL,218,'order_promotion','New Year',-89,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(368,NULL,NULL,218,'order_promotion','Christmas',-681,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(369,NULL,NULL,219,'order_promotion','New Year',-8,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(370,NULL,NULL,219,'order_promotion','Christmas',-56,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(371,NULL,NULL,220,'order_promotion','New Year',-7,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(372,NULL,NULL,220,'order_promotion','Christmas',-55,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(373,NULL,NULL,221,'order_promotion','New Year',-7,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(374,NULL,NULL,221,'order_promotion','Christmas',-55,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(375,26,NULL,NULL,'shipping','FedEx',246,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(376,27,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(377,NULL,NULL,223,'order_promotion','New Year',-10,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(378,NULL,NULL,223,'order_promotion','Christmas',-54,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(379,NULL,NULL,224,'order_promotion','New Year',-9,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(380,NULL,NULL,224,'order_promotion','Christmas',-54,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(381,NULL,NULL,225,'order_promotion','New Year',-9,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(382,NULL,NULL,225,'order_promotion','Christmas',-54,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(383,NULL,NULL,226,'order_promotion','New Year',-9,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(384,NULL,NULL,226,'order_promotion','Christmas',-53,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(385,NULL,NULL,227,'order_promotion','New Year',-9,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(386,NULL,NULL,227,'order_promotion','Christmas',-53,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(387,NULL,NULL,228,'order_promotion','New Year',-191,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(388,NULL,NULL,228,'order_promotion','Christmas',-1101,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(389,NULL,NULL,229,'order_promotion','New Year',-191,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(390,NULL,NULL,229,'order_promotion','Christmas',-1100,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(391,NULL,NULL,230,'order_promotion','New Year',-191,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(392,NULL,NULL,230,'order_promotion','Christmas',-1100,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(393,NULL,NULL,231,'order_promotion','New Year',-191,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(394,NULL,NULL,231,'order_promotion','Christmas',-1100,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(395,NULL,NULL,232,'order_promotion','New Year',-190,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(396,NULL,NULL,232,'order_promotion','Christmas',-1100,0,0,'christmas','2022-03-08 11:16:39','2022-03-08 11:16:39'),(397,28,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(398,NULL,NULL,233,'order_promotion','New Year',-200,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(399,NULL,NULL,234,'order_promotion','New Year',-200,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(400,NULL,NULL,235,'order_promotion','New Year',-200,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(401,NULL,NULL,236,'order_promotion','New Year',-200,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(402,NULL,NULL,237,'order_promotion','New Year',-200,0,0,'new_year','2022-03-08 11:16:39','2022-03-08 11:16:39'),(403,29,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(404,NULL,NULL,238,'order_promotion','New Year',-141,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(405,NULL,NULL,238,'order_promotion','Christmas',-951,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(406,NULL,NULL,239,'order_promotion','New Year',-140,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(407,NULL,NULL,239,'order_promotion','Christmas',-950,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(408,NULL,NULL,240,'order_promotion','New Year',-67,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(409,NULL,NULL,240,'order_promotion','Christmas',-456,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(410,NULL,NULL,241,'order_promotion','New Year',-135,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(411,NULL,NULL,241,'order_promotion','Christmas',-916,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(412,NULL,NULL,242,'order_promotion','New Year',-135,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(413,NULL,NULL,242,'order_promotion','Christmas',-915,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(414,NULL,NULL,243,'order_promotion','New Year',-134,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(415,NULL,NULL,243,'order_promotion','Christmas',-915,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(416,NULL,NULL,244,'order_promotion','New Year',-29,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(417,NULL,NULL,244,'order_promotion','Christmas',-196,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(418,NULL,NULL,245,'order_promotion','New Year',-29,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(419,NULL,NULL,245,'order_promotion','Christmas',-195,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(420,NULL,NULL,246,'order_promotion','New Year',-28,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(421,NULL,NULL,246,'order_promotion','Christmas',-195,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(422,NULL,NULL,247,'order_promotion','New Year',-162,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(423,NULL,NULL,247,'order_promotion','Christmas',-1104,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(424,30,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(425,NULL,NULL,248,'order_promotion','New Year',-55,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(426,NULL,NULL,248,'order_promotion','Christmas',-458,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(427,NULL,NULL,249,'order_promotion','New Year',-55,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(428,NULL,NULL,249,'order_promotion','Christmas',-457,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(429,NULL,NULL,250,'order_promotion','New Year',-55,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(430,NULL,NULL,250,'order_promotion','Christmas',-457,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(431,NULL,NULL,251,'order_promotion','New Year',-54,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(432,NULL,NULL,251,'order_promotion','Christmas',-457,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(433,NULL,NULL,252,'order_promotion','New Year',-54,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(434,NULL,NULL,252,'order_promotion','Christmas',-457,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(435,NULL,NULL,253,'order_promotion','New Year',-114,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(436,NULL,NULL,253,'order_promotion','Christmas',-954,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(437,NULL,NULL,254,'order_promotion','New Year',-114,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(438,NULL,NULL,254,'order_promotion','Christmas',-954,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(439,NULL,NULL,255,'order_promotion','New Year',-40,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(440,NULL,NULL,255,'order_promotion','Christmas',-330,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(441,NULL,NULL,256,'order_promotion','New Year',-39,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(442,NULL,NULL,256,'order_promotion','Christmas',-330,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(443,NULL,NULL,257,'order_promotion','New Year',-39,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(444,NULL,NULL,257,'order_promotion','Christmas',-329,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(445,NULL,NULL,258,'order_promotion','New Year',-39,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(446,NULL,NULL,258,'order_promotion','Christmas',-329,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(447,NULL,NULL,259,'order_promotion','New Year',-86,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(448,NULL,NULL,259,'order_promotion','Christmas',-716,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(449,NULL,NULL,260,'order_promotion','New Year',-86,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(450,NULL,NULL,260,'order_promotion','Christmas',-716,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(451,NULL,NULL,261,'order_promotion','New Year',-85,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(452,NULL,NULL,261,'order_promotion','Christmas',-716,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(453,NULL,NULL,262,'order_promotion','New Year',-85,0,0,'new_year','2022-03-08 11:16:40','2022-03-08 11:16:40'),(454,NULL,NULL,262,'order_promotion','Christmas',-716,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(455,31,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(456,NULL,NULL,263,'order_promotion','Christmas',-522,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(457,NULL,NULL,264,'order_promotion','Christmas',-522,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(458,NULL,NULL,265,'order_promotion','Christmas',-522,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(459,NULL,NULL,266,'order_promotion','Christmas',-522,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(460,NULL,NULL,267,'order_promotion','Christmas',-521,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(461,NULL,NULL,268,'order_promotion','Christmas',-982,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(462,NULL,NULL,269,'order_promotion','Christmas',-982,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(463,NULL,NULL,270,'order_promotion','Christmas',-981,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(464,NULL,NULL,271,'order_promotion','Christmas',-981,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(465,32,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(466,NULL,NULL,272,'order_promotion','Christmas',-199,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(467,NULL,NULL,273,'order_promotion','Christmas',-198,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(468,NULL,NULL,274,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(469,NULL,NULL,275,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(470,NULL,NULL,276,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(471,NULL,NULL,277,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(472,NULL,NULL,278,'order_promotion','Christmas',-740,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(473,NULL,NULL,279,'order_promotion','Christmas',-740,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(474,NULL,NULL,280,'order_promotion','Christmas',-740,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(475,NULL,NULL,281,'order_promotion','Christmas',-739,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(476,NULL,NULL,282,'order_promotion','Christmas',-739,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(477,NULL,NULL,283,'order_promotion','Christmas',-932,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(478,NULL,NULL,284,'order_promotion','Christmas',-932,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(479,NULL,NULL,285,'order_promotion','Christmas',-931,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(480,33,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(481,NULL,NULL,286,'order_promotion','Christmas',-1001,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(482,NULL,NULL,287,'order_promotion','Christmas',-1001,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(483,NULL,NULL,288,'order_promotion','Christmas',-1000,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(484,NULL,NULL,289,'order_promotion','Christmas',-128,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(485,NULL,NULL,290,'order_promotion','Christmas',-127,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(486,NULL,NULL,291,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(487,NULL,NULL,292,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(488,NULL,NULL,293,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(489,NULL,NULL,294,'order_promotion','Christmas',-295,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(490,NULL,NULL,295,'order_promotion','Christmas',-219,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(491,34,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(492,NULL,NULL,296,'order_promotion','Christmas',-764,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(493,NULL,NULL,297,'order_promotion','Christmas',-764,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(494,NULL,NULL,298,'order_promotion','Christmas',-764,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(495,NULL,NULL,299,'order_promotion','Christmas',-576,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(496,NULL,NULL,300,'order_promotion','Christmas',-575,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(497,NULL,NULL,301,'order_promotion','Christmas',-575,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(498,NULL,NULL,302,'order_promotion','Christmas',-125,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(499,NULL,NULL,303,'order_promotion','Christmas',-124,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(500,35,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(501,NULL,NULL,304,'order_promotion','Christmas',-776,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(502,NULL,NULL,305,'order_promotion','Christmas',-776,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(503,NULL,NULL,306,'order_promotion','Christmas',-776,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(504,NULL,NULL,307,'order_promotion','Christmas',-776,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(505,NULL,NULL,308,'order_promotion','Christmas',-968,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(506,NULL,NULL,309,'order_promotion','Christmas',-968,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(507,NULL,NULL,310,'order_promotion','Christmas',-968,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(508,NULL,NULL,311,'order_promotion','Christmas',-968,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(509,NULL,NULL,312,'order_promotion','Christmas',-967,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(510,NULL,NULL,313,'order_promotion','Christmas',-219,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(511,NULL,NULL,314,'order_promotion','Christmas',-982,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(512,NULL,NULL,315,'order_promotion','Christmas',-982,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(513,NULL,NULL,316,'order_promotion','Christmas',-982,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(514,NULL,NULL,317,'order_promotion','Christmas',-981,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(515,NULL,NULL,318,'order_promotion','Christmas',-981,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(516,NULL,NULL,319,'order_promotion','Christmas',-187,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(517,36,NULL,NULL,'shipping','FedEx',246,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(518,NULL,NULL,320,'order_promotion','Christmas',-1047,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(519,NULL,NULL,321,'order_promotion','Christmas',-1046,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(520,NULL,NULL,322,'order_promotion','Christmas',-1046,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(521,NULL,NULL,323,'order_promotion','Christmas',-1046,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(522,NULL,NULL,324,'order_promotion','Christmas',-647,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(523,NULL,NULL,325,'order_promotion','Christmas',-647,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(524,NULL,NULL,326,'order_promotion','Christmas',-647,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(525,NULL,NULL,327,'order_promotion','Christmas',-646,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(526,NULL,NULL,328,'order_promotion','Christmas',-182,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(527,NULL,NULL,329,'order_promotion','Christmas',-182,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(528,NULL,NULL,330,'order_promotion','Christmas',-181,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(529,NULL,NULL,331,'order_promotion','Christmas',-758,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(530,NULL,NULL,332,'order_promotion','Christmas',-758,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(531,NULL,NULL,333,'order_promotion','Christmas',-758,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(532,NULL,NULL,334,'order_promotion','Christmas',-757,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(533,37,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(534,NULL,NULL,335,'order_promotion','Christmas',-706,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(535,NULL,NULL,336,'order_promotion','Christmas',-706,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(536,NULL,NULL,337,'order_promotion','Christmas',-734,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(537,NULL,NULL,338,'order_promotion','Christmas',-107,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(538,NULL,NULL,339,'order_promotion','Christmas',-107,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(539,NULL,NULL,340,'order_promotion','Christmas',-107,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(540,NULL,NULL,341,'order_promotion','Christmas',-107,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(541,NULL,NULL,342,'order_promotion','Christmas',-217,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(542,NULL,NULL,343,'order_promotion','Christmas',-217,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(543,NULL,NULL,344,'order_promotion','Christmas',-1167,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(544,NULL,NULL,345,'order_promotion','Christmas',-1166,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(545,NULL,NULL,346,'order_promotion','Christmas',-1166,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(546,NULL,NULL,347,'order_promotion','Christmas',-1166,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(547,38,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(548,NULL,NULL,348,'order_promotion','Christmas',-1151,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(549,NULL,NULL,349,'order_promotion','Christmas',-1151,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(550,NULL,NULL,350,'order_promotion','Christmas',-1151,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(551,NULL,NULL,351,'order_promotion','Christmas',-1102,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(552,NULL,NULL,352,'order_promotion','Christmas',-1101,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(553,NULL,NULL,353,'order_promotion','Christmas',-1101,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(554,NULL,NULL,354,'order_promotion','Christmas',-1101,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(555,NULL,NULL,355,'order_promotion','Christmas',-727,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(556,NULL,NULL,356,'order_promotion','Christmas',-726,0,0,'christmas','2022-03-08 11:16:40','2022-03-08 11:16:40'),(557,39,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(558,40,NULL,NULL,'shipping','UPS',108,0,0,NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40');
/*!40000 ALTER TABLE `sylius_adjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_admin_api_access_token`
--

DROP TABLE IF EXISTS `sylius_admin_api_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_admin_api_access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2AA4915D5F37A13B` (`token`),
  KEY `IDX_2AA4915D19EB6921` (`client_id`),
  KEY `IDX_2AA4915DA76ED395` (`user_id`),
  CONSTRAINT `FK_2AA4915D19EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  CONSTRAINT `FK_2AA4915DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_admin_api_access_token`
--

LOCK TABLES `sylius_admin_api_access_token` WRITE;
/*!40000 ALTER TABLE `sylius_admin_api_access_token` DISABLE KEYS */;
INSERT INTO `sylius_admin_api_access_token` VALUES (2,2,5,'SampleToken',NULL,NULL);
/*!40000 ALTER TABLE `sylius_admin_api_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_admin_api_auth_code`
--

DROP TABLE IF EXISTS `sylius_admin_api_auth_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_admin_api_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `redirect_uri` longtext COLLATE utf8mb3_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E366D8485F37A13B` (`token`),
  KEY `IDX_E366D84819EB6921` (`client_id`),
  KEY `IDX_E366D848A76ED395` (`user_id`),
  CONSTRAINT `FK_E366D84819EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  CONSTRAINT `FK_E366D848A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_admin_api_auth_code`
--

LOCK TABLES `sylius_admin_api_auth_code` WRITE;
/*!40000 ALTER TABLE `sylius_admin_api_auth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_admin_api_auth_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_admin_api_client`
--

DROP TABLE IF EXISTS `sylius_admin_api_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_admin_api_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `random_id` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `redirect_uris` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `secret` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `allowed_grant_types` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_admin_api_client`
--

LOCK TABLES `sylius_admin_api_client` WRITE;
/*!40000 ALTER TABLE `sylius_admin_api_client` DISABLE KEYS */;
INSERT INTO `sylius_admin_api_client` VALUES (2,'demo_client','a:0:{}','secret_demo_client','a:1:{i:0;s:8:\"password\";}');
/*!40000 ALTER TABLE `sylius_admin_api_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_admin_api_refresh_token`
--

DROP TABLE IF EXISTS `sylius_admin_api_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_admin_api_refresh_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9160E3FA5F37A13B` (`token`),
  KEY `IDX_9160E3FA19EB6921` (`client_id`),
  KEY `IDX_9160E3FAA76ED395` (`user_id`),
  CONSTRAINT `FK_9160E3FA19EB6921` FOREIGN KEY (`client_id`) REFERENCES `sylius_admin_api_client` (`id`),
  CONSTRAINT `FK_9160E3FAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_admin_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_admin_api_refresh_token`
--

LOCK TABLES `sylius_admin_api_refresh_token` WRITE;
/*!40000 ALTER TABLE `sylius_admin_api_refresh_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_admin_api_refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_admin_user`
--

DROP TABLE IF EXISTS `sylius_admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `username_canonical` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `email_verification_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `email` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale_code` varchar(12) COLLATE utf8mb3_unicode_ci NOT NULL,
  `encoder_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_admin_user`
--

LOCK TABLES `sylius_admin_user` WRITE;
/*!40000 ALTER TABLE `sylius_admin_user` DISABLE KEYS */;
INSERT INTO `sylius_admin_user` VALUES (4,'sylius','sylius',1,'slzrhlkxutcw0ko8wgw0sgc0g0wscck','$argon2i$v=19$m=65536,t=4,p=1$c1I3bkdMYlhUb3k2dUtFTQ$lpo+JyOjQQ6AueX3Zf7FgaHA2KoOn4f+P133BMepEhw',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:26:\"ROLE_ADMINISTRATION_ACCESS\";}','sylius@example.com','sylius@example.com','2022-03-08 11:16:34','2022-03-08 11:16:34','John','Doe','lt_LT','argon2i'),(5,'api','api',1,'ibveezr7bz4kosck4wogkwk8ogw08o8','$argon2i$v=19$m=65536,t=4,p=1$RHR3dDJRMG1uQXFXd24yRw$T9k/Sl2/8cLCKF2qL3X7FJ1ig5pdWUtVytbZ3o9Q6lI',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:2:{i:0;s:26:\"ROLE_ADMINISTRATION_ACCESS\";i:1;s:15:\"ROLE_API_ACCESS\";}','api@example.com','api@example.com','2022-03-08 11:16:35','2022-03-08 11:16:35','Luke','Brushwood','lt_LT','argon2i'),(6,'admin','admin',1,'1600cvein5lw0kck44cwwcscw8g0w8s','$argon2i$v=19$m=65536,t=4,p=1$dEs4OFpQTWIwWGkyL1JTLw$uR+eKXmxp1pj1/hhuo+kCHdR923jEw4mwaxEElBmCr0',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:26:\"ROLE_ADMINISTRATION_ACCESS\";}','admin@example.com','admin@example.com','2022-03-08 11:16:35','2022-03-08 11:16:35','Admin','Admin','en_US','argon2i');
/*!40000 ALTER TABLE `sylius_admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_avatar_image`
--

DROP TABLE IF EXISTS `sylius_avatar_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_avatar_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1068A3A97E3C61F9` (`owner_id`),
  CONSTRAINT `FK_1068A3A97E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_admin_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_avatar_image`
--

LOCK TABLES `sylius_avatar_image` WRITE;
/*!40000 ALTER TABLE `sylius_avatar_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_avatar_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel`
--

DROP TABLE IF EXISTS `sylius_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_locale_id` int(11) NOT NULL,
  `base_currency_id` int(11) NOT NULL,
  `default_tax_zone_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `theme_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `tax_calculation_strategy` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `skipping_shipping_step_allowed` tinyint(1) NOT NULL,
  `skipping_payment_step_allowed` tinyint(1) NOT NULL,
  `account_verification_required` tinyint(1) NOT NULL,
  `shop_billing_data_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_16C8119E77153098` (`code`),
  UNIQUE KEY `UNIQ_16C8119EB5282EDF` (`shop_billing_data_id`),
  KEY `IDX_16C8119E743BF776` (`default_locale_id`),
  KEY `IDX_16C8119E3101778E` (`base_currency_id`),
  KEY `IDX_16C8119EA978C17` (`default_tax_zone_id`),
  KEY `IDX_16C8119EE551C011` (`hostname`),
  CONSTRAINT `FK_16C8119E3101778E` FOREIGN KEY (`base_currency_id`) REFERENCES `sylius_currency` (`id`),
  CONSTRAINT `FK_16C8119E743BF776` FOREIGN KEY (`default_locale_id`) REFERENCES `sylius_locale` (`id`),
  CONSTRAINT `FK_16C8119EA978C17` FOREIGN KEY (`default_tax_zone_id`) REFERENCES `sylius_zone` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_16C8119EB5282EDF` FOREIGN KEY (`shop_billing_data_id`) REFERENCES `sylius_shop_billing_data` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel`
--

LOCK TABLES `sylius_channel` WRITE;
/*!40000 ALTER TABLE `sylius_channel` DISABLE KEYS */;
INSERT INTO `sylius_channel` VALUES (2,4,4,4,'FASHION_WEB','US Web Store','DarkSlateGray',NULL,1,'localhost','2022-03-08 11:16:29','2022-03-08 11:16:43','nfq/ADTheme','order_items_based',NULL,0,0,1,2);
/*!40000 ALTER TABLE `sylius_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel_currencies`
--

DROP TABLE IF EXISTS `sylius_channel_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel_currencies` (
  `channel_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`currency_id`),
  KEY `IDX_AE491F9372F5A1AA` (`channel_id`),
  KEY `IDX_AE491F9338248176` (`currency_id`),
  CONSTRAINT `FK_AE491F9338248176` FOREIGN KEY (`currency_id`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AE491F9372F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel_currencies`
--

LOCK TABLES `sylius_channel_currencies` WRITE;
/*!40000 ALTER TABLE `sylius_channel_currencies` DISABLE KEYS */;
INSERT INTO `sylius_channel_currencies` VALUES (2,4);
/*!40000 ALTER TABLE `sylius_channel_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel_image`
--

DROP TABLE IF EXISTS `sylius_channel_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9EBB38E97E3C61F9` (`owner_id`),
  CONSTRAINT `FK_9EBB38E97E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel_image`
--

LOCK TABLES `sylius_channel_image` WRITE;
/*!40000 ALTER TABLE `sylius_channel_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_channel_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel_locales`
--

DROP TABLE IF EXISTS `sylius_channel_locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel_locales` (
  `channel_id` int(11) NOT NULL,
  `locale_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`locale_id`),
  KEY `IDX_786B7A8472F5A1AA` (`channel_id`),
  KEY `IDX_786B7A84E559DFD1` (`locale_id`),
  CONSTRAINT `FK_786B7A8472F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_786B7A84E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `sylius_locale` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel_locales`
--

LOCK TABLES `sylius_channel_locales` WRITE;
/*!40000 ALTER TABLE `sylius_channel_locales` DISABLE KEYS */;
INSERT INTO `sylius_channel_locales` VALUES (2,4),(2,5),(2,6);
/*!40000 ALTER TABLE `sylius_channel_locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel_logo`
--

DROP TABLE IF EXISTS `sylius_channel_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel_logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BC1C31AC7E3C61F9` (`owner_id`),
  CONSTRAINT `FK_BC1C31AC7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel_logo`
--

LOCK TABLES `sylius_channel_logo` WRITE;
/*!40000 ALTER TABLE `sylius_channel_logo` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_channel_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_channel_pricing`
--

DROP TABLE IF EXISTS `sylius_channel_pricing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_channel_pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_variant_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `original_price` int(11) DEFAULT NULL,
  `channel_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_variant_channel_idx` (`product_variant_id`,`channel_code`),
  KEY `IDX_7801820CA80EF684` (`product_variant_id`),
  CONSTRAINT `FK_7801820CA80EF684` FOREIGN KEY (`product_variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_channel_pricing`
--

LOCK TABLES `sylius_channel_pricing` WRITE;
/*!40000 ALTER TABLE `sylius_channel_pricing` DISABLE KEYS */;
INSERT INTO `sylius_channel_pricing` VALUES (120,120,9359,NULL,'FASHION_WEB'),(121,121,4001,NULL,'FASHION_WEB'),(122,122,7916,NULL,'FASHION_WEB'),(123,123,5119,NULL,'FASHION_WEB'),(124,124,4792,NULL,'FASHION_WEB'),(125,125,1558,NULL,'FASHION_WEB'),(126,126,7727,NULL,'FASHION_WEB'),(127,127,1154,NULL,'FASHION_WEB'),(128,128,894,NULL,'FASHION_WEB'),(129,129,1045,NULL,'FASHION_WEB'),(130,130,1628,NULL,'FASHION_WEB'),(131,131,568,NULL,'FASHION_WEB'),(132,132,8508,NULL,'FASHION_WEB'),(133,133,7764,NULL,'FASHION_WEB'),(134,134,6163,NULL,'FASHION_WEB'),(135,135,7801,NULL,'FASHION_WEB'),(136,136,2426,NULL,'FASHION_WEB'),(137,137,9176,NULL,'FASHION_WEB'),(138,138,2458,NULL,'FASHION_WEB'),(139,139,2769,NULL,'FASHION_WEB'),(140,140,8084,NULL,'FASHION_WEB'),(141,141,469,NULL,'FASHION_WEB'),(142,142,1515,NULL,'FASHION_WEB'),(143,143,9203,NULL,'FASHION_WEB'),(144,144,8759,NULL,'FASHION_WEB'),(145,145,7822,NULL,'FASHION_WEB'),(146,146,1827,NULL,'FASHION_WEB'),(147,147,2785,NULL,'FASHION_WEB'),(148,148,3501,NULL,'FASHION_WEB'),(149,149,456,NULL,'FASHION_WEB'),(150,150,891,NULL,'FASHION_WEB'),(151,151,8478,NULL,'FASHION_WEB'),(152,152,1656,NULL,'FASHION_WEB'),(153,153,9719,NULL,'FASHION_WEB'),(154,154,6602,NULL,'FASHION_WEB'),(155,155,7849,NULL,'FASHION_WEB'),(156,156,7340,NULL,'FASHION_WEB'),(157,157,8128,NULL,'FASHION_WEB'),(158,158,2066,NULL,'FASHION_WEB'),(159,159,4169,NULL,'FASHION_WEB'),(160,160,1756,NULL,'FASHION_WEB'),(161,161,806,NULL,'FASHION_WEB'),(162,162,5909,NULL,'FASHION_WEB'),(163,163,4452,NULL,'FASHION_WEB'),(164,164,6367,NULL,'FASHION_WEB'),(165,165,5170,NULL,'FASHION_WEB'),(166,166,3047,NULL,'FASHION_WEB'),(167,167,8704,NULL,'FASHION_WEB'),(168,168,5259,NULL,'FASHION_WEB'),(169,169,3172,NULL,'FASHION_WEB'),(170,170,6693,NULL,'FASHION_WEB'),(171,171,7061,NULL,'FASHION_WEB'),(172,172,9797,NULL,'FASHION_WEB'),(173,173,8717,NULL,'FASHION_WEB'),(174,174,6196,NULL,'FASHION_WEB'),(175,175,6053,NULL,'FASHION_WEB'),(176,176,1383,NULL,'FASHION_WEB'),(177,177,9929,NULL,'FASHION_WEB'),(178,178,3729,NULL,'FASHION_WEB'),(179,179,5884,NULL,'FASHION_WEB'),(180,180,5677,NULL,'FASHION_WEB'),(181,181,6500,NULL,'FASHION_WEB'),(182,182,3716,NULL,'FASHION_WEB'),(183,183,5651,NULL,'FASHION_WEB'),(184,184,8020,NULL,'FASHION_WEB'),(185,185,8770,NULL,'FASHION_WEB'),(186,186,289,NULL,'FASHION_WEB'),(187,187,5996,NULL,'FASHION_WEB'),(188,188,5109,NULL,'FASHION_WEB'),(189,189,265,NULL,'FASHION_WEB'),(190,190,9358,NULL,'FASHION_WEB'),(191,191,7110,NULL,'FASHION_WEB'),(192,192,3848,NULL,'FASHION_WEB'),(193,193,8310,NULL,'FASHION_WEB'),(194,194,1160,NULL,'FASHION_WEB'),(195,195,5053,NULL,'FASHION_WEB'),(196,196,9166,NULL,'FASHION_WEB'),(197,197,6471,NULL,'FASHION_WEB'),(198,198,6315,NULL,'FASHION_WEB'),(199,199,4926,NULL,'FASHION_WEB'),(200,200,6131,NULL,'FASHION_WEB'),(201,201,8602,NULL,'FASHION_WEB'),(202,202,495,NULL,'FASHION_WEB'),(203,203,1878,NULL,'FASHION_WEB'),(204,204,1907,NULL,'FASHION_WEB'),(205,205,8544,NULL,'FASHION_WEB'),(206,206,7734,NULL,'FASHION_WEB'),(207,207,863,NULL,'FASHION_WEB'),(208,208,1037,NULL,'FASHION_WEB'),(209,209,1274,NULL,'FASHION_WEB'),(210,210,6867,NULL,'FASHION_WEB'),(211,211,3956,NULL,'FASHION_WEB'),(212,212,456,NULL,'FASHION_WEB'),(213,213,1042,NULL,'FASHION_WEB'),(214,214,6114,NULL,'FASHION_WEB'),(215,215,4349,NULL,'FASHION_WEB'),(216,216,8064,NULL,'FASHION_WEB'),(217,217,835,NULL,'FASHION_WEB'),(218,218,9589,NULL,'FASHION_WEB'),(219,219,8179,NULL,'FASHION_WEB'),(220,220,6468,NULL,'FASHION_WEB'),(221,221,3864,NULL,'FASHION_WEB'),(222,222,6469,NULL,'FASHION_WEB'),(223,223,4558,NULL,'FASHION_WEB'),(224,224,4844,NULL,'FASHION_WEB'),(225,225,5224,NULL,'FASHION_WEB'),(226,226,1810,NULL,'FASHION_WEB'),(227,227,1064,NULL,'FASHION_WEB'),(228,228,5767,NULL,'FASHION_WEB'),(229,229,8337,NULL,'FASHION_WEB'),(230,230,5390,NULL,'FASHION_WEB'),(231,231,1442,NULL,'FASHION_WEB'),(232,232,7665,NULL,'FASHION_WEB'),(233,233,5601,NULL,'FASHION_WEB'),(234,234,1197,NULL,'FASHION_WEB'),(235,235,9593,NULL,'FASHION_WEB'),(236,236,4795,NULL,'FASHION_WEB'),(237,237,6848,NULL,'FASHION_WEB'),(238,238,3055,NULL,'FASHION_WEB');
/*!40000 ALTER TABLE `sylius_channel_pricing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_country`
--

DROP TABLE IF EXISTS `sylius_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8mb3_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E74256BF77153098` (`code`),
  KEY `IDX_E74256BF77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_country`
--

LOCK TABLES `sylius_country` WRITE;
/*!40000 ALTER TABLE `sylius_country` DISABLE KEYS */;
INSERT INTO `sylius_country` VALUES (5,'LT',1),(6,'US',1),(7,'FR',1),(8,'RU',1);
/*!40000 ALTER TABLE `sylius_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_currency`
--

DROP TABLE IF EXISTS `sylius_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_96EDD3D077153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_currency`
--

LOCK TABLES `sylius_currency` WRITE;
/*!40000 ALTER TABLE `sylius_currency` DISABLE KEYS */;
INSERT INTO `sylius_currency` VALUES (3,'USD','2022-03-08 11:16:29','2022-03-08 11:16:29'),(4,'EUR','2022-03-08 11:16:29','2022-03-08 11:16:29');
/*!40000 ALTER TABLE `sylius_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_customer`
--

DROP TABLE IF EXISTS `sylius_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(11) DEFAULT NULL,
  `default_address_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'u',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `subscribed_to_newsletter` tinyint(1) NOT NULL,
  `company_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `vat_number` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7E82D5E6E7927C74` (`email`),
  UNIQUE KEY `UNIQ_7E82D5E6A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_7E82D5E6BD94FB16` (`default_address_id`),
  KEY `IDX_7E82D5E6D2919A68` (`customer_group_id`),
  CONSTRAINT `FK_7E82D5E6BD94FB16` FOREIGN KEY (`default_address_id`) REFERENCES `sylius_address` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_7E82D5E6D2919A68` FOREIGN KEY (`customer_group_id`) REFERENCES `sylius_customer_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_customer`
--

LOCK TABLES `sylius_customer` WRITE;
/*!40000 ALTER TABLE `sylius_customer` DISABLE KEYS */;
INSERT INTO `sylius_customer` VALUES (23,3,NULL,'shop@example.com','shop@example.com','John','Doe','1977-12-29 16:43:52','u','2022-03-08 11:16:29','2022-03-08 11:16:29','1-392-505-3824',0,NULL,NULL,NULL),(24,3,NULL,'user@example.com','user@example.com','John','Doe','1926-03-01 22:48:48','u','2022-03-08 11:16:29','2022-03-08 11:16:30','(690) 533-0992',0,NULL,NULL,NULL),(25,3,NULL,'orn.audie@hotmail.com','orn.audie@hotmail.com','Jaydon','Crooks','2008-05-20 09:31:42','u','2022-03-08 11:16:30','2022-03-08 11:16:30','(992) 803-6024 x346',0,NULL,NULL,NULL),(26,3,NULL,'hermann.leola@haley.biz','hermann.leola@haley.biz','Loyce','McGlynn','1936-09-04 21:27:56','u','2022-03-08 11:16:30','2022-03-08 11:16:30','353.382.9702 x58372',0,NULL,NULL,NULL),(27,3,NULL,'will.wilton@streich.net','will.wilton@streich.net','Craig','Olson','1932-11-26 22:20:32','u','2022-03-08 11:16:30','2022-03-08 11:16:30','+1-205-516-6628',0,NULL,NULL,NULL),(28,3,NULL,'joshuah70@yahoo.com','joshuah70@yahoo.com','Anika','O\'Hara','1995-08-29 16:24:30','u','2022-03-08 11:16:30','2022-03-08 11:16:31','+13054781284',0,NULL,NULL,NULL),(29,3,NULL,'reymundo66@weber.com','reymundo66@weber.com','Marlene','Marquardt','2014-10-06 07:28:37','u','2022-03-08 11:16:31','2022-03-08 11:16:31','625.619.9490',0,NULL,NULL,NULL),(30,4,NULL,'tobin.kiehn@yahoo.com','tobin.kiehn@yahoo.com','Junior','Mayert','1944-02-15 14:07:05','u','2022-03-08 11:16:31','2022-03-08 11:16:31','543.684.4280 x876',0,NULL,NULL,NULL),(31,4,NULL,'unitzsche@gmail.com','unitzsche@gmail.com','Zachary','Ziemann','1968-07-21 13:26:52','u','2022-03-08 11:16:31','2022-03-08 11:16:31','+1-408-619-2236',0,NULL,NULL,NULL),(32,3,NULL,'zita28@yahoo.com','zita28@yahoo.com','Adolfo','Dibbert','1982-04-10 18:46:20','u','2022-03-08 11:16:31','2022-03-08 11:16:32','(417) 725-0251 x5430',0,NULL,NULL,NULL),(33,3,NULL,'avolkman@christiansen.com','avolkman@christiansen.com','Claire','Kuhlman','1956-12-23 16:10:37','u','2022-03-08 11:16:32','2022-03-08 11:16:32','808-480-0751 x63258',0,NULL,NULL,NULL),(34,4,NULL,'watsica.darren@crooks.com','watsica.darren@crooks.com','Brenden','Durgan','1934-11-05 22:39:10','u','2022-03-08 11:16:32','2022-03-08 11:16:32','287-841-7528 x600',0,NULL,NULL,NULL),(35,3,NULL,'eldon62@kemmer.biz','eldon62@kemmer.biz','Lea','Nienow','1990-09-04 04:05:10','u','2022-03-08 11:16:32','2022-03-08 11:16:32','920-581-3119',0,NULL,NULL,NULL),(36,4,NULL,'laisha.sipes@hotmail.com','laisha.sipes@hotmail.com','Wyman','Kilback','2005-11-08 01:39:51','u','2022-03-08 11:16:32','2022-03-08 11:16:33','717.808.9142 x8345',0,NULL,NULL,NULL),(37,3,NULL,'amir38@parker.com','amir38@parker.com','Paolo','Hudson','1959-12-03 08:58:34','u','2022-03-08 11:16:33','2022-03-08 11:16:33','1-771-723-8575 x020',0,NULL,NULL,NULL),(38,3,NULL,'yesenia99@hudson.net','yesenia99@hudson.net','Yoshiko','Morar','1999-05-13 01:49:32','u','2022-03-08 11:16:33','2022-03-08 11:16:33','680.701.3882 x9492',0,NULL,NULL,NULL),(39,3,NULL,'collier.kathleen@murphy.com','collier.kathleen@murphy.com','Jewel','Boyer','1991-09-28 06:54:37','u','2022-03-08 11:16:33','2022-03-08 11:16:33','1-445-750-6321',0,NULL,NULL,NULL),(40,4,NULL,'beahan.vivianne@collins.net','beahan.vivianne@collins.net','Kennedy','Walter','1930-06-26 22:48:58','u','2022-03-08 11:16:33','2022-03-08 11:16:33','376.961.5612',0,NULL,NULL,NULL),(41,4,NULL,'andy87@hotmail.com','andy87@hotmail.com','Frida','Homenick','1962-11-10 14:05:04','u','2022-03-08 11:16:33','2022-03-08 11:16:34','(753) 833-5677 x98753',0,NULL,NULL,NULL),(42,3,NULL,'ward.nora@yahoo.com','ward.nora@yahoo.com','Declan','Bode','1972-09-02 04:40:00','u','2022-03-08 11:16:34','2022-03-08 11:16:34','983-874-2393 x925',0,NULL,NULL,NULL),(43,3,NULL,'fletcher35@gmail.com','fletcher35@gmail.com','Danny','Dach','1926-06-04 14:42:23','u','2022-03-08 11:16:34','2022-03-08 11:16:34','+1-731-621-3290',0,NULL,NULL,NULL),(44,4,NULL,'ykris@yahoo.com','ykris@yahoo.com','Mervin','Kautzer','1929-07-16 10:06:27','u','2022-03-08 11:16:34','2022-03-08 11:16:34','1-605-662-5156',0,NULL,NULL,NULL),(45,NULL,NULL,'tautvydas@kiro.tech','tautvydas@kiro.tech','Tautvydas','Vaitkus',NULL,'u','2022-03-08 11:51:48','2022-03-08 11:51:49','+396454654654',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sylius_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_customer_consent_agreement`
--

DROP TABLE IF EXISTS `sylius_customer_consent_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_customer_consent_agreement` (
  `customer_id` int(11) NOT NULL,
  `consent_agreement_id` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`,`consent_agreement_id`),
  UNIQUE KEY `UNIQ_C4714DF0937BDF` (`consent_agreement_id`),
  KEY `IDX_C4714DF09395C3F3` (`customer_id`),
  CONSTRAINT `FK_C4714DF0937BDF` FOREIGN KEY (`consent_agreement_id`) REFERENCES `consent_agreement` (`id`),
  CONSTRAINT `FK_C4714DF09395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_customer_consent_agreement`
--

LOCK TABLES `sylius_customer_consent_agreement` WRITE;
/*!40000 ALTER TABLE `sylius_customer_consent_agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_customer_consent_agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_customer_group`
--

DROP TABLE IF EXISTS `sylius_customer_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_customer_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7FCF9B0577153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_customer_group`
--

LOCK TABLES `sylius_customer_group` WRITE;
/*!40000 ALTER TABLE `sylius_customer_group` DISABLE KEYS */;
INSERT INTO `sylius_customer_group` VALUES (3,'retail','Retail'),(4,'wholesale','Wholesale');
/*!40000 ALTER TABLE `sylius_customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_exchange_rate`
--

DROP TABLE IF EXISTS `sylius_exchange_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_exchange_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_currency` int(11) NOT NULL,
  `target_currency` int(11) NOT NULL,
  `ratio` decimal(10,5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5F52B852A76BEEDB3FD5856` (`source_currency`,`target_currency`),
  KEY `IDX_5F52B852A76BEED` (`source_currency`),
  KEY `IDX_5F52B85B3FD5856` (`target_currency`),
  CONSTRAINT `FK_5F52B852A76BEED` FOREIGN KEY (`source_currency`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_5F52B85B3FD5856` FOREIGN KEY (`target_currency`) REFERENCES `sylius_currency` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_exchange_rate`
--

LOCK TABLES `sylius_exchange_rate` WRITE;
/*!40000 ALTER TABLE `sylius_exchange_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_exchange_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_gateway_config`
--

DROP TABLE IF EXISTS `sylius_gateway_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_gateway_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `factory_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `config` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_gateway_config`
--

LOCK TABLES `sylius_gateway_config` WRITE;
/*!40000 ALTER TABLE `sylius_gateway_config` DISABLE KEYS */;
INSERT INTO `sylius_gateway_config` VALUES (4,'Offline','offline','[]'),(5,'Offline','offline','[]'),(6,'Paysera','omnipay_paysera','{\"paymentMethod\":null,\"project\":\"default\"}');
/*!40000 ALTER TABLE `sylius_gateway_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_internal_brand`
--

DROP TABLE IF EXISTS `sylius_internal_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_internal_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  `tec_doc_brand_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `internal_brand_unique` (`name`,`tec_doc_brand_id`),
  KEY `IDX_CC32C80A44F5D008` (`brand_id`),
  CONSTRAINT `FK_CC32C80A44F5D008` FOREIGN KEY (`brand_id`) REFERENCES `loevgaard_brand` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_internal_brand`
--

LOCK TABLES `sylius_internal_brand` WRITE;
/*!40000 ALTER TABLE `sylius_internal_brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_internal_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_internal_category`
--

DROP TABLE IF EXISTS `sylius_internal_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_internal_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxon_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E29B85255E237E06` (`name`),
  KEY `IDX_E29B8525DE13F470` (`taxon_id`),
  CONSTRAINT `FK_E29B8525DE13F470` FOREIGN KEY (`taxon_id`) REFERENCES `sylius_taxon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_internal_category`
--

LOCK TABLES `sylius_internal_category` WRITE;
/*!40000 ALTER TABLE `sylius_internal_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_internal_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_billing_data`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_billing_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_billing_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `province_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `province_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_billing_data`
--

LOCK TABLES `sylius_invoicing_plugin_billing_data` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_billing_data` DISABLE KEYS */;
INSERT INTO `sylius_invoicing_plugin_billing_data` VALUES (21,'Amely','Walsh',NULL,'3206 Helen Flats','Jaylenbury','56484-7072','FR',NULL,NULL),(22,'Jeffry','Pagac',NULL,'4617 Volkman Pass','South Monserrat','08932','US',NULL,NULL),(23,'Lacy','Dickinson',NULL,'9030 Gisselle Street','Handhaven','49278','US',NULL,NULL),(24,'Lorenzo','Mraz',NULL,'514 Zboncak Expressway Suite 314','Dietrichton','77862-7243','RU',NULL,NULL),(25,'Sienna','Welch',NULL,'858 Dare Via Suite 814','Langoshview','64875','US',NULL,NULL),(26,'Giovanni','Cummerata',NULL,'8520 Runte Via','Lake Myrticebury','93360-8872','US',NULL,NULL),(27,'Hailie','DuBuque',NULL,'2461 Mueller Turnpike','Port Telly','93963','RU',NULL,NULL),(28,'Romaine','Zieme',NULL,'89505 Moore Motorway','North Delphine','80573-9966','LT',NULL,NULL),(29,'Nora','Hartmann',NULL,'881 Eleazar Hills Suite 501','Mrazchester','74876-3574','RU',NULL,NULL),(30,'Aliza','McKenzie',NULL,'5386 Daugherty Harbor','Dessieton','70264','US',NULL,NULL),(31,'Willis','Altenwerth',NULL,'370 Alex Harbors','West Erika','73787-0814','RU',NULL,NULL),(32,'Jaylin','Kulas',NULL,'9222 Leffler Tunnel','Rogahnburgh','13821-3907','RU',NULL,NULL),(33,'Sylvan','Cassin',NULL,'380 Prosacco Court','Fidelmouth','97984','RU',NULL,NULL),(34,'William','Lemke',NULL,'41364 Haag Stravenue','Mervinmouth','71439-5378','RU',NULL,NULL),(35,'Boris','Hamill',NULL,'3927 Waylon Parkway','Jordanefurt','80390','RU',NULL,NULL),(36,'Cielo','Hand',NULL,'7380 Kelli Extensions Suite 910','Wolfburgh','93568','US',NULL,NULL),(37,'Brycen','Crona',NULL,'6056 Wuckert Brook','West Aracelishire','41781-7438','FR',NULL,NULL),(38,'Samir','Sawayn',NULL,'20115 Legros Brooks Apt. 242','Lake Brown','31046-5288','US',NULL,NULL),(39,'Jovany','Krajcik',NULL,'814 Bauch Shoals','Herzogborough','65541','LT',NULL,NULL),(40,'Adolphus','Rau',NULL,'530 Erick Keys Suite 034','Rextown','62388-1078','LT',NULL,NULL);
/*!40000 ALTER TABLE `sylius_invoicing_plugin_billing_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_invoice`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_invoice` (
  `id` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `shop_billing_data_id` int(11) DEFAULT NULL,
  `billing_data_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `order_number` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `issued_at` datetime NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3AA279BFB5282EDF` (`shop_billing_data_id`),
  UNIQUE KEY `UNIQ_3AA279BF5CDB2AEB` (`billing_data_id`),
  KEY `IDX_3AA279BF72F5A1AA` (`channel_id`),
  KEY `IDX_3AA279BF551F0F81` (`order_number`),
  CONSTRAINT `FK_3AA279BF5CDB2AEB` FOREIGN KEY (`billing_data_id`) REFERENCES `sylius_invoicing_plugin_billing_data` (`id`),
  CONSTRAINT `FK_3AA279BF72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`),
  CONSTRAINT `FK_3AA279BFB5282EDF` FOREIGN KEY (`shop_billing_data_id`) REFERENCES `sylius_invoicing_plugin_shop_billing_data` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_invoice`
--

LOCK TABLES `sylius_invoicing_plugin_invoice` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_invoice` DISABLE KEYS */;
INSERT INTO `sylius_invoicing_plugin_invoice` VALUES ('0406ab44-b07a-407e-b11f-9fe3cd9911fd',30,30,2,'2022/03/000000010','000000010','2022-03-08 11:16:42','USD','lt_LT',49926),('27d0e75e-807d-40a7-a080-441e65c9989d',23,23,2,'2022/03/000000003','000000003','2022-03-08 11:16:41','USD','lt_LT',25074),('2a7a9388-04fb-4d51-a569-96464269cb51',32,32,2,'2022/03/000000012','000000012','2022-03-08 11:16:42','USD','lt_LT',48034),('30d5f764-28fb-4129-b744-e23ceb94daed',22,22,2,'2022/03/000000002','000000002','2022-03-08 11:16:41','USD','lt_LT',22888),('45f45bd0-ae97-4d79-ab71-11506703090d',38,38,2,'2022/03/000000018','000000018','2022-03-08 11:16:42','USD','lt_LT',56377),('5112acd1-6a66-40cb-8636-f14ff9da550b',33,33,2,'2022/03/000000013','000000013','2022-03-08 11:16:42','USD','lt_LT',59289),('56be5585-5cb8-4cf8-b257-d58c98576bcd',35,35,2,'2022/03/000000015','000000015','2022-03-08 11:16:42','USD','lt_LT',31401),('86f061b8-94e4-40f1-ab47-7a322758fba7',29,29,2,'2022/03/000000009','000000009','2022-03-08 11:16:42','USD','lt_LT',23083),('9027f017-24c7-43e9-ba16-9a98ae9e3498',36,36,2,'2022/03/000000016','000000016','2022-03-08 11:16:42','USD','lt_LT',97461),('9452c58b-4ace-46e9-8167-7be92a4cd960',40,40,2,'2022/03/000000020','000000020','2022-03-08 11:16:43','USD','lt_LT',999),('9960174e-cb19-4951-a4c0-d6063d886d96',26,26,2,'2022/03/000000006','000000006','2022-03-08 11:16:41','USD','lt_LT',56123),('a09f7bc5-0aac-46b9-8039-d8527744693d',34,34,2,'2022/03/000000014','000000014','2022-03-08 11:16:42','USD','lt_LT',34250),('a779adca-1c27-4cd9-999e-dae556098ce6',31,31,2,'2022/03/000000011','000000011','2022-03-08 11:16:42','USD','lt_LT',61532),('c22b8f60-d0ca-452e-9a53-24beeedb7273',27,27,2,'2022/03/000000007','000000007','2022-03-08 11:16:41','USD','lt_LT',8652),('cdda489e-39b0-415f-a6e9-56c89ac66017',37,37,2,'2022/03/000000017','000000017','2022-03-08 11:16:42','USD','lt_LT',75993),('cdfb2982-2a67-4346-8161-b74687542d55',28,28,2,'2022/03/000000008','000000008','2022-03-08 11:16:41','USD','lt_LT',42414),('d726d2db-d15c-4cbe-8570-60dd2042d12a',25,25,2,'2022/03/000000005','000000005','2022-03-08 11:16:41','USD','lt_LT',49531),('d84b098f-3a86-4b3a-ad07-11b43d1ac854',21,21,2,'2022/03/000000001','000000001','2022-03-08 11:16:39','USD','lt_LT',41428),('e16fd9ea-e51e-47e7-8091-b515820e6fb0',39,39,2,'2022/03/000000019','000000019','2022-03-08 11:16:42','USD','lt_LT',68386),('fcd3d49e-fdb7-491f-a6d8-8aa4442b3c08',24,24,2,'2022/03/000000004','000000004','2022-03-08 11:16:41','USD','lt_LT',25823);
/*!40000 ALTER TABLE `sylius_invoicing_plugin_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_line_item`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_line_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_line_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `tax_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `variant_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `variant_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C91408292989F1FD` (`invoice_id`),
  CONSTRAINT `FK_C91408292989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `sylius_invoicing_plugin_invoice` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_line_item`
--

LOCK TABLES `sylius_invoicing_plugin_line_item` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_line_item` DISABLE KEYS */;
INSERT INTO `sylius_invoicing_plugin_line_item` VALUES (79,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','Oversize white cotton T-Shirt',3,2785,8355,0,7200,'Oversize_white_cotton_T_Shirt-variant-2','L'),(80,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','666F boyfriend jeans with rips',1,6848,6848,0,5900,'666F_boyfriend_jeans_with_rips-variant-3','XL'),(81,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','Cashmere-blend violet beanie',3,9719,29157,0,25123,'Cashmere_blend_violet_beanie-variant-0',''),(82,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','990M regular fit jeans',2,456,912,0,786,'990M_regular_fit_jeans-variant-3','XL'),(83,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','Loose white designer T-Shirt',3,894,2682,0,2311,'Loose_white_designer_T_Shirt-variant-3','XL'),(84,'d84b098f-3a86-4b3a-ad07-11b43d1ac854','UPS',1,108,108,0,108,NULL,NULL),(85,'30d5f764-28fb-4129-b744-e23ceb94daed','111F patched jeans with fancy badges',4,1810,7240,0,6135,'111F_patched_jeans_with_fancy_badges-variant-2','L'),(86,'30d5f764-28fb-4129-b744-e23ceb94daed','Oversize white cotton T-Shirt',5,2785,13925,0,11798,'Oversize_white_cotton_T_Shirt-variant-2','L'),(87,'30d5f764-28fb-4129-b744-e23ceb94daed','330M slim fit jeans',3,1907,5721,0,4847,'330M_slim_fit_jeans-variant-0','S'),(88,'30d5f764-28fb-4129-b744-e23ceb94daed','UPS',1,108,108,0,108,NULL,NULL),(89,'27d0e75e-807d-40a7-a080-441e65c9989d','727F patched cropped jeans',1,4558,4558,0,4382,'727F_patched_cropped_jeans-variant-4','XXL'),(90,'27d0e75e-807d-40a7-a080-441e65c9989d','Beige strappy summer dress',1,4452,4452,0,4281,'Beige_strappy_summer_dress-variant-9','XL Petite'),(91,'27d0e75e-807d-40a7-a080-441e65c9989d','Knitted wool-blend green cap',2,8478,16956,0,16303,'Knitted_wool_blend_green_cap-variant-0',''),(92,'27d0e75e-807d-40a7-a080-441e65c9989d','UPS',1,108,108,0,108,NULL,NULL),(93,'fcd3d49e-fdb7-491f-a6d8-8aa4442b3c08','990M regular fit jeans',2,1274,2548,0,2169,'990M_regular_fit_jeans-variant-0','S'),(94,'fcd3d49e-fdb7-491f-a6d8-8aa4442b3c08','Ribbed copper slim fit Tee',3,1628,4884,0,4155,'Ribbed_copper_slim_fit_Tee-variant-0','S'),(95,'fcd3d49e-fdb7-491f-a6d8-8aa4442b3c08','727F patched cropped jeans',5,4558,22790,0,19391,'727F_patched_cropped_jeans-variant-4','XXL'),(96,'fcd3d49e-fdb7-491f-a6d8-8aa4442b3c08','UPS',1,108,108,0,108,NULL,NULL),(97,'d726d2db-d15c-4cbe-8570-60dd2042d12a','Cashmere-blend violet beanie',3,9719,29157,0,25210,'Cashmere_blend_violet_beanie-variant-0',''),(98,'d726d2db-d15c-4cbe-8570-60dd2042d12a','000F office grey jeans',5,5601,28005,0,24213,'000F_office_grey_jeans-variant-4','XXL'),(99,'d726d2db-d15c-4cbe-8570-60dd2042d12a','UPS',1,108,108,0,108,NULL,NULL),(100,'9960174e-cb19-4951-a4c0-d6063d886d96','727F patched cropped jeans',1,8179,8179,0,7086,'727F_patched_cropped_jeans-variant-0','S'),(101,'9960174e-cb19-4951-a4c0-d6063d886d96','Knitted burgundy winter cap',3,891,2673,0,2316,'Knitted_burgundy_winter_cap-variant-0',''),(102,'9960174e-cb19-4951-a4c0-d6063d886d96','Sport basic white T-Shirt',3,7801,23403,0,20275,'Sport_basic_white_T_Shirt-variant-0','S'),(103,'9960174e-cb19-4951-a4c0-d6063d886d96','111F patched jeans with fancy badges',5,5767,28835,0,24981,'111F_patched_jeans_with_fancy_badges-variant-4','XXL'),(104,'9960174e-cb19-4951-a4c0-d6063d886d96','Raglan grey & black Tee',3,469,1407,0,1219,'Raglan_grey_&_black_Tee-variant-1','M'),(105,'9960174e-cb19-4951-a4c0-d6063d886d96','FedEx',1,246,246,0,246,NULL,NULL),(106,'c22b8f60-d0ca-452e-9a53-24beeedb7273','330M slim fit jeans',1,8544,8544,0,8544,'330M_slim_fit_jeans-variant-1','M'),(107,'c22b8f60-d0ca-452e-9a53-24beeedb7273','UPS',1,108,108,0,108,NULL,NULL),(108,'cdfb2982-2a67-4346-8161-b74687542d55','Oversize white cotton T-Shirt',5,456,2280,0,1966,'Oversize_white_cotton_T_Shirt-variant-4','XXL'),(109,'cdfb2982-2a67-4346-8161-b74687542d55','Everyday white basic T-Shirt',5,9359,46795,0,40340,'Everyday_white_basic_T_Shirt-variant-0','S'),(110,'cdfb2982-2a67-4346-8161-b74687542d55','UPS',1,108,108,0,108,NULL,NULL),(111,'86f061b8-94e4-40f1-ab47-7a322758fba7','666F boyfriend jeans with rips',5,4795,23975,0,22975,'666F_boyfriend_jeans_with_rips-variant-2','L'),(112,'86f061b8-94e4-40f1-ab47-7a322758fba7','UPS',1,108,108,0,108,NULL,NULL),(113,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','007M black elegance jeans',2,8064,16128,0,13946,'007M_black_elegance_jeans-variant-2','L'),(114,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','727F patched cropped jeans',1,3864,3864,0,3341,'727F_patched_cropped_jeans-variant-2','L'),(115,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','Ribbed copper slim fit Tee',3,7764,23292,0,20142,'Ribbed_copper_slim_fit_Tee-variant-3','XL'),(116,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','Knitted white pompom cap',3,1656,4968,0,4296,'Knitted_white_pompom_cap-variant-0',''),(117,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','Everyday white basic T-Shirt',1,9359,9359,0,8093,'Everyday_white_basic_T_Shirt-variant-0','S'),(118,'0406ab44-b07a-407e-b11f-9fe3cd9911fd','UPS',1,108,108,0,108,NULL,NULL),(119,'a779adca-1c27-4cd9-999e-dae556098ce6','727F patched cropped jeans',5,3864,19320,0,16761,'727F_patched_cropped_jeans-variant-2','L'),(120,'a779adca-1c27-4cd9-999e-dae556098ce6','007M black elegance jeans',2,8064,16128,0,13992,'007M_black_elegance_jeans-variant-2','L'),(121,'a779adca-1c27-4cd9-999e-dae556098ce6','Oversize white cotton T-Shirt',4,2785,11140,0,9665,'Oversize_white_cotton_T_Shirt-variant-2','L'),(122,'a779adca-1c27-4cd9-999e-dae556098ce6','Off shoulder boho dress',4,6053,24212,0,21006,'Off_shoulder_boho_dress-variant-6','L Petite'),(123,'a779adca-1c27-4cd9-999e-dae556098ce6','UPS',1,108,108,0,108,NULL,NULL),(124,'2a7a9388-04fb-4d51-a569-96464269cb51','007M black elegance jeans',5,4349,21745,0,19136,'007M_black_elegance_jeans-variant-1','M'),(125,'2a7a9388-04fb-4d51-a569-96464269cb51','727F patched cropped jeans',4,8179,32716,0,28790,'727F_patched_cropped_jeans-variant-0','S'),(126,'2a7a9388-04fb-4d51-a569-96464269cb51','UPS',1,108,108,0,108,NULL,NULL),(127,'5112acd1-6a66-40cb-8636-f14ff9da550b','Knitted white pompom cap',2,1656,3312,0,2915,'Knitted_white_pompom_cap-variant-0',''),(128,'5112acd1-6a66-40cb-8636-f14ff9da550b','Sport basic white T-Shirt',4,2458,9832,0,8652,'Sport_basic_white_T_Shirt-variant-3','XL'),(129,'5112acd1-6a66-40cb-8636-f14ff9da550b','Ribbed copper slim fit Tee',5,6163,30815,0,27117,'Ribbed_copper_slim_fit_Tee-variant-4','XXL'),(130,'5112acd1-6a66-40cb-8636-f14ff9da550b','Ribbed copper slim fit Tee',3,7764,23292,0,20497,'Ribbed_copper_slim_fit_Tee-variant-3','XL'),(131,'5112acd1-6a66-40cb-8636-f14ff9da550b','UPS',1,108,108,0,108,NULL,NULL),(132,'a09f7bc5-0aac-46b9-8039-d8527744693d','000F office grey jeans',3,8337,25011,0,22009,'000F_office_grey_jeans-variant-0','S'),(133,'a09f7bc5-0aac-46b9-8039-d8527744693d','111F patched jeans with fancy badges',2,1064,2128,0,1873,'111F_patched_jeans_with_fancy_badges-variant-3','XL'),(134,'a09f7bc5-0aac-46b9-8039-d8527744693d','Sport basic white T-Shirt',4,2458,9832,0,8652,'Sport_basic_white_T_Shirt-variant-3','XL'),(135,'a09f7bc5-0aac-46b9-8039-d8527744693d','Oversize white cotton T-Shirt',1,1827,1827,0,1608,'Oversize_white_cotton_T_Shirt-variant-1','M'),(136,'a09f7bc5-0aac-46b9-8039-d8527744693d','UPS',1,108,108,0,108,NULL,NULL),(137,'56be5585-5cb8-4cf8-b257-d58c98576bcd','Beige strappy summer dress',3,6367,19101,0,16809,'Beige_strappy_summer_dress-variant-10','XL Regular'),(138,'56be5585-5cb8-4cf8-b257-d58c98576bcd','666F boyfriend jeans with rips',3,4795,14385,0,12659,'666F_boyfriend_jeans_with_rips-variant-2','L'),(139,'56be5585-5cb8-4cf8-b257-d58c98576bcd','330M slim fit jeans',2,1037,2074,0,1825,'330M_slim_fit_jeans-variant-4','XXL'),(140,'56be5585-5cb8-4cf8-b257-d58c98576bcd','UPS',1,108,108,0,108,NULL,NULL),(141,'9027f017-24c7-43e9-ba16-9a98ae9e3498','727F patched cropped jeans',4,6468,25872,0,22768,'727F_patched_cropped_jeans-variant-1','M'),(142,'9027f017-24c7-43e9-ba16-9a98ae9e3498','007M black elegance jeans',5,8064,40320,0,35481,'007M_black_elegance_jeans-variant-2','L'),(143,'9027f017-24c7-43e9-ba16-9a98ae9e3498','Oversize white cotton T-Shirt',1,1827,1827,0,1608,'Oversize_white_cotton_T_Shirt-variant-1','M'),(144,'9027f017-24c7-43e9-ba16-9a98ae9e3498','727F patched cropped jeans',5,8179,40895,0,35987,'727F_patched_cropped_jeans-variant-0','S'),(145,'9027f017-24c7-43e9-ba16-9a98ae9e3498','Loose white designer T-Shirt',1,1558,1558,0,1371,'Loose_white_designer_T_Shirt-variant-0','S'),(146,'9027f017-24c7-43e9-ba16-9a98ae9e3498','FedEx',1,246,246,0,246,NULL,NULL),(147,'cdda489e-39b0-415f-a6e9-56c89ac66017','Off shoulder boho dress',4,8717,34868,0,30683,'Off_shoulder_boho_dress-variant-4','M Regular'),(148,'cdda489e-39b0-415f-a6e9-56c89ac66017','000F office grey jeans',4,5390,21560,0,18973,'000F_office_grey_jeans-variant-1','M'),(149,'cdda489e-39b0-415f-a6e9-56c89ac66017','Raglan grey & black Tee',3,1515,4545,0,4000,'Raglan_grey_&_black_Tee-variant-2','L'),(150,'cdda489e-39b0-415f-a6e9-56c89ac66017','Ruffle wrap festival dress',4,6315,25260,0,22229,'Ruffle_wrap_festival_dress-variant-14','XXL Tall'),(151,'cdda489e-39b0-415f-a6e9-56c89ac66017','UPS',1,108,108,0,108,NULL,NULL),(152,'45f45bd0-ae97-4d79-ab71-11506703090d','Off shoulder boho dress',2,5884,11768,0,10356,'Off_shoulder_boho_dress-variant-10','XL Regular'),(153,'45f45bd0-ae97-4d79-ab71-11506703090d','007M black elegance jeans',1,6114,6114,0,5380,'007M_black_elegance_jeans-variant-0','S'),(154,'45f45bd0-ae97-4d79-ab71-11506703090d','Knitted burgundy winter cap',4,891,3564,0,3136,'Knitted_burgundy_winter_cap-variant-0',''),(155,'45f45bd0-ae97-4d79-ab71-11506703090d','111F patched jeans with fancy badges',2,1810,3620,0,3186,'111F_patched_jeans_with_fancy_badges-variant-2','L'),(156,'45f45bd0-ae97-4d79-ab71-11506703090d','Cashmere-blend violet beanie',4,9719,38876,0,34211,'Cashmere_blend_violet_beanie-variant-0',''),(157,'45f45bd0-ae97-4d79-ab71-11506703090d','UPS',1,108,108,0,108,NULL,NULL),(158,'e16fd9ea-e51e-47e7-8091-b515820e6fb0','666F boyfriend jeans with rips',3,9593,28779,0,25326,'666F_boyfriend_jeans_with_rips-variant-1','M'),(159,'e16fd9ea-e51e-47e7-8091-b515820e6fb0','Sport basic white T-Shirt',4,9176,36704,0,32299,'Sport_basic_white_T_Shirt-variant-2','L'),(160,'e16fd9ea-e51e-47e7-8091-b515820e6fb0','Off shoulder boho dress',2,6053,12106,0,10653,'Off_shoulder_boho_dress-variant-6','L Petite'),(161,'e16fd9ea-e51e-47e7-8091-b515820e6fb0','UPS',1,108,108,0,108,NULL,NULL),(162,'9452c58b-4ace-46e9-8167-7be92a4cd960','Knitted burgundy winter cap',1,891,891,0,891,'Knitted_burgundy_winter_cap-variant-0',''),(163,'9452c58b-4ace-46e9-8167-7be92a4cd960','UPS',1,108,108,0,108,NULL,NULL);
/*!40000 ALTER TABLE `sylius_invoicing_plugin_line_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_sequence`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_sequence`
--

LOCK TABLES `sylius_invoicing_plugin_sequence` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_sequence` DISABLE KEYS */;
INSERT INTO `sylius_invoicing_plugin_sequence` VALUES (2,20,20);
/*!40000 ALTER TABLE `sylius_invoicing_plugin_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_shop_billing_data`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_shop_billing_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_shop_billing_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `tax_id` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `representative` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_shop_billing_data`
--

LOCK TABLES `sylius_invoicing_plugin_shop_billing_data` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_shop_billing_data` DISABLE KEYS */;
INSERT INTO `sylius_invoicing_plugin_shop_billing_data` VALUES (21,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(22,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(23,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(24,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(25,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(26,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(27,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(28,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(29,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(30,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(31,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(32,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(33,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(34,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(35,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(36,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(37,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(38,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(39,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL),(40,'Sylius','0001112222','Test St. 15','eCommerce Town','00 33 22','US',NULL);
/*!40000 ALTER TABLE `sylius_invoicing_plugin_shop_billing_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_invoicing_plugin_tax_item`
--

DROP TABLE IF EXISTS `sylius_invoicing_plugin_tax_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_invoicing_plugin_tax_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2951C61C2989F1FD` (`invoice_id`),
  CONSTRAINT `FK_2951C61C2989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `sylius_invoicing_plugin_invoice` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_invoicing_plugin_tax_item`
--

LOCK TABLES `sylius_invoicing_plugin_tax_item` WRITE;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_tax_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_invoicing_plugin_tax_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_locale`
--

DROP TABLE IF EXISTS `sylius_locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_locale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(12) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7BA1286477153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_locale`
--

LOCK TABLES `sylius_locale` WRITE;
/*!40000 ALTER TABLE `sylius_locale` DISABLE KEYS */;
INSERT INTO `sylius_locale` VALUES (4,'lt_LT','2022-03-08 11:16:29','2022-03-08 11:16:29'),(5,'en_US','2022-03-08 11:16:29','2022-03-08 11:16:29'),(6,'ru_RU','2022-03-08 11:16:29','2022-03-08 11:16:29');
/*!40000 ALTER TABLE `sylius_locale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_oenumber`
--

DROP TABLE IF EXISTS `sylius_oenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_oenumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `code_number` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `brand_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6D8EEB244584665A` (`product_id`),
  CONSTRAINT `FK_6D8EEB244584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_oenumber`
--

LOCK TABLES `sylius_oenumber` WRITE;
/*!40000 ALTER TABLE `sylius_oenumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_oenumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_order`
--

DROP TABLE IF EXISTS `sylius_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_address_id` int(11) DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `promotion_coupon_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `checkout_completed_at` datetime DEFAULT NULL,
  `items_total` int(11) NOT NULL,
  `adjustments_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `currency_code` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `checkout_state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `payment_state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `shipping_state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `token_value` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `customer_ip` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6196A1F996901F54` (`number`),
  UNIQUE KEY `UNIQ_6196A1F94D4CFF2B` (`shipping_address_id`),
  UNIQUE KEY `UNIQ_6196A1F979D0C0E4` (`billing_address_id`),
  KEY `IDX_6196A1F972F5A1AA` (`channel_id`),
  KEY `IDX_6196A1F917B24436` (`promotion_coupon_id`),
  KEY `IDX_6196A1F99395C3F3` (`customer_id`),
  KEY `IDX_6196A1F9A393D2FB43625D9F` (`state`,`updated_at`),
  CONSTRAINT `FK_6196A1F917B24436` FOREIGN KEY (`promotion_coupon_id`) REFERENCES `sylius_promotion_coupon` (`id`),
  CONSTRAINT `FK_6196A1F94D4CFF2B` FOREIGN KEY (`shipping_address_id`) REFERENCES `sylius_address` (`id`),
  CONSTRAINT `FK_6196A1F972F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`),
  CONSTRAINT `FK_6196A1F979D0C0E4` FOREIGN KEY (`billing_address_id`) REFERENCES `sylius_address` (`id`),
  CONSTRAINT `FK_6196A1F99395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_order`
--

LOCK TABLES `sylius_order` WRITE;
/*!40000 ALTER TABLE `sylius_order` DISABLE KEYS */;
INSERT INTO `sylius_order` VALUES (21,72,73,2,NULL,37,'000000001',NULL,'new','2021-03-29 13:27:27',41320,108,41428,'2022-03-08 11:16:38','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','_E_bsMpb8O',NULL),(22,75,76,2,NULL,32,'000000002',NULL,'new','2021-04-14 01:00:30',22780,108,22888,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','-VROCmX_FK',NULL),(23,78,79,2,NULL,25,'000000003',NULL,'new','2021-04-21 08:03:14',24966,108,25074,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','bZ07iKka4O',NULL),(24,81,82,2,NULL,30,'000000004',NULL,'new','2021-05-04 10:43:31',25715,108,25823,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','-2dZUmAUbW',NULL),(25,84,85,2,NULL,43,'000000005','Distinctio officiis dolore ut consequatur eum molestiae et.','new','2021-06-20 07:57:18',49423,108,49531,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','wTrUdfhKK6',NULL),(26,87,88,2,NULL,27,'000000006',NULL,'new','2021-07-06 12:04:12',55877,246,56123,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','arrGfeRC3e',NULL),(27,90,91,2,NULL,41,'000000007',NULL,'new','2021-07-06 14:00:36',8544,108,8652,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','wD_~w~MPZ1',NULL),(28,93,94,2,NULL,34,'000000008','Quam velit quaerat voluptatibus est.','new','2021-08-21 20:39:05',42306,108,42414,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','FgsZNKuz_q',NULL),(29,96,97,2,NULL,26,'000000009',NULL,'new','2021-08-31 21:20:50',22975,108,23083,'2022-03-08 11:16:39','2022-03-08 11:16:39','USD','lt_LT','completed','awaiting_payment','ready','D~31ayy~Xt',NULL),(30,99,100,2,NULL,34,'000000010',NULL,'new','2021-09-23 11:04:24',49818,108,49926,'2022-03-08 11:16:39','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','8RWyO3jpc3',NULL),(31,102,103,2,NULL,24,'000000011',NULL,'new','2021-10-29 20:23:57',61424,108,61532,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','HhVxeqehrI',NULL),(32,105,106,2,NULL,35,'000000012','Odit at saepe dicta quam est quia doloremque nostrum.','new','2021-11-22 17:09:01',47926,108,48034,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','zTPiZG9DZ3',NULL),(33,108,109,2,NULL,35,'000000013',NULL,'new','2021-11-23 20:22:29',59181,108,59289,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','mZlVQLjUUg',NULL),(34,111,112,2,NULL,44,'000000014',NULL,'new','2021-12-30 13:58:54',34142,108,34250,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','ErTPsZcenb',NULL),(35,114,115,2,NULL,44,'000000015',NULL,'new','2022-01-04 08:59:21',31293,108,31401,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','~V2tF7Zd27',NULL),(36,117,118,2,NULL,41,'000000016',NULL,'new','2022-01-04 12:00:31',97215,246,97461,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','E1cZhPVD4M',NULL),(37,120,121,2,NULL,34,'000000017',NULL,'new','2022-02-07 14:56:39',75885,108,75993,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','PjRkOjmJAj',NULL),(38,123,124,2,NULL,38,'000000018',NULL,'new','2022-02-08 17:19:04',56269,108,56377,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','DSIdPj75It',NULL),(39,126,127,2,NULL,40,'000000019',NULL,'new','2022-02-18 00:03:25',68278,108,68386,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','rWLcDtEqav',NULL),(40,129,130,2,NULL,41,'000000020',NULL,'new','2022-02-28 16:56:17',891,108,999,'2022-03-08 11:16:40','2022-03-08 11:16:40','USD','lt_LT','completed','awaiting_payment','ready','gXCSJuZT9i',NULL);
/*!40000 ALTER TABLE `sylius_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_order_item`
--

DROP TABLE IF EXISTS `sylius_order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `units_total` int(11) NOT NULL,
  `adjustments_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `is_immutable` tinyint(1) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `variant_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_77B587ED8D9F6D38` (`order_id`),
  KEY `IDX_77B587ED3B69A9AF` (`variant_id`),
  CONSTRAINT `FK_77B587ED3B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`),
  CONSTRAINT `FK_77B587ED8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_order_item`
--

LOCK TABLES `sylius_order_item` WRITE;
/*!40000 ALTER TABLE `sylius_order_item` DISABLE KEYS */;
INSERT INTO `sylius_order_item` VALUES (59,21,147,3,2785,7200,0,7200,0,'Oversize white cotton T-Shirt','L'),(60,21,237,1,6848,5900,0,5900,0,'666F boyfriend jeans with rips','XL'),(61,21,153,3,9719,25123,0,25123,0,'Cashmere-blend violet beanie',''),(62,21,212,2,456,786,0,786,0,'990M regular fit jeans','XL'),(63,21,128,3,894,2311,0,2311,0,'Loose white designer T-Shirt','XL'),(64,22,226,4,1810,6135,0,6135,0,'111F patched jeans with fancy badges','L'),(65,22,147,5,2785,11798,0,11798,0,'Oversize white cotton T-Shirt','L'),(66,22,204,3,1907,4847,0,4847,0,'330M slim fit jeans','S'),(67,23,223,1,4558,4382,0,4382,0,'727F patched cropped jeans','XXL'),(68,23,163,1,4452,4281,0,4281,0,'Beige strappy summer dress','XL Petite'),(69,23,151,2,8478,16303,0,16303,0,'Knitted wool-blend green cap',''),(70,24,209,2,1274,2169,0,2169,0,'990M regular fit jeans','S'),(71,24,130,3,1628,4155,0,4155,0,'Ribbed copper slim fit Tee','S'),(72,24,223,5,4558,19391,0,19391,0,'727F patched cropped jeans','XXL'),(73,25,153,3,9719,25210,0,25210,0,'Cashmere-blend violet beanie',''),(74,25,233,5,5601,24213,0,24213,0,'000F office grey jeans','XXL'),(75,26,219,1,8179,7086,0,7086,0,'727F patched cropped jeans','S'),(76,26,150,3,891,2316,0,2316,0,'Knitted burgundy winter cap',''),(77,26,135,3,7801,20275,0,20275,0,'Sport basic white T-Shirt','S'),(78,26,228,5,5767,24981,0,24981,0,'111F patched jeans with fancy badges','XXL'),(79,26,141,3,469,1219,0,1219,0,'Raglan grey & black Tee','M'),(80,27,205,1,8544,8544,0,8544,0,'330M slim fit jeans','M'),(81,28,149,5,456,1966,0,1966,0,'Oversize white cotton T-Shirt','XXL'),(82,28,120,5,9359,40340,0,40340,0,'Everyday white basic T-Shirt','S'),(83,29,236,5,4795,22975,0,22975,0,'666F boyfriend jeans with rips','L'),(84,30,216,2,8064,13946,0,13946,0,'007M black elegance jeans','L'),(85,30,221,1,3864,3341,0,3341,0,'727F patched cropped jeans','L'),(86,30,133,3,7764,20142,0,20142,0,'Ribbed copper slim fit Tee','XL'),(87,30,152,3,1656,4296,0,4296,0,'Knitted white pompom cap',''),(88,30,120,1,9359,8093,0,8093,0,'Everyday white basic T-Shirt','S'),(89,31,221,5,3864,16761,0,16761,0,'727F patched cropped jeans','L'),(90,31,216,2,8064,13992,0,13992,0,'007M black elegance jeans','L'),(91,31,147,4,2785,9665,0,9665,0,'Oversize white cotton T-Shirt','L'),(92,31,175,4,6053,21006,0,21006,0,'Off shoulder boho dress','L Petite'),(93,32,215,5,4349,19136,0,19136,0,'007M black elegance jeans','M'),(94,32,219,4,8179,28790,0,28790,0,'727F patched cropped jeans','S'),(95,33,152,2,1656,2915,0,2915,0,'Knitted white pompom cap',''),(96,33,138,4,2458,8652,0,8652,0,'Sport basic white T-Shirt','XL'),(97,33,134,5,6163,27117,0,27117,0,'Ribbed copper slim fit Tee','XXL'),(98,33,133,3,7764,20497,0,20497,0,'Ribbed copper slim fit Tee','XL'),(99,34,229,3,8337,22009,0,22009,0,'000F office grey jeans','S'),(100,34,227,2,1064,1873,0,1873,0,'111F patched jeans with fancy badges','XL'),(101,34,138,4,2458,8652,0,8652,0,'Sport basic white T-Shirt','XL'),(102,34,146,1,1827,1608,0,1608,0,'Oversize white cotton T-Shirt','M'),(103,35,164,3,6367,16809,0,16809,0,'Beige strappy summer dress','XL Regular'),(104,35,236,3,4795,12659,0,12659,0,'666F boyfriend jeans with rips','L'),(105,35,208,2,1037,1825,0,1825,0,'330M slim fit jeans','XXL'),(106,36,220,4,6468,22768,0,22768,0,'727F patched cropped jeans','M'),(107,36,216,5,8064,35481,0,35481,0,'007M black elegance jeans','L'),(108,36,146,1,1827,1608,0,1608,0,'Oversize white cotton T-Shirt','M'),(109,36,219,5,8179,35987,0,35987,0,'727F patched cropped jeans','S'),(110,36,125,1,1558,1371,0,1371,0,'Loose white designer T-Shirt','S'),(111,37,173,4,8717,30683,0,30683,0,'Off shoulder boho dress','M Regular'),(112,37,230,4,5390,18973,0,18973,0,'000F office grey jeans','M'),(113,37,142,3,1515,4000,0,4000,0,'Raglan grey & black Tee','L'),(114,37,198,4,6315,22229,0,22229,0,'Ruffle wrap festival dress','XXL Tall'),(115,38,179,2,5884,10356,0,10356,0,'Off shoulder boho dress','XL Regular'),(116,38,214,1,6114,5380,0,5380,0,'007M black elegance jeans','S'),(117,38,150,4,891,3136,0,3136,0,'Knitted burgundy winter cap',''),(118,38,226,2,1810,3186,0,3186,0,'111F patched jeans with fancy badges','L'),(119,38,153,4,9719,34211,0,34211,0,'Cashmere-blend violet beanie',''),(120,39,235,3,9593,25326,0,25326,0,'666F boyfriend jeans with rips','M'),(121,39,137,4,9176,32299,0,32299,0,'Sport basic white T-Shirt','L'),(122,39,175,2,6053,10653,0,10653,0,'Off shoulder boho dress','L Petite'),(123,40,150,1,891,891,0,891,0,'Knitted burgundy winter cap','');
/*!40000 ALTER TABLE `sylius_order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_order_item_unit`
--

DROP TABLE IF EXISTS `sylius_order_item_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_order_item_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `adjustments_total` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_82BF226EE415FB15` (`order_item_id`),
  KEY `IDX_82BF226E7BE036FC` (`shipment_id`),
  CONSTRAINT `FK_82BF226E7BE036FC` FOREIGN KEY (`shipment_id`) REFERENCES `sylius_shipment` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_82BF226EE415FB15` FOREIGN KEY (`order_item_id`) REFERENCES `sylius_order_item` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_order_item_unit`
--

LOCK TABLES `sylius_order_item_unit` WRITE;
/*!40000 ALTER TABLE `sylius_order_item_unit` DISABLE KEYS */;
INSERT INTO `sylius_order_item_unit` VALUES (161,59,21,-385,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(162,59,21,-385,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(163,59,21,-385,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(164,60,21,-948,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(165,61,21,-1345,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(166,61,21,-1345,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(167,61,21,-1344,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(168,62,21,-64,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(169,62,21,-62,'2022-03-08 11:16:38','2022-03-08 11:16:39'),(170,63,21,-124,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(171,63,21,-124,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(172,63,21,-123,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(173,64,22,-277,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(174,64,22,-276,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(175,64,22,-276,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(176,64,22,-276,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(177,65,22,-426,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(178,65,22,-426,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(179,65,22,-426,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(180,65,22,-425,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(181,65,22,-424,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(182,66,22,-292,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(183,66,22,-291,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(184,66,22,-291,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(185,67,23,-176,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(186,68,23,-171,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(187,69,23,-327,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(188,69,23,-326,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(189,70,24,-190,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(190,70,24,-189,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(191,71,24,-243,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(192,71,24,-243,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(193,71,24,-243,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(194,72,24,-680,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(195,72,24,-680,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(196,72,24,-680,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(197,72,24,-680,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(198,72,24,-679,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(199,73,25,-1316,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(200,73,25,-1316,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(201,73,25,-1315,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(202,74,25,-759,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(203,74,25,-759,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(204,74,25,-758,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(205,74,25,-758,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(206,74,25,-758,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(207,75,26,-1093,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(208,76,26,-120,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(209,76,26,-119,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(210,76,26,-118,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(211,77,26,-1043,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(212,77,26,-1043,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(213,77,26,-1042,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(214,78,26,-772,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(215,78,26,-772,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(216,78,26,-770,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(217,78,26,-770,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(218,78,26,-770,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(219,79,26,-64,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(220,79,26,-62,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(221,79,26,-62,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(222,80,27,0,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(223,81,28,-64,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(224,81,28,-63,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(225,81,28,-63,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(226,81,28,-62,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(227,81,28,-62,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(228,82,28,-1292,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(229,82,28,-1291,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(230,82,28,-1291,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(231,82,28,-1291,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(232,82,28,-1290,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(233,83,29,-200,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(234,83,29,-200,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(235,83,29,-200,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(236,83,29,-200,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(237,83,29,-200,'2022-03-08 11:16:39','2022-03-08 11:16:39'),(238,84,30,-1092,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(239,84,30,-1090,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(240,85,30,-523,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(241,86,30,-1051,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(242,86,30,-1050,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(243,86,30,-1049,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(244,87,30,-225,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(245,87,30,-224,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(246,87,30,-223,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(247,88,30,-1266,'2022-03-08 11:16:39','2022-03-08 11:16:40'),(248,89,31,-513,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(249,89,31,-512,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(250,89,31,-512,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(251,89,31,-511,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(252,89,31,-511,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(253,90,31,-1068,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(254,90,31,-1068,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(255,91,31,-370,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(256,91,31,-369,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(257,91,31,-368,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(258,91,31,-368,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(259,92,31,-802,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(260,92,31,-802,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(261,92,31,-801,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(262,92,31,-801,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(263,93,32,-522,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(264,93,32,-522,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(265,93,32,-522,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(266,93,32,-522,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(267,93,32,-521,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(268,94,32,-982,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(269,94,32,-982,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(270,94,32,-981,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(271,94,32,-981,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(272,95,33,-199,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(273,95,33,-198,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(274,96,33,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(275,96,33,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(276,96,33,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(277,96,33,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(278,97,33,-740,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(279,97,33,-740,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(280,97,33,-740,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(281,97,33,-739,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(282,97,33,-739,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(283,98,33,-932,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(284,98,33,-932,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(285,98,33,-931,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(286,99,34,-1001,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(287,99,34,-1001,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(288,99,34,-1000,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(289,100,34,-128,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(290,100,34,-127,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(291,101,34,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(292,101,34,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(293,101,34,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(294,101,34,-295,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(295,102,34,-219,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(296,103,35,-764,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(297,103,35,-764,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(298,103,35,-764,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(299,104,35,-576,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(300,104,35,-575,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(301,104,35,-575,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(302,105,35,-125,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(303,105,35,-124,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(304,106,36,-776,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(305,106,36,-776,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(306,106,36,-776,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(307,106,36,-776,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(308,107,36,-968,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(309,107,36,-968,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(310,107,36,-968,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(311,107,36,-968,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(312,107,36,-967,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(313,108,36,-219,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(314,109,36,-982,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(315,109,36,-982,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(316,109,36,-982,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(317,109,36,-981,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(318,109,36,-981,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(319,110,36,-187,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(320,111,37,-1047,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(321,111,37,-1046,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(322,111,37,-1046,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(323,111,37,-1046,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(324,112,37,-647,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(325,112,37,-647,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(326,112,37,-647,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(327,112,37,-646,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(328,113,37,-182,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(329,113,37,-182,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(330,113,37,-181,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(331,114,37,-758,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(332,114,37,-758,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(333,114,37,-758,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(334,114,37,-757,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(335,115,38,-706,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(336,115,38,-706,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(337,116,38,-734,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(338,117,38,-107,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(339,117,38,-107,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(340,117,38,-107,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(341,117,38,-107,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(342,118,38,-217,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(343,118,38,-217,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(344,119,38,-1167,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(345,119,38,-1166,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(346,119,38,-1166,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(347,119,38,-1166,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(348,120,39,-1151,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(349,120,39,-1151,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(350,120,39,-1151,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(351,121,39,-1102,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(352,121,39,-1101,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(353,121,39,-1101,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(354,121,39,-1101,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(355,122,39,-727,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(356,122,39,-726,'2022-03-08 11:16:40','2022-03-08 11:16:40'),(357,123,40,0,'2022-03-08 11:16:40','2022-03-08 11:16:40');
/*!40000 ALTER TABLE `sylius_order_item_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_order_sequence`
--

DROP TABLE IF EXISTS `sylius_order_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_order_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_order_sequence`
--

LOCK TABLES `sylius_order_sequence` WRITE;
/*!40000 ALTER TABLE `sylius_order_sequence` DISABLE KEYS */;
INSERT INTO `sylius_order_sequence` VALUES (2,20,2);
/*!40000 ALTER TABLE `sylius_order_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_payment`
--

DROP TABLE IF EXISTS `sylius_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb3_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D9191BD419883967` (`method_id`),
  KEY `IDX_D9191BD48D9F6D38` (`order_id`),
  CONSTRAINT `FK_D9191BD419883967` FOREIGN KEY (`method_id`) REFERENCES `sylius_payment_method` (`id`),
  CONSTRAINT `FK_D9191BD48D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_payment`
--

LOCK TABLES `sylius_payment` WRITE;
/*!40000 ALTER TABLE `sylius_payment` DISABLE KEYS */;
INSERT INTO `sylius_payment` VALUES (21,5,21,'USD',41428,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(22,4,22,'USD',22888,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(23,5,23,'USD',25074,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(24,6,24,'USD',25823,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(25,6,25,'USD',49531,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(26,4,26,'USD',56123,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(27,6,27,'USD',8652,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(28,4,28,'USD',42414,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(29,5,29,'USD',23083,'new','[]','2022-03-08 11:16:39','2022-03-08 11:16:39'),(30,5,30,'USD',49926,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(31,4,31,'USD',61532,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(32,6,32,'USD',48034,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(33,5,33,'USD',59289,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(34,4,34,'USD',34250,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(35,5,35,'USD',31401,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(36,4,36,'USD',97461,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(37,4,37,'USD',75993,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(38,6,38,'USD',56377,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(39,6,39,'USD',68386,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40'),(40,5,40,'USD',999,'new','[]','2022-03-08 11:16:40','2022-03-08 11:16:40');
/*!40000 ALTER TABLE `sylius_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_payment_method`
--

DROP TABLE IF EXISTS `sylius_payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_config_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `environment` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A75B0B0D77153098` (`code`),
  KEY `IDX_A75B0B0DF23D6140` (`gateway_config_id`),
  CONSTRAINT `FK_A75B0B0DF23D6140` FOREIGN KEY (`gateway_config_id`) REFERENCES `sylius_gateway_config` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_payment_method`
--

LOCK TABLES `sylius_payment_method` WRITE;
/*!40000 ALTER TABLE `sylius_payment_method` DISABLE KEYS */;
INSERT INTO `sylius_payment_method` VALUES (4,4,'cash_on_delivery',NULL,1,0,'2022-03-08 11:16:29','2022-03-08 11:16:29'),(5,5,'bank_transfer',NULL,1,1,'2022-03-08 11:16:29','2022-03-08 11:16:29'),(6,6,'paysera',NULL,1,2,'2022-03-08 11:16:29','2022-03-08 11:16:29');
/*!40000 ALTER TABLE `sylius_payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_payment_method_channels`
--

DROP TABLE IF EXISTS `sylius_payment_method_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_payment_method_channels` (
  `payment_method_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_method_id`,`channel_id`),
  KEY `IDX_543AC0CC5AA1164F` (`payment_method_id`),
  KEY `IDX_543AC0CC72F5A1AA` (`channel_id`),
  CONSTRAINT `FK_543AC0CC5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `sylius_payment_method` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_543AC0CC72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_payment_method_channels`
--

LOCK TABLES `sylius_payment_method_channels` WRITE;
/*!40000 ALTER TABLE `sylius_payment_method_channels` DISABLE KEYS */;
INSERT INTO `sylius_payment_method_channels` VALUES (4,2),(5,2),(6,2);
/*!40000 ALTER TABLE `sylius_payment_method_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_payment_method_translation`
--

DROP TABLE IF EXISTS `sylius_payment_method_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_payment_method_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `instructions` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_payment_method_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_966BE3A12C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_966BE3A12C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_payment_method` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_payment_method_translation`
--

LOCK TABLES `sylius_payment_method_translation` WRITE;
/*!40000 ALTER TABLE `sylius_payment_method_translation` DISABLE KEYS */;
INSERT INTO `sylius_payment_method_translation` VALUES (10,4,'Cash on delivery','Nihil alias autem facere alias quibusdam.',NULL,'lt_LT'),(11,4,'Cash on delivery','Nihil alias autem facere alias quibusdam.',NULL,'en_US'),(12,4,'Cash on delivery','Nihil alias autem facere alias quibusdam.',NULL,'ru_RU'),(13,5,'Bank transfer','Perferendis amet quia a et eveniet occaecati.',NULL,'lt_LT'),(14,5,'Bank transfer','Perferendis amet quia a et eveniet occaecati.',NULL,'en_US'),(15,5,'Bank transfer','Perferendis amet quia a et eveniet occaecati.',NULL,'ru_RU'),(16,6,'Paysera','Exercitationem ullam omnis officiis laborum eum minus.',NULL,'lt_LT'),(17,6,'Paysera','Exercitationem ullam omnis officiis laborum eum minus.',NULL,'en_US'),(18,6,'Paysera','Exercitationem ullam omnis officiis laborum eum minus.',NULL,'ru_RU');
/*!40000 ALTER TABLE `sylius_payment_method_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_payment_security_token`
--

DROP TABLE IF EXISTS `sylius_payment_security_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_payment_security_token` (
  `hash` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(DC2Type:object)',
  `after_url` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `target_url` longtext COLLATE utf8mb3_unicode_ci NOT NULL,
  `gateway_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_payment_security_token`
--

LOCK TABLES `sylius_payment_security_token` WRITE;
/*!40000 ALTER TABLE `sylius_payment_security_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_payment_security_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product`
--

DROP TABLE IF EXISTS `sylius_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_taxon_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `variant_selection_method` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `average_rating` double NOT NULL DEFAULT 0,
  `tag_index` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `brand_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `weekly` tinyint(1) NOT NULL DEFAULT 0,
  `most_popular` tinyint(1) NOT NULL DEFAULT 0,
  `internal_brand_id` int(11) DEFAULT NULL,
  `internal_category_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_677B9B7477153098` (`code`),
  KEY `IDX_677B9B74731E505` (`main_taxon_id`),
  KEY `IDX_677B9B7444F5D008` (`brand_id`),
  KEY `IDX_677B9B748B575DD0` (`internal_brand_id`),
  KEY `IDX_677B9B742729DD67` (`internal_category_id`),
  KEY `IDX_677B9B742ADD6D8C` (`supplier_id`),
  KEY `sylius_product_brand_code_index` (`brand_code`),
  FULLTEXT KEY `fulltext_search_idx` (`tag_index`),
  CONSTRAINT `FK_677B9B742729DD67` FOREIGN KEY (`internal_category_id`) REFERENCES `sylius_internal_category` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_677B9B742ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  CONSTRAINT `FK_677B9B7444F5D008` FOREIGN KEY (`brand_id`) REFERENCES `loevgaard_brand` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_677B9B74731E505` FOREIGN KEY (`main_taxon_id`) REFERENCES `sylius_taxon` (`id`),
  CONSTRAINT `FK_677B9B748B575DD0` FOREIGN KEY (`internal_brand_id`) REFERENCES `sylius_internal_brand` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product`
--

LOCK TABLES `sylius_product` WRITE;
/*!40000 ALTER TABLE `sylius_product` DISABLE KEYS */;
INSERT INTO `sylius_product` VALUES (22,15,'Everyday_white_basic_T_Shirt','2022-03-02 06:04:37','2022-03-08 11:16:35',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(23,15,'Loose_white_designer_T_Shirt','2022-03-07 20:06:14','2022-03-08 11:16:35',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(24,15,'Ribbed_copper_slim_fit_Tee','2022-03-05 07:49:28','2022-03-08 11:16:38',1,'match',4,'',NULL,NULL,0,0,NULL,NULL,NULL),(25,14,'Sport_basic_white_T_Shirt','2022-03-04 14:45:52','2022-03-08 11:16:37',1,'match',3,'',NULL,NULL,0,0,NULL,NULL,NULL),(26,14,'Raglan_grey_&_black_Tee','2022-03-05 09:53:04','2022-03-08 11:16:37',1,'match',1,'',NULL,NULL,0,0,NULL,NULL,NULL),(27,14,'Oversize_white_cotton_T_Shirt','2022-03-08 02:37:30','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(28,18,'Knitted_burgundy_winter_cap','2022-03-06 13:13:45','2022-03-08 11:16:38',1,'match',4,'',NULL,NULL,0,0,NULL,NULL,NULL),(29,17,'Knitted_wool_blend_green_cap','2022-03-01 13:35:56','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(30,18,'Knitted_white_pompom_cap','2022-03-02 22:37:38','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(31,17,'Cashmere_blend_violet_beanie','2022-03-06 23:03:26','2022-03-08 11:16:38',1,'match',2,'',NULL,NULL,0,0,NULL,NULL,NULL),(32,19,'Beige_strappy_summer_dress','2022-03-07 15:16:50','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(33,19,'Off_shoulder_boho_dress','2022-03-02 07:01:33','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(34,19,'Ruffle_wrap_festival_dress','2022-03-05 05:46:46','2022-03-08 11:16:36',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(35,21,'911M_regular_fit_jeans','2022-03-05 22:36:05','2022-03-08 11:16:37',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(36,21,'330M_slim_fit_jeans','2022-03-03 14:27:40','2022-03-08 11:16:37',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(37,21,'990M_regular_fit_jeans','2022-03-08 10:45:07','2022-03-08 11:16:37',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(38,21,'007M_black_elegance_jeans','2022-03-02 19:02:46','2022-03-08 11:16:37',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL),(39,22,'727F_patched_cropped_jeans','2022-03-06 11:35:48','2022-03-08 11:16:37',1,'match',4,'',NULL,NULL,0,0,NULL,NULL,NULL),(40,22,'111F_patched_jeans_with_fancy_badges','2022-03-03 15:18:55','2022-03-08 11:16:38',1,'match',5,'',NULL,NULL,0,0,NULL,NULL,NULL),(41,22,'000F_office_grey_jeans','2022-03-05 09:26:39','2022-03-08 11:16:37',1,'match',4,'',NULL,NULL,0,0,NULL,NULL,NULL),(42,22,'666F_boyfriend_jeans_with_rips','2022-03-03 14:18:57','2022-03-08 11:16:37',1,'match',0,'',NULL,NULL,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sylius_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_association`
--

DROP TABLE IF EXISTS `sylius_product_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `association_type_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_association_idx` (`product_id`,`association_type_id`),
  KEY `IDX_48E9CDABB1E1C39` (`association_type_id`),
  KEY `IDX_48E9CDAB4584665A` (`product_id`),
  CONSTRAINT `FK_48E9CDAB4584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_48E9CDABB1E1C39` FOREIGN KEY (`association_type_id`) REFERENCES `sylius_product_association_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_association`
--

LOCK TABLES `sylius_product_association` WRITE;
/*!40000 ALTER TABLE `sylius_product_association` DISABLE KEYS */;
INSERT INTO `sylius_product_association` VALUES (4,2,41,'2022-03-08 11:16:38','2022-03-08 11:16:38'),(5,2,26,'2022-03-08 11:16:38','2022-03-08 11:16:38'),(6,2,22,'2022-03-08 11:16:38','2022-03-08 11:16:38');
/*!40000 ALTER TABLE `sylius_product_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_association_product`
--

DROP TABLE IF EXISTS `sylius_product_association_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_association_product` (
  `association_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`association_id`,`product_id`),
  KEY `IDX_A427B983EFB9C8A5` (`association_id`),
  KEY `IDX_A427B9834584665A` (`product_id`),
  CONSTRAINT `FK_A427B9834584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A427B983EFB9C8A5` FOREIGN KEY (`association_id`) REFERENCES `sylius_product_association` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_association_product`
--

LOCK TABLES `sylius_product_association_product` WRITE;
/*!40000 ALTER TABLE `sylius_product_association_product` DISABLE KEYS */;
INSERT INTO `sylius_product_association_product` VALUES (4,41),(4,42),(5,25),(5,26),(6,23),(6,24);
/*!40000 ALTER TABLE `sylius_product_association_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_association_type`
--

DROP TABLE IF EXISTS `sylius_product_association_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_association_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CCB8914C77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_association_type`
--

LOCK TABLES `sylius_product_association_type` WRITE;
/*!40000 ALTER TABLE `sylius_product_association_type` DISABLE KEYS */;
INSERT INTO `sylius_product_association_type` VALUES (2,'similar_products','2022-03-08 11:16:38','2022-03-08 11:16:38');
/*!40000 ALTER TABLE `sylius_product_association_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_association_type_translation`
--

DROP TABLE IF EXISTS `sylius_product_association_type_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_association_type_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_association_type_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_4F618E52C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_4F618E52C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_association_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_association_type_translation`
--

LOCK TABLES `sylius_product_association_type_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_association_type_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_association_type_translation` VALUES (4,2,'Similar products','lt_LT'),(5,2,'Similar products','en_US'),(6,2,'Similar products','ru_RU');
/*!40000 ALTER TABLE `sylius_product_association_type_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_attribute`
--

DROP TABLE IF EXISTS `sylius_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `storage_type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) NOT NULL,
  `filterable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BFAF484A77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_attribute`
--

LOCK TABLES `sylius_product_attribute` WRITE;
/*!40000 ALTER TABLE `sylius_product_attribute` DISABLE KEYS */;
INSERT INTO `sylius_product_attribute` VALUES (13,'t_shirt_brand','text','text','a:0:{}','2022-03-08 11:16:35','2022-03-08 11:16:35',0,0),(14,'t_shirt_collection','text','text','a:0:{}','2022-03-08 11:16:35','2022-03-08 11:16:35',1,0),(15,'t_shirt_material','text','text','a:0:{}','2022-03-08 11:16:35','2022-03-08 11:16:35',2,0),(16,'cap_brand','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',3,0),(17,'cap_collection','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',4,0),(18,'cap_material','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',5,0),(19,'dress_brand','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',6,0),(20,'dress_collection','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',7,0),(21,'dress_material','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',8,0),(22,'jeans_brand','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',9,0),(23,'jeans_collection','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',10,0),(24,'jeans_material','text','text','a:0:{}','2022-03-08 11:16:36','2022-03-08 11:16:36',11,0);
/*!40000 ALTER TABLE `sylius_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_attribute_translation`
--

DROP TABLE IF EXISTS `sylius_product_attribute_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_attribute_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_attribute_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_93850EBA2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_93850EBA2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_attribute` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_attribute_translation`
--

LOCK TABLES `sylius_product_attribute_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_attribute_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_attribute_translation` VALUES (37,13,'T-shirt brand','lt_LT'),(38,13,'T-shirt brand','en_US'),(39,13,'T-shirt brand','ru_RU'),(40,14,'T-shirt collection','lt_LT'),(41,14,'T-shirt collection','en_US'),(42,14,'T-shirt collection','ru_RU'),(43,15,'T-shirt material','lt_LT'),(44,15,'T-shirt material','en_US'),(45,15,'T-shirt material','ru_RU'),(46,16,'Cap brand','lt_LT'),(47,16,'Cap brand','en_US'),(48,16,'Cap brand','ru_RU'),(49,17,'Cap collection','lt_LT'),(50,17,'Cap collection','en_US'),(51,17,'Cap collection','ru_RU'),(52,18,'Cap material','lt_LT'),(53,18,'Cap material','en_US'),(54,18,'Cap material','ru_RU'),(55,19,'Dress brand','lt_LT'),(56,19,'Dress brand','en_US'),(57,19,'Dress brand','ru_RU'),(58,20,'Dress collection','lt_LT'),(59,20,'Dress collection','en_US'),(60,20,'Dress collection','ru_RU'),(61,21,'Dress material','lt_LT'),(62,21,'Dress material','en_US'),(63,21,'Dress material','ru_RU'),(64,22,'Jeans brand','lt_LT'),(65,22,'Jeans brand','en_US'),(66,22,'Jeans brand','ru_RU'),(67,23,'Jeans collection','lt_LT'),(68,23,'Jeans collection','en_US'),(69,23,'Jeans collection','ru_RU'),(70,24,'Jeans material','lt_LT'),(71,24,'Jeans material','en_US'),(72,24,'Jeans material','ru_RU');
/*!40000 ALTER TABLE `sylius_product_attribute_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_attribute_value`
--

DROP TABLE IF EXISTS `sylius_product_attribute_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `locale_code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `text_value` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int(11) DEFAULT NULL,
  `float_value` double DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`),
  KEY `IDX_8A053E544584665A` (`product_id`),
  KEY `IDX_8A053E54B6E62EFA` (`attribute_id`),
  CONSTRAINT `FK_8A053E544584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8A053E54B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `sylius_product_attribute` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_attribute_value`
--

LOCK TABLES `sylius_product_attribute_value` WRITE;
/*!40000 ALTER TABLE `sylius_product_attribute_value` DISABLE KEYS */;
INSERT INTO `sylius_product_attribute_value` VALUES (190,22,13,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(191,22,13,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(192,22,13,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(193,22,14,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(194,22,14,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(195,22,14,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(196,22,15,'lt_LT','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(197,22,15,'en_US','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(198,22,15,'ru_RU','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(199,23,13,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(200,23,13,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(201,23,13,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(202,23,14,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(203,23,14,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(204,23,14,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(205,23,15,'lt_LT','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(206,23,15,'en_US','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(207,23,15,'ru_RU','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(208,24,13,'lt_LT','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(209,24,13,'en_US','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(210,24,13,'ru_RU','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(211,24,14,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(212,24,14,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(213,24,14,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(214,24,15,'lt_LT','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(215,24,15,'en_US','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(216,24,15,'ru_RU','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(217,25,13,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(218,25,13,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(219,25,13,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(220,25,14,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(221,25,14,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(222,25,14,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(223,25,15,'lt_LT','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(224,25,15,'en_US','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(225,25,15,'ru_RU','100% viscose',NULL,NULL,NULL,NULL,NULL,NULL),(226,26,13,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(227,26,13,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(228,26,13,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(229,26,14,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(230,26,14,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(231,26,14,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(232,26,15,'lt_LT','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(233,26,15,'en_US','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(234,26,15,'ru_RU','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(235,27,13,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(236,27,13,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(237,27,13,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(238,27,14,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(239,27,14,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(240,27,14,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(241,27,15,'lt_LT','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(242,27,15,'en_US','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(243,27,15,'ru_RU','100% cotton',NULL,NULL,NULL,NULL,NULL,NULL),(244,28,16,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(245,28,16,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(246,28,16,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(247,28,17,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(248,28,17,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(249,28,17,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(250,28,18,'lt_LT','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(251,28,18,'en_US','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(252,28,18,'ru_RU','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(253,29,16,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(254,29,16,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(255,29,16,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(256,29,17,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(257,29,17,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(258,29,17,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(259,29,18,'lt_LT','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(260,29,18,'en_US','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(261,29,18,'ru_RU','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(262,30,16,'lt_LT','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(263,30,16,'en_US','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(264,30,16,'ru_RU','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(265,30,17,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(266,30,17,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(267,30,17,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(268,30,18,'lt_LT','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(269,30,18,'en_US','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(270,30,18,'ru_RU','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(271,31,16,'lt_LT','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(272,31,16,'en_US','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(273,31,16,'ru_RU','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(274,31,17,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(275,31,17,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(276,31,17,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(277,31,18,'lt_LT','100% cashmere',NULL,NULL,NULL,NULL,NULL,NULL),(278,31,18,'en_US','100% cashmere',NULL,NULL,NULL,NULL,NULL,NULL),(279,31,18,'ru_RU','100% cashmere',NULL,NULL,NULL,NULL,NULL,NULL),(280,32,19,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(281,32,19,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(282,32,19,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(283,32,20,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(284,32,20,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(285,32,20,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(286,32,21,'lt_LT','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(287,32,21,'en_US','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(288,32,21,'ru_RU','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(289,33,19,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(290,33,19,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(291,33,19,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(292,33,20,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(293,33,20,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(294,33,20,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(295,33,21,'lt_LT','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(296,33,21,'en_US','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(297,33,21,'ru_RU','100% wool',NULL,NULL,NULL,NULL,NULL,NULL),(298,34,19,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(299,34,19,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(300,34,19,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(301,34,20,'lt_LT','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(302,34,20,'en_US','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(303,34,20,'ru_RU','Sylius Summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(304,34,21,'lt_LT','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(305,34,21,'en_US','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(306,34,21,'ru_RU','100% polyester',NULL,NULL,NULL,NULL,NULL,NULL),(307,35,22,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(308,35,22,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(309,35,22,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(310,35,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(311,35,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(312,35,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(313,35,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(314,35,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(315,35,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(316,36,22,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(317,36,22,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(318,36,22,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(319,36,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(320,36,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(321,36,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(322,36,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(323,36,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(324,36,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(325,37,22,'lt_LT','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(326,37,22,'en_US','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(327,37,22,'ru_RU','Celsius Small',NULL,NULL,NULL,NULL,NULL,NULL),(328,37,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(329,37,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(330,37,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(331,37,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(332,37,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(333,37,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(334,38,22,'lt_LT','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(335,38,22,'en_US','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(336,38,22,'ru_RU','Date & Banana',NULL,NULL,NULL,NULL,NULL,NULL),(337,38,23,'lt_LT','Sylius summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(338,38,23,'en_US','Sylius summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(339,38,23,'ru_RU','Sylius summer 2019',NULL,NULL,NULL,NULL,NULL,NULL),(340,38,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(341,38,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(342,38,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(343,39,22,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(344,39,22,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(345,39,22,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(346,39,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(347,39,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(348,39,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(349,39,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(350,39,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(351,39,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(352,40,22,'lt_LT','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(353,40,22,'en_US','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(354,40,22,'ru_RU','You are breathtaking',NULL,NULL,NULL,NULL,NULL,NULL),(355,40,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(356,40,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(357,40,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(358,40,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(359,40,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(360,40,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(361,41,22,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(362,41,22,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(363,41,22,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(364,41,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(365,41,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(366,41,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(367,41,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(368,41,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(369,41,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(370,42,22,'lt_LT','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(371,42,22,'en_US','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(372,42,22,'ru_RU','Modern Wear',NULL,NULL,NULL,NULL,NULL,NULL),(373,42,23,'lt_LT','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(374,42,23,'en_US','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(375,42,23,'ru_RU','Sylius Winter 2019',NULL,NULL,NULL,NULL,NULL,NULL),(376,42,24,'lt_LT','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(377,42,24,'en_US','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL),(378,42,24,'ru_RU','100% jeans',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sylius_product_attribute_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_channels`
--

DROP TABLE IF EXISTS `sylius_product_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_channels` (
  `product_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`channel_id`),
  KEY `IDX_F9EF269B4584665A` (`product_id`),
  KEY `IDX_F9EF269B72F5A1AA` (`channel_id`),
  CONSTRAINT `FK_F9EF269B4584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F9EF269B72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_channels`
--

LOCK TABLES `sylius_product_channels` WRITE;
/*!40000 ALTER TABLE `sylius_product_channels` DISABLE KEYS */;
INSERT INTO `sylius_product_channels` VALUES (22,2),(23,2),(24,2),(25,2),(26,2),(27,2),(28,2),(29,2),(30,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,2),(37,2),(38,2),(39,2),(40,2),(41,2),(42,2);
/*!40000 ALTER TABLE `sylius_product_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_image`
--

DROP TABLE IF EXISTS `sylius_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_88C64B2D7E3C61F9` (`owner_id`),
  CONSTRAINT `FK_88C64B2D7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_image`
--

LOCK TABLES `sylius_product_image` WRITE;
/*!40000 ALTER TABLE `sylius_product_image` DISABLE KEYS */;
INSERT INTO `sylius_product_image` VALUES (22,22,'main','1d/e9/728c5702dacc1a4ddf50d8df7fd6.jpg'),(23,23,'main','46/9b/91f6edbf1af4cefae57157facb53.jpg'),(24,24,'main','77/c0/8fef9f14d73cbb914ac79ce6e480.jpg'),(25,25,'main','77/0c/66ca533e0a86dbedfb5063f20384.jpg'),(26,26,'main','19/81/93340270d0c2ea719298b55503d1.jpg'),(27,27,'main','02/d8/9b28c2b5401c0f8a33e9ef43586a.jpg'),(28,28,'main','d8/3b/5a99edd458e61930057e40297db4.jpg'),(29,29,'main','e5/95/e4fa8f65d4ec160cd7e233742970.jpg'),(30,30,'main','19/c1/efa21e45ce995143923277d9aa53.jpg'),(31,31,'main','71/7d/1fc80d6d708162d309898a396796.jpg'),(32,32,'main','5d/79/cc8b8c13f83aa9483d337d31c07c.jpg'),(33,33,'main','1a/5f/2e7f3576f1d031967d7729d17f3d.jpg'),(34,34,'main','6c/5b/cab8bc0f6ab47a1fe1e3bc18eb51.jpg'),(35,35,'main','38/f3/01fbe48b6e3af2ee563913d18991.jpg'),(36,36,'main','fb/42/f1bb9f083edaff7bc6289325accb.jpg'),(37,37,'main','fb/74/d261d4b78b0596f50d7d8d1fa9c5.jpg'),(38,38,'main','ac/6a/2d2a28ac3640b3d87f168b829fd4.jpg'),(39,39,'main','32/a5/3a82d6b594570609ab618a99ebf2.jpg'),(40,40,'main','8a/80/294262204a4db8ce18d58c88e323.jpg'),(41,41,'main','7a/c8/e9a90fd36e1b0e701db3122e5f1a.jpg'),(42,42,'main','86/b4/384fe466c290fd57fee9194ef41a.jpg');
/*!40000 ALTER TABLE `sylius_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_image_product_variants`
--

DROP TABLE IF EXISTS `sylius_product_image_product_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_image_product_variants` (
  `image_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  PRIMARY KEY (`image_id`,`variant_id`),
  KEY `IDX_8FFDAE8D3DA5256D` (`image_id`),
  KEY `IDX_8FFDAE8D3B69A9AF` (`variant_id`),
  CONSTRAINT `FK_8FFDAE8D3B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8FFDAE8D3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `sylius_product_image` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_image_product_variants`
--

LOCK TABLES `sylius_product_image_product_variants` WRITE;
/*!40000 ALTER TABLE `sylius_product_image_product_variants` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_product_image_product_variants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_option`
--

DROP TABLE IF EXISTS `sylius_product_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E4C0EBEF77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_option`
--

LOCK TABLES `sylius_product_option` WRITE;
/*!40000 ALTER TABLE `sylius_product_option` DISABLE KEYS */;
INSERT INTO `sylius_product_option` VALUES (5,'t_shirt_size',0,'2022-03-08 11:16:35','2022-03-08 11:16:35'),(6,'dress_size',1,'2022-03-08 11:16:36','2022-03-08 11:16:36'),(7,'dress_height',2,'2022-03-08 11:16:36','2022-03-08 11:16:36'),(8,'jeans_size',3,'2022-03-08 11:16:36','2022-03-08 11:16:36');
/*!40000 ALTER TABLE `sylius_product_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_option_translation`
--

DROP TABLE IF EXISTS `sylius_product_option_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_option_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_option_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_CBA491AD2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_CBA491AD2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_option_translation`
--

LOCK TABLES `sylius_product_option_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_option_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_option_translation` VALUES (13,5,'T-shirt size','lt_LT'),(14,5,'T-shirt size','en_US'),(15,5,'T-shirt size','ru_RU'),(16,6,'Dress size','lt_LT'),(17,6,'Dress size','en_US'),(18,6,'Dress size','ru_RU'),(19,7,'Dress height','lt_LT'),(20,7,'Dress height','en_US'),(21,7,'Dress height','ru_RU'),(22,8,'Jeans size','lt_LT'),(23,8,'Jeans size','en_US'),(24,8,'Jeans size','ru_RU');
/*!40000 ALTER TABLE `sylius_product_option_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_option_value`
--

DROP TABLE IF EXISTS `sylius_product_option_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_option_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F7FF7D4B77153098` (`code`),
  KEY `IDX_F7FF7D4BA7C41D6F` (`option_id`),
  CONSTRAINT `FK_F7FF7D4BA7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_option_value`
--

LOCK TABLES `sylius_product_option_value` WRITE;
/*!40000 ALTER TABLE `sylius_product_option_value` DISABLE KEYS */;
INSERT INTO `sylius_product_option_value` VALUES (19,5,'t_shirt_size_s'),(20,5,'t_shirt_size_m'),(21,5,'t_shirt_size_l'),(22,5,'t_shirt_size_xl'),(23,5,'t_shirt_size_xxl'),(24,6,'dress_s'),(25,6,'dress_m'),(26,6,'dress_l'),(27,6,'dress_xl'),(28,6,'dress_xxl'),(29,7,'dress_height_petite'),(30,7,'dress_height_regular'),(31,7,'dress_height_tall'),(32,8,'jeans_size_s'),(33,8,'jeans_size_m'),(34,8,'jeans_size_l'),(35,8,'jeans_size_xl'),(36,8,'jeans_size_xxl');
/*!40000 ALTER TABLE `sylius_product_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_option_value_translation`
--

DROP TABLE IF EXISTS `sylius_product_option_value_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_option_value_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_option_value_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_8D4382DC2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_8D4382DC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_option_value` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_option_value_translation`
--

LOCK TABLES `sylius_product_option_value_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_option_value_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_option_value_translation` VALUES (55,19,'S','lt_LT'),(56,19,'S','en_US'),(57,19,'S','ru_RU'),(58,20,'M','lt_LT'),(59,20,'M','en_US'),(60,20,'M','ru_RU'),(61,21,'L','lt_LT'),(62,21,'L','en_US'),(63,21,'L','ru_RU'),(64,22,'XL','lt_LT'),(65,22,'XL','en_US'),(66,22,'XL','ru_RU'),(67,23,'XXL','lt_LT'),(68,23,'XXL','en_US'),(69,23,'XXL','ru_RU'),(70,24,'S','lt_LT'),(71,24,'S','en_US'),(72,24,'S','ru_RU'),(73,25,'M','lt_LT'),(74,25,'M','en_US'),(75,25,'M','ru_RU'),(76,26,'L','lt_LT'),(77,26,'L','en_US'),(78,26,'L','ru_RU'),(79,27,'XL','lt_LT'),(80,27,'XL','en_US'),(81,27,'XL','ru_RU'),(82,28,'XXL','lt_LT'),(83,28,'XXL','en_US'),(84,28,'XXL','ru_RU'),(85,29,'Petite','lt_LT'),(86,29,'Petite','en_US'),(87,29,'Petite','ru_RU'),(88,30,'Regular','lt_LT'),(89,30,'Regular','en_US'),(90,30,'Regular','ru_RU'),(91,31,'Tall','lt_LT'),(92,31,'Tall','en_US'),(93,31,'Tall','ru_RU'),(94,32,'S','lt_LT'),(95,32,'S','en_US'),(96,32,'S','ru_RU'),(97,33,'M','lt_LT'),(98,33,'M','en_US'),(99,33,'M','ru_RU'),(100,34,'L','lt_LT'),(101,34,'L','en_US'),(102,34,'L','ru_RU'),(103,35,'XL','lt_LT'),(104,35,'XL','en_US'),(105,35,'XL','ru_RU'),(106,36,'XXL','lt_LT'),(107,36,'XXL','en_US'),(108,36,'XXL','ru_RU');
/*!40000 ALTER TABLE `sylius_product_option_value_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_options`
--

DROP TABLE IF EXISTS `sylius_product_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_options` (
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`option_id`),
  KEY `IDX_2B5FF0094584665A` (`product_id`),
  KEY `IDX_2B5FF009A7C41D6F` (`option_id`),
  CONSTRAINT `FK_2B5FF0094584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_2B5FF009A7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `sylius_product_option` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_options`
--

LOCK TABLES `sylius_product_options` WRITE;
/*!40000 ALTER TABLE `sylius_product_options` DISABLE KEYS */;
INSERT INTO `sylius_product_options` VALUES (22,5),(23,5),(24,5),(25,5),(26,5),(27,5),(32,6),(32,7),(33,6),(33,7),(34,6),(34,7),(35,8),(36,8),(37,8),(38,8),(39,8),(40,8),(41,8),(42,8);
/*!40000 ALTER TABLE `sylius_product_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_review`
--

DROP TABLE IF EXISTS `sylius_product_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C7056A994584665A` (`product_id`),
  KEY `IDX_C7056A99F675F31B` (`author_id`),
  CONSTRAINT `FK_C7056A994584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C7056A99F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sylius_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_review`
--

LOCK TABLES `sylius_product_review` WRITE;
/*!40000 ALTER TABLE `sylius_product_review` DISABLE KEYS */;
INSERT INTO `sylius_product_review` VALUES (41,39,38,'beatae eius exercitationem',4,'Aliquid voluptatum quisquam ut nihil. Ipsam deserunt dolore necessitatibus similique expedita. Impedit sit id aut.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(42,31,38,'autem ab nisi',1,'Sed sed ex pariatur perferendis. Et sed non voluptas quam itaque. Dolores ipsam voluptas fugiat cupiditate consequuntur sit amet.','new','2022-03-08 11:16:37','2022-03-08 11:16:37'),(43,42,43,'voluptatem enim quidem',2,'Temporibus doloribus accusamus et ut nihil illum. Magni quia quis dolorem. Recusandae perferendis voluptatem neque soluta.','rejected','2022-03-08 11:16:37','2022-03-08 11:16:37'),(44,31,38,'nam optio voluptas',5,'Et commodi eveniet hic perspiciatis exercitationem. Optio deserunt et quo dicta dolore nostrum voluptate. Impedit eos consequatur dolore.','rejected','2022-03-08 11:16:37','2022-03-08 11:16:37'),(45,30,28,'aut vitae qui',4,'Molestiae ea eveniet debitis animi quibusdam amet. Tempora asperiores perspiciatis doloribus. Ut maiores consequatur aperiam et autem sit quae.','new','2022-03-08 11:16:37','2022-03-08 11:16:37'),(46,41,44,'et praesentium in',3,'Error distinctio impedit sint vel ea veritatis quia. Sed delectus quo sequi explicabo accusantium quibusdam. Ex eveniet qui modi incidunt voluptatem.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(47,33,34,'et consectetur excepturi',2,'Nam doloribus omnis perspiciatis qui consectetur quidem blanditiis. Quae quod impedit ut mollitia ratione est cumque voluptate. Perspiciatis veniam sint distinctio amet hic.','new','2022-03-08 11:16:37','2022-03-08 11:16:37'),(48,41,44,'minus illo qui',2,'Et aut rerum sit architecto repellendus sapiente ut. Repellendus fugit nulla necessitatibus voluptatem cupiditate est voluptatem. Accusantium earum qui dolores libero delectus.','new','2022-03-08 11:16:37','2022-03-08 11:16:37'),(49,41,28,'labore quod optio',5,'Assumenda delectus iure voluptatibus dolorum. Illum quidem voluptatem in nihil sequi consectetur. Aperiam et voluptatum sit ea qui rerum.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(50,39,30,'enim ut fuga',5,'Cumque magni et dolor in sunt ut architecto. Consequuntur dolor voluptatem corrupti cum eius sit. Reprehenderit placeat omnis adipisci nobis itaque culpa.','rejected','2022-03-08 11:16:37','2022-03-08 11:16:37'),(51,31,36,'omnis modi quisquam',5,'Pariatur consequatur dignissimos sapiente quasi. Ullam illo expedita reiciendis ea. Pariatur eligendi est nemo excepturi ut aut quasi.','rejected','2022-03-08 11:16:37','2022-03-08 11:16:37'),(52,25,43,'est quaerat impedit',3,'Iusto quidem dolorum omnis. Totam eum aut eaque quibusdam illo perspiciatis aliquam. Non et nemo eligendi impedit.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(53,28,43,'recusandae recusandae enim',1,'Aspernatur laborum et molestiae voluptas aut quibusdam qui. Ut delectus voluptates eos cupiditate. Rerum et similique iste rerum doloribus.','rejected','2022-03-08 11:16:37','2022-03-08 11:16:37'),(54,26,33,'voluptatibus quisquam aut',1,'Laborum dolore ex dolorem. Quod commodi natus repellat quia facere cumque. Est fuga impedit quos.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(55,24,23,'facere et quia',5,'Nesciunt nemo dolore cumque doloremque accusamus ipsam. Id qui fugiat quisquam quisquam. Saepe deserunt sed corrupti eaque totam amet.','accepted','2022-03-08 11:16:37','2022-03-08 11:16:37'),(56,24,44,'est sit quis',4,'Voluptas reiciendis cum tenetur adipisci tempora omnis dolor. In delectus et qui dicta officiis quia impedit. Quas deserunt et dicta.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(57,40,38,'illum quas neque',3,'Voluptatum eius alias vel ex laudantium et. Voluptatibus doloremque rerum aut. Deleniti velit blanditiis sit qui sed tenetur deserunt.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(58,40,36,'quae consectetur delectus',5,'Qui provident laboriosam quisquam et dolorem. Architecto impedit saepe aut a ut corporis. Rem cupiditate fugit excepturi impedit nihil.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(59,33,23,'eos consequatur saepe',3,'Id esse nulla nemo autem eum dolore. Adipisci dolores ut dolores quasi mollitia. Corrupti autem soluta omnis quo.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(60,41,39,'labore dolor laboriosam',3,'Laboriosam consequatur deleniti sed quia. Fugiat sit qui voluptatem amet facere. Qui eum et est autem adipisci et ut cum.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(61,42,41,'illum error tempore',2,'Voluptatum velit officia placeat aliquid asperiores aut. Libero quo officiis voluptas expedita tempora voluptatem. Ipsa esse assumenda occaecati sed dolorem quaerat.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(62,28,39,'quod quaerat labore',4,'Ut ullam adipisci voluptatem incidunt aliquam vel. Eos vero voluptates ea ut accusamus. Dolorum possimus ea quam est.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(63,40,29,'excepturi sunt dolor',5,'Voluptates magni ut suscipit. Qui velit est et rerum corrupti iusto. Est sint omnis sit quam blanditiis doloremque dolorem.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(64,40,28,'sit modi sit',3,'Itaque ut praesentium nemo saepe. Dolorem illo est molestiae aut ducimus. Molestiae dolore dignissimos minus eos laboriosam deleniti.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(65,24,44,'rerum dolorem optio',3,'Quia dolore aut eum cupiditate. Aspernatur quasi nemo saepe nostrum autem. Fuga laborum qui alias.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(66,40,43,'aut ipsam ea',5,'Tempore omnis voluptatibus vitae consequuntur ipsum neque. Ipsa et expedita qui quis. Repudiandae cum consequatur quia voluptas at unde ipsa.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(67,32,35,'amet voluptatem deserunt',4,'Reprehenderit nihil dignissimos est sed saepe tempore expedita quaerat. Beatae quidem aut dolor aut. Nihil sapiente fugiat eligendi distinctio dicta et.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(68,28,26,'accusamus aut blanditiis',5,'Totam nulla at magnam accusamus ad magnam voluptatem. Et tempore laborum consectetur magnam. Quasi ut quo pariatur quam suscipit quam.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(69,39,39,'tenetur consequatur eos',3,'Rerum commodi ut ipsum et et repudiandae iusto accusamus. Incidunt enim porro quis repudiandae cupiditate aliquid cumque. Fugiat vitae alias amet fuga.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(70,28,27,'et consequatur accusantium',1,'Et quia quis iure quod dignissimos. Sit soluta eaque tempora dolore. Quae quia unde maiores dolores quo cumque.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(71,36,27,'quia dolores harum',4,'Et sunt blanditiis nam qui et enim. Quia beatae enim doloremque minus non nemo consequatur nemo. Quidem dolore et ut magni consequatur quae consectetur quis.','new','2022-03-08 11:16:38','2022-03-08 11:16:38'),(72,28,42,'fugiat ad architecto',3,'Nisi qui voluptatem ut sed sit vel. Aspernatur sed corrupti maiores. Magni explicabo dolore molestias delectus.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(73,42,28,'libero magni ut',2,'Nihil est quos nam ipsum enim quia unde. Ratione aut recusandae odio reprehenderit modi libero. Vel iure porro repudiandae delectus hic harum voluptatum.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(74,29,32,'voluptate odio sint',4,'Ut quidem nemo aperiam ab illum qui. Eum asperiores eveniet voluptates rerum est dolor consectetur qui. Animi eaque eum odio dolorum excepturi magnam.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(75,35,41,'id vero consectetur',1,'Vel harum minus tempore voluptas dignissimos eligendi. Est sint sint qui error dolorem harum placeat. Quo deserunt minus omnis tempore animi modi.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(76,27,37,'ut explicabo consequatur',2,'Animi mollitia amet consequuntur quasi ut maiores quos. Consequatur minus repellat possimus quibusdam aut. Quo voluptas sed debitis molestias optio.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(77,28,38,'reprehenderit voluptatibus enim',1,'In dolore eos velit modi. Id assumenda adipisci fugiat ut quis asperiores. Nesciunt qui possimus id autem laboriosam saepe.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(78,35,34,'necessitatibus nulla cum',4,'Deleniti alias tenetur tempora dolorem. Ducimus eaque laudantium minima autem fugiat iste. Facilis dolorem omnis et ullam doloremque non molestias.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38'),(79,31,34,'sint voluptas atque',2,'Recusandae asperiores tempore labore qui quasi optio voluptas. Modi nesciunt voluptatem quos odit voluptatibus amet. Dolorum necessitatibus ut eos iure qui quia sed.','accepted','2022-03-08 11:16:38','2022-03-08 11:16:38'),(80,42,40,'magni perspiciatis praesentium',3,'Omnis quia tempora labore repudiandae. Et animi voluptatum sed minus cupiditate ea earum. Corporis consequuntur esse laboriosam architecto nisi.','rejected','2022-03-08 11:16:38','2022-03-08 11:16:38');
/*!40000 ALTER TABLE `sylius_product_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_taxon`
--

DROP TABLE IF EXISTS `sylius_product_taxon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_taxon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `taxon_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_taxon_idx` (`product_id`,`taxon_id`),
  KEY `IDX_169C6CD94584665A` (`product_id`),
  KEY `IDX_169C6CD9DE13F470` (`taxon_id`),
  CONSTRAINT `FK_169C6CD94584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_169C6CD9DE13F470` FOREIGN KEY (`taxon_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_taxon`
--

LOCK TABLES `sylius_product_taxon` WRITE;
/*!40000 ALTER TABLE `sylius_product_taxon` DISABLE KEYS */;
INSERT INTO `sylius_product_taxon` VALUES (40,22,13,0),(41,22,15,0),(42,23,13,1),(43,23,15,1),(44,24,13,2),(45,24,15,2),(46,25,13,3),(47,25,14,0),(48,26,13,4),(49,26,14,1),(50,27,13,5),(51,27,14,2),(52,28,16,0),(53,28,18,0),(54,29,16,1),(55,29,17,0),(56,30,16,2),(57,30,18,1),(58,31,16,3),(59,31,17,1),(60,32,19,0),(61,33,19,1),(62,34,19,2),(63,35,20,0),(64,35,21,0),(65,36,20,1),(66,36,21,1),(67,37,20,2),(68,37,21,2),(69,38,20,3),(70,38,21,3),(71,39,20,4),(72,39,22,0),(73,40,20,5),(74,40,22,1),(75,41,20,6),(76,41,22,2),(77,42,20,7),(78,42,22,3);
/*!40000 ALTER TABLE `sylius_product_taxon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_translation`
--

DROP TABLE IF EXISTS `sylius_product_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_105A9084180C698989D9B62` (`locale`,`slug`),
  UNIQUE KEY `sylius_product_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_105A9082C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_105A9082C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_translation`
--

LOCK TABLES `sylius_product_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_translation` VALUES (64,22,'Everyday white basic T-Shirt','everyday-white-basic-t-shirt','Dolorem ipsam ut sequi velit modi quis eos. Ea dicta ea saepe rem quod.\n\nVeritatis vero consequatur id. Ipsam cupiditate fugit voluptate. Quia consectetur et et ex sint amet.\n\nDistinctio exercitationem ab alias consequatur atque qui. Quisquam ab laboriosam aut aut eveniet id. Tenetur omnis similique earum voluptatum vel voluptate magnam.',NULL,NULL,'Et sunt delectus magni qui earum aliquam dignissimos. Est voluptas harum ex.','lt_LT'),(65,22,'Everyday white basic T-Shirt','everyday-white-basic-t-shirt','Dolorem ipsam ut sequi velit modi quis eos. Ea dicta ea saepe rem quod.\n\nVeritatis vero consequatur id. Ipsam cupiditate fugit voluptate. Quia consectetur et et ex sint amet.\n\nDistinctio exercitationem ab alias consequatur atque qui. Quisquam ab laboriosam aut aut eveniet id. Tenetur omnis similique earum voluptatum vel voluptate magnam.',NULL,NULL,'Et sunt delectus magni qui earum aliquam dignissimos. Est voluptas harum ex.','en_US'),(66,22,'Everyday white basic T-Shirt','everyday-white-basic-t-shirt','Dolorem ipsam ut sequi velit modi quis eos. Ea dicta ea saepe rem quod.\n\nVeritatis vero consequatur id. Ipsam cupiditate fugit voluptate. Quia consectetur et et ex sint amet.\n\nDistinctio exercitationem ab alias consequatur atque qui. Quisquam ab laboriosam aut aut eveniet id. Tenetur omnis similique earum voluptatum vel voluptate magnam.',NULL,NULL,'Et sunt delectus magni qui earum aliquam dignissimos. Est voluptas harum ex.','ru_RU'),(67,23,'Loose white designer T-Shirt','loose-white-designer-t-shirt','Delectus dolor dolores minus dicta quia. Occaecati qui dolor error. Alias aut libero reprehenderit non harum repudiandae. Tempore harum rerum adipisci incidunt asperiores aliquam asperiores. Maiores quos non non nam.\n\nMolestias ullam dicta possimus aut voluptates omnis. Quisquam hic qui minus consequatur quis deserunt voluptas. Labore maiores et nihil atque saepe. Nemo hic sit debitis minima fuga possimus aut odio.\n\nQuaerat aliquid voluptas id dolorem. Odio dolorem voluptatibus porro non earum voluptas. Molestiae illo nesciunt culpa sunt odit voluptatem. Suscipit quos quidem deserunt suscipit sed et. Quia cum error voluptas ullam ut quod.',NULL,NULL,'Et molestiae laborum amet dicta accusantium enim. Maiores qui aliquam placeat fuga eaque voluptatem quos. Velit ea iste et quae voluptas magni.','lt_LT'),(68,23,'Loose white designer T-Shirt','loose-white-designer-t-shirt','Delectus dolor dolores minus dicta quia. Occaecati qui dolor error. Alias aut libero reprehenderit non harum repudiandae. Tempore harum rerum adipisci incidunt asperiores aliquam asperiores. Maiores quos non non nam.\n\nMolestias ullam dicta possimus aut voluptates omnis. Quisquam hic qui minus consequatur quis deserunt voluptas. Labore maiores et nihil atque saepe. Nemo hic sit debitis minima fuga possimus aut odio.\n\nQuaerat aliquid voluptas id dolorem. Odio dolorem voluptatibus porro non earum voluptas. Molestiae illo nesciunt culpa sunt odit voluptatem. Suscipit quos quidem deserunt suscipit sed et. Quia cum error voluptas ullam ut quod.',NULL,NULL,'Et molestiae laborum amet dicta accusantium enim. Maiores qui aliquam placeat fuga eaque voluptatem quos. Velit ea iste et quae voluptas magni.','en_US'),(69,23,'Loose white designer T-Shirt','loose-white-designer-t-shirt','Delectus dolor dolores minus dicta quia. Occaecati qui dolor error. Alias aut libero reprehenderit non harum repudiandae. Tempore harum rerum adipisci incidunt asperiores aliquam asperiores. Maiores quos non non nam.\n\nMolestias ullam dicta possimus aut voluptates omnis. Quisquam hic qui minus consequatur quis deserunt voluptas. Labore maiores et nihil atque saepe. Nemo hic sit debitis minima fuga possimus aut odio.\n\nQuaerat aliquid voluptas id dolorem. Odio dolorem voluptatibus porro non earum voluptas. Molestiae illo nesciunt culpa sunt odit voluptatem. Suscipit quos quidem deserunt suscipit sed et. Quia cum error voluptas ullam ut quod.',NULL,NULL,'Et molestiae laborum amet dicta accusantium enim. Maiores qui aliquam placeat fuga eaque voluptatem quos. Velit ea iste et quae voluptas magni.','ru_RU'),(70,24,'Ribbed copper slim fit Tee','ribbed-copper-slim-fit-tee','Non non magni fugiat quis iste aliquam. Mollitia doloremque sed asperiores et quibusdam qui. Aspernatur magni assumenda est voluptates et non et nam. Qui hic nihil aut occaecati tempora enim vitae aliquam.\n\nQuidem qui sunt dolor ex amet sed. Qui veniam consectetur ea numquam. Non autem quidem consequuntur perferendis.\n\nQui sint architecto nulla et voluptas exercitationem. Eum et nulla delectus quia consequuntur est.',NULL,NULL,'Exercitationem quisquam est consequatur sint neque alias aliquid dolore. Adipisci eos consequuntur ut et consequatur hic. Voluptate sed voluptas dolores est.','lt_LT'),(71,24,'Ribbed copper slim fit Tee','ribbed-copper-slim-fit-tee','Non non magni fugiat quis iste aliquam. Mollitia doloremque sed asperiores et quibusdam qui. Aspernatur magni assumenda est voluptates et non et nam. Qui hic nihil aut occaecati tempora enim vitae aliquam.\n\nQuidem qui sunt dolor ex amet sed. Qui veniam consectetur ea numquam. Non autem quidem consequuntur perferendis.\n\nQui sint architecto nulla et voluptas exercitationem. Eum et nulla delectus quia consequuntur est.',NULL,NULL,'Exercitationem quisquam est consequatur sint neque alias aliquid dolore. Adipisci eos consequuntur ut et consequatur hic. Voluptate sed voluptas dolores est.','en_US'),(72,24,'Ribbed copper slim fit Tee','ribbed-copper-slim-fit-tee','Non non magni fugiat quis iste aliquam. Mollitia doloremque sed asperiores et quibusdam qui. Aspernatur magni assumenda est voluptates et non et nam. Qui hic nihil aut occaecati tempora enim vitae aliquam.\n\nQuidem qui sunt dolor ex amet sed. Qui veniam consectetur ea numquam. Non autem quidem consequuntur perferendis.\n\nQui sint architecto nulla et voluptas exercitationem. Eum et nulla delectus quia consequuntur est.',NULL,NULL,'Exercitationem quisquam est consequatur sint neque alias aliquid dolore. Adipisci eos consequuntur ut et consequatur hic. Voluptate sed voluptas dolores est.','ru_RU'),(73,25,'Sport basic white T-Shirt','sport-basic-white-t-shirt','Ullam vel non asperiores. Consequatur optio excepturi voluptatem ea et. Illum in omnis cum aut. Architecto iusto sit quia voluptas deserunt similique occaecati.\n\nDolores ullam quaerat suscipit voluptatem earum non nostrum. Voluptatem facilis dolorem architecto molestiae nihil iusto sed voluptatem. Porro dolorem dolorum reiciendis suscipit qui aut repudiandae. Sit itaque odio velit sint.\n\nSint ea distinctio quo voluptate at molestiae. Recusandae nesciunt soluta nemo perspiciatis accusamus. Doloremque sed quaerat fuga quod neque adipisci quaerat cum.',NULL,NULL,'Ducimus rerum non velit rerum laborum. Impedit saepe maiores sint ea. Rem blanditiis et id temporibus. Eveniet at quam quis autem consequuntur dolor.','lt_LT'),(74,25,'Sport basic white T-Shirt','sport-basic-white-t-shirt','Ullam vel non asperiores. Consequatur optio excepturi voluptatem ea et. Illum in omnis cum aut. Architecto iusto sit quia voluptas deserunt similique occaecati.\n\nDolores ullam quaerat suscipit voluptatem earum non nostrum. Voluptatem facilis dolorem architecto molestiae nihil iusto sed voluptatem. Porro dolorem dolorum reiciendis suscipit qui aut repudiandae. Sit itaque odio velit sint.\n\nSint ea distinctio quo voluptate at molestiae. Recusandae nesciunt soluta nemo perspiciatis accusamus. Doloremque sed quaerat fuga quod neque adipisci quaerat cum.',NULL,NULL,'Ducimus rerum non velit rerum laborum. Impedit saepe maiores sint ea. Rem blanditiis et id temporibus. Eveniet at quam quis autem consequuntur dolor.','en_US'),(75,25,'Sport basic white T-Shirt','sport-basic-white-t-shirt','Ullam vel non asperiores. Consequatur optio excepturi voluptatem ea et. Illum in omnis cum aut. Architecto iusto sit quia voluptas deserunt similique occaecati.\n\nDolores ullam quaerat suscipit voluptatem earum non nostrum. Voluptatem facilis dolorem architecto molestiae nihil iusto sed voluptatem. Porro dolorem dolorum reiciendis suscipit qui aut repudiandae. Sit itaque odio velit sint.\n\nSint ea distinctio quo voluptate at molestiae. Recusandae nesciunt soluta nemo perspiciatis accusamus. Doloremque sed quaerat fuga quod neque adipisci quaerat cum.',NULL,NULL,'Ducimus rerum non velit rerum laborum. Impedit saepe maiores sint ea. Rem blanditiis et id temporibus. Eveniet at quam quis autem consequuntur dolor.','ru_RU'),(76,26,'Raglan grey & black Tee','raglan-grey-black-tee','Vero vitae enim voluptates quisquam. Consectetur eum repellat eligendi minima vel consequuntur cumque. Veritatis aut ipsa rem dolores commodi aliquam ducimus. Dolores rerum natus necessitatibus et est.\n\nVelit et non sint et modi ut. Dolorem qui et tempora quis voluptas sapiente sed. Et dolor est quas omnis omnis iusto error ea. Repellat id ea sed maiores.\n\nEt ut saepe voluptatem. Ea quas nisi et vitae sed. Et harum velit magni et quis.',NULL,NULL,'Dicta et nisi ea doloremque expedita debitis. Quia aliquam nulla incidunt. Eveniet hic sint sed aut et consectetur. Quia non facilis minima tempore eos.','lt_LT'),(77,26,'Raglan grey & black Tee','raglan-grey-black-tee','Vero vitae enim voluptates quisquam. Consectetur eum repellat eligendi minima vel consequuntur cumque. Veritatis aut ipsa rem dolores commodi aliquam ducimus. Dolores rerum natus necessitatibus et est.\n\nVelit et non sint et modi ut. Dolorem qui et tempora quis voluptas sapiente sed. Et dolor est quas omnis omnis iusto error ea. Repellat id ea sed maiores.\n\nEt ut saepe voluptatem. Ea quas nisi et vitae sed. Et harum velit magni et quis.',NULL,NULL,'Dicta et nisi ea doloremque expedita debitis. Quia aliquam nulla incidunt. Eveniet hic sint sed aut et consectetur. Quia non facilis minima tempore eos.','en_US'),(78,26,'Raglan grey & black Tee','raglan-grey-black-tee','Vero vitae enim voluptates quisquam. Consectetur eum repellat eligendi minima vel consequuntur cumque. Veritatis aut ipsa rem dolores commodi aliquam ducimus. Dolores rerum natus necessitatibus et est.\n\nVelit et non sint et modi ut. Dolorem qui et tempora quis voluptas sapiente sed. Et dolor est quas omnis omnis iusto error ea. Repellat id ea sed maiores.\n\nEt ut saepe voluptatem. Ea quas nisi et vitae sed. Et harum velit magni et quis.',NULL,NULL,'Dicta et nisi ea doloremque expedita debitis. Quia aliquam nulla incidunt. Eveniet hic sint sed aut et consectetur. Quia non facilis minima tempore eos.','ru_RU'),(79,27,'Oversize white cotton T-Shirt','oversize-white-cotton-t-shirt','Aut rerum voluptatem nesciunt. Est et alias et. Suscipit ad officia id et sit amet cumque. Voluptas placeat voluptas soluta.\n\nAssumenda veniam dignissimos labore mollitia doloribus quas. Animi blanditiis animi in distinctio. Quis fugit nihil facilis perferendis esse.\n\nIste rem sunt quibusdam enim. Rerum fuga molestiae perferendis deleniti ut distinctio. Ratione sequi optio reprehenderit eum rerum.',NULL,NULL,'Animi ducimus qui nisi pariatur molestias alias aspernatur. Perferendis tempora eos qui et ex. Ducimus aut quo quisquam.','lt_LT'),(80,27,'Oversize white cotton T-Shirt','oversize-white-cotton-t-shirt','Aut rerum voluptatem nesciunt. Est et alias et. Suscipit ad officia id et sit amet cumque. Voluptas placeat voluptas soluta.\n\nAssumenda veniam dignissimos labore mollitia doloribus quas. Animi blanditiis animi in distinctio. Quis fugit nihil facilis perferendis esse.\n\nIste rem sunt quibusdam enim. Rerum fuga molestiae perferendis deleniti ut distinctio. Ratione sequi optio reprehenderit eum rerum.',NULL,NULL,'Animi ducimus qui nisi pariatur molestias alias aspernatur. Perferendis tempora eos qui et ex. Ducimus aut quo quisquam.','en_US'),(81,27,'Oversize white cotton T-Shirt','oversize-white-cotton-t-shirt','Aut rerum voluptatem nesciunt. Est et alias et. Suscipit ad officia id et sit amet cumque. Voluptas placeat voluptas soluta.\n\nAssumenda veniam dignissimos labore mollitia doloribus quas. Animi blanditiis animi in distinctio. Quis fugit nihil facilis perferendis esse.\n\nIste rem sunt quibusdam enim. Rerum fuga molestiae perferendis deleniti ut distinctio. Ratione sequi optio reprehenderit eum rerum.',NULL,NULL,'Animi ducimus qui nisi pariatur molestias alias aspernatur. Perferendis tempora eos qui et ex. Ducimus aut quo quisquam.','ru_RU'),(82,28,'Knitted burgundy winter cap','knitted-burgundy-winter-cap','Necessitatibus vero distinctio quia nihil et nemo aut. Illum ratione quia impedit perspiciatis quia eum iure. Dolor vel voluptatem impedit totam sit quia.\n\nRecusandae nam dolores nostrum eveniet blanditiis expedita. Nesciunt saepe nesciunt odit. Necessitatibus dolor commodi eum ex officiis nihil qui.\n\nVel laboriosam impedit laborum doloremque aut non est est. Sint in consequuntur animi placeat consequatur est. Necessitatibus enim est sed aut occaecati et nemo. Voluptate eligendi aut at sint sed.',NULL,NULL,'Officiis et perspiciatis velit odit in. Nam laudantium magni explicabo mollitia.','lt_LT'),(83,28,'Knitted burgundy winter cap','knitted-burgundy-winter-cap','Necessitatibus vero distinctio quia nihil et nemo aut. Illum ratione quia impedit perspiciatis quia eum iure. Dolor vel voluptatem impedit totam sit quia.\n\nRecusandae nam dolores nostrum eveniet blanditiis expedita. Nesciunt saepe nesciunt odit. Necessitatibus dolor commodi eum ex officiis nihil qui.\n\nVel laboriosam impedit laborum doloremque aut non est est. Sint in consequuntur animi placeat consequatur est. Necessitatibus enim est sed aut occaecati et nemo. Voluptate eligendi aut at sint sed.',NULL,NULL,'Officiis et perspiciatis velit odit in. Nam laudantium magni explicabo mollitia.','en_US'),(84,28,'Knitted burgundy winter cap','knitted-burgundy-winter-cap','Necessitatibus vero distinctio quia nihil et nemo aut. Illum ratione quia impedit perspiciatis quia eum iure. Dolor vel voluptatem impedit totam sit quia.\n\nRecusandae nam dolores nostrum eveniet blanditiis expedita. Nesciunt saepe nesciunt odit. Necessitatibus dolor commodi eum ex officiis nihil qui.\n\nVel laboriosam impedit laborum doloremque aut non est est. Sint in consequuntur animi placeat consequatur est. Necessitatibus enim est sed aut occaecati et nemo. Voluptate eligendi aut at sint sed.',NULL,NULL,'Officiis et perspiciatis velit odit in. Nam laudantium magni explicabo mollitia.','ru_RU'),(85,29,'Knitted wool-blend green cap','knitted-wool-blend-green-cap','Ad deserunt adipisci molestiae eaque rem. Ea modi modi tempore est. Quidem sit aut a illum. Et praesentium facere nostrum assumenda.\n\nQuae minima adipisci minus ducimus. Ipsum impedit quisquam enim velit facilis occaecati reprehenderit. Quod eveniet et occaecati modi magnam. Maiores quod nihil veritatis est ad rem. Aspernatur dolores rerum quaerat recusandae sunt necessitatibus.\n\nQuae eum rerum molestiae laborum eligendi ratione. Minus qui et aliquid dicta et et. Magni earum fugit eveniet repellendus accusamus quia. Nihil repudiandae neque delectus sed soluta omnis.',NULL,NULL,'Voluptatem est voluptas qui maxime quae molestiae excepturi. Aut vel consequuntur et ex voluptatum quo. Qui nostrum neque cumque quibusdam expedita sequi. Impedit sed esse nesciunt consequatur est.','lt_LT'),(86,29,'Knitted wool-blend green cap','knitted-wool-blend-green-cap','Ad deserunt adipisci molestiae eaque rem. Ea modi modi tempore est. Quidem sit aut a illum. Et praesentium facere nostrum assumenda.\n\nQuae minima adipisci minus ducimus. Ipsum impedit quisquam enim velit facilis occaecati reprehenderit. Quod eveniet et occaecati modi magnam. Maiores quod nihil veritatis est ad rem. Aspernatur dolores rerum quaerat recusandae sunt necessitatibus.\n\nQuae eum rerum molestiae laborum eligendi ratione. Minus qui et aliquid dicta et et. Magni earum fugit eveniet repellendus accusamus quia. Nihil repudiandae neque delectus sed soluta omnis.',NULL,NULL,'Voluptatem est voluptas qui maxime quae molestiae excepturi. Aut vel consequuntur et ex voluptatum quo. Qui nostrum neque cumque quibusdam expedita sequi. Impedit sed esse nesciunt consequatur est.','en_US'),(87,29,'Knitted wool-blend green cap','knitted-wool-blend-green-cap','Ad deserunt adipisci molestiae eaque rem. Ea modi modi tempore est. Quidem sit aut a illum. Et praesentium facere nostrum assumenda.\n\nQuae minima adipisci minus ducimus. Ipsum impedit quisquam enim velit facilis occaecati reprehenderit. Quod eveniet et occaecati modi magnam. Maiores quod nihil veritatis est ad rem. Aspernatur dolores rerum quaerat recusandae sunt necessitatibus.\n\nQuae eum rerum molestiae laborum eligendi ratione. Minus qui et aliquid dicta et et. Magni earum fugit eveniet repellendus accusamus quia. Nihil repudiandae neque delectus sed soluta omnis.',NULL,NULL,'Voluptatem est voluptas qui maxime quae molestiae excepturi. Aut vel consequuntur et ex voluptatum quo. Qui nostrum neque cumque quibusdam expedita sequi. Impedit sed esse nesciunt consequatur est.','ru_RU'),(88,30,'Knitted white pompom cap','knitted-white-pompom-cap','Aut officiis eos qui odit ex maxime. Eum veritatis quia eos vero non. Eum et aut accusantium sint.\n\nEt quidem nemo voluptatem. Consequuntur molestiae et odit libero eveniet. Dolorem nulla quas occaecati facere vero fuga.\n\nEnim sit rerum et qui sed dicta aperiam. Animi earum suscipit iste. Aut earum esse consequuntur. Dolor sunt sapiente explicabo et.',NULL,NULL,'Facilis optio veritatis atque nemo hic necessitatibus. Consequatur enim repellat ut deleniti est et quod. Dolorem et eligendi in ea animi aut perspiciatis. Veritatis tempore a ducimus laborum error.','lt_LT'),(89,30,'Knitted white pompom cap','knitted-white-pompom-cap','Aut officiis eos qui odit ex maxime. Eum veritatis quia eos vero non. Eum et aut accusantium sint.\n\nEt quidem nemo voluptatem. Consequuntur molestiae et odit libero eveniet. Dolorem nulla quas occaecati facere vero fuga.\n\nEnim sit rerum et qui sed dicta aperiam. Animi earum suscipit iste. Aut earum esse consequuntur. Dolor sunt sapiente explicabo et.',NULL,NULL,'Facilis optio veritatis atque nemo hic necessitatibus. Consequatur enim repellat ut deleniti est et quod. Dolorem et eligendi in ea animi aut perspiciatis. Veritatis tempore a ducimus laborum error.','en_US'),(90,30,'Knitted white pompom cap','knitted-white-pompom-cap','Aut officiis eos qui odit ex maxime. Eum veritatis quia eos vero non. Eum et aut accusantium sint.\n\nEt quidem nemo voluptatem. Consequuntur molestiae et odit libero eveniet. Dolorem nulla quas occaecati facere vero fuga.\n\nEnim sit rerum et qui sed dicta aperiam. Animi earum suscipit iste. Aut earum esse consequuntur. Dolor sunt sapiente explicabo et.',NULL,NULL,'Facilis optio veritatis atque nemo hic necessitatibus. Consequatur enim repellat ut deleniti est et quod. Dolorem et eligendi in ea animi aut perspiciatis. Veritatis tempore a ducimus laborum error.','ru_RU'),(91,31,'Cashmere-blend violet beanie','cashmere-blend-violet-beanie','Officiis et hic ut vero sit. Provident consequatur dolorum aut commodi. Harum illum assumenda maiores. Ut vel laborum quo alias id. Perferendis illum est ut deleniti.\n\nId officia laborum provident. Repellendus natus aut quaerat et nihil amet. Exercitationem quia tempore impedit aut molestiae et eum doloremque.\n\nVeritatis commodi inventore est omnis est. Neque consectetur qui cumque ducimus in. Voluptas omnis enim vel.',NULL,NULL,'Mollitia quis repellendus ut quia. Necessitatibus enim accusamus perferendis repudiandae repellendus error. Non corrupti repellendus vel doloremque repellat et odit.','lt_LT'),(92,31,'Cashmere-blend violet beanie','cashmere-blend-violet-beanie','Officiis et hic ut vero sit. Provident consequatur dolorum aut commodi. Harum illum assumenda maiores. Ut vel laborum quo alias id. Perferendis illum est ut deleniti.\n\nId officia laborum provident. Repellendus natus aut quaerat et nihil amet. Exercitationem quia tempore impedit aut molestiae et eum doloremque.\n\nVeritatis commodi inventore est omnis est. Neque consectetur qui cumque ducimus in. Voluptas omnis enim vel.',NULL,NULL,'Mollitia quis repellendus ut quia. Necessitatibus enim accusamus perferendis repudiandae repellendus error. Non corrupti repellendus vel doloremque repellat et odit.','en_US'),(93,31,'Cashmere-blend violet beanie','cashmere-blend-violet-beanie','Officiis et hic ut vero sit. Provident consequatur dolorum aut commodi. Harum illum assumenda maiores. Ut vel laborum quo alias id. Perferendis illum est ut deleniti.\n\nId officia laborum provident. Repellendus natus aut quaerat et nihil amet. Exercitationem quia tempore impedit aut molestiae et eum doloremque.\n\nVeritatis commodi inventore est omnis est. Neque consectetur qui cumque ducimus in. Voluptas omnis enim vel.',NULL,NULL,'Mollitia quis repellendus ut quia. Necessitatibus enim accusamus perferendis repudiandae repellendus error. Non corrupti repellendus vel doloremque repellat et odit.','ru_RU'),(94,32,'Beige strappy summer dress','beige-strappy-summer-dress','Sunt qui quo quia beatae ullam iusto. Voluptate in magnam et natus fugiat architecto reprehenderit. Velit fugit eligendi dolores maiores. Facilis consequatur nihil deserunt recusandae et ratione illum.\n\nVoluptate ullam maiores ipsa. Tenetur molestiae aspernatur praesentium earum qui iste est. Vero molestias sed consequuntur quam nulla. Molestiae ut quibusdam ratione voluptatem distinctio. Nam voluptas vero beatae est eos.\n\nQuis praesentium quia impedit. Enim voluptates est quas.',NULL,NULL,'Modi ipsam dignissimos nam unde eos totam. Sapiente fuga porro quidem et. Voluptate qui corporis error sint quam et. Excepturi ut ut temporibus et. Adipisci veritatis et praesentium mollitia.','lt_LT'),(95,32,'Beige strappy summer dress','beige-strappy-summer-dress','Sunt qui quo quia beatae ullam iusto. Voluptate in magnam et natus fugiat architecto reprehenderit. Velit fugit eligendi dolores maiores. Facilis consequatur nihil deserunt recusandae et ratione illum.\n\nVoluptate ullam maiores ipsa. Tenetur molestiae aspernatur praesentium earum qui iste est. Vero molestias sed consequuntur quam nulla. Molestiae ut quibusdam ratione voluptatem distinctio. Nam voluptas vero beatae est eos.\n\nQuis praesentium quia impedit. Enim voluptates est quas.',NULL,NULL,'Modi ipsam dignissimos nam unde eos totam. Sapiente fuga porro quidem et. Voluptate qui corporis error sint quam et. Excepturi ut ut temporibus et. Adipisci veritatis et praesentium mollitia.','en_US'),(96,32,'Beige strappy summer dress','beige-strappy-summer-dress','Sunt qui quo quia beatae ullam iusto. Voluptate in magnam et natus fugiat architecto reprehenderit. Velit fugit eligendi dolores maiores. Facilis consequatur nihil deserunt recusandae et ratione illum.\n\nVoluptate ullam maiores ipsa. Tenetur molestiae aspernatur praesentium earum qui iste est. Vero molestias sed consequuntur quam nulla. Molestiae ut quibusdam ratione voluptatem distinctio. Nam voluptas vero beatae est eos.\n\nQuis praesentium quia impedit. Enim voluptates est quas.',NULL,NULL,'Modi ipsam dignissimos nam unde eos totam. Sapiente fuga porro quidem et. Voluptate qui corporis error sint quam et. Excepturi ut ut temporibus et. Adipisci veritatis et praesentium mollitia.','ru_RU'),(97,33,'Off shoulder boho dress','off-shoulder-boho-dress','Voluptatum tempore ut voluptatem recusandae rerum quis et. Quisquam incidunt commodi ipsum architecto architecto. Laborum libero odit incidunt. Cumque ea quia qui laboriosam dolorum. Voluptatem harum sit magni architecto neque.\n\nNeque dicta aliquam laborum distinctio necessitatibus atque. Est eos ipsam repudiandae sit nihil tenetur quia architecto. Voluptatem architecto ipsum velit nobis omnis. Repellendus ratione et alias quis vitae autem ducimus provident.\n\nSint non facilis cumque numquam quas et mollitia nisi. Vero fugiat sint nulla et quia nam eos. In eum quasi dicta deserunt illo. Libero nostrum cum modi modi. Quaerat voluptatibus omnis nihil voluptatem odit asperiores.',NULL,NULL,'Ut aperiam consequatur molestiae ipsam voluptatem atque. Ut aspernatur autem ipsa quisquam consequuntur odit sunt. Quas et consectetur quae et. Eaque eveniet sequi natus et.','lt_LT'),(98,33,'Off shoulder boho dress','off-shoulder-boho-dress','Voluptatum tempore ut voluptatem recusandae rerum quis et. Quisquam incidunt commodi ipsum architecto architecto. Laborum libero odit incidunt. Cumque ea quia qui laboriosam dolorum. Voluptatem harum sit magni architecto neque.\n\nNeque dicta aliquam laborum distinctio necessitatibus atque. Est eos ipsam repudiandae sit nihil tenetur quia architecto. Voluptatem architecto ipsum velit nobis omnis. Repellendus ratione et alias quis vitae autem ducimus provident.\n\nSint non facilis cumque numquam quas et mollitia nisi. Vero fugiat sint nulla et quia nam eos. In eum quasi dicta deserunt illo. Libero nostrum cum modi modi. Quaerat voluptatibus omnis nihil voluptatem odit asperiores.',NULL,NULL,'Ut aperiam consequatur molestiae ipsam voluptatem atque. Ut aspernatur autem ipsa quisquam consequuntur odit sunt. Quas et consectetur quae et. Eaque eveniet sequi natus et.','en_US'),(99,33,'Off shoulder boho dress','off-shoulder-boho-dress','Voluptatum tempore ut voluptatem recusandae rerum quis et. Quisquam incidunt commodi ipsum architecto architecto. Laborum libero odit incidunt. Cumque ea quia qui laboriosam dolorum. Voluptatem harum sit magni architecto neque.\n\nNeque dicta aliquam laborum distinctio necessitatibus atque. Est eos ipsam repudiandae sit nihil tenetur quia architecto. Voluptatem architecto ipsum velit nobis omnis. Repellendus ratione et alias quis vitae autem ducimus provident.\n\nSint non facilis cumque numquam quas et mollitia nisi. Vero fugiat sint nulla et quia nam eos. In eum quasi dicta deserunt illo. Libero nostrum cum modi modi. Quaerat voluptatibus omnis nihil voluptatem odit asperiores.',NULL,NULL,'Ut aperiam consequatur molestiae ipsam voluptatem atque. Ut aspernatur autem ipsa quisquam consequuntur odit sunt. Quas et consectetur quae et. Eaque eveniet sequi natus et.','ru_RU'),(100,34,'Ruffle wrap festival dress','ruffle-wrap-festival-dress','Hic voluptatum et quo. Velit aut tenetur saepe aperiam. Est ut dolor nihil ad animi tenetur omnis ut. Alias aut aut sint magni.\n\nEum dolorum pariatur voluptate voluptatem itaque consequatur ut. Aspernatur ut tempora et natus qui non.\n\nPossimus quidem nihil qui eum corporis qui. Voluptatem ut alias laborum consequatur cumque libero itaque. Error et neque voluptas totam accusamus.',NULL,NULL,'Id reprehenderit alias incidunt aut molestias quia quia. Et reiciendis eos inventore omnis aut reiciendis. Unde voluptate beatae eum quasi ad repellat dolorum. Alias rerum iusto nemo reprehenderit molestiae perspiciatis velit consequuntur. Vitae quos hic consequatur qui.','lt_LT'),(101,34,'Ruffle wrap festival dress','ruffle-wrap-festival-dress','Hic voluptatum et quo. Velit aut tenetur saepe aperiam. Est ut dolor nihil ad animi tenetur omnis ut. Alias aut aut sint magni.\n\nEum dolorum pariatur voluptate voluptatem itaque consequatur ut. Aspernatur ut tempora et natus qui non.\n\nPossimus quidem nihil qui eum corporis qui. Voluptatem ut alias laborum consequatur cumque libero itaque. Error et neque voluptas totam accusamus.',NULL,NULL,'Id reprehenderit alias incidunt aut molestias quia quia. Et reiciendis eos inventore omnis aut reiciendis. Unde voluptate beatae eum quasi ad repellat dolorum. Alias rerum iusto nemo reprehenderit molestiae perspiciatis velit consequuntur. Vitae quos hic consequatur qui.','en_US'),(102,34,'Ruffle wrap festival dress','ruffle-wrap-festival-dress','Hic voluptatum et quo. Velit aut tenetur saepe aperiam. Est ut dolor nihil ad animi tenetur omnis ut. Alias aut aut sint magni.\n\nEum dolorum pariatur voluptate voluptatem itaque consequatur ut. Aspernatur ut tempora et natus qui non.\n\nPossimus quidem nihil qui eum corporis qui. Voluptatem ut alias laborum consequatur cumque libero itaque. Error et neque voluptas totam accusamus.',NULL,NULL,'Id reprehenderit alias incidunt aut molestias quia quia. Et reiciendis eos inventore omnis aut reiciendis. Unde voluptate beatae eum quasi ad repellat dolorum. Alias rerum iusto nemo reprehenderit molestiae perspiciatis velit consequuntur. Vitae quos hic consequatur qui.','ru_RU'),(103,35,'911M regular fit jeans','911m-regular-fit-jeans','Amet distinctio inventore aut molestiae ipsum nobis iste. Ut sit suscipit corporis et. Sint modi qui tempore et porro fuga nihil. Error placeat animi quaerat reprehenderit voluptas. Maiores nisi fuga quisquam non quos.\n\nRem consequatur architecto vitae nesciunt. Ratione illum ea quo qui cumque. Assumenda dignissimos veritatis expedita sed id.\n\nMollitia pariatur beatae architecto aut culpa. Adipisci ipsa quia sapiente autem. Doloremque rerum id et laborum. Ab quibusdam ipsa natus.',NULL,NULL,'Officia doloribus ut aut maxime. Provident omnis voluptas ullam molestias perferendis officiis. Qui dolorem quia voluptate minus.','lt_LT'),(104,35,'911M regular fit jeans','911m-regular-fit-jeans','Amet distinctio inventore aut molestiae ipsum nobis iste. Ut sit suscipit corporis et. Sint modi qui tempore et porro fuga nihil. Error placeat animi quaerat reprehenderit voluptas. Maiores nisi fuga quisquam non quos.\n\nRem consequatur architecto vitae nesciunt. Ratione illum ea quo qui cumque. Assumenda dignissimos veritatis expedita sed id.\n\nMollitia pariatur beatae architecto aut culpa. Adipisci ipsa quia sapiente autem. Doloremque rerum id et laborum. Ab quibusdam ipsa natus.',NULL,NULL,'Officia doloribus ut aut maxime. Provident omnis voluptas ullam molestias perferendis officiis. Qui dolorem quia voluptate minus.','en_US'),(105,35,'911M regular fit jeans','911m-regular-fit-jeans','Amet distinctio inventore aut molestiae ipsum nobis iste. Ut sit suscipit corporis et. Sint modi qui tempore et porro fuga nihil. Error placeat animi quaerat reprehenderit voluptas. Maiores nisi fuga quisquam non quos.\n\nRem consequatur architecto vitae nesciunt. Ratione illum ea quo qui cumque. Assumenda dignissimos veritatis expedita sed id.\n\nMollitia pariatur beatae architecto aut culpa. Adipisci ipsa quia sapiente autem. Doloremque rerum id et laborum. Ab quibusdam ipsa natus.',NULL,NULL,'Officia doloribus ut aut maxime. Provident omnis voluptas ullam molestias perferendis officiis. Qui dolorem quia voluptate minus.','ru_RU'),(106,36,'330M slim fit jeans','330m-slim-fit-jeans','Quidem ut voluptatibus consequatur sapiente vero. Dolorem culpa labore et ipsam quisquam voluptatem dolor. Aut qui rem quisquam qui.\n\nVelit eum quasi animi quibusdam fugit. Voluptatibus minima sit et nam dignissimos. Quod mollitia et et nobis unde sequi omnis ut. In eum mollitia sit repellendus suscipit.\n\nAut quo nemo minus quis est. Nostrum neque deserunt inventore est est fugit molestiae. Omnis omnis nesciunt consequatur praesentium eveniet et possimus. Dolorum maiores impedit ab autem omnis blanditiis excepturi.',NULL,NULL,'Quod qui consequatur sed. Qui est atque laborum magnam molestias cupiditate sunt quidem. Ea est ipsum fuga et omnis soluta qui.','lt_LT'),(107,36,'330M slim fit jeans','330m-slim-fit-jeans','Quidem ut voluptatibus consequatur sapiente vero. Dolorem culpa labore et ipsam quisquam voluptatem dolor. Aut qui rem quisquam qui.\n\nVelit eum quasi animi quibusdam fugit. Voluptatibus minima sit et nam dignissimos. Quod mollitia et et nobis unde sequi omnis ut. In eum mollitia sit repellendus suscipit.\n\nAut quo nemo minus quis est. Nostrum neque deserunt inventore est est fugit molestiae. Omnis omnis nesciunt consequatur praesentium eveniet et possimus. Dolorum maiores impedit ab autem omnis blanditiis excepturi.',NULL,NULL,'Quod qui consequatur sed. Qui est atque laborum magnam molestias cupiditate sunt quidem. Ea est ipsum fuga et omnis soluta qui.','en_US'),(108,36,'330M slim fit jeans','330m-slim-fit-jeans','Quidem ut voluptatibus consequatur sapiente vero. Dolorem culpa labore et ipsam quisquam voluptatem dolor. Aut qui rem quisquam qui.\n\nVelit eum quasi animi quibusdam fugit. Voluptatibus minima sit et nam dignissimos. Quod mollitia et et nobis unde sequi omnis ut. In eum mollitia sit repellendus suscipit.\n\nAut quo nemo minus quis est. Nostrum neque deserunt inventore est est fugit molestiae. Omnis omnis nesciunt consequatur praesentium eveniet et possimus. Dolorum maiores impedit ab autem omnis blanditiis excepturi.',NULL,NULL,'Quod qui consequatur sed. Qui est atque laborum magnam molestias cupiditate sunt quidem. Ea est ipsum fuga et omnis soluta qui.','ru_RU'),(109,37,'990M regular fit jeans','990m-regular-fit-jeans','Sapiente nihil quia velit quisquam sit magni. Enim rem aut quis deserunt eveniet illum sed. Qui quidem consequuntur sunt qui a distinctio a. Aut tempora ea magni.\n\nExpedita consequuntur quod omnis repudiandae cupiditate tempore. Temporibus explicabo ratione est mollitia aut.\n\nUt nesciunt et sed repellendus. Eos voluptas est possimus quo recusandae. Facere minima minus rem aut molestias in asperiores.',NULL,NULL,'Natus debitis assumenda non et eos et et. Rerum dolorem aut quia odit. Tenetur voluptatem magnam est velit. Possimus ea maxime aut officiis eveniet dicta at.','lt_LT'),(110,37,'990M regular fit jeans','990m-regular-fit-jeans','Sapiente nihil quia velit quisquam sit magni. Enim rem aut quis deserunt eveniet illum sed. Qui quidem consequuntur sunt qui a distinctio a. Aut tempora ea magni.\n\nExpedita consequuntur quod omnis repudiandae cupiditate tempore. Temporibus explicabo ratione est mollitia aut.\n\nUt nesciunt et sed repellendus. Eos voluptas est possimus quo recusandae. Facere minima minus rem aut molestias in asperiores.',NULL,NULL,'Natus debitis assumenda non et eos et et. Rerum dolorem aut quia odit. Tenetur voluptatem magnam est velit. Possimus ea maxime aut officiis eveniet dicta at.','en_US'),(111,37,'990M regular fit jeans','990m-regular-fit-jeans','Sapiente nihil quia velit quisquam sit magni. Enim rem aut quis deserunt eveniet illum sed. Qui quidem consequuntur sunt qui a distinctio a. Aut tempora ea magni.\n\nExpedita consequuntur quod omnis repudiandae cupiditate tempore. Temporibus explicabo ratione est mollitia aut.\n\nUt nesciunt et sed repellendus. Eos voluptas est possimus quo recusandae. Facere minima minus rem aut molestias in asperiores.',NULL,NULL,'Natus debitis assumenda non et eos et et. Rerum dolorem aut quia odit. Tenetur voluptatem magnam est velit. Possimus ea maxime aut officiis eveniet dicta at.','ru_RU'),(112,38,'007M black elegance jeans','007m-black-elegance-jeans','Voluptatem qui maiores voluptatem quod. Voluptatibus distinctio et in recusandae maiores quibusdam. Quaerat exercitationem cumque qui sunt.\n\nInventore rerum qui assumenda nostrum quibusdam voluptatem. Tempore consequatur sed aperiam repellendus. Beatae unde fuga qui.\n\nNeque doloremque illum qui vitae modi quae deserunt. Cum accusamus quidem at soluta qui. Omnis nobis dicta numquam. Provident voluptate corrupti vel praesentium ducimus debitis.',NULL,NULL,'Delectus nostrum minima cum at laborum est vel. Veritatis quam voluptas error maxime est. Odit et ipsam alias harum.','lt_LT'),(113,38,'007M black elegance jeans','007m-black-elegance-jeans','Voluptatem qui maiores voluptatem quod. Voluptatibus distinctio et in recusandae maiores quibusdam. Quaerat exercitationem cumque qui sunt.\n\nInventore rerum qui assumenda nostrum quibusdam voluptatem. Tempore consequatur sed aperiam repellendus. Beatae unde fuga qui.\n\nNeque doloremque illum qui vitae modi quae deserunt. Cum accusamus quidem at soluta qui. Omnis nobis dicta numquam. Provident voluptate corrupti vel praesentium ducimus debitis.',NULL,NULL,'Delectus nostrum minima cum at laborum est vel. Veritatis quam voluptas error maxime est. Odit et ipsam alias harum.','en_US'),(114,38,'007M black elegance jeans','007m-black-elegance-jeans','Voluptatem qui maiores voluptatem quod. Voluptatibus distinctio et in recusandae maiores quibusdam. Quaerat exercitationem cumque qui sunt.\n\nInventore rerum qui assumenda nostrum quibusdam voluptatem. Tempore consequatur sed aperiam repellendus. Beatae unde fuga qui.\n\nNeque doloremque illum qui vitae modi quae deserunt. Cum accusamus quidem at soluta qui. Omnis nobis dicta numquam. Provident voluptate corrupti vel praesentium ducimus debitis.',NULL,NULL,'Delectus nostrum minima cum at laborum est vel. Veritatis quam voluptas error maxime est. Odit et ipsam alias harum.','ru_RU'),(115,39,'727F patched cropped jeans','727f-patched-cropped-jeans','Nulla sequi ea saepe ab et. Nam delectus magnam eos impedit omnis quis. Et hic consequuntur ut qui molestias odio. Est odio sit molestias inventore maiores autem aut. Pariatur magnam nesciunt voluptatum cumque architecto et quas.\n\nPerferendis impedit maiores incidunt est repellat non. Adipisci quae eaque asperiores sint. In sequi debitis nulla velit.\n\nUllam reprehenderit nihil culpa eligendi rerum et. Ut eligendi magnam quo itaque. Corrupti omnis autem nisi ut eum at commodi. Quia voluptatem laboriosam minima temporibus.',NULL,NULL,'Eum aliquid earum asperiores iure ad fugiat earum. Et atque quia perspiciatis recusandae eius expedita.','lt_LT'),(116,39,'727F patched cropped jeans','727f-patched-cropped-jeans','Nulla sequi ea saepe ab et. Nam delectus magnam eos impedit omnis quis. Et hic consequuntur ut qui molestias odio. Est odio sit molestias inventore maiores autem aut. Pariatur magnam nesciunt voluptatum cumque architecto et quas.\n\nPerferendis impedit maiores incidunt est repellat non. Adipisci quae eaque asperiores sint. In sequi debitis nulla velit.\n\nUllam reprehenderit nihil culpa eligendi rerum et. Ut eligendi magnam quo itaque. Corrupti omnis autem nisi ut eum at commodi. Quia voluptatem laboriosam minima temporibus.',NULL,NULL,'Eum aliquid earum asperiores iure ad fugiat earum. Et atque quia perspiciatis recusandae eius expedita.','en_US'),(117,39,'727F patched cropped jeans','727f-patched-cropped-jeans','Nulla sequi ea saepe ab et. Nam delectus magnam eos impedit omnis quis. Et hic consequuntur ut qui molestias odio. Est odio sit molestias inventore maiores autem aut. Pariatur magnam nesciunt voluptatum cumque architecto et quas.\n\nPerferendis impedit maiores incidunt est repellat non. Adipisci quae eaque asperiores sint. In sequi debitis nulla velit.\n\nUllam reprehenderit nihil culpa eligendi rerum et. Ut eligendi magnam quo itaque. Corrupti omnis autem nisi ut eum at commodi. Quia voluptatem laboriosam minima temporibus.',NULL,NULL,'Eum aliquid earum asperiores iure ad fugiat earum. Et atque quia perspiciatis recusandae eius expedita.','ru_RU'),(118,40,'111F patched jeans with fancy badges','111f-patched-jeans-with-fancy-badges','Est aut consectetur explicabo dolorum recusandae sit natus. Quia quia vitae reprehenderit dolor iste ex asperiores. Mollitia dolore ullam consequatur earum. Sit repellat fuga nisi ducimus consequuntur id ipsum.\n\nEt dolore error deserunt neque libero quibusdam facilis quia. Id nihil voluptatem voluptatem nulla aspernatur. Doloribus consequatur in asperiores neque tempora sequi.\n\nEaque velit voluptas totam fugit sunt sit. Laboriosam aut dicta aut. Ut sed qui alias eum. Ab molestiae natus iure tenetur asperiores impedit velit.',NULL,NULL,'Corrupti accusamus quis consequatur minus mollitia soluta magni. Hic voluptatem aut nisi et suscipit qui. Voluptatem nostrum consequatur aspernatur libero sit. Autem provident voluptatem odit animi.','lt_LT'),(119,40,'111F patched jeans with fancy badges','111f-patched-jeans-with-fancy-badges','Est aut consectetur explicabo dolorum recusandae sit natus. Quia quia vitae reprehenderit dolor iste ex asperiores. Mollitia dolore ullam consequatur earum. Sit repellat fuga nisi ducimus consequuntur id ipsum.\n\nEt dolore error deserunt neque libero quibusdam facilis quia. Id nihil voluptatem voluptatem nulla aspernatur. Doloribus consequatur in asperiores neque tempora sequi.\n\nEaque velit voluptas totam fugit sunt sit. Laboriosam aut dicta aut. Ut sed qui alias eum. Ab molestiae natus iure tenetur asperiores impedit velit.',NULL,NULL,'Corrupti accusamus quis consequatur minus mollitia soluta magni. Hic voluptatem aut nisi et suscipit qui. Voluptatem nostrum consequatur aspernatur libero sit. Autem provident voluptatem odit animi.','en_US'),(120,40,'111F patched jeans with fancy badges','111f-patched-jeans-with-fancy-badges','Est aut consectetur explicabo dolorum recusandae sit natus. Quia quia vitae reprehenderit dolor iste ex asperiores. Mollitia dolore ullam consequatur earum. Sit repellat fuga nisi ducimus consequuntur id ipsum.\n\nEt dolore error deserunt neque libero quibusdam facilis quia. Id nihil voluptatem voluptatem nulla aspernatur. Doloribus consequatur in asperiores neque tempora sequi.\n\nEaque velit voluptas totam fugit sunt sit. Laboriosam aut dicta aut. Ut sed qui alias eum. Ab molestiae natus iure tenetur asperiores impedit velit.',NULL,NULL,'Corrupti accusamus quis consequatur minus mollitia soluta magni. Hic voluptatem aut nisi et suscipit qui. Voluptatem nostrum consequatur aspernatur libero sit. Autem provident voluptatem odit animi.','ru_RU'),(121,41,'000F office grey jeans','000f-office-grey-jeans','Vero sapiente sunt quia labore et quia nemo. Omnis quibusdam cupiditate et est est voluptatem ipsam quas. Excepturi aliquid illo eum dolore. Rerum et animi veniam sint non aperiam.\n\nNatus quos nesciunt facere et inventore est est aut. Dolor at est impedit enim sed et eligendi. Aut ut praesentium aut eos accusamus est voluptas voluptas. Quas consequatur fugit qui est voluptas sint ipsam enim. Sunt voluptate in officiis.\n\nQuaerat laborum eligendi voluptas ad laudantium facilis aut. Tenetur in consectetur et alias necessitatibus saepe eos. Hic non consequuntur eum architecto occaecati ut.',NULL,NULL,'Quis commodi consequatur quod quia nam quidem. Numquam error dolorem dolores quisquam eligendi earum. Vel quo ut aut facere nemo maiores. Numquam hic labore repudiandae corporis dolor ea voluptas.','lt_LT'),(122,41,'000F office grey jeans','000f-office-grey-jeans','Vero sapiente sunt quia labore et quia nemo. Omnis quibusdam cupiditate et est est voluptatem ipsam quas. Excepturi aliquid illo eum dolore. Rerum et animi veniam sint non aperiam.\n\nNatus quos nesciunt facere et inventore est est aut. Dolor at est impedit enim sed et eligendi. Aut ut praesentium aut eos accusamus est voluptas voluptas. Quas consequatur fugit qui est voluptas sint ipsam enim. Sunt voluptate in officiis.\n\nQuaerat laborum eligendi voluptas ad laudantium facilis aut. Tenetur in consectetur et alias necessitatibus saepe eos. Hic non consequuntur eum architecto occaecati ut.',NULL,NULL,'Quis commodi consequatur quod quia nam quidem. Numquam error dolorem dolores quisquam eligendi earum. Vel quo ut aut facere nemo maiores. Numquam hic labore repudiandae corporis dolor ea voluptas.','en_US'),(123,41,'000F office grey jeans','000f-office-grey-jeans','Vero sapiente sunt quia labore et quia nemo. Omnis quibusdam cupiditate et est est voluptatem ipsam quas. Excepturi aliquid illo eum dolore. Rerum et animi veniam sint non aperiam.\n\nNatus quos nesciunt facere et inventore est est aut. Dolor at est impedit enim sed et eligendi. Aut ut praesentium aut eos accusamus est voluptas voluptas. Quas consequatur fugit qui est voluptas sint ipsam enim. Sunt voluptate in officiis.\n\nQuaerat laborum eligendi voluptas ad laudantium facilis aut. Tenetur in consectetur et alias necessitatibus saepe eos. Hic non consequuntur eum architecto occaecati ut.',NULL,NULL,'Quis commodi consequatur quod quia nam quidem. Numquam error dolorem dolores quisquam eligendi earum. Vel quo ut aut facere nemo maiores. Numquam hic labore repudiandae corporis dolor ea voluptas.','ru_RU'),(124,42,'666F boyfriend jeans with rips','666f-boyfriend-jeans-with-rips','Facere odio veritatis molestias velit consequatur non. Necessitatibus corporis vitae ut enim. Nisi aut iusto voluptatem in.\n\nEsse sed voluptas ut corrupti voluptatem. Natus nobis eos quisquam qui voluptatem. Repellendus corporis ipsum sequi ab est necessitatibus. Animi inventore architecto necessitatibus magni est qui.\n\nBeatae qui esse nobis voluptatum dignissimos. Facere dicta inventore voluptatem dolores porro tenetur reiciendis.',NULL,NULL,'Ad fugit odio ullam eum eos. Vel velit facere recusandae minus maxime quia et veniam. Dolor ea at laborum recusandae rerum.','lt_LT'),(125,42,'666F boyfriend jeans with rips','666f-boyfriend-jeans-with-rips','Facere odio veritatis molestias velit consequatur non. Necessitatibus corporis vitae ut enim. Nisi aut iusto voluptatem in.\n\nEsse sed voluptas ut corrupti voluptatem. Natus nobis eos quisquam qui voluptatem. Repellendus corporis ipsum sequi ab est necessitatibus. Animi inventore architecto necessitatibus magni est qui.\n\nBeatae qui esse nobis voluptatum dignissimos. Facere dicta inventore voluptatem dolores porro tenetur reiciendis.',NULL,NULL,'Ad fugit odio ullam eum eos. Vel velit facere recusandae minus maxime quia et veniam. Dolor ea at laborum recusandae rerum.','en_US'),(126,42,'666F boyfriend jeans with rips','666f-boyfriend-jeans-with-rips','Facere odio veritatis molestias velit consequatur non. Necessitatibus corporis vitae ut enim. Nisi aut iusto voluptatem in.\n\nEsse sed voluptas ut corrupti voluptatem. Natus nobis eos quisquam qui voluptatem. Repellendus corporis ipsum sequi ab est necessitatibus. Animi inventore architecto necessitatibus magni est qui.\n\nBeatae qui esse nobis voluptatum dignissimos. Facere dicta inventore voluptatem dolores porro tenetur reiciendis.',NULL,NULL,'Ad fugit odio ullam eum eos. Vel velit facere recusandae minus maxime quia et veniam. Dolor ea at laborum recusandae rerum.','ru_RU');
/*!40000 ALTER TABLE `sylius_product_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_variant`
--

DROP TABLE IF EXISTS `sylius_product_variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `tax_category_id` int(11) DEFAULT NULL,
  `shipping_category_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT 1,
  `on_hold` int(11) NOT NULL,
  `on_hand` int(11) NOT NULL,
  `tracked` tinyint(1) NOT NULL,
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `depth` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `shipping_required` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A29B52377153098` (`code`),
  KEY `IDX_A29B5234584665A` (`product_id`),
  KEY `IDX_A29B5239DF894ED` (`tax_category_id`),
  KEY `IDX_A29B5239E2D1A41` (`shipping_category_id`),
  CONSTRAINT `FK_A29B5234584665A` FOREIGN KEY (`product_id`) REFERENCES `sylius_product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A29B5239DF894ED` FOREIGN KEY (`tax_category_id`) REFERENCES `sylius_tax_category` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_A29B5239E2D1A41` FOREIGN KEY (`shipping_category_id`) REFERENCES `sylius_shipping_category` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_variant`
--

LOCK TABLES `sylius_product_variant` WRITE;
/*!40000 ALTER TABLE `sylius_product_variant` DISABLE KEYS */;
INSERT INTO `sylius_product_variant` VALUES (120,22,3,NULL,'Everyday_white_basic_T_Shirt-variant-0','2022-03-08 11:16:35','2022-03-08 11:16:35',0,1,0,9,0,NULL,NULL,NULL,NULL,1),(121,22,3,NULL,'Everyday_white_basic_T_Shirt-variant-1','2022-03-08 11:16:35','2022-03-08 11:16:35',1,1,0,0,0,NULL,NULL,NULL,NULL,1),(122,22,3,NULL,'Everyday_white_basic_T_Shirt-variant-2','2022-03-08 11:16:35','2022-03-08 11:16:35',2,1,0,1,0,NULL,NULL,NULL,NULL,1),(123,22,3,NULL,'Everyday_white_basic_T_Shirt-variant-3','2022-03-08 11:16:35','2022-03-08 11:16:35',3,1,0,5,0,NULL,NULL,NULL,NULL,1),(124,22,3,NULL,'Everyday_white_basic_T_Shirt-variant-4','2022-03-08 11:16:35','2022-03-08 11:16:35',4,1,0,6,0,NULL,NULL,NULL,NULL,1),(125,23,3,NULL,'Loose_white_designer_T_Shirt-variant-0','2022-03-08 11:16:35','2022-03-08 11:16:35',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(126,23,3,NULL,'Loose_white_designer_T_Shirt-variant-1','2022-03-08 11:16:35','2022-03-08 11:16:35',1,1,0,6,0,NULL,NULL,NULL,NULL,1),(127,23,3,NULL,'Loose_white_designer_T_Shirt-variant-2','2022-03-08 11:16:35','2022-03-08 11:16:35',2,1,0,2,0,NULL,NULL,NULL,NULL,1),(128,23,3,NULL,'Loose_white_designer_T_Shirt-variant-3','2022-03-08 11:16:35','2022-03-08 11:16:35',3,1,0,4,0,NULL,NULL,NULL,NULL,1),(129,23,3,NULL,'Loose_white_designer_T_Shirt-variant-4','2022-03-08 11:16:35','2022-03-08 11:16:35',4,1,0,6,0,NULL,NULL,NULL,NULL,1),(130,24,3,NULL,'Ribbed_copper_slim_fit_Tee-variant-0','2022-03-08 11:16:35','2022-03-08 11:16:35',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(131,24,3,NULL,'Ribbed_copper_slim_fit_Tee-variant-1','2022-03-08 11:16:35','2022-03-08 11:16:35',1,1,0,3,0,NULL,NULL,NULL,NULL,1),(132,24,3,NULL,'Ribbed_copper_slim_fit_Tee-variant-2','2022-03-08 11:16:35','2022-03-08 11:16:35',2,1,0,5,0,NULL,NULL,NULL,NULL,1),(133,24,3,NULL,'Ribbed_copper_slim_fit_Tee-variant-3','2022-03-08 11:16:35','2022-03-08 11:16:35',3,1,0,5,0,NULL,NULL,NULL,NULL,1),(134,24,3,NULL,'Ribbed_copper_slim_fit_Tee-variant-4','2022-03-08 11:16:35','2022-03-08 11:16:35',4,1,0,4,0,NULL,NULL,NULL,NULL,1),(135,25,3,NULL,'Sport_basic_white_T_Shirt-variant-0','2022-03-08 11:16:35','2022-03-08 11:16:35',0,1,0,0,0,NULL,NULL,NULL,NULL,1),(136,25,3,NULL,'Sport_basic_white_T_Shirt-variant-1','2022-03-08 11:16:35','2022-03-08 11:16:35',1,1,0,2,0,NULL,NULL,NULL,NULL,1),(137,25,3,NULL,'Sport_basic_white_T_Shirt-variant-2','2022-03-08 11:16:35','2022-03-08 11:16:35',2,1,0,7,0,NULL,NULL,NULL,NULL,1),(138,25,3,NULL,'Sport_basic_white_T_Shirt-variant-3','2022-03-08 11:16:35','2022-03-08 11:16:35',3,1,0,0,0,NULL,NULL,NULL,NULL,1),(139,25,3,NULL,'Sport_basic_white_T_Shirt-variant-4','2022-03-08 11:16:35','2022-03-08 11:16:35',4,1,0,1,0,NULL,NULL,NULL,NULL,1),(140,26,3,NULL,'Raglan_grey_&_black_Tee-variant-0','2022-03-08 11:16:35','2022-03-08 11:16:36',0,1,0,5,0,NULL,NULL,NULL,NULL,1),(141,26,3,NULL,'Raglan_grey_&_black_Tee-variant-1','2022-03-08 11:16:35','2022-03-08 11:16:36',1,1,0,9,0,NULL,NULL,NULL,NULL,1),(142,26,3,NULL,'Raglan_grey_&_black_Tee-variant-2','2022-03-08 11:16:36','2022-03-08 11:16:36',2,1,0,8,0,NULL,NULL,NULL,NULL,1),(143,26,3,NULL,'Raglan_grey_&_black_Tee-variant-3','2022-03-08 11:16:36','2022-03-08 11:16:36',3,1,0,6,0,NULL,NULL,NULL,NULL,1),(144,26,3,NULL,'Raglan_grey_&_black_Tee-variant-4','2022-03-08 11:16:36','2022-03-08 11:16:36',4,1,0,3,0,NULL,NULL,NULL,NULL,1),(145,27,3,NULL,'Oversize_white_cotton_T_Shirt-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(146,27,3,NULL,'Oversize_white_cotton_T_Shirt-variant-1','2022-03-08 11:16:36','2022-03-08 11:16:36',1,1,0,7,0,NULL,NULL,NULL,NULL,1),(147,27,3,NULL,'Oversize_white_cotton_T_Shirt-variant-2','2022-03-08 11:16:36','2022-03-08 11:16:36',2,1,0,3,0,NULL,NULL,NULL,NULL,1),(148,27,3,NULL,'Oversize_white_cotton_T_Shirt-variant-3','2022-03-08 11:16:36','2022-03-08 11:16:36',3,1,0,0,0,NULL,NULL,NULL,NULL,1),(149,27,3,NULL,'Oversize_white_cotton_T_Shirt-variant-4','2022-03-08 11:16:36','2022-03-08 11:16:36',4,1,0,6,0,NULL,NULL,NULL,NULL,1),(150,28,4,NULL,'Knitted_burgundy_winter_cap-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(151,29,4,NULL,'Knitted_wool_blend_green_cap-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,7,0,NULL,NULL,NULL,NULL,1),(152,30,4,NULL,'Knitted_white_pompom_cap-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,2,0,NULL,NULL,NULL,NULL,1),(153,31,4,NULL,'Cashmere_blend_violet_beanie-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,0,0,NULL,NULL,NULL,NULL,1),(154,32,3,NULL,'Beige_strappy_summer_dress-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,7,0,NULL,NULL,NULL,NULL,1),(155,32,3,NULL,'Beige_strappy_summer_dress-variant-1','2022-03-08 11:16:36','2022-03-08 11:16:36',1,1,0,0,0,NULL,NULL,NULL,NULL,1),(156,32,3,NULL,'Beige_strappy_summer_dress-variant-2','2022-03-08 11:16:36','2022-03-08 11:16:36',2,1,0,1,0,NULL,NULL,NULL,NULL,1),(157,32,3,NULL,'Beige_strappy_summer_dress-variant-3','2022-03-08 11:16:36','2022-03-08 11:16:36',3,1,0,9,0,NULL,NULL,NULL,NULL,1),(158,32,3,NULL,'Beige_strappy_summer_dress-variant-4','2022-03-08 11:16:36','2022-03-08 11:16:36',4,1,0,7,0,NULL,NULL,NULL,NULL,1),(159,32,3,NULL,'Beige_strappy_summer_dress-variant-5','2022-03-08 11:16:36','2022-03-08 11:16:36',5,1,0,6,0,NULL,NULL,NULL,NULL,1),(160,32,3,NULL,'Beige_strappy_summer_dress-variant-6','2022-03-08 11:16:36','2022-03-08 11:16:36',6,1,0,0,0,NULL,NULL,NULL,NULL,1),(161,32,3,NULL,'Beige_strappy_summer_dress-variant-7','2022-03-08 11:16:36','2022-03-08 11:16:36',7,1,0,5,0,NULL,NULL,NULL,NULL,1),(162,32,3,NULL,'Beige_strappy_summer_dress-variant-8','2022-03-08 11:16:36','2022-03-08 11:16:36',8,1,0,3,0,NULL,NULL,NULL,NULL,1),(163,32,3,NULL,'Beige_strappy_summer_dress-variant-9','2022-03-08 11:16:36','2022-03-08 11:16:36',9,1,0,4,0,NULL,NULL,NULL,NULL,1),(164,32,3,NULL,'Beige_strappy_summer_dress-variant-10','2022-03-08 11:16:36','2022-03-08 11:16:36',10,1,0,2,0,NULL,NULL,NULL,NULL,1),(165,32,3,NULL,'Beige_strappy_summer_dress-variant-11','2022-03-08 11:16:36','2022-03-08 11:16:36',11,1,0,7,0,NULL,NULL,NULL,NULL,1),(166,32,3,NULL,'Beige_strappy_summer_dress-variant-12','2022-03-08 11:16:36','2022-03-08 11:16:36',12,1,0,4,0,NULL,NULL,NULL,NULL,1),(167,32,3,NULL,'Beige_strappy_summer_dress-variant-13','2022-03-08 11:16:36','2022-03-08 11:16:36',13,1,0,3,0,NULL,NULL,NULL,NULL,1),(168,32,3,NULL,'Beige_strappy_summer_dress-variant-14','2022-03-08 11:16:36','2022-03-08 11:16:36',14,1,0,2,0,NULL,NULL,NULL,NULL,1),(169,33,3,NULL,'Off_shoulder_boho_dress-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,2,0,NULL,NULL,NULL,NULL,1),(170,33,3,NULL,'Off_shoulder_boho_dress-variant-1','2022-03-08 11:16:36','2022-03-08 11:16:36',1,1,0,1,0,NULL,NULL,NULL,NULL,1),(171,33,3,NULL,'Off_shoulder_boho_dress-variant-2','2022-03-08 11:16:36','2022-03-08 11:16:36',2,1,0,8,0,NULL,NULL,NULL,NULL,1),(172,33,3,NULL,'Off_shoulder_boho_dress-variant-3','2022-03-08 11:16:36','2022-03-08 11:16:36',3,1,0,7,0,NULL,NULL,NULL,NULL,1),(173,33,3,NULL,'Off_shoulder_boho_dress-variant-4','2022-03-08 11:16:36','2022-03-08 11:16:36',4,1,0,9,0,NULL,NULL,NULL,NULL,1),(174,33,3,NULL,'Off_shoulder_boho_dress-variant-5','2022-03-08 11:16:36','2022-03-08 11:16:36',5,1,0,8,0,NULL,NULL,NULL,NULL,1),(175,33,3,NULL,'Off_shoulder_boho_dress-variant-6','2022-03-08 11:16:36','2022-03-08 11:16:36',6,1,0,6,0,NULL,NULL,NULL,NULL,1),(176,33,3,NULL,'Off_shoulder_boho_dress-variant-7','2022-03-08 11:16:36','2022-03-08 11:16:36',7,1,0,5,0,NULL,NULL,NULL,NULL,1),(177,33,3,NULL,'Off_shoulder_boho_dress-variant-8','2022-03-08 11:16:36','2022-03-08 11:16:36',8,1,0,9,0,NULL,NULL,NULL,NULL,1),(178,33,3,NULL,'Off_shoulder_boho_dress-variant-9','2022-03-08 11:16:36','2022-03-08 11:16:36',9,1,0,3,0,NULL,NULL,NULL,NULL,1),(179,33,3,NULL,'Off_shoulder_boho_dress-variant-10','2022-03-08 11:16:36','2022-03-08 11:16:36',10,1,0,1,0,NULL,NULL,NULL,NULL,1),(180,33,3,NULL,'Off_shoulder_boho_dress-variant-11','2022-03-08 11:16:36','2022-03-08 11:16:36',11,1,0,2,0,NULL,NULL,NULL,NULL,1),(181,33,3,NULL,'Off_shoulder_boho_dress-variant-12','2022-03-08 11:16:36','2022-03-08 11:16:36',12,1,0,7,0,NULL,NULL,NULL,NULL,1),(182,33,3,NULL,'Off_shoulder_boho_dress-variant-13','2022-03-08 11:16:36','2022-03-08 11:16:36',13,1,0,8,0,NULL,NULL,NULL,NULL,1),(183,33,3,NULL,'Off_shoulder_boho_dress-variant-14','2022-03-08 11:16:36','2022-03-08 11:16:36',14,1,0,7,0,NULL,NULL,NULL,NULL,1),(184,34,3,NULL,'Ruffle_wrap_festival_dress-variant-0','2022-03-08 11:16:36','2022-03-08 11:16:36',0,1,0,1,0,NULL,NULL,NULL,NULL,1),(185,34,3,NULL,'Ruffle_wrap_festival_dress-variant-1','2022-03-08 11:16:36','2022-03-08 11:16:36',1,1,0,6,0,NULL,NULL,NULL,NULL,1),(186,34,3,NULL,'Ruffle_wrap_festival_dress-variant-2','2022-03-08 11:16:36','2022-03-08 11:16:36',2,1,0,4,0,NULL,NULL,NULL,NULL,1),(187,34,3,NULL,'Ruffle_wrap_festival_dress-variant-3','2022-03-08 11:16:36','2022-03-08 11:16:36',3,1,0,5,0,NULL,NULL,NULL,NULL,1),(188,34,3,NULL,'Ruffle_wrap_festival_dress-variant-4','2022-03-08 11:16:36','2022-03-08 11:16:36',4,1,0,0,0,NULL,NULL,NULL,NULL,1),(189,34,3,NULL,'Ruffle_wrap_festival_dress-variant-5','2022-03-08 11:16:36','2022-03-08 11:16:36',5,1,0,3,0,NULL,NULL,NULL,NULL,1),(190,34,3,NULL,'Ruffle_wrap_festival_dress-variant-6','2022-03-08 11:16:36','2022-03-08 11:16:36',6,1,0,0,0,NULL,NULL,NULL,NULL,1),(191,34,3,NULL,'Ruffle_wrap_festival_dress-variant-7','2022-03-08 11:16:36','2022-03-08 11:16:36',7,1,0,7,0,NULL,NULL,NULL,NULL,1),(192,34,3,NULL,'Ruffle_wrap_festival_dress-variant-8','2022-03-08 11:16:36','2022-03-08 11:16:36',8,1,0,6,0,NULL,NULL,NULL,NULL,1),(193,34,3,NULL,'Ruffle_wrap_festival_dress-variant-9','2022-03-08 11:16:36','2022-03-08 11:16:36',9,1,0,1,0,NULL,NULL,NULL,NULL,1),(194,34,3,NULL,'Ruffle_wrap_festival_dress-variant-10','2022-03-08 11:16:36','2022-03-08 11:16:36',10,1,0,6,0,NULL,NULL,NULL,NULL,1),(195,34,3,NULL,'Ruffle_wrap_festival_dress-variant-11','2022-03-08 11:16:36','2022-03-08 11:16:36',11,1,0,2,0,NULL,NULL,NULL,NULL,1),(196,34,3,NULL,'Ruffle_wrap_festival_dress-variant-12','2022-03-08 11:16:36','2022-03-08 11:16:36',12,1,0,1,0,NULL,NULL,NULL,NULL,1),(197,34,3,NULL,'Ruffle_wrap_festival_dress-variant-13','2022-03-08 11:16:36','2022-03-08 11:16:36',13,1,0,2,0,NULL,NULL,NULL,NULL,1),(198,34,3,NULL,'Ruffle_wrap_festival_dress-variant-14','2022-03-08 11:16:36','2022-03-08 11:16:36',14,1,0,8,0,NULL,NULL,NULL,NULL,1),(199,35,3,NULL,'911M_regular_fit_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,1,0,NULL,NULL,NULL,NULL,1),(200,35,3,NULL,'911M_regular_fit_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,3,0,NULL,NULL,NULL,NULL,1),(201,35,3,NULL,'911M_regular_fit_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,3,0,NULL,NULL,NULL,NULL,1),(202,35,3,NULL,'911M_regular_fit_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,1,0,NULL,NULL,NULL,NULL,1),(203,35,3,NULL,'911M_regular_fit_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,9,0,NULL,NULL,NULL,NULL,1),(204,36,3,NULL,'330M_slim_fit_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,2,0,NULL,NULL,NULL,NULL,1),(205,36,3,NULL,'330M_slim_fit_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,5,0,NULL,NULL,NULL,NULL,1),(206,36,3,NULL,'330M_slim_fit_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,7,0,NULL,NULL,NULL,NULL,1),(207,36,3,NULL,'330M_slim_fit_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,4,0,NULL,NULL,NULL,NULL,1),(208,36,3,NULL,'330M_slim_fit_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,2,0,NULL,NULL,NULL,NULL,1),(209,37,3,NULL,'990M_regular_fit_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,2,0,NULL,NULL,NULL,NULL,1),(210,37,3,NULL,'990M_regular_fit_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,1,0,NULL,NULL,NULL,NULL,1),(211,37,3,NULL,'990M_regular_fit_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,2,0,NULL,NULL,NULL,NULL,1),(212,37,3,NULL,'990M_regular_fit_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,9,0,NULL,NULL,NULL,NULL,1),(213,37,3,NULL,'990M_regular_fit_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,0,0,NULL,NULL,NULL,NULL,1),(214,38,3,NULL,'007M_black_elegance_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,0,0,NULL,NULL,NULL,NULL,1),(215,38,3,NULL,'007M_black_elegance_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,3,0,NULL,NULL,NULL,NULL,1),(216,38,3,NULL,'007M_black_elegance_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,4,0,NULL,NULL,NULL,NULL,1),(217,38,3,NULL,'007M_black_elegance_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,1,0,NULL,NULL,NULL,NULL,1),(218,38,3,NULL,'007M_black_elegance_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,1,0,NULL,NULL,NULL,NULL,1),(219,39,3,NULL,'727F_patched_cropped_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(220,39,3,NULL,'727F_patched_cropped_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,0,0,NULL,NULL,NULL,NULL,1),(221,39,3,NULL,'727F_patched_cropped_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,1,0,NULL,NULL,NULL,NULL,1),(222,39,3,NULL,'727F_patched_cropped_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,4,0,NULL,NULL,NULL,NULL,1),(223,39,3,NULL,'727F_patched_cropped_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,0,0,NULL,NULL,NULL,NULL,1),(224,40,3,NULL,'111F_patched_jeans_with_fancy_badges-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,4,0,NULL,NULL,NULL,NULL,1),(225,40,3,NULL,'111F_patched_jeans_with_fancy_badges-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,2,0,NULL,NULL,NULL,NULL,1),(226,40,3,NULL,'111F_patched_jeans_with_fancy_badges-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,8,0,NULL,NULL,NULL,NULL,1),(227,40,3,NULL,'111F_patched_jeans_with_fancy_badges-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,5,0,NULL,NULL,NULL,NULL,1),(228,40,3,NULL,'111F_patched_jeans_with_fancy_badges-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,6,0,NULL,NULL,NULL,NULL,1),(229,41,3,NULL,'000F_office_grey_jeans-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,2,0,NULL,NULL,NULL,NULL,1),(230,41,3,NULL,'000F_office_grey_jeans-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,8,0,NULL,NULL,NULL,NULL,1),(231,41,3,NULL,'000F_office_grey_jeans-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,1,0,NULL,NULL,NULL,NULL,1),(232,41,3,NULL,'000F_office_grey_jeans-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,9,0,NULL,NULL,NULL,NULL,1),(233,41,3,NULL,'000F_office_grey_jeans-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,9,0,NULL,NULL,NULL,NULL,1),(234,42,3,NULL,'666F_boyfriend_jeans_with_rips-variant-0','2022-03-08 11:16:37','2022-03-08 11:16:37',0,1,0,8,0,NULL,NULL,NULL,NULL,1),(235,42,3,NULL,'666F_boyfriend_jeans_with_rips-variant-1','2022-03-08 11:16:37','2022-03-08 11:16:37',1,1,0,0,0,NULL,NULL,NULL,NULL,1),(236,42,3,NULL,'666F_boyfriend_jeans_with_rips-variant-2','2022-03-08 11:16:37','2022-03-08 11:16:37',2,1,0,7,0,NULL,NULL,NULL,NULL,1),(237,42,3,NULL,'666F_boyfriend_jeans_with_rips-variant-3','2022-03-08 11:16:37','2022-03-08 11:16:37',3,1,0,3,0,NULL,NULL,NULL,NULL,1),(238,42,3,NULL,'666F_boyfriend_jeans_with_rips-variant-4','2022-03-08 11:16:37','2022-03-08 11:16:37',4,1,0,2,0,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `sylius_product_variant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_variant_option_value`
--

DROP TABLE IF EXISTS `sylius_product_variant_option_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_variant_option_value` (
  `variant_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  PRIMARY KEY (`variant_id`,`option_value_id`),
  KEY `IDX_76CDAFA13B69A9AF` (`variant_id`),
  KEY `IDX_76CDAFA1D957CA06` (`option_value_id`),
  CONSTRAINT `FK_76CDAFA13B69A9AF` FOREIGN KEY (`variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_76CDAFA1D957CA06` FOREIGN KEY (`option_value_id`) REFERENCES `sylius_product_option_value` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_variant_option_value`
--

LOCK TABLES `sylius_product_variant_option_value` WRITE;
/*!40000 ALTER TABLE `sylius_product_variant_option_value` DISABLE KEYS */;
INSERT INTO `sylius_product_variant_option_value` VALUES (120,19),(121,20),(122,21),(123,22),(124,23),(125,19),(126,20),(127,21),(128,22),(129,23),(130,19),(131,20),(132,21),(133,22),(134,23),(135,19),(136,20),(137,21),(138,22),(139,23),(140,19),(141,20),(142,21),(143,22),(144,23),(145,19),(146,20),(147,21),(148,22),(149,23),(154,24),(154,29),(155,24),(155,30),(156,24),(156,31),(157,25),(157,29),(158,25),(158,30),(159,25),(159,31),(160,26),(160,29),(161,26),(161,30),(162,26),(162,31),(163,27),(163,29),(164,27),(164,30),(165,27),(165,31),(166,28),(166,29),(167,28),(167,30),(168,28),(168,31),(169,24),(169,29),(170,24),(170,30),(171,24),(171,31),(172,25),(172,29),(173,25),(173,30),(174,25),(174,31),(175,26),(175,29),(176,26),(176,30),(177,26),(177,31),(178,27),(178,29),(179,27),(179,30),(180,27),(180,31),(181,28),(181,29),(182,28),(182,30),(183,28),(183,31),(184,24),(184,29),(185,24),(185,30),(186,24),(186,31),(187,25),(187,29),(188,25),(188,30),(189,25),(189,31),(190,26),(190,29),(191,26),(191,30),(192,26),(192,31),(193,27),(193,29),(194,27),(194,30),(195,27),(195,31),(196,28),(196,29),(197,28),(197,30),(198,28),(198,31),(199,32),(200,33),(201,34),(202,35),(203,36),(204,32),(205,33),(206,34),(207,35),(208,36),(209,32),(210,33),(211,34),(212,35),(213,36),(214,32),(215,33),(216,34),(217,35),(218,36),(219,32),(220,33),(221,34),(222,35),(223,36),(224,32),(225,33),(226,34),(227,35),(228,36),(229,32),(230,33),(231,34),(232,35),(233,36),(234,32),(235,33),(236,34),(237,35),(238,36);
/*!40000 ALTER TABLE `sylius_product_variant_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_variant_translation`
--

DROP TABLE IF EXISTS `sylius_product_variant_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_variant_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_product_variant_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_8DC18EDC2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_8DC18EDC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_variant_translation`
--

LOCK TABLES `sylius_product_variant_translation` WRITE;
/*!40000 ALTER TABLE `sylius_product_variant_translation` DISABLE KEYS */;
INSERT INTO `sylius_product_variant_translation` VALUES (120,120,'S','lt_LT'),(121,121,'M','lt_LT'),(122,122,'L','lt_LT'),(123,123,'XL','lt_LT'),(124,124,'XXL','lt_LT'),(125,125,'S','lt_LT'),(126,126,'M','lt_LT'),(127,127,'L','lt_LT'),(128,128,'XL','lt_LT'),(129,129,'XXL','lt_LT'),(130,130,'S','lt_LT'),(131,131,'M','lt_LT'),(132,132,'L','lt_LT'),(133,133,'XL','lt_LT'),(134,134,'XXL','lt_LT'),(135,135,'S','lt_LT'),(136,136,'M','lt_LT'),(137,137,'L','lt_LT'),(138,138,'XL','lt_LT'),(139,139,'XXL','lt_LT'),(140,140,'S','lt_LT'),(141,141,'M','lt_LT'),(142,142,'L','lt_LT'),(143,143,'XL','lt_LT'),(144,144,'XXL','lt_LT'),(145,145,'S','lt_LT'),(146,146,'M','lt_LT'),(147,147,'L','lt_LT'),(148,148,'XL','lt_LT'),(149,149,'XXL','lt_LT'),(150,150,'','lt_LT'),(151,151,'','lt_LT'),(152,152,'','lt_LT'),(153,153,'','lt_LT'),(154,154,'S Petite','lt_LT'),(155,155,'S Regular','lt_LT'),(156,156,'S Tall','lt_LT'),(157,157,'M Petite','lt_LT'),(158,158,'M Regular','lt_LT'),(159,159,'M Tall','lt_LT'),(160,160,'L Petite','lt_LT'),(161,161,'L Regular','lt_LT'),(162,162,'L Tall','lt_LT'),(163,163,'XL Petite','lt_LT'),(164,164,'XL Regular','lt_LT'),(165,165,'XL Tall','lt_LT'),(166,166,'XXL Petite','lt_LT'),(167,167,'XXL Regular','lt_LT'),(168,168,'XXL Tall','lt_LT'),(169,169,'S Petite','lt_LT'),(170,170,'S Regular','lt_LT'),(171,171,'S Tall','lt_LT'),(172,172,'M Petite','lt_LT'),(173,173,'M Regular','lt_LT'),(174,174,'M Tall','lt_LT'),(175,175,'L Petite','lt_LT'),(176,176,'L Regular','lt_LT'),(177,177,'L Tall','lt_LT'),(178,178,'XL Petite','lt_LT'),(179,179,'XL Regular','lt_LT'),(180,180,'XL Tall','lt_LT'),(181,181,'XXL Petite','lt_LT'),(182,182,'XXL Regular','lt_LT'),(183,183,'XXL Tall','lt_LT'),(184,184,'S Petite','lt_LT'),(185,185,'S Regular','lt_LT'),(186,186,'S Tall','lt_LT'),(187,187,'M Petite','lt_LT'),(188,188,'M Regular','lt_LT'),(189,189,'M Tall','lt_LT'),(190,190,'L Petite','lt_LT'),(191,191,'L Regular','lt_LT'),(192,192,'L Tall','lt_LT'),(193,193,'XL Petite','lt_LT'),(194,194,'XL Regular','lt_LT'),(195,195,'XL Tall','lt_LT'),(196,196,'XXL Petite','lt_LT'),(197,197,'XXL Regular','lt_LT'),(198,198,'XXL Tall','lt_LT'),(199,199,'S','lt_LT'),(200,200,'M','lt_LT'),(201,201,'L','lt_LT'),(202,202,'XL','lt_LT'),(203,203,'XXL','lt_LT'),(204,204,'S','lt_LT'),(205,205,'M','lt_LT'),(206,206,'L','lt_LT'),(207,207,'XL','lt_LT'),(208,208,'XXL','lt_LT'),(209,209,'S','lt_LT'),(210,210,'M','lt_LT'),(211,211,'L','lt_LT'),(212,212,'XL','lt_LT'),(213,213,'XXL','lt_LT'),(214,214,'S','lt_LT'),(215,215,'M','lt_LT'),(216,216,'L','lt_LT'),(217,217,'XL','lt_LT'),(218,218,'XXL','lt_LT'),(219,219,'S','lt_LT'),(220,220,'M','lt_LT'),(221,221,'L','lt_LT'),(222,222,'XL','lt_LT'),(223,223,'XXL','lt_LT'),(224,224,'S','lt_LT'),(225,225,'M','lt_LT'),(226,226,'L','lt_LT'),(227,227,'XL','lt_LT'),(228,228,'XXL','lt_LT'),(229,229,'S','lt_LT'),(230,230,'M','lt_LT'),(231,231,'L','lt_LT'),(232,232,'XL','lt_LT'),(233,233,'XXL','lt_LT'),(234,234,'S','lt_LT'),(235,235,'M','lt_LT'),(236,236,'L','lt_LT'),(237,237,'XL','lt_LT'),(238,238,'XXL','lt_LT');
/*!40000 ALTER TABLE `sylius_product_variant_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_product_variant_warehouse_stock`
--

DROP TABLE IF EXISTS `sylius_product_variant_warehouse_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_product_variant_warehouse_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_variant_id` int(11) DEFAULT NULL,
  `warehouse_name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `IDX_61B10C91A80EF684` (`product_variant_id`),
  CONSTRAINT `FK_61B10C91A80EF684` FOREIGN KEY (`product_variant_id`) REFERENCES `sylius_product_variant` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_product_variant_warehouse_stock`
--

LOCK TABLES `sylius_product_variant_warehouse_stock` WRITE;
/*!40000 ALTER TABLE `sylius_product_variant_warehouse_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_product_variant_warehouse_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion`
--

DROP TABLE IF EXISTS `sylius_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `exclusive` tinyint(1) NOT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `used` int(11) NOT NULL,
  `coupon_based` tinyint(1) NOT NULL,
  `starts_at` datetime DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F157396377153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion`
--

LOCK TABLES `sylius_promotion` WRITE;
/*!40000 ALTER TABLE `sylius_promotion` DISABLE KEYS */;
INSERT INTO `sylius_promotion` VALUES (3,'christmas','Christmas','Autem ipsum ullam ut.',0,0,NULL,16,0,NULL,NULL,'2022-03-08 11:16:35','2022-03-08 11:16:40'),(4,'new_year','New Year','Autem ipsum ullam ut.',2,0,10,10,0,'2022-03-01 11:16:35','2022-03-15 11:16:35','2022-03-08 11:16:35','2022-03-08 11:16:40');
/*!40000 ALTER TABLE `sylius_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion_action`
--

DROP TABLE IF EXISTS `sylius_promotion_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_933D0915139DF194` (`promotion_id`),
  CONSTRAINT `FK_933D0915139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion_action`
--

LOCK TABLES `sylius_promotion_action` WRITE;
/*!40000 ALTER TABLE `sylius_promotion_action` DISABLE KEYS */;
INSERT INTO `sylius_promotion_action` VALUES (3,3,'order_percentage_discount','a:1:{s:10:\"percentage\";d:0.12;}'),(4,4,'order_fixed_discount','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:1000;}}');
/*!40000 ALTER TABLE `sylius_promotion_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion_channels`
--

DROP TABLE IF EXISTS `sylius_promotion_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion_channels` (
  `promotion_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`promotion_id`,`channel_id`),
  KEY `IDX_1A044F64139DF194` (`promotion_id`),
  KEY `IDX_1A044F6472F5A1AA` (`channel_id`),
  CONSTRAINT `FK_1A044F64139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1A044F6472F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion_channels`
--

LOCK TABLES `sylius_promotion_channels` WRITE;
/*!40000 ALTER TABLE `sylius_promotion_channels` DISABLE KEYS */;
INSERT INTO `sylius_promotion_channels` VALUES (3,2),(4,2);
/*!40000 ALTER TABLE `sylius_promotion_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion_coupon`
--

DROP TABLE IF EXISTS `sylius_promotion_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `used` int(11) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `per_customer_usage_limit` int(11) DEFAULT NULL,
  `reusable_from_cancelled_orders` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B04EBA8577153098` (`code`),
  KEY `IDX_B04EBA85139DF194` (`promotion_id`),
  CONSTRAINT `FK_B04EBA85139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion_coupon`
--

LOCK TABLES `sylius_promotion_coupon` WRITE;
/*!40000 ALTER TABLE `sylius_promotion_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_promotion_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion_order`
--

DROP TABLE IF EXISTS `sylius_promotion_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion_order` (
  `order_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`promotion_id`),
  KEY `IDX_BF9CF6FB8D9F6D38` (`order_id`),
  KEY `IDX_BF9CF6FB139DF194` (`promotion_id`),
  CONSTRAINT `FK_BF9CF6FB139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`),
  CONSTRAINT `FK_BF9CF6FB8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion_order`
--

LOCK TABLES `sylius_promotion_order` WRITE;
/*!40000 ALTER TABLE `sylius_promotion_order` DISABLE KEYS */;
INSERT INTO `sylius_promotion_order` VALUES (21,3),(21,4),(22,3),(22,4),(23,4),(24,3),(24,4),(25,3),(25,4),(26,3),(26,4),(28,3),(28,4),(29,4),(30,3),(30,4),(31,3),(31,4),(32,3),(33,3),(34,3),(35,3),(36,3),(37,3),(38,3),(39,3);
/*!40000 ALTER TABLE `sylius_promotion_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_promotion_rule`
--

DROP TABLE IF EXISTS `sylius_promotion_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_promotion_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_2C188EA8139DF194` (`promotion_id`),
  CONSTRAINT `FK_2C188EA8139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `sylius_promotion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_promotion_rule`
--

LOCK TABLES `sylius_promotion_rule` WRITE;
/*!40000 ALTER TABLE `sylius_promotion_rule` DISABLE KEYS */;
INSERT INTO `sylius_promotion_rule` VALUES (3,3,'cart_quantity','a:1:{s:5:\"count\";i:7;}'),(4,4,'item_total','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:10000;}}');
/*!40000 ALTER TABLE `sylius_promotion_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_province`
--

DROP TABLE IF EXISTS `sylius_province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `abbreviation` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B5618FE477153098` (`code`),
  UNIQUE KEY `UNIQ_B5618FE4F92F3E705E237E06` (`country_id`,`name`),
  KEY `IDX_B5618FE4F92F3E70` (`country_id`),
  CONSTRAINT `FK_B5618FE4F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `sylius_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_province`
--

LOCK TABLES `sylius_province` WRITE;
/*!40000 ALTER TABLE `sylius_province` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shipment`
--

DROP TABLE IF EXISTS `sylius_shipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `tracking` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `parcel_machine_id` int(11) DEFAULT NULL,
  `shipment_total` int(11) DEFAULT NULL,
  `shipper_status` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `shipper_info` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD707B3319883967` (`method_id`),
  KEY `IDX_FD707B338D9F6D38` (`order_id`),
  KEY `IDX_FD707B33F624B39D` (`sender_id`),
  KEY `IDX_FD707B33A4EBB061` (`parcel_machine_id`),
  CONSTRAINT `FK_FD707B3319883967` FOREIGN KEY (`method_id`) REFERENCES `sylius_shipping_method` (`id`),
  CONSTRAINT `FK_FD707B338D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `sylius_order` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FD707B33A4EBB061` FOREIGN KEY (`parcel_machine_id`) REFERENCES `omni_parcel_machine` (`id`),
  CONSTRAINT `FK_FD707B33F624B39D` FOREIGN KEY (`sender_id`) REFERENCES `sylius_channel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shipment`
--

LOCK TABLES `sylius_shipment` WRITE;
/*!40000 ALTER TABLE `sylius_shipment` DISABLE KEYS */;
INSERT INTO `sylius_shipment` VALUES (21,5,21,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,47954,NULL,NULL),(22,5,22,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,26886,NULL,NULL),(23,5,23,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,25966,NULL,NULL),(24,5,24,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,30222,NULL,NULL),(25,5,25,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,57162,NULL,NULL),(26,7,26,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,64497,NULL,NULL),(27,5,27,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,8544,NULL,NULL),(28,5,28,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,49075,NULL,NULL),(29,5,29,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:39',NULL,NULL,23975,NULL,NULL),(30,5,30,'ready',NULL,'2022-03-08 11:16:39','2022-03-08 11:16:40',NULL,NULL,57611,NULL,NULL),(31,5,31,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,70800,NULL,NULL),(32,5,32,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,54461,NULL,NULL),(33,5,33,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,67251,NULL,NULL),(34,5,34,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,38798,NULL,NULL),(35,5,35,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,35560,NULL,NULL),(36,7,36,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,110472,NULL,NULL),(37,5,37,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,86233,NULL,NULL),(38,5,38,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,63942,NULL,NULL),(39,5,39,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,77589,NULL,NULL),(40,5,40,'ready',NULL,'2022-03-08 11:16:40','2022-03-08 11:16:40',NULL,NULL,891,NULL,NULL);
/*!40000 ALTER TABLE `sylius_shipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shipping_category`
--

DROP TABLE IF EXISTS `sylius_shipping_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shipping_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B1D6465277153098` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shipping_category`
--

LOCK TABLES `sylius_shipping_category` WRITE;
/*!40000 ALTER TABLE `sylius_shipping_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_shipping_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shipping_method`
--

DROP TABLE IF EXISTS `sylius_shipping_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shipping_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `zone_id` int(11) NOT NULL,
  `tax_category_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `configuration` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `category_requirement` int(11) NOT NULL,
  `calculator` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `archived_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parcel_machine_provider_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5FB0EE1177153098` (`code`),
  KEY `IDX_5FB0EE1112469DE2` (`category_id`),
  KEY `IDX_5FB0EE119F2C3FAB` (`zone_id`),
  KEY `IDX_5FB0EE119DF894ED` (`tax_category_id`),
  CONSTRAINT `FK_5FB0EE1112469DE2` FOREIGN KEY (`category_id`) REFERENCES `sylius_shipping_category` (`id`),
  CONSTRAINT `FK_5FB0EE119DF894ED` FOREIGN KEY (`tax_category_id`) REFERENCES `sylius_tax_category` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_5FB0EE119F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `sylius_zone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shipping_method`
--

LOCK TABLES `sylius_shipping_method` WRITE;
/*!40000 ALTER TABLE `sylius_shipping_method` DISABLE KEYS */;
INSERT INTO `sylius_shipping_method` VALUES (5,NULL,6,NULL,'ups','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:108;}}',1,'flat_rate',1,0,NULL,'2022-03-08 11:16:29','2022-03-08 11:16:29',NULL),(6,NULL,5,NULL,'dhl_express','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:364;}}',1,'flat_rate',0,1,NULL,'2022-03-08 11:16:29','2022-03-08 11:16:29',NULL),(7,NULL,4,NULL,'fedex','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:246;}}',1,'flat_rate',1,2,NULL,'2022-03-08 11:16:29','2022-03-08 11:16:29',NULL),(8,NULL,5,NULL,'pickup','a:1:{s:11:\"FASHION_WEB\";a:1:{s:6:\"amount\";i:0;}}',1,'flat_rate',1,3,NULL,'2022-03-08 11:16:43','2022-03-08 11:16:43',NULL);
/*!40000 ALTER TABLE `sylius_shipping_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shipping_method_channels`
--

DROP TABLE IF EXISTS `sylius_shipping_method_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shipping_method_channels` (
  `shipping_method_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  PRIMARY KEY (`shipping_method_id`,`channel_id`),
  KEY `IDX_2D9833355F7D6850` (`shipping_method_id`),
  KEY `IDX_2D98333572F5A1AA` (`channel_id`),
  CONSTRAINT `FK_2D9833355F7D6850` FOREIGN KEY (`shipping_method_id`) REFERENCES `sylius_shipping_method` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_2D98333572F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `sylius_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shipping_method_channels`
--

LOCK TABLES `sylius_shipping_method_channels` WRITE;
/*!40000 ALTER TABLE `sylius_shipping_method_channels` DISABLE KEYS */;
INSERT INTO `sylius_shipping_method_channels` VALUES (5,2),(6,2),(7,2),(8,2);
/*!40000 ALTER TABLE `sylius_shipping_method_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shipping_method_translation`
--

DROP TABLE IF EXISTS `sylius_shipping_method_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shipping_method_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sylius_shipping_method_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_2B37DB3D2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_2B37DB3D2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_shipping_method` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shipping_method_translation`
--

LOCK TABLES `sylius_shipping_method_translation` WRITE;
/*!40000 ALTER TABLE `sylius_shipping_method_translation` DISABLE KEYS */;
INSERT INTO `sylius_shipping_method_translation` VALUES (13,5,'UPS','Voluptatem amet tempore et est et dignissimos ducimus.','lt_LT'),(14,5,'UPS','Voluptatem amet tempore et est et dignissimos ducimus.','en_US'),(15,5,'UPS','Voluptatem amet tempore et est et dignissimos ducimus.','ru_RU'),(16,6,'DHL Express','Est sed sequi similique ab quia nostrum consequatur.','lt_LT'),(17,6,'DHL Express','Est sed sequi similique ab quia nostrum consequatur.','en_US'),(18,6,'DHL Express','Est sed sequi similique ab quia nostrum consequatur.','ru_RU'),(19,7,'FedEx','Fugit eum officiis placeat et reiciendis.','lt_LT'),(20,7,'FedEx','Fugit eum officiis placeat et reiciendis.','en_US'),(21,7,'FedEx','Fugit eum officiis placeat et reiciendis.','ru_RU'),(22,8,'Pick up location',NULL,'lt_LT'),(23,8,'Pick up location',NULL,'en_US'),(24,8,'Pick up location',NULL,'ru_RU');
/*!40000 ALTER TABLE `sylius_shipping_method_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shop_billing_data`
--

DROP TABLE IF EXISTS `sylius_shop_billing_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shop_billing_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `tax_id` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shop_billing_data`
--

LOCK TABLES `sylius_shop_billing_data` WRITE;
/*!40000 ALTER TABLE `sylius_shop_billing_data` DISABLE KEYS */;
INSERT INTO `sylius_shop_billing_data` VALUES (2,'Sylius','0001112222','US','Test St. 15','eCommerce Town','00 33 22');
/*!40000 ALTER TABLE `sylius_shop_billing_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_shop_user`
--

DROP TABLE IF EXISTS `sylius_shop_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_shop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `username_canonical` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `email_verification_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `email` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `encoder_name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7C2B74809395C3F3` (`customer_id`),
  CONSTRAINT `FK_7C2B74809395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `sylius_customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_shop_user`
--

LOCK TABLES `sylius_shop_user` WRITE;
/*!40000 ALTER TABLE `sylius_shop_user` DISABLE KEYS */;
INSERT INTO `sylius_shop_user` VALUES (23,23,'shop@example.com','shop@example.com',1,'76i26yfmvascog4wcowgswo8gok8skc','$argon2i$v=19$m=65536,t=4,p=1$eVF4OXB4NDRtdTMxdFFvWQ$PUyqrs0cSZFt5HkgiNO/Lo9Ota9pCSr/+pBRI4MJqOo',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:29','2022-03-08 11:16:29','argon2i'),(24,24,'user@example.com','user@example.com',1,'tgc9nuxfhtwk0wgks448cowgwo0g048','$argon2i$v=19$m=65536,t=4,p=1$MWJhb0owZkY1QlgwQ3Focg$Oj492DfxF/cKdxpbdaOHx3HS3TMVPpPhF6n0AfhwdvY',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:29','2022-03-08 11:16:29','argon2i'),(25,25,'orn.audie@hotmail.com','orn.audie@hotmail.com',1,'mie31eo2568o0gckosooo8gssgcsgks','$argon2i$v=19$m=65536,t=4,p=1$Q0RCT3J5cHp6dEtnWWJ6Rg$alSzNEdoyWEXN2yJdYstshm/apwT4gwO3JdncDT+9nI',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:30','2022-03-08 11:16:30','argon2i'),(26,26,'hermann.leola@haley.biz','hermann.leola@haley.biz',1,'rz6bi37gzi8w4kgwws4ocwwocsoc08w','$argon2i$v=19$m=65536,t=4,p=1$Qk5SUTRZejB1MTFqSlhBcw$5viPLaLWEBwZNQJFdJZr6KYFwiDXqnpNE8hrSVjCWto',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:30','2022-03-08 11:16:30','argon2i'),(27,27,'will.wilton@streich.net','will.wilton@streich.net',1,'9lvqivlttnggcos4w8kgo0oc80884cw','$argon2i$v=19$m=65536,t=4,p=1$RnRnbEZ3MHNLTGFVYW9PdA$A9JaPkza+vEoMUr3jYkZUiIsTNWvjzVHqfZeTsHjuns',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:30','2022-03-08 11:16:30','argon2i'),(28,28,'joshuah70@yahoo.com','joshuah70@yahoo.com',1,'779gxwcx1544kso8ww0og4wo8kco8sc','$argon2i$v=19$m=65536,t=4,p=1$QWptMlp1SU1NNmdFczVFLw$ExvieCDZ0CDggTBBm9uPkhCAaaxGeP0xz5AbZ0wXk1Q',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:30','2022-03-08 11:16:30','argon2i'),(29,29,'reymundo66@weber.com','reymundo66@weber.com',1,'sk67ujfytes4ogc0okcw4ckg0wog4cg','$argon2i$v=19$m=65536,t=4,p=1$dXlqUlFmdTE0OWYvdnVqRw$lEpRHUCKcnkzluJK0EFzCFSV9kFroW3FpdpDEacXjOk',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:31','2022-03-08 11:16:31','argon2i'),(30,30,'tobin.kiehn@yahoo.com','tobin.kiehn@yahoo.com',1,'taxv3v3e7ggwggwsc4wo0w4wkogw8oc','$argon2i$v=19$m=65536,t=4,p=1$NWRsakNhYlFiT08vbjRBWQ$rnEJAcJq9EC+5DJeJEhk+olsuQqbkQ2an3tsP5l2K/E',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:31','2022-03-08 11:16:31','argon2i'),(31,31,'unitzsche@gmail.com','unitzsche@gmail.com',1,'qkxs58adj7k04kcgs4o40skgsk4wsoo','$argon2i$v=19$m=65536,t=4,p=1$VC5sdXRobzNpNS5kaFI2SA$tFO/NGz6jv4K2uVFZJDaAfavJu3bXZLesP1ItZHyq7o',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:31','2022-03-08 11:16:31','argon2i'),(32,32,'zita28@yahoo.com','zita28@yahoo.com',1,'m0f51btb0vkow0s4s4w8880so80ck04','$argon2i$v=19$m=65536,t=4,p=1$TkxMeW9ZRHFlSXlaUzVULw$eeCElKLKzCw6lpG6T8PW1ate/xYMmd0c6HW9taOoAFc',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:31','2022-03-08 11:16:31','argon2i'),(33,33,'avolkman@christiansen.com','avolkman@christiansen.com',1,'ca0wtx9ja48woksgw00wk8ssg4cwwgk','$argon2i$v=19$m=65536,t=4,p=1$bU5iVWJ6OEdGajcuWXBPUg$m8L6hQvtE2pNjCf4nidaHFjKndZe/40eDT1vyHVH+kQ',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:32','2022-03-08 11:16:32','argon2i'),(34,34,'watsica.darren@crooks.com','watsica.darren@crooks.com',1,'i52qnu81n4ocgk0ggkco848o4oc88c4','$argon2i$v=19$m=65536,t=4,p=1$R0Juc1Vid0FvNm4zRmllVA$JFdIkMahOkQh0HzUaIC3qHS6fR7+gkrTYg0hMUla0vU',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:32','2022-03-08 11:16:32','argon2i'),(35,35,'eldon62@kemmer.biz','eldon62@kemmer.biz',1,'di8g2dac0o0kwgwo0o0kso8ww8kokkc','$argon2i$v=19$m=65536,t=4,p=1$UFpObGNlSnFTci5CV3pWUA$8vSXeNLCtErWDEhkqsnvcUb7Roqnu7gG7hIK5TAcvf4',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:32','2022-03-08 11:16:32','argon2i'),(36,36,'laisha.sipes@hotmail.com','laisha.sipes@hotmail.com',1,'choweqxgwjwos4wsscswwsko0csccgs','$argon2i$v=19$m=65536,t=4,p=1$aEhNUVJUS3RwZkN5WUxaWA$WrFkNVNjcT3ZuCX4bmImH9eKxZvNRNe7eopiVgdHpTs',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:32','2022-03-08 11:16:32','argon2i'),(37,37,'amir38@parker.com','amir38@parker.com',1,'r9nv19swdyoo0c480kg8o0ckcw4ssk8','$argon2i$v=19$m=65536,t=4,p=1$OGlCWHRLYUFwdEI4S0xYRQ$vV75pTnLsrMMe2F5dKw5DXY2ghC4jNg6eEU/YRzG2cM',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:33','2022-03-08 11:16:33','argon2i'),(38,38,'yesenia99@hudson.net','yesenia99@hudson.net',1,'1oppz9fv9u1wwcsckg8w8k8c440ogog','$argon2i$v=19$m=65536,t=4,p=1$bmNtLmttRGd0YWNGT3pQVg$nOaP8xK22JeSM9gbccBEysuA7x4NknA7LhshuabHsTI',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:33','2022-03-08 11:16:33','argon2i'),(39,39,'collier.kathleen@murphy.com','collier.kathleen@murphy.com',1,'6v82mndkxf0owccc0wkco8s0cockgsg','$argon2i$v=19$m=65536,t=4,p=1$LzE2NnBrdllGRnoyY1F0Sg$FzMO6lA0TAk0eSGfW0mGCWV5EhQ1bpJxxg4byCnTX2I',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:33','2022-03-08 11:16:33','argon2i'),(40,40,'beahan.vivianne@collins.net','beahan.vivianne@collins.net',1,'qrj8977r2ggockcwkkw0cogccogcskw','$argon2i$v=19$m=65536,t=4,p=1$bTlyQ1VLNnVKVTBmSzBnaA$ov7pq5znPeuIyLbS4Nj/Lh6GewZcxohIjCNrUXDdFjU',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:33','2022-03-08 11:16:33','argon2i'),(41,41,'andy87@hotmail.com','andy87@hotmail.com',1,'6r0qs7qrdrswksgw4s48os0okggs4cs','$argon2i$v=19$m=65536,t=4,p=1$UWJCNE9LS2FFbHNlaFlpeg$AL1f5k4W/xLKL35K9RjVTdbM2jYSPfxVessjQ9BgwYo',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:33','2022-03-08 11:16:33','argon2i'),(42,42,'ward.nora@yahoo.com','ward.nora@yahoo.com',1,'45skonwx9fwg488ow4s0kwgskw0s04g','$argon2i$v=19$m=65536,t=4,p=1$V2FjV1JNbDFSRERTUVRtTQ$pb+Ad/bGNdBdsf9FwkwUOeatJrqSbONSBSq02KYiZgk',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:34','2022-03-08 11:16:34','argon2i'),(43,43,'fletcher35@gmail.com','fletcher35@gmail.com',1,'dlhviy9d8hcsogg0kow84s8g0co4o44','$argon2i$v=19$m=65536,t=4,p=1$ODJUcVVKVUdZcnpmV2c2Yg$1ni8Kk2FyE9PmSUvD0/ABvWONXOvin5B9Xt1SIvTpts',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:34','2022-03-08 11:16:34','argon2i'),(44,44,'ykris@yahoo.com','ykris@yahoo.com',1,'awnjpsnn0u808o88o00884sgcgscgcs','$argon2i$v=19$m=65536,t=4,p=1$a29wTzFGYTZEWmtzRFBvSw$unIjGxAO7JxXfbJM8Z5UQSQY21gZ8ZErz2M23g7Iivw',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:16:34','2022-03-08 11:16:34','argon2i'),(45,45,'tautvydas@kiro.tech','tautvydas@kiro.tech',0,'r1xyvyls4gg8s0kg4ckswocs0080k4s','$argon2i$v=19$m=65536,t=4,p=1$NU0zUTZQN0JOZkdOSnZWRw$vQq8ZrTdDZlIIHc6T8S3TIDI+qzLNzCIs5jaW28I2UA',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}',NULL,NULL,'2022-03-08 11:51:48','2022-03-08 11:51:49',NULL);
/*!40000 ALTER TABLE `sylius_shop_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_tax_category`
--

DROP TABLE IF EXISTS `sylius_tax_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_tax_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_221EB0BE77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_tax_category`
--

LOCK TABLES `sylius_tax_category` WRITE;
/*!40000 ALTER TABLE `sylius_tax_category` DISABLE KEYS */;
INSERT INTO `sylius_tax_category` VALUES (3,'clothing','Clothing','Soluta alias error repellat enim aut. Fugit dignissimos ut deserunt unde. Id perspiciatis incidunt aliquid dolorem minus ut nulla. Dolor qui sint adipisci neque libero soluta officia.','2022-03-08 11:16:35','2022-03-08 11:16:35'),(4,'other','Other','Est omnis voluptatem ut. Autem exercitationem explicabo reprehenderit possimus at sit. Et maiores ut neque ducimus cumque.','2022-03-08 11:16:35','2022-03-08 11:16:35');
/*!40000 ALTER TABLE `sylius_tax_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_tax_rate`
--

DROP TABLE IF EXISTS `sylius_tax_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_tax_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `amount` decimal(10,5) NOT NULL,
  `included_in_price` tinyint(1) NOT NULL,
  `calculator` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3CD86B2E77153098` (`code`),
  KEY `IDX_3CD86B2E12469DE2` (`category_id`),
  KEY `IDX_3CD86B2E9F2C3FAB` (`zone_id`),
  CONSTRAINT `FK_3CD86B2E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `sylius_tax_category` (`id`),
  CONSTRAINT `FK_3CD86B2E9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `sylius_zone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_tax_rate`
--

LOCK TABLES `sylius_tax_rate` WRITE;
/*!40000 ALTER TABLE `sylius_tax_rate` DISABLE KEYS */;
INSERT INTO `sylius_tax_rate` VALUES (3,3,4,'clothing_sales_tax_7','Clothing Sales Tax 7%',0.07000,0,'default','2022-03-08 11:16:35','2022-03-08 11:16:35'),(4,4,4,'sales_tax_20','Sales Tax 20%',0.20000,0,'default','2022-03-08 11:16:35','2022-03-08 11:16:35');
/*!40000 ALTER TABLE `sylius_tax_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_taxon`
--

DROP TABLE IF EXISTS `sylius_taxon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_taxon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree_root` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `tree_left` int(11) NOT NULL,
  `tree_right` int(11) NOT NULL,
  `tree_level` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `generic_article_ids` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`generic_article_ids`)),
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CFD811CA77153098` (`code`),
  KEY `IDX_CFD811CAA977936C` (`tree_root`),
  KEY `IDX_CFD811CA727ACA70` (`parent_id`),
  CONSTRAINT `FK_CFD811CA727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_CFD811CAA977936C` FOREIGN KEY (`tree_root`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_taxon`
--

LOCK TABLES `sylius_taxon` WRITE;
/*!40000 ALTER TABLE `sylius_taxon` DISABLE KEYS */;
INSERT INTO `sylius_taxon` VALUES (12,12,NULL,'category',1,22,0,0,'2022-03-08 11:16:35','2022-03-08 11:16:35','[]'),(13,12,12,'t_shirts',2,7,1,0,'2022-03-08 11:16:35','2022-03-08 11:16:35','[]'),(14,12,13,'mens_t_shirts',3,4,2,0,'2022-03-08 11:16:35','2022-03-08 11:16:35','[]'),(15,12,13,'womens_t_shirts',5,6,2,1,'2022-03-08 11:16:35','2022-03-08 11:16:35','[]'),(16,12,12,'caps',8,13,1,1,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(17,12,16,'simple_caps',9,10,2,0,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(18,12,16,'caps_with_pompons',11,12,2,1,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(19,12,12,'dresses',14,15,1,2,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(20,12,12,'jeans',16,21,1,3,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(21,12,20,'mens_jeans',17,18,2,0,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]'),(22,12,20,'womens_jeans',19,20,2,1,'2022-03-08 11:16:36','2022-03-08 11:16:36','[]');
/*!40000 ALTER TABLE `sylius_taxon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_taxon_image`
--

DROP TABLE IF EXISTS `sylius_taxon_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_taxon_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DBE52B287E3C61F9` (`owner_id`),
  CONSTRAINT `FK_DBE52B287E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_taxon_image`
--

LOCK TABLES `sylius_taxon_image` WRITE;
/*!40000 ALTER TABLE `sylius_taxon_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_taxon_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_taxon_translation`
--

DROP TABLE IF EXISTS `sylius_taxon_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_taxon_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_uidx` (`locale`,`slug`),
  UNIQUE KEY `sylius_taxon_translation_uniq_trans` (`translatable_id`,`locale`),
  KEY `IDX_1487DFCF2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_1487DFCF2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `sylius_taxon` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_taxon_translation`
--

LOCK TABLES `sylius_taxon_translation` WRITE;
/*!40000 ALTER TABLE `sylius_taxon_translation` DISABLE KEYS */;
INSERT INTO `sylius_taxon_translation` VALUES (45,12,'Category','category','Sint ut et est pariatur. Qui veniam reprehenderit qui rerum. Omnis molestiae eos minima reprehenderit id ipsam.','lt_LT'),(46,12,'Category','category','Eius repellendus explicabo aut beatae suscipit ad quos iure. Enim dolores iusto dolore iure ullam recusandae quae.','en_US'),(47,12,'Category','category','Sint ut et est pariatur. Qui veniam reprehenderit qui rerum. Omnis molestiae eos minima reprehenderit id ipsam.','ru_RU'),(48,12,'Categorie','categorie','Numquam praesentium repellendus amet suscipit rerum nobis. Deleniti incidunt ut rem aut dolor. Non necessitatibus impedit consequatur ducimus.','fr_FR'),(49,13,'T-shirts','t-shirts','Delectus necessitatibus tenetur voluptatem ut et ut tenetur. Hic sint dolorem quis repellat. Repellat officiis tenetur qui voluptas voluptas. Dolorem illo non magnam aut blanditiis perferendis libero.','lt_LT'),(50,13,'T-shirts','category/t-shirts','Et sapiente quibusdam tenetur repellat et qui sed rerum. Sit voluptatem reprehenderit amet tenetur omnis. Consectetur repellendus quod facilis atque accusamus. Alias alias hic repellendus veniam et molestias.','en_US'),(51,13,'T-shirts','t-shirts','Delectus necessitatibus tenetur voluptatem ut et ut tenetur. Hic sint dolorem quis repellat. Repellat officiis tenetur qui voluptas voluptas. Dolorem illo non magnam aut blanditiis perferendis libero.','ru_RU'),(52,13,'T-shirts','categorie/t-shirts','Rem aut enim eum dolorem vitae. Eveniet veritatis suscipit sed et omnis ut facere facere. Reprehenderit voluptatem natus eveniet et. Officiis nihil ipsum officia explicabo ipsam consequatur nulla. Dolor accusamus nisi pariatur illo rerum minus quis consequuntur.','fr_FR'),(53,14,'distinctio nemo in','t-shirts/distinctio-nemo-in','Error necessitatibus aut velit aspernatur sint et et. Culpa aut rerum ad. Voluptates dolore perspiciatis reiciendis molestiae. Accusamus ex numquam ut dolor.','lt_LT'),(54,14,'Men','t-shirts/men','Autem dicta minima nobis eos dolorem vel. Eos consequatur saepe cum unde. Quia tenetur molestiae accusantium expedita totam.','en_US'),(55,14,'distinctio nemo in','t-shirts/distinctio-nemo-in','Error necessitatibus aut velit aspernatur sint et et. Culpa aut rerum ad. Voluptates dolore perspiciatis reiciendis molestiae. Accusamus ex numquam ut dolor.','ru_RU'),(56,14,'Hommes','t-shirts/hommes','Quis quisquam eos minima quia. Facilis non expedita sed consequuntur. Repellat sint est omnis non dolor distinctio. Dolor omnis saepe maxime veritatis.','fr_FR'),(57,15,'nihil delectus temporibus','t-shirts/nihil-delectus-temporibus','Est illum provident culpa eius aut. Vel nihil nam qui facilis sed saepe autem. Nobis doloribus et natus neque sed a. Est eum omnis nostrum maiores.','lt_LT'),(58,15,'Women','t-shirts/women','Commodi debitis doloremque consequatur id voluptates aut et cum. Minima eum rerum officia rerum. Libero praesentium accusamus omnis porro.','en_US'),(59,15,'nihil delectus temporibus','t-shirts/nihil-delectus-temporibus','Est illum provident culpa eius aut. Vel nihil nam qui facilis sed saepe autem. Nobis doloribus et natus neque sed a. Est eum omnis nostrum maiores.','ru_RU'),(60,15,'Femme','t-shirts/femmes','Qui sint dolorem magnam ratione. Quia quae cumque aut saepe itaque. Vel molestiae sit vero autem. Atque et tenetur dolor quia nihil modi velit.','fr_FR'),(61,16,'Caps','caps','Corrupti omnis voluptatem ut eum. Dolor necessitatibus odio aliquam autem. Repellendus eius facilis porro non labore eius voluptatem.','lt_LT'),(62,16,'Caps','category/caps','Sed natus qui non saepe voluptatem blanditiis magni. Tenetur harum natus autem et occaecati ipsam ipsum. Similique error a quos voluptatem tempora sequi incidunt. Sit eum nostrum quibusdam autem qui et eos pariatur. Iusto quis id quasi ut.','en_US'),(63,16,'Caps','caps','Corrupti omnis voluptatem ut eum. Dolor necessitatibus odio aliquam autem. Repellendus eius facilis porro non labore eius voluptatem.','ru_RU'),(64,16,'Bonnets','categorie/bonnets','Omnis et omnis in iure sed ut. Ea in aperiam eius sint praesentium. Ex enim aut sed adipisci veniam enim.','fr_FR'),(65,17,'voluptatibus et est','caps/voluptatibus-et-est','Et eos asperiores quis. Dolore iure occaecati voluptate repudiandae totam fugiat. Maiores quos necessitatibus eius temporibus at ratione incidunt. Id enim qui vel voluptatem ut culpa cupiditate.','lt_LT'),(66,17,'Simple','caps/simple','Pariatur esse dicta amet quia sed voluptatum voluptas dolorem. Ab reprehenderit rerum autem eaque. Quae ad et quia. Placeat sit sed corrupti autem soluta.','en_US'),(67,17,'voluptatibus et est','caps/voluptatibus-et-est','Et eos asperiores quis. Dolore iure occaecati voluptate repudiandae totam fugiat. Maiores quos necessitatibus eius temporibus at ratione incidunt. Id enim qui vel voluptatem ut culpa cupiditate.','ru_RU'),(68,17,'Simple','bonnets/simple','Vel expedita non rerum sit natus et consequatur. Illo harum neque odio. Unde quis amet et impedit sit occaecati nobis.','fr_FR'),(69,18,'qui ab debitis','caps/qui-ab-debitis','Vel hic ducimus error earum placeat et esse. Ut ipsam nulla mollitia nihil doloremque consequatur ea velit. Recusandae qui consequatur animi veniam unde.','lt_LT'),(70,18,'With pompons','caps/with-pompons','Dolorum sunt laboriosam eveniet. Dicta ut et est at distinctio. Culpa aliquam unde ut rerum non. Incidunt et quos quisquam incidunt.','en_US'),(71,18,'qui ab debitis','caps/qui-ab-debitis','Vel hic ducimus error earum placeat et esse. Ut ipsam nulla mollitia nihil doloremque consequatur ea velit. Recusandae qui consequatur animi veniam unde.','ru_RU'),(72,18,'À pompon','bonnets/a-pompon','Reprehenderit assumenda adipisci aliquam quo quod. Id quos tempora corporis nemo quis reprehenderit cupiditate laborum. Et fugit voluptatum est quasi rem aut.','fr_FR'),(73,19,'molestiae nesciunt itaque','category/molestiae-nesciunt-itaque','Consequatur rerum quos dolore dicta id sit adipisci. Non delectus similique doloribus quos necessitatibus et maxime. Deserunt illo sit repellat esse. Voluptatem culpa veritatis consequatur.','lt_LT'),(74,19,'Dresses','category/dresses','Eius ipsum doloribus corrupti maiores eaque illo officiis. Dolor repellendus eum alias doloremque officiis impedit. Officiis molestiae earum rerum voluptatibus. Quaerat quia temporibus reprehenderit esse qui recusandae.','en_US'),(75,19,'molestiae nesciunt itaque','category/molestiae-nesciunt-itaque','Consequatur rerum quos dolore dicta id sit adipisci. Non delectus similique doloribus quos necessitatibus et maxime. Deserunt illo sit repellat esse. Voluptatem culpa veritatis consequatur.','ru_RU'),(76,19,'Robes','categorie/robes','Aut deleniti inventore culpa delectus officia omnis id. Quia velit natus mollitia aut libero. Quo nam enim fugiat rerum. Ea sed ab consequuntur possimus.','fr_FR'),(77,20,'Jeans','jeans','Et ea facere debitis. Doloremque eos fugiat consequatur vitae ipsa eum dolores. Quo qui laboriosam consequuntur pariatur sit asperiores non ut. Et veniam saepe sint illum quas impedit alias.','lt_LT'),(78,20,'Jeans','category/jeans','Commodi voluptatem veritatis mollitia iure explicabo qui. Soluta aliquid quas et voluptas eligendi similique quidem. Sunt pariatur necessitatibus corrupti est et.','en_US'),(79,20,'Jeans','jeans','Et ea facere debitis. Doloremque eos fugiat consequatur vitae ipsa eum dolores. Quo qui laboriosam consequuntur pariatur sit asperiores non ut. Et veniam saepe sint illum quas impedit alias.','ru_RU'),(80,20,'Jeans','categorie/jeans','Corporis commodi praesentium est optio nulla architecto at. Repudiandae assumenda pariatur qui et nam. Id ex voluptatibus tempore.','fr_FR'),(81,21,'facilis voluptate omnis','jeans/facilis-voluptate-omnis','Hic nulla at velit est culpa. Qui nemo non porro.','lt_LT'),(82,21,'Men','jeans/men','Hic fugit veniam ipsum et quaerat velit. Omnis qui est quidem et omnis molestiae nam. Sapiente rerum unde culpa voluptatem optio sunt. Illum eum optio veniam enim aut aut.','en_US'),(83,21,'facilis voluptate omnis','jeans/facilis-voluptate-omnis','Hic nulla at velit est culpa. Qui nemo non porro.','ru_RU'),(84,21,'Hommes','jeans/hommes','Odit et consequatur eius nisi. Beatae quaerat id hic quo soluta qui qui. Libero quae incidunt laborum officia. Aut tenetur ut molestiae porro molestias.','fr_FR'),(85,22,'neque rerum dolores','jeans/neque-rerum-dolores','Et in provident nobis sit quos. Ut tempora distinctio cumque voluptatem saepe eum. Vero quia qui tenetur voluptatem eum.','lt_LT'),(86,22,'Women','jeans/women','Debitis natus rem quia quia. Minima sed soluta dolor saepe ducimus. Modi cum ullam quasi pariatur sed. Omnis eius animi voluptas. Sunt eligendi ut culpa voluptatibus incidunt ullam illo delectus.','en_US'),(87,22,'neque rerum dolores','jeans/neque-rerum-dolores','Et in provident nobis sit quos. Ut tempora distinctio cumque voluptatem saepe eum. Vero quia qui tenetur voluptatem eum.','ru_RU'),(88,22,'Femme','jeans/femme','Sit quam veritatis aperiam officiis. Voluptatibus et quia inventore rem labore. Eum possimus minus aspernatur minima. Omnis dicta voluptates eaque quo.','fr_FR');
/*!40000 ALTER TABLE `sylius_taxon_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_user_oauth`
--

DROP TABLE IF EXISTS `sylius_user_oauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_user_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_provider` (`user_id`,`provider`),
  KEY `IDX_C3471B78A76ED395` (`user_id`),
  CONSTRAINT `FK_C3471B78A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sylius_shop_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_user_oauth`
--

LOCK TABLES `sylius_user_oauth` WRITE;
/*!40000 ALTER TABLE `sylius_user_oauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `sylius_user_oauth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_zone`
--

DROP TABLE IF EXISTS `sylius_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `type` varchar(8) COLLATE utf8mb3_unicode_ci NOT NULL,
  `scope` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7BE2258E77153098` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_zone`
--

LOCK TABLES `sylius_zone` WRITE;
/*!40000 ALTER TABLE `sylius_zone` DISABLE KEYS */;
INSERT INTO `sylius_zone` VALUES (4,'US','United States of America','country','all'),(5,'LT','Lithuania','country','all'),(6,'WORLD','World','country','all');
/*!40000 ALTER TABLE `sylius_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sylius_zone_member`
--

DROP TABLE IF EXISTS `sylius_zone_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sylius_zone_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `belongs_to` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E8B5ABF34B0E929B77153098` (`belongs_to`,`code`),
  KEY `IDX_E8B5ABF34B0E929B` (`belongs_to`),
  CONSTRAINT `FK_E8B5ABF34B0E929B` FOREIGN KEY (`belongs_to`) REFERENCES `sylius_zone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sylius_zone_member`
--

LOCK TABLES `sylius_zone_member` WRITE;
/*!40000 ALTER TABLE `sylius_zone_member` DISABLE KEYS */;
INSERT INTO `sylius_zone_member` VALUES (7,4,'US'),(8,5,'LT'),(11,6,'FR'),(9,6,'LT'),(12,6,'RU'),(10,6,'US');
/*!40000 ALTER TABLE `sylius_zone_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_config_value`
--

DROP TABLE IF EXISTS `system_config_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_config_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B36922798CDE5729` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_config_value`
--

LOCK TABLES `system_config_value` WRITE;
/*!40000 ALTER TABLE `system_config_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_config_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-10 13:52:04
