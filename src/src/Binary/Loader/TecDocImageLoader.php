<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Binary\Loader;

use App\Downloader\TecDocDocumentDownloader;
use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Binary\Loader\LoaderInterface;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Liip\ImagineBundle\Model\Binary;
use LogicException;
use Symfony\Component\Routing\RouterInterface;
use Throwable;

use function ltrim;

final class TecDocImageLoader implements LoaderInterface
{
    /** @var TecDocDocumentDownloader */
    private $imageDownloader;

    /** @var RouterInterface */
    private $router;

    public function __construct(TecDocDocumentDownloader $imageDownloader, RouterInterface $router)
    {
        $this->imageDownloader = $imageDownloader;
        $this->router          = $router;
    }

    public function find($path): BinaryInterface
    {
        try {
            $params = $this->router->match('/' . ltrim($path, '/'));

            if ($params['_route'] !== 'app_document_download_image') {
                throw new LogicException('Cannot load image for route: ' . $params['_route']);
            }

            $content = $this->imageDownloader->downloadImage((int)$params['thumbnailId']);

            return new Binary($content, 'image/jpeg', 'jpeg');
        } catch (Throwable $e) {
            throw new NotLoadableException('Unable to load image for path: ' . $path);
        }
    }
}
