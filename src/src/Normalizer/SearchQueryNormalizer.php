<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Normalizer;

use function preg_replace;
use function str_replace;
use function trim;

final class SearchQueryNormalizer
{
    public function normalize(string $term): string
    {
        $term = str_replace('||', '|', $term);
        $term = trim($term, ' |');
        $searchTerm = preg_replace('/\s/', '', trim($term));

        return str_replace(['-', '_'], '', $searchTerm);
    }
}
