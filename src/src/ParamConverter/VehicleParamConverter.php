<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\ParamConverter;

use App\Transformer\UrlParameter\UrlParameterTransformationFailedException;
use App\Transformer\UrlParameter\VehicleUrlParameterTransformer;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Nfq\Bundle\TecDocBundle\ParamConverter\VehicleParamConverter as TecDocVehicleParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use function sprintf;

class VehicleParamConverter extends TecDocVehicleParamConverter
{
    /** @var VehicleUrlParameterTransformer */
    private $vehicleUrlTransformer;

    public function __construct(VehicleManager $vehicleManager, VehicleUrlParameterTransformer $vehicleUrlTransformer)
    {
        parent::__construct($vehicleManager);

        $this->vehicleUrlTransformer = $vehicleUrlTransformer;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $name    = $configuration->getName();
        $vehicle = $request->attributes->get($name);

        try {
            $data = $this->vehicleUrlTransformer->reverse((string)$vehicle);

            $request->attributes->set($name, $data['id']);
        } catch (UrlParameterTransformationFailedException $e) {
            throw new NotFoundHttpException(sprintf('%s object not found.', $configuration->getClass()));
        }

        return parent::apply($request, $configuration);
    }
}
