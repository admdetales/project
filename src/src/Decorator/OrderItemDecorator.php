<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Decorator;

use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Currency\Context\CurrencyContextInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Order\Model\OrderItemInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

final class OrderItemDecorator
{
    /** @var FactoryInterface */
    private $orderFactory;

    /** @var OrderProcessorInterface */
    private $orderProcessor;

    /** @var ChannelContextInterface */
    private $channelContext;

    /** @var LocaleContextInterface */
    private $localeContext;

    /** @var CurrencyContextInterface */
    private $currencyContext;

    /** @var ProductVariantPriceCalculatorInterface */
    private $productVariantPriceCalculator;

    public function __construct(
        FactoryInterface $orderFactory,
        OrderProcessorInterface $orderProcessor,
        ChannelContextInterface $channelContext,
        LocaleContextInterface $localeContext,
        CurrencyContextInterface $currencyContext,
        ProductVariantPriceCalculatorInterface $productVariantPriceCalculator
    ) {
        $this->orderFactory = $orderFactory;
        $this->orderProcessor = $orderProcessor;
        $this->channelContext = $channelContext;
        $this->localeContext = $localeContext;
        $this->currencyContext = $currencyContext;
        $this->productVariantPriceCalculator = $productVariantPriceCalculator;
    }

    /**
     * Decorate order item interface to include discount calculations
     *
     * @param OrderItemInterface $orderItem
     */
    public function decorate(OrderItemInterface $orderItem): void
    {
        /** @var OrderInterface $order */
        $order = $this->orderFactory->createNew();

        $order->setChannel($this->channelContext->getChannel());
        $order->setLocaleCode($this->localeContext->getLocaleCode());
        $order->setCurrencyCode($this->currencyContext->getCurrencyCode());
        $order->addItem($orderItem);

        $this->orderProcessor->process($order);

        $orderItem->setProductVariantPriceCaltulator($this->productVariantPriceCalculator);
    }
}
