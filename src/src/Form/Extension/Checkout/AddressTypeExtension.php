<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension\Checkout;

use App\Entity\Order\OrderInterface;
use Sylius\Bundle\CoreBundle\Form\Type\Checkout\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class AddressTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            static function (FormEvent $event): void {
                /** @var OrderInterface $order */
                $order = $event->getData();

                $customer = $order->getCustomer();
                $address  = $order->getBillingAddress();
                if (!$customer || !$address) {
                    return;
                }

                $customer->setFirstName($address->getFirstName());
                $customer->setLastName($address->getLastName());
                $customer->setPhoneNumber($address->getPhoneNumber());
            },
            -255
        );
    }

    public static function getExtendedTypes(): iterable
    {
        return [AddressType::class];
    }
}
