<?php

declare(strict_types=1);

namespace App\Form\Extension\Checkout;

use App\Entity\Consent\Consent;
use App\Form\Type\Consent\ConsentType;
use Sylius\Bundle\CoreBundle\Form\Type\Checkout\SelectPaymentType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Valid;

class SelectPaymentExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'consent',
            ConsentType::class,
            [
                'locationCode' => Consent::LOCATION_PAYMENT_METHOD,
                'label' => false,
                'mapped' => false,
                'constraints' => [
                    new Valid(['groups' => ['sylius']]),
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [SelectPaymentType::class];
    }
}
