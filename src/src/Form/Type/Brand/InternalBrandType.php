<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\Brand;

use App\Entity\Brand\Brand;
use App\Repository\Brand\BrandRepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InternalBrandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'tecDocBrandId',
                IntegerType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'brand',
                EntityType::class,
                [
                    'class' => Brand::class,
                    'query_builder' => static function (BrandRepositoryInterface $er) {
                        return $er->createListQueryBuilderForChoiceForm();
                    },
                    'choice_label' => static function (Brand $brand): ?string {
                        return $brand->getName();
                    },
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
        ;
    }
}
