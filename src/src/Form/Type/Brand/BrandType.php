<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\Brand;

use App\Loader\BrandFilterLoader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BrandType extends EntityType
{

    /**
     * @param ObjectManager $manager
     * @param QueryBuilder $queryBuilder
     * @param string $class
     * @return BrandFilterLoader|ORMQueryBuilderLoader
     */
    public function getLoader(ObjectManager $manager, $queryBuilder, $class)
    {
        if (!$queryBuilder instanceof QueryBuilder) {
            throw new \TypeError(
                sprintf(
                    'Expected an instance of %s, but got %s.',
                    QueryBuilder::class,
                    \is_object($queryBuilder) ? \get_class($queryBuilder) : \gettype($queryBuilder)
                )
            );
        }

        return new BrandFilterLoader($queryBuilder);
    }
}
