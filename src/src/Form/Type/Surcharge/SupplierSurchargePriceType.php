<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type\Surcharge;

use App\Entity\Surcharge\SupplierSurchargePrice;
use Sylius\Bundle\CustomerBundle\Form\Type\CustomerChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierSurchargePriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'customer',
                CustomerChoiceType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.customer',
                    'attr' => [
                        'class' => 'ui fluid search dropdown selection',
                    ],
                ]
            )
            ->add(
                'amount',
                PercentType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.surcharge',
                    'scale' => 2,
                    'type' => 'integer',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => SupplierSurchargePrice::class,
            ]
        );
    }
}
