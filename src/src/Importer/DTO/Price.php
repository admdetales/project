<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\DTO;

class Price
{
    /**
     * @var int
     */
    private $productPrice;

    /**
     * @var int
     */
    private $productOriginalPrice;

    /**
     * @param int $productPrice
     * @param int $productOriginalPrice
     */
    public function __construct(int $productPrice, int $productOriginalPrice)
    {
        $this->productPrice = $productPrice;
        $this->productOriginalPrice = $productOriginalPrice;
    }

    /**
     * @return int
     */
    public function getProductPrice(): int
    {
        return $this->productPrice;
    }

    /**
     * @return int
     */
    public function getProductOriginalPrice(): int
    {
        return $this->productOriginalPrice;
    }
}
