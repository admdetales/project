<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer;

use Doctrine\Common\Persistence\ObjectManager;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterResultInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImportResultLoggerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ResourceImporter as BaseResourceImporter;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use League\Csv\Reader;
use Port\Reader\ReaderFactory;
use Symfony\Component\Filesystem\Filesystem;

class ResourceImporter extends BaseResourceImporter
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        ReaderFactory $readerFactory,
        ObjectManager $objectManager,
        ResourceProcessorInterface $resourceProcessor,
        ImportResultLoggerInterface $importerResult,
        int $batchSize,
        bool $failOnIncomplete,
        bool $stopOnFailure
    ) {
        parent::__construct(
            $readerFactory,
            $objectManager,
            $resourceProcessor,
            $importerResult,
            $batchSize,
            $failOnIncomplete,
            $stopOnFailure
        );

        $this->filesystem = new Filesystem();
    }

    public function import(string $fileName): ImporterResultInterface
    {
        $this->result->start();

        if (!$this->filesystem->exists($fileName)) {
            $this->result->getLogger()->notice(\sprintf('Data import filename %s does not exist', $fileName));
            $this->result->stop();

            return $this->result;
        }

        $fileName = $this->renameFileForPreImport($fileName);

        $delimiter = $this->detectDelimiter($fileName);
        $csvReader = Reader::createFromPath($fileName);
        $csvReader->setHeaderOffset(0);
        $csvReader->setDelimiter($delimiter);

        foreach ($csvReader as $i => $row) {
            if ($this->importData((int) $i, $row)) {
                break;
            }

            if ((int) $i % $this->batchSize === 0) {
                $this->objectManager->flush();
                $this->objectManager->clear();
            }
        }

        $this->objectManager->flush();
        $this->result->stop();

        $this->renameFileForPostImport($fileName);

        return $this->result;
    }

    protected function renameFileForPreImport(string $fileName): string
    {
        ['dirname' => $dir, 'extension' => $ext, 'filename' => $fn] = \pathinfo($fileName);

        $newFileName = \sprintf('%s%s%s-started-%s.%s', $dir, \DIRECTORY_SEPARATOR, $fn, \time(), $ext);

        $this->filesystem->rename($fileName, $newFileName);

        return $newFileName;
    }

    protected function renameFileForPostImport(string $fileName): void
    {
        ['dirname' => $dir, 'extension' => $ext, 'filename' => $fn] = \pathinfo($fileName);

        $newFileName = \sprintf('%s%s%s-imported-%s.%s', $dir, \DIRECTORY_SEPARATOR, $fn, \time(), $ext);

        $this->filesystem->rename($fileName, $newFileName);
    }

    /**
     * @param string $filename
     * @return string
     */
    protected function detectDelimiter(string $filename): string
    {
        $fh = \fopen($filename, 'rb');

        $delimiters = [',', ';', '|', "\t"];

        $data2 = [];

        $delimiter = $delimiters[0];
        foreach ($delimiters as $d) {
            $data1 = \fgetcsv($fh, 4096, $d);

            if (\count($data1) > \count($data2)) {
                $delimiter = $d;
                $data2 = $data1;
            }

            \rewind($fh);
        }

        \fclose($fh);

        return $delimiter;
    }
}
