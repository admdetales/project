<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Hart;

use App\Importer\DTO\ProductInfo;
use App\Importer\Reader\ProductInfoReaderInterface;
use App\Importer\Utils\Assert;
use Symfony\Component\String\Slugger\AsciiSlugger;
use App\Importer\Utils\OeNumberHelper;

class HartProductInfoReader implements ProductInfoReaderInterface
{
    /** @var AsciiSlugger */
    private $slugger;

    public function __construct()
    {
        $this->slugger = new AsciiSlugger();
    }

    /**
     * {@inheritdoc}
     */
    public function read(array $data): ProductInfo
    {
        Assert::arrayHasKeys(
            $data,
            [
                HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_PRODUCT_IMPORT,
                HartDataReader::BRAND_CODE_HEADER,
                HartDataReader::BRAND_NAME_HEADER,
                HartDataReader::PRODUCT_BRAND_CODE_HEADER,
                HartDataReader::PRODUCT_NAME_HEADER,
                HartDataReader::PRODUCT_PROVIDER_CATEGORY_HEADER,
                HartDataReader::PRODUCT_OE_NUMBERS_HEADER,
            ]
        );

        $productProviderCode = $this->readProductProviderCode($data);

        return new ProductInfo(
            HartDataReader::PRODUCT_PROVIDER_NAME,
            $productProviderCode,
            $this->readBrandCode($data),
            $this->readBrandName($data),
            $this->readProductBrandCode($data),
            [
                ProductInfoReaderInterface::DEFAULT_LOCALE => $this->readProductName(
                    $data,
                    HartDataReader::PRODUCT_NAME_HEADER
                ),
                ProductInfoReaderInterface::LOCALE_RU => $this->readProductName(
                    $data,
                    HartDataReader::PRODUCT_NAME_HEADER
                ),
                ProductInfoReaderInterface::LOCALE_EN => $this->readProductName(
                    $data,
                    HartDataReader::PRODUCT_NAME_HEADER
                ),
            ],
            $this->readProductProviderCategory($data),
            $this->readOeNumbers($data),
            'HART',
            'H-' . $this->slugger->slug($productProviderCode),
            true
        );
    }

    private function readProductProviderCode(array $data): string
    {
        return \trim((string)$data[HartDataReader::PRODUCT_PROVIDER_CODE_HEADER_PRODUCT_IMPORT]);
    }

    private function readBrandCode(array $data): ?int
    {
        $brandCodeData = \trim((string)$data[HartDataReader::BRAND_CODE_HEADER]);

        return $brandCodeData !== '' ? (int)$brandCodeData : null;
    }

    private function readBrandName(array $data): ?string
    {
        $brandNameData = \trim((string)$data[HartDataReader::BRAND_NAME_HEADER]);

        return $brandNameData !== '' ? $brandNameData : null;
    }

    private function readProductBrandCode(array $data): ?string
    {
        $productBrandCodeData = \trim((string)$data[HartDataReader::PRODUCT_BRAND_CODE_HEADER]);

        return $productBrandCodeData !== '' ? $productBrandCodeData : null;
    }

    private function readProductName(array $data, string $header): ?string
    {
        $productName = \trim((string)$data[$header]);

        return $productName !== '' ? $productName : null;
    }

    private function readProductProviderCategory(array $data): ?string
    {
        $productProviderCategoryData = \trim((string)$data[HartDataReader::PRODUCT_PROVIDER_CATEGORY_HEADER]);

        return $productProviderCategoryData !== '' ? $productProviderCategoryData : null;
    }

    private function readOeNumbers(array $data): array
    {
        $inputData = $data[HartDataReader::PRODUCT_OE_NUMBERS_HEADER];

        return OeNumberHelper::tokenizeOeNumbers($inputData);
    }
}
