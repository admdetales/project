<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Reader\Ad;

use App\Importer\DTO\ProductInfo;
use App\Importer\Reader\ProductInfoReaderInterface;
use App\Importer\Utils\Assert;
use App\Importer\Utils\OeNumberHelper;

class AdProductInfoReader implements ProductInfoReaderInterface
{
    /**
     * {@inheritdoc}
     */
    public function read(array $data): ProductInfo
    {
        Assert::arrayHasKeys(
            $data,
            [
                AdDataReader::PRODUCT_PROVIDER_CODE_HEADER,
                AdDataReader::BRAND_CODE_HEADER,
                AdDataReader::BRAND_NAME_HEADER,
                AdDataReader::PRODUCT_NAME_HEADER,
                AdDataReader::PRODUCT_NAME_RU_HEADER,
                AdDataReader::PRODUCT_NAME_EN_HEADER,
                AdDataReader::PRODUCT_PROVIDER_CATEGORY_HEADER,
                AdDataReader::PRODUCT_OE_NUMBERS_HEADER,
                AdDataReader::PRODUCT_SUPPLIER_HEADER,
                AdDataReader::PRODUCT_DISPLAY_HEADER,
            ]
        );

        $productProviderCode = $this->readProductProviderCode($data);

        return new ProductInfo(
            AdDataReader::PRODUCT_PROVIDER_NAME,
            $productProviderCode,
            $this->readBrandCode($data),
            $this->readBrandName($data),
            $this->readProductBrandCode($data),
            $this->readProductNames($data),
            $this->readProductProviderCategory($data),
            $this->readOeNumbers($data),
            $this->readSupplier($data),
            AdDataReader::PRODUCT_PROVIDER_NAME . '-' . $productProviderCode,
            $this->readIsVisible($data)
        );
    }

    private function readProductProviderCode(array $data): string
    {
        return \trim((string)$data[AdDataReader::PRODUCT_PROVIDER_CODE_HEADER]);
    }

    private function readBrandCode(array $data): ?int
    {
        $brandCodeData = \trim((string)$data[AdDataReader::BRAND_CODE_HEADER]);

        return $brandCodeData !== '' ? (int)$brandCodeData : null;
    }

    private function readBrandName(array $data): ?string
    {
        $brandNameData = \trim((string)$data[AdDataReader::BRAND_NAME_HEADER]);

        return $brandNameData !== '' ? $brandNameData : null;
    }

    private function readProductBrandCode(array $data): ?string
    {
        $productBrandCodeData = \trim((string)$data[AdDataReader::PRODUCT_BRAND_CODE_HEADER]);

        return $productBrandCodeData !== '' ? $productBrandCodeData : null;
    }

    private function readProductNames(array $data): array
    {
        $default = $this->readProductName($data, AdDataReader::PRODUCT_NAME_HEADER);
        $ru = $this->readProductName($data, AdDataReader::PRODUCT_NAME_RU_HEADER) ?? $default;
        $en = $this->readProductName($data, AdDataReader::PRODUCT_NAME_EN_HEADER) ?? $default;

        return [
            ProductInfoReaderInterface::DEFAULT_LOCALE => $default,
            ProductInfoReaderInterface::LOCALE_RU => $ru,
            ProductInfoReaderInterface::LOCALE_EN => $en,
        ];
    }

    private function readProductName(array $data, string $header): ?string
    {
        $productName = \trim((string)$data[$header]);

        return $productName !== '' ? $productName : null;
    }

    private function readProductProviderCategory(array $data): ?string
    {
        $productProviderCategoryData = \trim((string)$data[AdDataReader::PRODUCT_PROVIDER_CATEGORY_HEADER]);

        return $productProviderCategoryData !== '' ? $productProviderCategoryData : null;
    }

    private function readOeNumbers(array $data): array
    {
        $inputData = $data[AdDataReader::PRODUCT_OE_NUMBERS_HEADER];

        return OeNumberHelper::tokenizeOeNumbers($inputData);
    }

    private function readSupplier(array $data): ?string
    {
        $supplierData = \trim((string)$data[AdDataReader::PRODUCT_SUPPLIER_HEADER]);

        return $supplierData !== '' ? $supplierData : null;
    }

    private function readIsVisible(array $data): bool
    {
        return \trim((string)$data[AdDataReader::PRODUCT_DISPLAY_HEADER]) === '0';
    }
}
