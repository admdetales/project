<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer;

use App\Exception\DatabaseException;
use App\Importer\Processor\ProductInfo;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectManager;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImportResultLoggerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Port\Reader\ReaderFactory;
use Psr\Log\LoggerInterface;

class ProductImporter extends ResourceImporter
{
    /** @var Registry  */
    private $doctrine;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        ReaderFactory $readerFactory,
        ObjectManager $objectManager,
        ResourceProcessorInterface $resourceProcessor,
        ImportResultLoggerInterface $importerResult,
        Registry $doctrine,
        int $batchSize,
        bool $failOnIncomplete,
        bool $stopOnFailure
    ) {
        parent::__construct(
            $readerFactory,
            $objectManager,
            $resourceProcessor,
            $importerResult,
            $batchSize,
            $failOnIncomplete,
            $stopOnFailure
        );

        $this->doctrine = $doctrine;
    }

    public function importData(int $i, array $row): bool
    {
        try {
            $this->resourceProcessor->process($row);
            $this->result->success($i);

            ++$this->batchCount;
        } catch (ItemIncompleteException $e) {
            $this->result->setMessage($e->getMessage());
            $this->result->getLogger()->critical($e->getMessage());
            if ($this->failOnIncomplete) {
                $this->result->failed($i);
            } else {
                $this->result->skipped($i);
            }
        } catch (ImporterException $e) {
            $this->result->failed($i);
            $this->result->setMessage($e->getMessage());
            $this->result->getLogger()->critical($e->getMessage());
        } catch (DatabaseException $e) {
            $this->recreateEntityManager();
        } catch (\Throwable $e) {
            if (!$this->objectManager->isOpen()) {
                $this->recreateEntityManager();
            }
        }

        return false;
    }

    private function recreateEntityManager(): void
    {
        $manager = $this->doctrine->resetManager();

        $this->objectManager = $manager;

        if ($this->resourceProcessor instanceof ProductInfo) {
            $this->resourceProcessor->setEntityManager($manager);
        }

        $this->batchCount = 0;
    }
}
