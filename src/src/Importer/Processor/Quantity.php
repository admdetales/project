<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Processor;

use App\Constant\ImportExport;
use App\Entity\Product\ProductInterface;
use App\Entity\Product\ProductVariantInterface;
use App\Entity\Product\ProductVariantWarehouseStock;
use App\Exception\ResourceNotFoundException;
use App\Importer\DTO\ProductQuantity;
use App\Importer\Reader\ProductQuantityReaderInterface;
use App\Repository\Channel\ChannelRepositoryInterface;
use App\Repository\Product\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Product\Factory\ProductVariantFactoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Taxation\Model\TaxCategoryInterface;
use Sylius\Component\Taxation\Repository\TaxCategoryRepositoryInterface;

class Quantity implements ResourceProcessorInterface
{
    /** @var ProductQuantityReaderInterface */
    private $quantityReader;
    /** @var ProductRepositoryInterface */
    private $productRepository;
    /** @var ProductVariantFactoryInterface */
    private $productVariantFactory;
    /** @var FactoryInterface */
    private $channelPricingFactory;
    /** @var ChannelRepositoryInterface */
    private $channelRepository;
    /** @var TaxCategoryRepositoryInterface */
    private $taxCategoryRepository;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var string[]|null */
    private $channelsCode;
    /** @var array */
    private $stockCache = [];

    public function __construct(
        ProductQuantityReaderInterface $quantityReader,
        ProductRepositoryInterface $productRepository,
        ProductVariantFactoryInterface $productVariantFactory,
        FactoryInterface $channelPricingFactory,
        ChannelRepositoryInterface $channelRepository,
        TaxCategoryRepositoryInterface $taxCategoryRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->quantityReader = $quantityReader;
        $this->productRepository = $productRepository;
        $this->productVariantFactory = $productVariantFactory;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->channelRepository = $channelRepository;
        $this->taxCategoryRepository = $taxCategoryRepository;
        $this->entityManager = $entityManager;
    }

    public function process(array $data): void
    {
        $productQuantity = $this->quantityReader->read($data);
        $productCode = $productQuantity->getProduct()->getCode();

        try {
            $product = $this->productRepository->getOneByCodeWithVariants($productCode);
        } catch (ResourceNotFoundException $e) {
            return;
        }

        if ($productQuantity->getProduct()->getProductProviderName() != 'AD') {
            if (
                !is_null($product->getBrandCode()) &&
                !is_null($product->getBrand()) &&
                !is_null($product->getBrand()->getTecDocBrandId())
            ) {
                try {
                    $mainProduct = $this->productRepository->getOneForImportByBrandAndProductId(
                        $product->getBrandCode(),
                        $product->getBrand()->getTecDocBrandId()
                    );
                    $mainProduct = is_array($mainProduct) ? reset($mainProduct) : $mainProduct;

                    if (is_object($mainProduct) && $mainProduct->getCode() != $product->getCode()) {
                        $productVariant = $mainProduct->findVariantByCode($mainProduct->getCode());
                        $product->setEnabled(false);
                    }
                    $this->entityManager->persist($product);
                } catch (ResourceNotFoundException $e) {
                    $productVariant = $mainProduct = null;
                }
            }
        }

        $productVariant = $productVariant ?? $this->getProductVariant($product, $productQuantity);
        $productVariant->setUpdatedAt(new \DateTimeImmutable());

        $stock = $productQuantity->getQuantity()->getQuantity();
        $warehouseName = $productQuantity->getQuantity()->getWarehouse();
        $warehouseStock = $productVariant->getWarehouseStockByName($warehouseName);

        if ($warehouseStock === null) {
            $warehouseStock = new ProductVariantWarehouseStock($productVariant, $warehouseName, $stock);
            $this->entityManager->persist($warehouseStock);
        }

        $stockCacheKey = $productCode . $warehouseName;
        if (isset($this->stockCache[$stockCacheKey])) {
            $this->stockCache[$stockCacheKey] += $stock;
            $stock = $this->stockCache[$stockCacheKey];
        } else {
            $this->stockCache[$stockCacheKey] = $stock;
        }
        $warehouseStock->setStock($stock);

        $productVariant->recalculateOnHandFromWarehouseStock();
        // Always track stock
        $productVariant->setTracked(true);
    }

    private function getProductVariant(
        ProductInterface $product,
        ProductQuantity $productQuantity
    ): ProductVariantInterface {
        $productVariantCode = $productQuantity->getProduct()->getCode();
        $productVariant = $product->findVariantByCode($productVariantCode);

        if ($productVariant === null) {
            $productVariant = $this->productVariantFactory->createForProduct($product);
            $productVariant->setCode($productVariantCode);
            $productVariant->setName($productVariantCode);
            $productVariant->setCurrentLocale(ImportExport::LOCALE_RU);
            $productVariant->setFallbackLocale(ImportExport::LOCALE_RU);
            $productVariant->setName($productVariantCode);
            $productVariant->setCurrentLocale(ImportExport::LOCALE_EN);
            $productVariant->setFallbackLocale(ImportExport::LOCALE_EN);
            $productVariant->setName($productVariantCode);

            $product->addVariant($productVariant);
            $this->entityManager->persist($productVariant);
        }

        if ($productVariant->getTaxCategory() === null) {
            /** @var TaxCategoryInterface|null $defaultTaxCategory */
            $defaultTaxCategory = $this->taxCategoryRepository->findOneBy(
                ['code' => ImportExport::DEFAULT_TAX_CATEGORY]
            );
            if ($defaultTaxCategory !== null) {
                $productVariant->setTaxCategory($defaultTaxCategory);
            }
        }

        foreach ($this->getChannelsCode() as $channelCode) {
            $channelPricing = $productVariant->getChannelPricingForChannelCode($channelCode);

            if (null === $channelPricing) {
                /** @var ChannelPricingInterface $channelPricing */
                $channelPricing = $this->channelPricingFactory->createNew();
                $channelPricing->setChannelCode($channelCode);
                $channelPricing->setPrice(0);
                $channelPricing->setOriginalPrice(0);
                $productVariant->addChannelPricing($channelPricing);

                $this->entityManager->persist($channelPricing);
            }
        }

        return $productVariant;
    }

    /**
     * @return string[]
     */
    private function getChannelsCode(): array
    {
        if ($this->channelsCode === null) {
            $this->channelsCode = $this->channelRepository->getChannelsCode();
        }

        return $this->channelsCode;
    }
}
