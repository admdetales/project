<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Processor;

use App\Entity\Product\ProductVariantInterface;
use App\Exception\ResourceNotFoundException;
use App\Importer\Reader\ProductPriceReaderInterface;
use App\Repository\Channel\ChannelRepositoryInterface;
use App\Repository\Product\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class Price implements ResourceProcessorInterface
{
    /** @var ProductRepositoryInterface */
    private $productRepository;
    /** @var ChannelRepositoryInterface */
    private $channelRepository;
    /** @var FactoryInterface */
    private $channelPricingFactory;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var ProductPriceReaderInterface */
    private $productPriceReader;
    /** @var string[]|null */
    private $channelsCode;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ChannelRepositoryInterface $channelRepository,
        FactoryInterface $channelPricingFactory,
        EntityManagerInterface $entityManager,
        ProductPriceReaderInterface $reader
    ) {
        $this->productRepository = $productRepository;
        $this->channelRepository = $channelRepository;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->entityManager = $entityManager;
        $this->productPriceReader = $reader;
    }

    /** {@inheritdoc} */
    public function process(array $data): void
    {
        $productPrice = $this->productPriceReader->read($data);

        try {
            $product = $this->productRepository->getOneByCodeWithVariants($productPrice->getProduct()->getCode());
        } catch (ResourceNotFoundException $e) {
            return;
        }

        if ($productPrice->getProduct()->getProductProviderName() != 'AD') {
            if (
                !is_null($product->getBrandCode()) &&
                !is_null($product->getBrand()) &&
                !is_null($product->getBrand()->getTecDocBrandId())
            ) {
                try {
                    $mainProduct = $this->productRepository->getOneForImportByBrandAndProductId(
                        $product->getBrandCode(),
                        $product->getBrand()->getTecDocBrandId()
                    );
                    $mainProduct = is_array($mainProduct) ? reset($mainProduct) : $mainProduct;
                } catch (ResourceNotFoundException $e) {
                    $mainProduct = null;
                }
            }
        }

        $price = $productPrice->getPrice()->getProductPrice();
        $originalPrice = $productPrice->getPrice()->getProductOriginalPrice();

//      Check if this is duplicate from Other import
        if (
            !empty($mainProduct) &&
            is_object($mainProduct) && $mainProduct->getCode() != $product->getCode()
        ) {
            $product->setEnabled(false);
        } else {
            // disable product if one of price is zero.
            // Product enabled status is managed by product info processor, so disable only on invalid price here
            if (!($price > 0 && $originalPrice > 0)) {
                // disable product if one of price is zero
                $product->setEnabled(false);
            }
        }

        $channelsCode = $this->getChannelsCode();

        /** @var ProductVariantInterface $productVariant */
        foreach ($product->getVariants() as $productVariant) {
            foreach ($channelsCode as $channelCode) {
                $channelPricing = $productVariant->getChannelPricingForChannelCode($channelCode);

                if (null === $channelPricing) {
                    /** @var ChannelPricingInterface $channelPricing */
                    $channelPricing = $this->channelPricingFactory->createNew();
                    $channelPricing->setChannelCode($channelCode);
                    $productVariant->addChannelPricing($channelPricing);

                    $this->entityManager->persist($channelPricing);
                }

                $channelPricing->setOriginalPrice($originalPrice);
                $channelPricing->setPrice($price);
            }
        }
    }

    /**
     * @return string[]
     */
    private function getChannelsCode(): array
    {
        if ($this->channelsCode === null) {
            $this->channelsCode = $this->channelRepository->getChannelsCode();
        }

        return $this->channelsCode;
    }
}
