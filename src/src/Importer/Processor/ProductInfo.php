<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Importer\Processor;

use App\Constant\ImportExport;
use App\Entity\Brand\BrandInterface;
use App\Entity\Brand\InternalBrand;
use App\Entity\Product\OeNumber;
use App\Entity\Product\Product;
use App\Entity\Product\ProductInterface;
use App\Entity\Supplier\Supplier;
use App\Entity\Taxonomy\InternalCategory;
use App\Entity\Taxonomy\TaxonInterface;
use App\Exception\DatabaseException;
use App\Exception\ResourceNotFoundException;
use App\Importer\DTO\ProductInfo as ProductDTO;
use App\Importer\Reader\ProductInfoReaderInterface;
use App\Repository\Brand\BrandRepositoryInterface;
use App\Repository\Brand\InternalBrandRepositoryInterface;
use App\Repository\Channel\ChannelRepositoryInterface;
use App\Repository\Product\ProductRepositoryInterface;
use App\Repository\Taxon\InternalCategoryRepositoryInterface;
use App\Repository\Taxon\TaxonRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Product\Factory\ProductFactoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ProductInfo implements ResourceProcessorInterface
{
    /** @var ProductInfoReaderInterface */
    private $reader;
    /** @var ChannelRepositoryInterface */
    private $channelRepository;
    /** @var ProductRepositoryInterface */
    private $productRepository;
    /** @var TaxonRepositoryInterface */
    private $taxonRepository;
    /** @var BrandRepositoryInterface */
    private $brandRepository;
    /** @var InternalBrandRepositoryInterface */
    private $internalBrandRepository;
    /** @var InternalCategoryRepositoryInterface */
    private $internalCategoryRepository;
    /** @var ProductFactoryInterface */
    private $productFactory;
    /** @var FactoryInterface */
    private $productTaxonFactory;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        ProductInfoReaderInterface $reader,
        ChannelRepositoryInterface $channelRepository,
        ProductRepositoryInterface $productRepository,
        BrandRepositoryInterface $brandRepository,
        InternalBrandRepositoryInterface $internalBrandRepository,
        TaxonRepositoryInterface $taxonRepository,
        InternalCategoryRepositoryInterface $internalCategoryRepository,
        ProductFactoryInterface $productFactory,
        FactoryInterface $productTaxonFactory,
        EntityManagerInterface $entityManager
    ) {
        $this->reader = $reader;
        $this->channelRepository = $channelRepository;
        $this->productRepository = $productRepository;
        $this->taxonRepository = $taxonRepository;
        $this->brandRepository = $brandRepository;
        $this->internalBrandRepository = $internalBrandRepository;
        $this->internalCategoryRepository = $internalCategoryRepository;
        $this->productFactory = $productFactory;
        $this->productTaxonFactory = $productTaxonFactory;
        $this->entityManager = $entityManager;
    }

    public function setEntityManager(EntityManagerInterface $em): void
    {
        $this->entityManager = $em;
    }

    /** @inheritDoc */
    public function process(array $data): void
    {
        try {
            $productInfo = $this->reader->read($data);

            $this->doProcess($productInfo);
        } catch (\Throwable $e) {
            if (!$this->entityManager->isOpen()) {
                throw new DatabaseException($e->getMessage());
            }

            throw new ImporterException($e->getMessage());
        }
    }

    private function doProcess(ProductDTO $productInfo): void
    {
        foreach ($this->entityManager->getUnitOfWork()->getScheduledEntityInsertions() as $insertion) {
            if ($insertion instanceof ProductInterface && $insertion->getCode() === $productInfo->getCode()) {
                return;
            }
        }

        $mainProduct = null;
        if (
            $productInfo->getProductProviderName() != 'AD' &&
            !is_null($productInfo->getProductBrandCode()) &&
            !is_null($productInfo->getBrandCode())
        ) {
            $mainProduct = $this->getProductBrandAndProductId(
                $productInfo->getProductBrandCode(),
                $productInfo->getBrandCode()
            );
        }

        $product = $this->getProduct($productInfo->getCode());

        $internalBrand = $this->getProductInternalBrand($product, $productInfo);
        $product->setInternalBrand($internalBrand);

        $brand = $this->getProductBrand($product, $productInfo, $internalBrand);
        if ($brand !== null) {
            $product->setBrand($brand);
            $brand->addInternalBrand($internalBrand);
        }

        $product->setBrandCode($productInfo->getProductBrandCode());
        $product->setSupplier($this->getSupplierByName($productInfo->getSupplier()));

//      Check if this is duplicate from Other import
        if (
            $productInfo->getProductProviderName() != 'AD' &&
            !empty($mainProduct) &&
            is_object($mainProduct) && $mainProduct->getCode() != $product->getCode()
        ) {
            $product->setEnabled(false);
        } else {
            $product->setEnabled($productInfo->isVisible());
        }

        $this->setProductName($product, $productInfo, ProductInfoReaderInterface::DEFAULT_LOCALE);
        $this->setProductName($product, $productInfo, ProductInfoReaderInterface::LOCALE_RU);
        $this->setProductName($product, $productInfo, ProductInfoReaderInterface::LOCALE_EN);

        $this->setProductSlug($product, $productInfo, ProductInfoReaderInterface::DEFAULT_LOCALE);
        $this->setProductSlug($product, $productInfo, ProductInfoReaderInterface::LOCALE_RU);
        $this->setProductSlug($product, $productInfo, ProductInfoReaderInterface::LOCALE_EN);

        $internalCategory = $this->getProductInternalCategory($product, $productInfo);
        $product->setInternalCategory($internalCategory);

        if ($internalCategory !== null) {
            $taxon = $this->getProductTaxon($product, $productInfo, $internalCategory);

            if ($taxon !== null) {
                $product->setMainTaxon($taxon);
                $this->addTaxonToProduct($product, $taxon);
                $internalCategory->setTaxon($taxon);
            } elseif (
                $internalCategory->getTaxon() === null
                && $product->getMainTaxon() !== null
            ) {
                $product->setMainTaxon(null);
                $product->getProductTaxons()->clear();
            }
        }

        $this->setProductOeNumbers($product, $productInfo->getProductOeNumbers());
        $this->setProductChannels($product);
    }

    private function getProduct(string $code): ProductInterface
    {
        try {
            return $this->productRepository->getOneForImport($code);
        } catch (ResourceNotFoundException $e) {
            /** @var ProductInterface $product */
            $product = $this->productFactory->createNew();
            $product->setCode($code);
            $this->entityManager->persist($product);

            return $product;
        }
    }

    private function getProductBrandAndProductId(string $productBrandCode, $brandCode)
    {
        try {
            $product = $this->productRepository->getOneForImportByBrandAndProductId($productBrandCode, $brandCode);
            return is_array($product) ? reset($product) : $product;
        } catch (ResourceNotFoundException $e) {
            return null;
        }
    }

    private function setProductOeNumbers(ProductInterface $product, array $oeNumbers): void
    {
        $productOeNumbers = [];
        foreach ($product->getOeNumbers() as $oeNumber) {
            $productOeNumber = $oeNumber->getCodeNumber();
            $productOeNumbers[] = $productOeNumber;
        }

        /** @var string $oeNumber */
        foreach ($oeNumbers as $oeNumber) {
            if (\in_array($oeNumber, $productOeNumbers, true)) {
                continue;
            }

            $newOeNumber = new OeNumber();
            $newOeNumber->setCodeNumber($oeNumber);
            $product->addOeNumber($newOeNumber);
            $this->entityManager->persist($newOeNumber);
        }
    }

    private function setProductChannels(ProductInterface $product): void
    {
        /** @var ChannelInterface[]|null $channels */
        $channels = $this->channelRepository->findAll();

        foreach ($channels as $channel) {
            $product->addChannel($channel);
        }
    }

    private function addTaxonToProduct(ProductInterface $product, TaxonInterface $taxon): void
    {
        $productTaxon = $product->findProductTaxonByCode($taxon->getCode());

        if (null === $productTaxon && !$this->isTaxonSet($product, $taxon)) {
            /** @var ProductTaxonInterface $productTaxon */
            $productTaxon = $this->productTaxonFactory->createNew();
            $productTaxon->setTaxon($taxon);
            $product->replaceProductTaxon($productTaxon);
        }
    }

    private function isTaxonSet(Product $product, TaxonInterface $taxon): bool
    {
        $stmt = $this->entityManager->getConnection()->prepare(
            'SELECT id FROM sylius_product_taxon WHERE product_id = :product_id AND taxon_id = :taxon_id'
        );

        $stmt->execute(['product_id' => $product->getId(), 'taxon_id' => $taxon->getId()]);

        return !empty($stmt->fetchAll());
    }

    private function setProductName(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo,
        string $locale
    ): void {
        $product->setCurrentLocale($locale);
        $product->setFallbackLocale($locale);
        $product->setName($productInfo->getProductNameByLocale($locale));
    }

    private function setProductSlug(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo,
        string $locale
    ): void {
        $product->setCurrentLocale($locale);
        $product->setFallbackLocale($locale);
        $product->setSlug($productInfo->getSlug());
    }

    private function getProductBrand(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo,
        InternalBrand $internalBrand
    ): ?BrandInterface {
        $productBrand = $product->getBrand();
        $brandName = $productInfo->getBrandName();
        $brandCode = $productInfo->getBrandCode();

        // First check assigned product brand.
        if (
            $productBrand !== null
            && ($productBrand->getTecDocBrandId() === $brandCode || $productBrand->getName() === $brandName)
        ) {
            return $productBrand;
        }

        // Next try to get brand from internal brand.
        $productBrand = $internalBrand->getBrand();
        if ($productBrand !== null) {
            return $productBrand;
        }

        if ($brandCode !== null) {
            /** @var BrandInterface $productBrand */
            $productBrand = $this->brandRepository->findOneBy(['tecDocBrandId' => $brandCode]);
        }

        if ($brandName !== null && $productBrand === null) {
            /** @var BrandInterface $productBrand */
            $productBrand = $this->brandRepository->findOneBy(['name' => $brandName]);
        }

        return $productBrand;
    }

    private function getProductInternalBrand(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo
    ): InternalBrand {
        $productInternalBrand = $product->getInternalBrand();
        $brandName = $productInfo->getBrandName();
        $brandCode = $productInfo->getBrandCode();

        if (
            $productInternalBrand !== null
            && $productInternalBrand->getName() === $brandName
            && $productInternalBrand->getTecDocBrandId() === $brandCode
        ) {
            return $productInternalBrand;
        }

        foreach ($this->entityManager->getUnitOfWork()->getScheduledEntityInsertions() as $insertion) {
            if (
                $insertion instanceof InternalBrand
                && $insertion->getName() === $brandName
                && $insertion->getTecDocBrandId() === $brandCode
            ) {
                return $insertion;
            }
        }

        $internalBrand = $this->internalBrandRepository->findOneBy(
            [
                'name' => $brandName,
                'tecDocBrandId' => $brandCode,
            ]
        );

        if ($internalBrand === null) {
            $internalBrand = new InternalBrand();
            $internalBrand->setName($brandName);
            $internalBrand->setTecDocBrandId($brandCode);
            $this->entityManager->persist($internalBrand);
        }

        return $internalBrand;
    }

    private function getProductInternalCategory(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo
    ): ?InternalCategory {
        $productProviderCategory = $productInfo->getProductProviderCategory();

        $productInternalCategory = $product->getInternalCategory();
        if ($productInternalCategory !== null && $productInternalCategory->getName() === $productProviderCategory) {
            return $productInternalCategory;
        }

        if ($productProviderCategory === null) {
            return null;
        }

        foreach ($this->entityManager->getUnitOfWork()->getScheduledEntityInsertions() as $insertion) {
            if ($insertion instanceof InternalCategory && $insertion->getName() === $productProviderCategory) {
                return $insertion;
            }
        }

        /** @var InternalCategory|null $internalCategory */
        $internalCategory = $this->internalCategoryRepository->findOneBy(['name' => $productProviderCategory]);
        if ($internalCategory === null) {
            $internalCategory = new InternalCategory();
            $internalCategory->setName($productProviderCategory);
            $this->entityManager->persist($internalCategory);
        }

        return $internalCategory;
    }

    private function getProductTaxon(
        ProductInterface $product,
        \App\Importer\DTO\ProductInfo $productInfo,
        InternalCategory $internalCategory
    ): ?TaxonInterface {
        /** @var TaxonInterface|null $productTaxon */
        $productTaxon = $product->getMainTaxon();
        if ($productTaxon !== null) {
            $productTaxon->setCurrentLocale(ProductInfoReaderInterface::DEFAULT_LOCALE);
            $productTaxon->setFallbackLocale(ProductInfoReaderInterface::DEFAULT_LOCALE);
            if (
                \strcasecmp((string)$productTaxon->getName(), (string)$productInfo->getProductProviderCategory()) === 0
            ) {
                return $productTaxon;
            }
        }

        $internalTaxon = $internalCategory->getTaxon();
        if ($internalTaxon !== null) {
            return $internalTaxon;
        }

        return $this->taxonRepository->findByName(
            $productInfo->getProductProviderCategory(),
            ImportExport::DEFAULT_LOCALE
        )[0] ?? null;
    }

    /**
     * @param string|null $supplierName
     * @return Supplier|object|null
     */
    private function getSupplierByName(?string $supplierName)
    {
        $supplier = null;

        if (!empty($supplierName)) {
            $supplier = $this->entityManager->getRepository(Supplier::class)
                ->findOneBy(['name' => $supplierName])
            ;

            if (is_null($supplier)) {
                $supplier = (new Supplier())->setName($supplierName);
                $this->entityManager->persist($supplier);
                $this->entityManager->flush();
            }
        }

        return $supplier;
    }
}
