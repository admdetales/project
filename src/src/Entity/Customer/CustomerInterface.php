<?php

declare(strict_types=1);

namespace App\Entity\Customer;

use App\Entity\Consent\ConsentAgreement;
use Sylius\Component\Core\Model\CustomerInterface as BaseCustomerInterface;

interface CustomerInterface extends BaseCustomerInterface
{
    public function addConsentAgreement(ConsentAgreement $consentAgreement): void;

    public function removeConsentAgreement(ConsentAgreement $consentAgreement): void;
}
