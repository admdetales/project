<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Brand\BrandInterface;
use App\Entity\Brand\InternalBrand;
use App\Entity\Supplier\Supplier;
use App\Entity\Supplier\SupplierInterface;
use App\Entity\Taxonomy\InternalCategory;
use App\Entity\TecDoc\TecDocDocument;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ProductInterface as BaseProductInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;

interface ProductInterface extends BaseProductInterface
{
    public function getBrandCode(): ?string;
    public function setBrandCode(?string $brandCode): void;

    public function getBrand(): ?BrandInterface;
    public function setBrand(?BrandInterface $brand): void;
    public function getBrandNameIndex(): string;

    public function isMostPopular(): bool;
    public function setMostPopular(bool $mostPopular): void;

    public function isWeekly(): bool;
    public function isInStock(): bool;
    public function setWeekly(bool $weekly): void;

    /**
     * @return OeNumber[]|Collection
     */
    public function getOeNumbers(): Collection;
    public function addOeNumber(OeNumber $code): void;
    public function getOeNumbersSearchIndex(): string;

    /** @return TecDocDocument[] */
    public function getTecDocDocuments(): array;
    public function addTecDocDocument(TecDocDocument $document): void;

    public function hasVehicles(): bool;
    public function setHasVehicles(bool $hasVehicles): void;

    public function getInternalBrand(): ?InternalBrand;
    public function setInternalBrand(?InternalBrand $internalBrand): void;

    public function getInternalCategory(): ?InternalCategory;
    public function setInternalCategory(?InternalCategory $internalCategory): void;

    public function replaceProductTaxon(ProductTaxonInterface $productTaxon): void;

    public function setTecDocName(string $name): void;
    public function addTecDocAttribute(AttributeValueInterface $attribute): void;
    /** @return AttributeValueInterface[] */
    public function getTecDocAttributes(): array;
    public function addTecDocImage(ImageInterface $image): void;

    public function findVariantByCode(string $code): ?ProductVariantInterface;

    public function findProductTaxonByCode(string $code): ?ProductTaxonInterface;

    public function getSupplier(): ?Supplier;
    public function setSupplier(?SupplierInterface $supplier): void;
}
