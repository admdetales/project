<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ProductVariantInterface as BaseProductVariantInterface;

interface ProductVariantInterface extends BaseProductVariantInterface
{
    public function getChannelPricingForChannelCode(string $channelCode): ?ChannelPricingInterface;

    /**
     * @return ProductVariantWarehouseStock[]|Collection
     */
    public function getWarehousesStock(): Collection;

    /**
     * @param string $warehouseName
     * @return ProductVariantWarehouseStock|null
     */
    public function getWarehouseStockByName(string $warehouseName): ?ProductVariantWarehouseStock;

    /**
     * @param ProductVariantWarehouseStock $warehouseStock
     */
    public function addWarehouseStock(ProductVariantWarehouseStock $warehouseStock): void;

    /**
     * @param ProductVariantWarehouseStock $warehouseStock
     */
    public function removeWarehouseStock(ProductVariantWarehouseStock $warehouseStock): void;

    /**
     * @return void
     */
    public function recalculateOnHandFromWarehouseStock(): void;
}
