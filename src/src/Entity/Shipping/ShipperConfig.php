<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Shipping;

use App\Entity\Channel\Channel;
use Omni\Sylius\ShippingPlugin\Entity\ShipperConfig as BaseShipperConfig;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="omni_shipper_config")
 */
class ShipperConfig extends BaseShipperConfig
{
    /**
     * @var Channel|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel\Channel", inversedBy="shipperConfigs")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @return Channel|null
     */
    public function getSender(): ?Channel
    {
        return $this->sender;
    }

    /**
     * @param Channel $sender
     */
    public function setSender($sender): void
    {
        $this->sender = $sender;
    }
}
