<?php

declare(strict_types=1);

namespace App\Entity\Order;

use App\Entity\Customer\CustomerInterface;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Order as BaseOrder;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order")
 */
class Order extends BaseOrder implements OrderInterface
{
    public function getConsentSubject(): CustomerInterface
    {
        /** @var CustomerInterface $customer */
        $customer = $this->getCustomer();

        return $customer;
    }
}
