<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Brand;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Loevgaard\SyliusBrandPlugin\Model\Brand as BaseBrand;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="loevgaard_brand")
 */
class Brand extends BaseBrand implements BrandInterface
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $tecDocBrandId;

    /**
     * @var Collection|InternalBrand[]
     * @ORM\OneToMany(targetEntity="App\Entity\Brand\InternalBrand", mappedBy="brand")
     */
    private $internalBrands;

    public function __construct()
    {
        parent::__construct();
        $this->internalBrands = new ArrayCollection();
    }

    public function getTecDocBrandId(): ?string
    {
        return $this->tecDocBrandId;
    }

    public function setTecDocBrandId(?string $tecDocBrandId): void
    {
        $this->tecDocBrandId = $tecDocBrandId;
    }

    /**
     * @return Collection|InternalBrand[]
     */
    public function getInternalBrands(): Collection
    {
        return $this->internalBrands;
    }

    public function addInternalBrand(InternalBrand $internalBrand): void
    {
        if (!$this->internalBrands->contains($internalBrand)) {
            $this->internalBrands->add($internalBrand);
            $internalBrand->setBrand($this);
        }
    }

    public function removeInternalBrand(InternalBrand $internalBrand): void
    {
        if ($this->internalBrands->contains($internalBrand)) {
            $this->internalBrands->removeElement($internalBrand);
            $internalBrand->setBrand(null);
        }
    }
}
