<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Brand;

use Doctrine\Common\Collections\Collection;
use Loevgaard\SyliusBrandPlugin\Model\BrandInterface as BaseBrandInterface;

interface BrandInterface extends BaseBrandInterface
{
    public function getTecDocBrandId(): ?string;
    public function setTecDocBrandId(?string $tecDocBrandId): void;
    public function getInternalBrands(): Collection;
    public function addInternalBrand(InternalBrand $internalBrand): void;
    public function removeInternalBrand(InternalBrand $internalBrand): void;
}
