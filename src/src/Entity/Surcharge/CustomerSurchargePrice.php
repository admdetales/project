<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Surcharge;

use App\Entity\Channel\ChannelPricingInterface;
use App\Entity\Customer\CustomerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Surcharge\CustomerSurchargePriceRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *         name="ux_surcharge_customer_channel_pricing",
 *         columns={"customer_id", "channel_pricing_id"}
 *     )}
 * )
 * @UniqueEntity(fields={"customer", "channelPricing"}, errorPath="customer")
 */
class CustomerSurchargePrice implements CustomerSurchargePriceInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CustomerInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer\Customer")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $customer;

    /**
     * @var ChannelPricingInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel\ChannelPricing", inversedBy="surchargePrices")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $channelPricing;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Assert\NotBlank()
     * @Assert\Range(min=0, max=100)
     */
    private $amount;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomer(): ?CustomerInterface
    {
        return $this->customer;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomer(CustomerInterface $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function getChannelPricing(): ?ChannelPricingInterface
    {
        return $this->channelPricing;
    }

    /**
     * {@inheritdoc}
     */
    public function setChannelPricing(ChannelPricingInterface $channelPricing): void
    {
        if ($this->channelPricing !== $channelPricing) {
            $this->channelPricing = $channelPricing;
            $channelPricing->addSurchargePrice($this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }
}
