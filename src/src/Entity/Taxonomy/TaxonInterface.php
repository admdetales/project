<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Taxonomy;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\TaxonInterface as BaseTaxonInterface;
use Sylius\Component\Taxonomy\Model\TaxonTranslationInterface;

interface TaxonInterface extends BaseTaxonInterface
{
    public const TREE_CODE = '0';

    public const HOMEPAGE_CODES = [
        '1',
        '2',
        '3',
        '5',
        '6',
        '7',
        '11',
        '12',
        '15',
        '17',
        '19',
        '20',
        '22',
        '26',
        '31',
    ];

    public const INNER_PAGE_CODES = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '22',
        '23',
        '24',
        '25',
        '26',
        '30',
        '31',
    ];

    /** @return array<int> */
    public function getGenericArticleIds(): array;
    /**
     * @param array<int> $genericArticleIds
     */
    public function setGenericArticleIds(array $genericArticleIds): void;
    /**
     * @return Collection|InternalCategory[]
     */
    public function getInternalCategories(): Collection;
    public function addInternalCategory(InternalCategory $internalCategory): void;
    public function removeInternalCategory(InternalCategory $internalCategory): void;
    public function createTranslation(): TaxonTranslationInterface;
}
