<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Product;

use App\Entity\Brand\InternalBrand;
use App\Entity\Product\ProductInterface;
use App\Entity\Taxonomy\InternalCategory;
use App\Exception\ResourceNotFoundException;
use App\Normalizer\SearchQueryNormalizer;
use Doctrine\ORM\QueryBuilder;
use Loevgaard\SyliusBrandPlugin\Doctrine\ORM\ProductRepositoryInterface as BaseProductRepositoryInterface;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Sylius\Component\Channel\Model\ChannelInterface as ModelChannelInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\TaxonInterface;

/**
 * @method ProductInterface|null find(int $id)
 * @method ProductInterface[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 * @method ProductInterface|null findOneBy(array $criteria)
 */
interface ProductRepositoryInterface extends BaseProductRepositoryInterface
{
    /**
     * @param ChannelInterface $channel
     * @param string           $locale
     *
     * @return ProductInterface[]
     */
    public function findWeeklyByChannel(ChannelInterface $channel, string $locale): array;

    /**
     * @param ChannelInterface $channel
     * @param string           $locale
     *
     * @return ProductInterface[]
     */
    public function findMostPopularByChannel(ChannelInterface $channel, string $locale): array;

    /**
     * @param ModelChannelInterface $channel
     * @param string                $locale
     * @param Article[]             $articles
     *
     * @return ProductInterface[]
     */
    public function getProductsByArticles(ModelChannelInterface $channel, string $locale, array $articles): array;

    /**
     * @param ChannelInterface $channel
     * @param string           $locale
     * @param array            $oeNumbers
     * @param ProductInterface $product
     *
     * @return ProductInterface[]
     */
    public function findByOeNumbers(
        ChannelInterface $channel,
        string $locale,
        array $oeNumbers,
        ProductInterface $product
    ): array;

    /**
     * @param array $brandCodes
     * @return array
     */
    public function findAllByBrandCode(array $brandCodes): array;

    /**
     * @param int[]            $products
     * @param ChannelInterface $channel
     * @param TaxonInterface   $taxon
     * @param string           $locale
     * @param array            $sorting
     * @param bool             $includeAllDescendants
     *
     * @return QueryBuilder
     */
    public function createVehicleShopListQueryBuilder(
        array $products,
        ChannelInterface $channel,
        TaxonInterface $taxon,
        string $locale,
        array $sorting = [],
        bool $includeAllDescendants = false
    ): QueryBuilder;

    public function createSearchQueryBuilder(
        SearchQueryNormalizer $searchQueryNormalizer,
        ChannelInterface $channel,
        string $locale,
        string $query,
        array $sorting = []
    ): QueryBuilder;

    /**
     * @param string      $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null);

    public function updateProductsBrandByInternalBrand(InternalBrand $internalBrand): void;

    public function updateProductsTaxonByInternalCategory(InternalCategory $internalCategory): void;

    /**
     * @param TaxonInterface $taxon
     * @return int[]
     */
    public function getProductsIdsWithTaxon(TaxonInterface $taxon): array;

    /**
     * @param string $code
     * @return ProductInterface
     * @throws ResourceNotFoundException
     */
    public function getOneByCodeWithVariants(string $code): ProductInterface;

    /**
     * @param string $code
     * @return ProductInterface
     * @throws ResourceNotFoundException
     */
    public function getOneForImport(string $code): ProductInterface;

    /**
     * @param string $productBrandCode
     * @param string $brandCode
     */
    public function getOneForImportByBrandAndProductId(string $productBrandCode, $brandCode);
}
