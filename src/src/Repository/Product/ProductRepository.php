<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Product;

use App\Entity\Brand\InternalBrand;
use App\Entity\Product\ProductInterface;
use App\Entity\Taxonomy\InternalCategory;
use App\Exception\ResourceNotFoundException;
use App\Normalizer\SearchQueryNormalizer;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Loevgaard\SyliusBrandPlugin\Doctrine\ORM\ProductRepositoryTrait;
use Nfq\Bundle\TecDocBundle\Entity\Article;
use Omni\Sylius\SearchPlugin\Model\SearchIndexInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository as BaseProductRepository;
use Sylius\Component\Channel\Model\ChannelInterface as ModelChannelInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\TaxonInterface;

use function addcslashes;
use function array_map;

class ProductRepository extends BaseProductRepository implements ProductRepositoryInterface
{
    use ProductRepositoryTrait;

    /**
     * {@inheritDoc}
     */
    public function findWeeklyByChannel(ChannelInterface $channel, string $locale): array
    {
        return $this->createListQueryBuilder($locale)
            ->andWhere(':channel MEMBER OF o.channels')
            ->andWhere('o.enabled = true')
            ->andWhere('o.weekly = true')
            ->setParameter('channel', $channel)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findMostPopularByChannel(ChannelInterface $channel, string $locale): array
    {
        return $this->createListQueryBuilder($locale)
            ->andWhere(':channel MEMBER OF o.channels')
            ->andWhere('o.enabled = true')
            ->andWhere('o.mostPopular = true')
            ->setParameter('channel', $channel)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function getProductsByArticles(ModelChannelInterface $channel, string $locale, array $articles): array
    {
        if (!$articles) {
            return [];
        }

        $qb = $this->createListQueryBuilder($locale);
        $qb->innerJoin('o.brand', 'brand');

        $qb->andWhere(':channel MEMBER OF o.channels');
        $qb->andWhere('o.enabled = true');
        $qb->setParameter('channel', $channel);

        $qb->andWhere('o.brandCode IN (:brandCodes)');
        $qb->setParameter(
            'brandCodes',
            $this->getBrandCodes($articles)
        );

        $qb->andWhere('brand.tecDocBrandId IN (:brandNumbers)');
        $qb->setParameter(
            'brandNumbers',
            array_map(
                static function (Article $article) {
                    return $article->getBrandNumber();
                },
                $articles
            )
        );

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findByOeNumbers(
        ChannelInterface $channel,
        string $locale,
        array $oeNumbers,
        ProductInterface $product
    ): array {
        return $this->createListQueryBuilder($locale)
            ->join('o.oeNumbers', 'oc', 'WITH', 'oc.codeNumber IN (:oeNumbers)')
            ->andWhere(':channel MEMBER OF o.channels')
            ->andWhere('o.enabled = true')
            ->andWhere('o.id != :product')
            ->setParameter('channel', $channel)
            ->setParameter('oeNumbers', $oeNumbers)
            ->setParameter('product', $product)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findAllByBrandCode(array $brandCodes): array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.enabled = :enabled')
            ->andWhere('o.brandCode IN (:brandCodes)')
            ->setParameter('enabled', true)
            ->setParameter('brandCodes', $brandCodes)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function createVehicleShopListQueryBuilder(
        array $products,
        ChannelInterface $channel,
        TaxonInterface $taxon,
        string $locale,
        array $sorting = [],
        bool $includeAllDescendants = false
    ): QueryBuilder {
        $qb = $this->createShopListQueryBuilder($channel, $taxon, $locale, $sorting, $includeAllDescendants);

        $qb->andWhere('o.id IN (:products)');
        $qb->setParameter('products', $products);

        return $qb;
    }

    public function createSearchQueryBuilder(
        SearchQueryNormalizer $searchQueryNormalizer,
        ChannelInterface $channel,
        string $locale,
        string $query,
        array $sorting = []
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('o');

        $qb->addSelect('translation');
        $qb->innerJoin('o.translations', 'translation', Join::WITH, 'translation.locale = :locale');
        $qb->setParameter('locale', $locale);

        $qb->andWhere(':channel MEMBER OF o.channels');
        $qb->setParameter('channel', $channel);

        $qb->andWhere('o.enabled = true');

        $searchQuery = $searchQueryNormalizer->normalize($query);
        if ($searchQuery !== '') {
            $qb->innerJoin(
                SearchIndexInterface::class,
                'i',
                Join::WITH,
                $qb->expr()
                    ->andX(
                        $qb->expr()->eq('i.resourceId', 'o.id'),
                        $qb->expr()->eq('i.resourceClass', $qb->expr()->literal($this->getClassName())),
                        $qb->expr()->like('i.index', ':query')
                    )
            );
            $qb->setParameter('query', '%' . addcslashes($searchQuery, '%_') . '%');
        }

        $qb->groupBy('o.id');

        if (isset($sorting['price'])) {
            $qb->innerJoin('o.variants', 'variant');
            $qb->innerJoin('variant.channelPricings', 'channelPricing');

            $qb->andWhere('channelPricing.channelCode = :channelCode');
            $qb->setParameter('channelCode', $channel->getCode());
            $qb->addGroupBy('channelPricing.price');
        }

        return $qb;
    }

    public function updateProductsBrandByInternalBrand(InternalBrand $internalBrand): void
    {
        $internalBrandId = $internalBrand->getId();
        if ($internalBrandId === null) {
            return;
        }

        $brand = $internalBrand->getBrand();

        $this->createQueryBuilder('p')
            ->update()
            ->set('p.brand', ':brandId')
            ->setParameter('brandId', $brand !== null ? $brand->getId() : null)
            ->where('p.internalBrand = (:internalBrandId)')
            ->setParameter('internalBrandId', $internalBrandId)
            ->getQuery()->execute();
    }

    public function updateProductsTaxonByInternalCategory(InternalCategory $internalCategory): void
    {
        $internalCategoryId = $internalCategory->getId();
        if ($internalCategoryId === null) {
            return;
        }

        $taxon = $internalCategory->getTaxon();

        $this->createQueryBuilder('p')
            ->update()
            ->set('p.mainTaxon', ':mainTaxon')
            ->setParameter('mainTaxon', $taxon !== null ? $taxon->getId() : null)
            ->where('p.internalCategory = (:internalCategoryId)')
            ->setParameter('internalCategoryId', $internalCategoryId)
            ->getQuery()->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function getProductsIdsWithTaxon(TaxonInterface $taxon): array
    {
        $result = $this->createQueryBuilder('p')
            ->select('p.id AS id')
            ->andWhere('p.mainTaxon = :mainTaxon')
            ->setParameter('mainTaxon', $taxon)
            ->getQuery()->getArrayResult();

        return !empty($result) ? \array_column($result, 'id') : [];
    }

    /**
     * {@inheritdoc}
     */
    public function getOneByCodeWithVariants(string $code): ProductInterface
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('p', 'pv', 'pvcp')
                ->leftJoin('p.variants', 'pv')
                ->leftJoin('pv.channelPricings', 'pvcp')
                ->andWhere('p.code = :code')
                ->setParameter('code', $code)
                ->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            throw new ResourceNotFoundException(\sprintf('Product with code "%s" not found', $code));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOneForImport(string $code): ProductInterface
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('p', 'pib', 'pb', 'pic', 'ppt', 'poen')
                ->leftJoin('p.internalBrand', 'pib')
                ->leftJoin('p.brand', 'pb')
                ->leftJoin('p.internalCategory', 'pic')
                ->leftJoin('p.productTaxons', 'ppt')
                ->leftJoin('p.oeNumbers', 'poen')
                ->andWhere('p.code = :code')
                ->setParameter('code', $code)
                ->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            throw new ResourceNotFoundException(\sprintf('Product with code "%s" not found', $code));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByNamePart(string $phrase, string $locale, ?int $limit = null): array
    {
        if (!empty($phrase)) {
            $results = $this->createQueryBuilder('o')
                ->andWhere('o.code LIKE :code')
                ->setParameter('code', $phrase)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult()
            ;

            if (!empty($results)) {
                return $results;
            }
        }

        return $this->createQueryBuilder('o')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.name LIKE :name')
            ->setParameter('name', '%' . $phrase . '%')
            ->setParameter('locale', $locale)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getOneForImportByBrandAndProductId(string $productBrandCode, $brandCode)
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('p', 'pib', 'pb', 'pic', 'ppt', 'poen')
                ->leftJoin('p.internalBrand', 'pib')
                ->leftJoin('p.brand', 'pb')
                ->leftJoin('p.internalCategory', 'pic')
                ->leftJoin('p.productTaxons', 'ppt')
                ->leftJoin('p.oeNumbers', 'poen')
                ->andWhere('p.brandCode = :productBrandCode')
                ->andWhere('pb.tecDocBrandId = :brandCode')
                ->setParameter('productBrandCode', $productBrandCode)
                ->setParameter('brandCode', $brandCode)
                ->setMaxResults(1)
                ->orderBy('p.code')
                ->getQuery()->getResult();
        } catch (NoResultException $e) {
            throw new ResourceNotFoundException(
                \sprintf(
                    'Product with brandCode "%s" and brandCode "%s" "tecDocBrandId" not found',
                    $productBrandCode,
                    $brandCode
                )
            );
        }
    }

    /**
     * @param array $articles
     * @return string[]
     */
    protected function getBrandCodes(array $articles): array
    {
        $codes = array_map(
            static function (Article $article) {
                return $article->getNumber();
            },
            $articles
        );

        foreach ($codes as $code) {
            if (strpos($code, ' ') !== false) {
                $codes[] = str_replace(' ', '', $code);
            }
        }

        return $codes;
    }
}
