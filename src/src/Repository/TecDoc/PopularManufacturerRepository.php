<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\TecDoc;

use App\Entity\TecDoc\PopularManufacturer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Nfq\Bundle\TecDocBundle\Entity\VehicleManufacturer;

/**
 * @method PopularManufacturer|null find($id, $lockMode = null, $lockVersion = null)
 * @method PopularManufacturer|null findOneBy(array $criteria, array $orderBy = null)
 * @method PopularManufacturer[]    findAll()
 * @method PopularManufacturer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PopularManufacturerRepository extends ServiceEntityRepository implements PopularManufacturerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PopularManufacturer::class);
    }

    public function findByVehicleManufacturer(VehicleManufacturer $manufacturer): ?PopularManufacturer
    {
        return $this->findOneBy(['manufacturerId' => $manufacturer->getId()]);
    }
}
