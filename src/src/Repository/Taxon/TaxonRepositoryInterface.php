<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Taxon;

use App\Entity\Taxonomy\TaxonInterface;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface as BaseTaxonRepositoryInterface;

/**
 * @method TaxonInterface|null findOneBySlug(string $slug, string $locale)
 */
interface TaxonRepositoryInterface extends BaseTaxonRepositoryInterface
{
    public function findOneByNameAndParent(string $name, string $locale, ?TaxonInterface $taxon): ?TaxonInterface;

    /**
     * @param string[] $codes
     *
     * @return TaxonInterface[]
     */
    public function findByCode(array $codes): array;

    /**
     * @param string[] $codes
     * @param string $locale
     * @param string $sort
     *
     * @return TaxonInterface[]
     */
    public function findByCodesWithSort(array $codes, string $locale, ?string $sort = 'ASC'): array;

    /**
     * @param string $parentCode
     * @param string $locale
     * @param string $sort

     * @return TaxonInterface[]
     */
    public function findChildrenByCodeWithSort(
        string $parentCode,
        ?string $locale = null,
        ?string $sort = 'ASC'
    ): array;

    public function findOneByCode(string $code): ?TaxonInterface;

    /**
     * @return QueryBuilder
     */
    public function createListQueryBuilderForChoiceForm(): QueryBuilder;
}
