<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SystemConfigValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SystemConfigValue|null findOneBy(array $criteria, array $orderBy = null)
 */
final class SystemConfigValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SystemConfigValue::class);
    }

    public function replace(string $type, string $value): void
    {
        $configValue = $this->findOneBy(['type' => $type]);

        if (!$configValue) {
            $configValue = new SystemConfigValue($type, $value);
        } else {
            $configValue->update($value);
        }

        $this->getEntityManager()->persist($configValue);
    }
}
