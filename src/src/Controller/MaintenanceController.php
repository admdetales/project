<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use App\Manager\MaintenanceManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function implode;

final class MaintenanceController extends AbstractController
{
    /** @var MaintenanceManager */
    private $maintenanceManager;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(MaintenanceManager $maintenanceManager, EntityManagerInterface $entityManager)
    {
        $this->maintenanceManager = $maintenanceManager;
        $this->entityManager      = $entityManager;
    }

    /**
     * @Route("/", name="app_admin_maintenance_index", methods={"GET"})
     *
     * @return Response
     */
    public function manageMaintenanceAction(): Response
    {
        return $this->render(
            'Maintenance/manage.html.twig',
            [
                'isUnderMaintenance' => $this->maintenanceManager->isUnderMaintenance(),
                'whitelist'          => implode(',', $this->maintenanceManager->getWhitelist()),
            ]
        );
    }

    /**
     * @Route("/toggle", name="app_admin_maintenance_toggle", methods={"GET"})
     *
     * @return Response
     */
    public function toggleMaintenanceAction(): Response
    {
        $this->entityManager->transactional(
            function () {
                if ($this->maintenanceManager->isUnderMaintenance()) {
                    $this->maintenanceManager->disable();
                } else {
                    $this->maintenanceManager->enable();
                }
            }
        );

        return $this->redirectToRoute('app_admin_maintenance_index');
    }

    /**
     * @Route("/whitelist", name="app_admin_maintenance_whitelist", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function saveWhitelist(Request $request): Response
    {
        $this->entityManager->transactional(function () use ($request) {
            $this->maintenanceManager->saveWhitelist($request->get('whitelist', ''));
        });

        return $this->redirectToRoute('app_admin_maintenance_index');
    }
}
