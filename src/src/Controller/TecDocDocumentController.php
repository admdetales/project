<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use App\Downloader\TecDocDocumentDownloader;
use App\Exception\TecDoc\DocumentNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function sprintf;

final class TecDocDocumentController extends AbstractController
{
    /** @var TecDocDocumentDownloader */
    private $downloader;

    public function __construct(TecDocDocumentDownloader $downloader)
    {
        $this->downloader = $downloader;
    }

    /**
     * @Route("/image/{thumbnailId}.jpg", name="app_document_download_image", methods={"GET"})
     *
     * @param int $thumbnailId
     *
     * @return Response
     */
    public function downloadImageAction(int $thumbnailId): Response
    {
        try {
            $response = new Response($this->downloader->downloadImage($thumbnailId));

            $response->headers->set(
                'Content-Disposition',
                HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_INLINE, sprintf('%s.jpg', $thumbnailId))
            );
            $response->headers->set('Content-Type', 'image/jpeg');

            return $response;
        } catch (DocumentNotFoundException $e) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/document/{documentId}.pdf", name="app_document_download_document", methods={"GET"})
     *
     * @param int $documentId
     *
     * @return Response
     */
    public function downloadDocumentAction(int $documentId): Response
    {
        try {
            $response = new Response($this->downloader->downloadDocument($documentId));

            $response->headers->set(
                'Content-Disposition',
                HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_INLINE, sprintf('%s.pdf', $documentId))
            );
            $response->headers->set('Content-Type', 'application/pdf');

            return $response;
        } catch (DocumentNotFoundException $e) {
            throw $this->createNotFoundException();
        }
    }
}
