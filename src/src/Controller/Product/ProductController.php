<?php

declare(strict_types=1);

namespace App\Controller\Product;

use App\Decorator\ProductDecorator;
use App\Entity\Product\Product;
use App\Entity\Product\ProductInterface;
use App\Entity\Taxonomy\Taxon;
use App\Provider\Product\ProductAnaloguesProvider;
use App\Provider\Product\VehicleProductProvider;
use App\Provider\Taxon\VehicleTaxonProvider;
use App\Provider\TecDoc\ArticleProvider;
use FOS\RestBundle\View\View;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Grid\View\ResourceGridView;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use function array_map;
use function in_array;

class ProductController extends ResourceController
{
    private const ANALOGUE_PRODUCT_DISPLAY_LIMIT = 50;

    private const DECORATED_ROUTES = [
        'omni_sylius_search',
        'sylius_shop_weekly_product',
        'sylius_shop_most_popular_product',
        'sylius_shop_partial_product_association_show',
        'sylius_shop_partial_product_index_latest',
        'sylius_shop_partial_product_show_by_slug',
        'sylius_shop_product_index',
        'app_vehicle_product',
    ];

    public function indexAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::INDEX);
        $resources = $this->getResources($configuration);

        $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $resources);

        $view = View::create($resources);

        if ($configuration->isHtmlRequest()) {
            $view
                ->setTemplate($configuration->getTemplate(ResourceActions::INDEX . '.html'))
                ->setTemplateVar($this->metadata->getPluralName())
                ->setData(
                    [
                        'configuration'                  => $configuration,
                        'metadata'                       => $this->metadata,
                        'resources'                      => $resources,
                        $this->metadata->getPluralName() => $resources,
                    ]
                );
        }

        return $this->viewHandler->handle($configuration, $view);
    }

    public function productAnalogs(Request $request): Response
    {
        $productId = $request->get('productId');

        /** @var ProductAnaloguesProvider $productAnaloguesProvider */
        $productAnaloguesProvider = $this->get(ProductAnaloguesProvider::class);

        $analogueProducts = $productAnaloguesProvider->getProductAnalogues($productId);
        $limitedAnalogueProducts = array_slice($analogueProducts, 0, static::ANALOGUE_PRODUCT_DISPLAY_LIMIT);

        /** @var ProductDecorator $productDecorator */
        $productDecorator = $this->get(ProductDecorator::class);
        $productDecorator->decorateMultipleProducts($limitedAnalogueProducts);

        return $this->render(
            '@SyliusShop/Product/Show/_analogueCards.html.twig',
            [
                'analogues' => $limitedAnalogueProducts,
            ],
        );
    }

    protected function findOr404(RequestConfiguration $configuration): ResourceInterface
    {
        /** @var ProductInterface $resource */
        $resource = parent::findOr404($configuration);

        $route = $configuration->getRequest()->get('_route');
        if ($route === 'sylius_shop_product_show') {
            /** @var ProductDecorator $productDecorator */
            $productDecorator = $this->get(ProductDecorator::class);

            $productDecorator->decorate($resource);
        }

        return $resource;
    }

    public function vehicleProducts(Request $request, Vehicle $vehicle, ?Taxon $taxon = null): Response
    {
        /** @var VehicleTaxonProvider $taxonProvider */
        $taxonProvider = $this->get(VehicleTaxonProvider::class);

        /** @var VehicleProductProvider $productProvider */
        $productProvider = $this->get(VehicleProductProvider::class);

        $request->attributes->set(
            'products',
            array_map(
                static function (ProductInterface $product) {
                    return $product->getId();
                },
                $productProvider->getProducts($vehicle, $taxon)
            )
        );

        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $resources     = $this->getResources($configuration);

        $view = View::create($resources);

        if ($configuration->isHtmlRequest()) {
            $view->setTemplate($configuration->getTemplate(ResourceActions::INDEX . '.html'));
            $view->setTemplateVar($this->metadata->getPluralName());
            $view->setData(
                [
                    'configuration'                  => $configuration,
                    'metadata'                       => $this->metadata,
                    'resources'                      => $resources,
                    $this->metadata->getPluralName() => $resources,
                    'manufacturer'                   => $vehicle->getManufacturer(),
                    'model'                          => $vehicle->getModel(),
                    'vehicle'                        => $vehicle,
                    'currentTaxon'                   => $taxon,
                    'taxons'                         => $taxon
                        ? $taxonProvider->findChildrenByCodeWithSort($taxon->getCode())
                        : [],
                    'innerTaxons'                    => $taxonProvider->getInnerPageCategoryTaxons(),
                ]
            );
        }

        return $this->viewHandler->handle($configuration, $view);
    }

    /**
     * @param RequestConfiguration $configuration
     *
     * @return ResourceGridView|ProductInterface[]
     */
    private function getResources(RequestConfiguration $configuration)
    {
        /** @var ProductDecorator $productDecorator */
        $productDecorator = $this->get(ProductDecorator::class);

        /** @var ArticleProvider $articleProvider */
        $articleProvider = $this->get(ArticleProvider::class);
        $parameters = $configuration->getRequest()->query;
        $decoratedProducts = [];
        if ($parameters->get('q')) {
            $matchedProducts = $articleProvider->findArticlesByOeNumber($parameters->get('q'));
            $productDecorator->decorateMultipleProducts($matchedProducts);

            foreach ($matchedProducts as $matchedProduct) {
                $decoratedProducts[$matchedProduct->getId()] = $matchedProduct;
            }
        }

        $this->get('omni_filter.grid.choices_provider')->setRequestConfiguration($configuration);

        $route     = $configuration->getRequest()->get('_route');
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);

        if (in_array($route, self::DECORATED_ROUTES, true)) {
            /** @var ProductInterface[] $products */
            $products = $resources instanceof ResourceGridView ? iterator_to_array($resources->getData()) : $resources;
            $productsId = [];
            foreach ($products as $item) {
                $productsId[] = $item->getId();
            }

            if ($parameters->get('q')) {
                $productsByCode = $articleProvider->findArticlesByBrandCode($parameters->get('q'));
                if (!empty($productsByCode)) {
                    $currentResults = $resources->getData()->getCurrentPageResults();
                    /** @var Product $product */
                    foreach ($productsByCode as $product) {
                        if (!in_array($product->getId(), $productsId)) {
                            $currentResults->append($product);
                            $products[] = $product;
                        }
                    }
                }
            }

            $productDecorator->decorateMultipleProducts($products, $decoratedProducts);
        }

        return $resources;
    }
}
