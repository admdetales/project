<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511140741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sylius_product_oecodes');
        $this->addSql('ALTER TABLE sylius_oecode ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_oecode ADD CONSTRAINT FK_75C2E5694584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id)');
        $this->addSql('CREATE INDEX IDX_75C2E5694584665A ON sylius_oecode (product_id)');
        $this->addSql('CREATE TABLE sylius_oenumber (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, code_number VARCHAR(255) NOT NULL, INDEX IDX_6D8EEB244584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_oenumber ADD CONSTRAINT FK_6D8EEB244584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id)');
        $this->addSql('DROP TABLE sylius_oecode');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D8EEB244584665AE91647F6 ON sylius_oenumber (product_id, code_number)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sylius_product_oecodes (product_id INT NOT NULL, oe_code_id INT NOT NULL, INDEX IDX_604E41454584665A (product_id), INDEX IDX_604E4145E3223363 (oe_code_id), PRIMARY KEY(product_id, oe_code_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sylius_product_oecodes ADD CONSTRAINT FK_604E41454584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_oecodes ADD CONSTRAINT FK_604E4145E3223363 FOREIGN KEY (oe_code_id) REFERENCES sylius_oecode (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_oecode DROP FOREIGN KEY FK_75C2E5694584665A');
        $this->addSql('DROP INDEX IDX_75C2E5694584665A ON sylius_oecode');
        $this->addSql('ALTER TABLE sylius_oecode DROP product_id');
        $this->addSql('CREATE TABLE sylius_oecode (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, code_number VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, INDEX IDX_75C2E5694584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sylius_oecode ADD CONSTRAINT FK_75C2E5694584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id)');
        $this->addSql('DROP TABLE sylius_oenumber');
        $this->addSql('DROP INDEX UNIQ_6D8EEB244584665AE91647F6 ON sylius_oenumber');
    }
}
