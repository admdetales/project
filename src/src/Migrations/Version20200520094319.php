<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200520094319 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product ADD internal_brand_id INT DEFAULT NULL, ADD internal_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B748B575DD0 FOREIGN KEY (internal_brand_id) REFERENCES sylius_internal_brand (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B742729DD67 FOREIGN KEY (internal_category_id) REFERENCES sylius_internal_category (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_677B9B748B575DD0 ON sylius_product (internal_brand_id)');
        $this->addSql('CREATE INDEX IDX_677B9B742729DD67 ON sylius_product (internal_category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B748B575DD0');
        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B742729DD67');
        $this->addSql('DROP INDEX IDX_677B9B748B575DD0 ON sylius_product');
        $this->addSql('DROP INDEX IDX_677B9B742729DD67 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP internal_brand_id, DROP internal_category_id');
    }
}
