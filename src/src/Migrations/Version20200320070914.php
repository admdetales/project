<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200320070914 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product_attribute ADD filterable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD tagIndex LONGTEXT DEFAULT NULL');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON sylius_product (tagIndex)');
        $this->addSql('CREATE TABLE sylius_channel_logo (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_BC1C31AC7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sylius_channel_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_9EBB38E97E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_channel_logo ADD CONSTRAINT FK_BC1C31AC7E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_channel_image ADD CONSTRAINT FK_9EBB38E97E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fulltext_search_idx ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP tagIndex');
        $this->addSql('ALTER TABLE sylius_product_attribute DROP filterable');
        $this->addSql('DROP TABLE sylius_channel_logo');
        $this->addSql('DROP TABLE sylius_channel_image');
    }
}
