<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622113949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_oenumber DROP FOREIGN KEY FK_6D8EEB244584665A');
        $this->addSql('ALTER TABLE sylius_oenumber ADD CONSTRAINT FK_6D8EEB244584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE sylius_product ADD supplier VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_oenumber DROP FOREIGN KEY FK_6D8EEB244584665A');
        $this->addSql('ALTER TABLE sylius_oenumber ADD CONSTRAINT FK_6D8EEB244584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id)');

        $this->addSql('ALTER TABLE sylius_product DROP supplier');
    }
}
