<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Remove all invalid OE entries like "Nera" or ones with empty values
 *
 * Note/Reminder: This is only a temporary solution to heal current data
 * Ultimate goal: Notify client to fix import data (Not include invalid OE entries)
 */
final class Version20200807102211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM sylius_oenumber WHERE code_number LIKE 'nera';");
        $this->addSql("DELETE FROM sylius_oenumber WHERE code_number LIKE '' AND (brand_name IS NULL OR brand_name LIKE '');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
