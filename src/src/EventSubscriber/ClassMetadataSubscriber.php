<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Product\Product;
use Doctrine\Common\EventSubscriber;
use Doctrine\Persistence\Event\LoadClassMetadataEventArgs;

class ClassMetadataSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return ['loadClassMetadata'];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args): void
    {
        $classMetadata = $args->getClassMetadata();

        if ($classMetadata->getName() === Product::class) {
            foreach ($classMetadata->table['indexes'] as &$index) {
                foreach ($index['columns'] as &$column) {
                    if ($column === 'tagIndex') { // rename hard-coded column name for index in the bundle
                        $column = 'tag_index';
                        break;
                    }
                }
                unset($column);
            }
            unset($index);
        }
    }
}
