<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Brand\InternalBrand;
use App\Entity\Product\ProductInterface;
use App\Entity\Taxonomy\InternalCategory;
use App\Repository\Product\ProductRepositoryInterface;
use App\Repository\Taxon\ProductTaxonRepositoryInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\ObjectManager;
use Sylius\Component\Core\Model\ProductTaxonInterface;

final class MappingPostUpdateSubscriber implements EventSubscriber
{
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->map($args);
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->map($args);
    }

    private function map(LifecycleEventArgs $args): void
    {
        $entity  = $args->getObject();

        if ($entity instanceof InternalBrand) {
            $this->mapInternalBrand($entity, $args->getObjectManager());

            return;
        }

        if ($entity instanceof InternalCategory) {
            $this->mapInternalCategory($entity, $args->getObjectManager());
        }
    }

    private function mapInternalBrand(InternalBrand $internalBrand, ObjectManager $manager): void
    {
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $manager->getRepository(ProductInterface::class);
        $productRepository->updateProductsBrandByInternalBrand($internalBrand);
    }

    private function mapInternalCategory(InternalCategory $internalCategory, ObjectManager $manager): void
    {
        $taxon = $internalCategory->getTaxon();
        if ($taxon === null) {
            return;
        }

        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $manager->getRepository(ProductInterface::class);
        $productRepository->updateProductsTaxonByInternalCategory($internalCategory);

        /** @var ProductTaxonRepositoryInterface $productTaxonRepository */
        $productTaxonRepository = $manager->getRepository(ProductTaxonInterface::class);
        $productTaxonRepository->replaceProductsTaxon(
            $productRepository->getProductsIdsWithTaxon($taxon),
            $taxon
        );
    }
}
