<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Manager;

use App\Entity\SystemConfigValue;
use App\Provider\SystemConfiguration;

use function explode;

final class MaintenanceManager
{
    /** @var SystemConfiguration */
    private $systemConfiguration;

    public function __construct(SystemConfiguration $systemConfiguration)
    {
        $this->systemConfiguration = $systemConfiguration;
    }

    public function enable(): void
    {
        if ($this->isUnderMaintenance()) {
            return;
        }

        $this->systemConfiguration->replace(SystemConfigValue::TYPE_MAINTENANCE, 'on');
    }

    public function disable(): void
    {
        if (!$this->isUnderMaintenance()) {
            return;
        }

        $this->systemConfiguration->replace(SystemConfigValue::TYPE_MAINTENANCE, 'off');
    }

    public function isUnderMaintenance(): bool
    {
        return $this->systemConfiguration->get(SystemConfigValue::TYPE_MAINTENANCE) === 'on';
    }

    /**
     * @return string[]
     */
    public function getWhitelist(): array
    {
        $list = explode(',', $this->systemConfiguration->get(SystemConfigValue::TYPE_MAINTENANCE_WHITELIST));

        return $list ?: [];
    }

    public function saveWhitelist(string $whitelist): void
    {
        $this->systemConfiguration->replace(SystemConfigValue::TYPE_MAINTENANCE_WHITELIST, $whitelist);
    }
}
