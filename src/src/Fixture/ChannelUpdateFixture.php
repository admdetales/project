<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare(strict_types=1);

namespace App\Fixture;

use App\Entity\Channel\Channel;
use App\Entity\Currency\Currency;
use App\Entity\Locale\Locale;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Component\Channel\Context\ChannelNotFoundException;
use Sylius\Component\Currency\Model\CurrencyInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ChannelUpdateFixture extends AbstractFixture
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $options
     * @throws ChannelNotFoundException
     */
    public function load(array $options): void
    {
        /** @var Channel|null $channel */
        $channel = $this->entityManager->getRepository(Channel::class)->findOneBy(['code' => $options['code']]);
        if ($channel === null) {
            throw new ChannelNotFoundException('Channel not found.');
        }

        /** @var Locale[] $locales */
        $locales = $this->entityManager->getRepository(Locale::class)->findBy(['code' => $options['locales']]);
        foreach ($locales as $locale) {
            $channel->addLocale($locale);
        }

        foreach ($channel->getCurrencies() as $currency) {
            $channel->removeCurrency($currency);
        }

        /** @var Currency[] $currencies */
        $currencies = $this->entityManager->getRepository(Currency::class)->findBy(['code' => $options['currencies']]);
        foreach ($currencies as $currency) {
            $channel->addCurrency($currency);
        }

        /** @var CurrencyInterface|null $baseCurrency */
        $baseCurrency = $this->entityManager->getRepository(Currency::class)
            ->findOneBy(['code' => $options['baseCurrency']]);
        $channel->setBaseCurrency($baseCurrency);

        $channel->setThemeName($options['themeName']);

        $this->entityManager->persist($channel);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'channel_update';
    }

    /**
     * @param ArrayNodeDefinition $optionsNode
     */
    protected function configureOptionsNode(ArrayNodeDefinition $optionsNode): void
    {
        $children = $optionsNode->children();
        $children->scalarNode('code')->cannotBeEmpty()->end();
        $children->scalarNode('themeName')->cannotBeEmpty()->end();
        $children->arrayNode('locales')->scalarPrototype()->end();
        $children->arrayNode('currencies')->scalarPrototype()->end();
        $children->scalarNode('baseCurrency')->cannotBeEmpty()->end();
    }
}
