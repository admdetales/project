<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Processor;

use App\Constant\ImportExport;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Webmozart\Assert\Assert;

class ProductOptionValueProcessor implements ResourceProcessorInterface
{
    /** @var RepositoryInterface */
    private $productOptionValueRepository;

    /** @var FactoryInterface */
    private $productOptionValueFactory;

    /** @var RepositoryInterface */
    private $productOptionRepository;

    /** @var FactoryInterface */
    private $productOptionFactory;

    /**
     * @param RepositoryInterface $productOptionValueRepository
     * @param FactoryInterface $productOptionValueFactory
     * @param RepositoryInterface $productOptionRepository
     * @param FactoryInterface $productOptionFactory
     */
    public function __construct(
        RepositoryInterface $productOptionValueRepository,
        FactoryInterface $productOptionValueFactory,
        RepositoryInterface $productOptionRepository,
        FactoryInterface $productOptionFactory
    ) {
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionFactory = $productOptionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $errorMessage = \sprintf(
            'Wrong data structure provided for %s. %s',
            self::class,
            \implode(',', \array_keys($data))
        );
        Assert::keyExists($data, 'Code', $errorMessage);
        Assert::keyExists($data, 'Attribute code', $errorMessage);
        Assert::keyExists($data, 'Value', $errorMessage);

        $productOptionValue = $this->getProductOptionValue($data['Code']);

        $productOptionValue->setOption($this->getProductOption($data['Attribute code']));
        $productOptionValue->setValue($data['Value']);

        $this->productOptionValueRepository->add($productOptionValue);
    }

    /**
     * @param string $code
     *
     * @return ProductOptionValueInterface
     */
    private function getProductOptionValue(string $code): ProductOptionValueInterface
    {
        /** @var ProductOptionValueInterface|null $productOptionValue */
        $productOptionValue = $this->productOptionValueRepository->findOneBy(['code' => $code]);
        if (null === $productOptionValue) {
            /** @var ProductOptionValueInterface $productOptionValue */
            $productOptionValue = $this->productOptionValueFactory->createNew();
            $productOptionValue->setCode($code);
        }

        return $productOptionValue;
    }

    /**
     * @param string $code
     *
     * @return ProductOptionInterface
     */
    private function getProductOption(string $code): ProductOptionInterface
    {
        /** @var ProductOptionInterface|null $productOption */
        $productOption = $this->productOptionRepository->findOneBy(['code' => $code]);
        if (null === $productOption) {
            /** @var ProductOptionInterface $productOption */
            $productOption = $this->productOptionFactory->createNew();
            $productOption->setCode($code);

            $this->productOptionRepository->add($productOption);
        }

        return $productOption;
    }
}
