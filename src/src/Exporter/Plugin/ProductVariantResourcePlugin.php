<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exporter\Plugin;

use App\Constant\ImportExport;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\ResourcePlugin;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ProductVariantResourcePlugin extends ResourcePlugin
{
    /**
     * @param RepositoryInterface $productVariantRepository
     * @param PropertyAccessorInterface $propertyAccessor
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RepositoryInterface $productVariantRepository,
        PropertyAccessorInterface $propertyAccessor,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($productVariantRepository, $propertyAccessor, $entityManager);
    }

    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductVariantInterface $resource */
        foreach ($this->resources as $resource) {
            $this->addTranslationData($resource);
            $this->addProductData($resource);
            $this->addOptionsData($resource);
            $this->addPriceData($resource);
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    private function addTranslationData(ProductVariantInterface $resource): void
    {
        $translation = $resource->getTranslation();

        $this->addDataForResource($resource, 'Name', $translation->getName());
    }

    /**
     * @param ProductVariantInterface $resource
     */
    private function addProductData(ProductVariantInterface $resource): void
    {
        $product = $resource->getProduct();

        $productCode = $product ? $product->getCode() : '';

        $this->addDataForResource($resource, 'Product code', $productCode);
    }

    /**
     * @param ProductVariantInterface $resource
     */
    private function addOptionsData(ProductVariantInterface $resource): void
    {
        /** @var ProductOptionValueInterface|null $optionValue */
        $optionValues = $resource->getOptionValues();

        $optionValueCodes = [];
        foreach ($optionValues as $optionValue) {
            $optionValueCodes[] = $optionValue->getCode();
        }

        $this->addDataForResource(
            $resource,
            'Option value code',
            implode(ImportExport::COLLECTION_DELIMITER, $optionValueCodes)
        );
    }

    /**
     * @param ProductVariantInterface $resource
     */
    private function addPriceData(ProductVariantInterface $resource): void
    {
        $channelPricings = $resource->getChannelPricings();

        foreach ($channelPricings as $channelPricing) {
            $originalPriceInCents = $channelPricing ? $channelPricing->getOriginalPrice() : '';
            $priceInCents = $channelPricing ? $channelPricing->getPrice() : '';

            $this->addDataForResource($resource, 'Original price in cents', $originalPriceInCents);
            $this->addDataForResource($resource, 'Price in cents', $priceInCents);
        }
    }
}
