<?php

declare(strict_types=1);

namespace App\Provider\Product;

use App\Entity\Product\OeNumber;
use App\Entity\Product\ProductInterface;
use App\Repository\Product\ProductRepositoryInterface;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;

class ProductAnaloguesProvider
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var ChannelContextInterface */
    private $channelContext;

    /** @var LocaleContextInterface */
    private $localeContext;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ChannelContextInterface $channelContext,
        LocaleContextInterface $localeContext
    ) {
        $this->productRepository = $productRepository;
        $this->channelContext = $channelContext;
        $this->localeContext = $localeContext;
    }

    /**
     * @param string $productId
     * @return ProductInterface[]
     */
    public function getProductAnalogues(string $productId): array
    {
        /** @var ProductInterface|null $product */
        $product = $this->productRepository->find($productId);

        if (!$product) {
            throw new \LogicException("Product not found by id: ${productId}.");
        }

        $codes = $product->getOeNumbers()->map(function (OeNumber $oeNumber) {
            return $oeNumber->getCodeNumber();
        });

        return $this->productRepository->findByOeNumbers(
            $this->channelContext->getChannel(),
            $this->localeContext->getLocaleCode(),
            $codes->getValues(),
            $product
        );
    }
}
