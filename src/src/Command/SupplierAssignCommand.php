<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace App\Command;

use App\Entity\Supplier\Supplier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SupplierAssignCommand
 * @package App\Command
 *
 * @todo this is temporary command to fix suppliers
 */
class SupplierAssignCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $createdSuppliers = [];

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }
    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('supplier:assign')
            ->setDescription('Re-assign suppliers with new logic - temporary')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $supplierNames = $this->getUniqueSupplierNames();

        foreach ($supplierNames as $supplierName) {
            if (
                in_array($supplierName['supplier_legacy'], $this->createdSuppliers) ||
                is_null($supplierName['supplier_legacy'])
            ) {
                continue;
            }

            $supplier = $this->em->getRepository(Supplier::class)
                ->findOneBy(['name' => $supplierName['supplier_legacy']])
            ;

            if (!$supplier) {
                $supplier = (new Supplier())->setName($supplierName['supplier_legacy']);
                $this->em->persist($supplier);
                $this->em->flush();
            }
            $this->createdSuppliers[$supplierName['supplier_legacy']] = $supplier->getId();
        }

        $this->assignSuppliersForProducts();

        return 0;
    }

    private function getUniqueSupplierNames()
    {
        $conn = $this->em->getConnection();
        $stmt = $conn->prepare('
            SELECT supplier_legacy FROM sylius_product
        ');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    private function assignSuppliersForProducts()
    {
        $conn = $this->em->getConnection();
        foreach ($this->createdSuppliers as $name => $id) {
            $stmt = $conn->prepare('
                UPDATE sylius_product SET supplier_id = ' . $id . ' WHERE supplier_legacy = "' . $name . '"
            ');
            $stmt->execute();
        }
    }
}
