<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Command;

use App\Decorator\OrderItemDecorator;
use App\Entity\Product\Product;
use App\Entity\Product\ProductImage;
use App\Entity\Product\ProductVariant;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class GenerateRemarketingFeed
 * @package App\Command
 */
class GenerateRemarketingFeedCommand extends Command
{
    private const BATCH_SIZE = 500;
    private const CURRENCY = 'EUR';
    private const PRODUCT_ROUTE = 'sylius_shop_product_show';

    protected static $defaultName = 'generate:remarketing:feed';

    /** @var string $baseImagePath */
    private $baseImagePath;

    /** @var FactoryInterface */
    private $orderItemFactory;

    /** @var OrderItemDecorator */
    private $orderItemDecorator;

    /** @var OrderItemQuantityModifierInterface */
    private $orderItemQuantityModifier;

    /** @var EntityManager $em */
    private $entityManager;

    /** @var RouterInterface $router */
    private $router;

    /** @var String $locale */
    private $locale;

    /** @var string $directory */
    private $directory;

    public function __construct(
        FactoryInterface $orderItemFactory,
        OrderItemDecorator $orderItemDecorator,
        OrderItemQuantityModifierInterface $orderItemQuantityModifier,
        ParameterBagInterface $params,
        RouterInterface $router,
        EntityManagerInterface $entityManager
    ) {
        $context = $router->getContext();
        $context->setHost($params->get('application.host'));
        $context->setScheme($params->get('application.scheme'));
        $this->router = $router;
        $this->baseImagePath = $this->router->generate(
            'sylius_shop_default_locale',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        ) . 'media/cache/sylius_shop_product_original/';
        $this->locale = $params->get('locale');
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemDecorator = $orderItemDecorator;
        $this->orderItemQuantityModifier = $orderItemQuantityModifier;
        $this->entityManager = $entityManager;
        $this->directory = $params->get('remarketing_directory') . $params->get('remarketing_file_name');
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Generates remarketing feed');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $totalRecordsProcessed = 0;
        $filePointer = fopen($this->directory, 'wb');
        $query = $this->entityManager->createQuery(
            'SELECT pv FROM App\Entity\Product\ProductVariant pv JOIN pv.product p WHERE p.enabled = true'
        );
        $iterableResult = $query->iterate();

        fputcsv($filePointer, [
            'ID',
            'ID2',
            'Item title',
            'Final URL',
            'Image URL',
            'Item subtitle',
            'Item description',
            'Item category',
            'Price',
            'Sale price',
            'Contextual keywords',
            'Tracking template',
            'Custom parameter',
            'Final mobile URL'
        ]);

        foreach ($iterableResult as $row) {
            fputcsv($filePointer, $this->generateFeedRow($row[0]));
            if (($totalRecordsProcessed % self::BATCH_SIZE) === 0) {
                $this->entityManager->clear();
            }

            ++$totalRecordsProcessed;
        }

        fclose($filePointer);

        $output->writeln("Remarketing csv generated. $totalRecordsProcessed items exported.");

        return 0;
    }

    private function generateFeedRow(ProductVariant $variant): array
    {
        /** @var Product $product */
        $product = $variant->getProduct();
        $decoratedVariant = $this->getDecoratedOrderItemFromVariant($variant);
        $unitPrice = ($decoratedVariant->getUnitPrice() / 100) . ' ' . self::CURRENCY;
        $discountedPrice = ($decoratedVariant->getDiscountedUnitPrice() / 100) . ' ' . self::CURRENCY;
        $category = $product->getInternalCategory();
        $url = $this->router->generate(
            self::PRODUCT_ROUTE,
            ['_locale' => $this->locale, 'slug' => $product->getSlug()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return [
            $variant->getId(),
            '',
            $product->getName() . ' ' . $variant->getName(),
            $url,
            $this->getProductImagePath($product->getImages()),
            $product->getName(),
            $product->getDescription(),
            $category === null ? '' : $category->getName(),
            $unitPrice,
            $unitPrice === $discountedPrice ? '' : $discountedPrice,
            '',
            '',
            '',
            $url,
        ];
    }

    public function getDecoratedOrderItemFromVariant(ProductVariantInterface $productVariant): OrderItemInterface
    {
        /** @var OrderItemInterface $orderItem */
        $orderItem = $this->orderItemFactory->createNew();

        $orderItem->setVariant($productVariant);
        $this->orderItemQuantityModifier->modify($orderItem, 1);
        $this->orderItemDecorator->decorate($orderItem);

        return $orderItem;
    }

    /**
     * @param Collection<int, ProductImage> $images
     * @return string
     */
    private function getProductImagePath(Collection $images): string
    {
        return $images->isEmpty() ? '' : $this->baseImagePath . $images->first()->getPath();
    }
}
