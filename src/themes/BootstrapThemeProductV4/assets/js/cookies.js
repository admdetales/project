export default function initCookies() {
  const cookieName = "notice_agreement";
  const cookieValue = "agreed";
  const cookieExpirationDays = 999;

  if (getCookie(cookieName) === null) {
    createNoticeElement();
  }

  function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + expiry * 24 * 60 * 60 * 1000);
    document.cookie = key + "=" + value + ";expires=" + expires.toUTCString();
  }

  function getCookie(key) {
    var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
    return keyValue ? keyValue[2] : null;
  }

  function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, "-1");
  }

  function createNoticeElement() {
    const cookieHolder = document.getElementById("cookie-holder");
    const cookieNoticeText = document.getElementById('cookie_notice').innerText
    const cookieNoticeTextBtn = document.getElementById('cookie_notice_btn').innerText

    cookieHolder.innerHTML = `
          <div class="cookie-notice">
            <div class="cookie-notice__text">
              ${cookieNoticeText}
            </div>
            <button class="cookie-notice__btn btn btn-primary">${cookieNoticeTextBtn}</button>
          </div>`;
  }

  if (document.querySelector(".cookie-notice__btn")) {
    document.querySelector(".cookie-notice__btn").onclick = () => {
      setCookie(cookieName, cookieValue, cookieExpirationDays);
      document.querySelector(".cookie-notice").remove();
    };
  }
}
