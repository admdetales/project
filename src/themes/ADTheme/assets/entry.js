import 'bootstrap.native/dist/bootstrap-native-v4';

// polyfills
import 'bootstrap.native/dist/polyfill.min';
import 'nodelist-foreach-polyfill';
import 'events-polyfill';
import 'whatwg-fetch';
import 'promise-polyfill/src/polyfill';

import './images/logo.png';

import './js/fontawesome';
import './js/swiper';
import './js/select2';
import './js/toggle-by-selector';
import './js/form';
import './js/filterable-table-columns';
import './js/cookie_consent';
import './js/newsletter';
import './js/product-analogues';
import './js/compatible-vehicles';

import './scss/index.scss';
import '../../BootstrapTheme/assets/js/fontawesome';
import '../../BootstrapTheme/assets/js/index';
