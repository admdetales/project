/* global document */
import Swiper from 'swiper/js/swiper.min';
import 'swiper/css/swiper.css';

const swiperContainers = document.querySelectorAll('.swiper-container');
swiperContainers.forEach((swiperContainer) => {
  const productCount = swiperContainer.dataset.productcount;

  new Swiper(swiperContainer, {
    loop: productCount > 4,
    slidesPerView: 'auto',
    slidesPerGroup: 4,

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
});
