up:
	docker-compose up -d --remove-orphan 

stop:
	docker stop $$(docker ps -q)

install:
	docker-compose up -d --remove-orphan
	chmod -R 777 ./docker/wait_docker_up.sh
	./docker/wait_docker_up.sh
	docker exec -i $$(docker ps -qf "name=mysql") mysql -uroot -proot database < ./docker/mysql/sqldump.sql

php:
	docker exec -it $$(docker ps -qf "name=php") bash

mysql:
	docker exec -it $$(docker ps -qf "name=mysql") bash

down:
	docker-compose down